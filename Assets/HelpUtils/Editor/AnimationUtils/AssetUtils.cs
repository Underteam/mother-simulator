﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace MainUtils
{
    public class AssetUtils
    {
        #if UNITY_EDITOR
        // Возвращает путь до объекта
        public static string GetAssetPath(Object obj)
        {
            if (obj == null)
                return string.Empty;

            return AssetDatabase.GetAssetPath(obj);
        }

        // Возвращает объект по пути
        public static Object GetAsset(string path)
        {
            Object obj = null;
            obj = AssetDatabase.LoadAssetAtPath(path, typeof(Object));
            return obj;
        }

        // Является ли объект основным ассетом в окне проекта?
        public static bool IsMainAsset(Object obj)
        {
            return AssetDatabase.IsMainAsset(obj);
        }

        // Является ли объект вложенным ассетом в окне проекта?
        public static bool HasSubAssets(Object obj)
        {
            return (AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(obj)).Length > 1);
        }

        // Возвращает вложенные объекты объекта
        public static Object[] GetSubAssets(Object obj)
        {
            Object[] assets = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(obj));
            List<Object> subAssets = new List<Object>();

            foreach (Object asset in assets)
            {
                if (AssetDatabase.IsSubAsset(asset))
                    subAssets.Add(asset);
            }

            return subAssets.ToArray();
        }

        // Проверяет, существует ли указанная папка
        public static bool IsDirectory(string path)
        {
            if (string.IsNullOrEmpty(path))
                return false;

            return System.IO.Directory.Exists(path);
        }

        // Возвращает ассеты в указанном каталоге.
        public static Object[] GetDirectoryAssets(string path, bool addSubFolders = false)
        {
            List<Object> assets = new List<Object>();

            // Получаем пути всех файлов в указанной папке
            string[] assetPaths = System.IO.Directory.GetFiles(path);

            // Загружаем ассеты
            foreach (string assetPath in assetPaths)
            {
                // Check if it's a meta file
                if (assetPath.Contains(".meta"))
                    continue;

                Object objAsset = AssetDatabase.LoadAssetAtPath(assetPath, typeof(Object));

                if (objAsset != null)
                    assets.Add(objAsset);
            }

            // Добавляем вложенные папки
            if (addSubFolders)
            {
                List<string> directoriesPaths = new List<string>(System.IO.Directory.GetDirectories(path));
                for (int i = 0; i < directoriesPaths.Count; i++)
                {
                    assets.AddRange(GetDirectoryAssets(directoriesPaths[i], addSubFolders));
                }

            }

            // Возвращаем массив объектов
            return assets.ToArray();
        }
        #endif
    }
}