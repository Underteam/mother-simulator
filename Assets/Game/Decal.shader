// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "Custom/MaxDecal" {
Properties {
    _Color ("Main Color", Color) = (1,1,1,1)
    _MainTex ("Base (RGB)", 2D) = "white" {}
    _DecalTex ("Decal (RGBA)", 2D) = "black" {}
	_DecalIntensity("DecalIntensity", Range(0,1)) = 0.0
}

SubShader {
    Tags { "RenderType"="Opaque" }
    LOD 250

CGPROGRAM
#pragma surface surf Lambert

sampler2D _MainTex;
sampler2D _DecalTex;
fixed4 _Color;
half _DecalIntensity;

struct Input {
    float2 uv_MainTex;
    float2 uv_DecalTex;
};

void surf (Input IN, inout SurfaceOutput o) {
    fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
    half4 decal = tex2D(_DecalTex, IN.uv_DecalTex);
    c.rgb = lerp (c.rgb, decal.rgb, decal.a * _DecalIntensity);
    c *= _Color;
    o.Albedo = c.rgb;
    o.Alpha = c.a;
}
ENDCG
}

Fallback "Legacy Shaders/Diffuse"
}
