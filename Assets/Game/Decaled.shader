﻿Shader "Custom/Decaled" 
{
	Properties 
	{
		_MainColor("MainColor", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_DecalTex("Decal", 2D) = "white" {}
		_DecalIntensity("DecalIntensity", Range(0,1)) = 0.0
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }

		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _DecalTex;

		struct Input {
			float2 uv_MainTex;
			float2 uv_DecalTex;
		};

		half4 _MainColor;
		half _DecalIntensity;

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);

			//o.Albedo = c.rgb + _MainColor + tex2D(_DecalTex, IN.uv_DecalTex) * _EmissionColor;

			half4 decal_intens = tex2D(_DecalTex, IN.uv_DecalTex)*_DecalIntensity;
			
			o.Albedo = c.rgb*lerp(half4(1, 1, 1, 1), half4(0, 0, 0, 0), decal_intens) + _MainColor;

			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
