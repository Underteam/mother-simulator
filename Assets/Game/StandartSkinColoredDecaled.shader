﻿Shader "Custom/StandartSkinColoredDecaled" {
	Properties {
		_MainColor("MainColor", Color) = (1,1,1,1)
		_Color ("Color", Color) = (1,1,1,1)
		_MaskColor ("MaskColor", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_MaskTex ("MaskTex", 2D) = "white" {}
		_DecalTex("Decal", 2D) = "white" {}

		_MettalicTex ("Mettalic (RGB)", 2D) = "white" {}
		_OcclusionTex ("Occlusion (RGB)", 2D) = "white" {}
		_NormalTex ("Normal", 2D) = "white" {}

		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_EmissionColor("EmissionColor", Color) = (0,0,0,0)

		_DecalIntensity("DecalIntensity", Range(0,1)) = 0.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _MaskTex;
		sampler2D _MettalicTex;
		sampler2D _OcclusionTex;
		sampler2D _NormalTex;
		sampler2D _DecalTex;

		struct Input {
			float2 uv_MainTex;
			float2 uv_MaskTex;
			float2 uv_MettalicTex;
			float2 uv_OcclusionTex;
			float2 uv_NormalTex;
			float2 uv_DecalTex;
		};

		half _Glossiness;
		half _Metallic;
		half4 _MainColor;
		fixed4 _Color;
		fixed4 _MaskColor;
		float _intensity;
		half4 _EmissionColor;
		half _DecalIntensity;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 color_mask = tex2D (_MaskTex, IN.uv_MaskTex);
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * lerp(_Color,_MaskColor,1.0f-color_mask.r)*(1.0+_intensity);
			//o.Albedo = c.rgb + _MainColor + tex2D(_DecalTex, IN.uv_DecalTex) * _EmissionColor;

			half4 decal_intens = tex2D(_DecalTex, IN.uv_DecalTex)*_DecalIntensity;
			o.Albedo = c.rgb*lerp(half4(1, 1, 1, 1), half4(0, 0, 0, 0), decal_intens) + _MainColor;

			o.Normal =  UnpackNormal(tex2D (_NormalTex, IN.uv_NormalTex));

			o.Metallic = _Metallic*tex2D (_MettalicTex, IN.uv_MettalicTex);
			o.Smoothness = _Glossiness*tex2D (_OcclusionTex, IN.uv_OcclusionTex);
			o.Emission = _EmissionColor;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
