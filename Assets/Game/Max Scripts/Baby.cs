﻿using Facebook.Unity;
using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Baby : MonoBehaviour
{
    public static Baby instance { get; private set; }

    private Animator animator;
    public RagdollHelper ragdoll;

    public AudioSource audioSource;

    public AudioClip clipSad;
    public AudioClip clipHappy;
    public AudioClip clipCry;
    public AudioClip clipTalk;

    public GameObject myPacifier;

    public Transform bottlePlace;

    private Pacifier pacifier;

    private Rigidbody[] rbs;
    private List<Vector3> velocities = new List<Vector3>();
    private List<Vector3> torques = new List<Vector3>();

    public GameObject diaper;
    public Item usedDiaperPrefab;
    public Item unusedDiaperPrefab;
    private Item currDiaperPrefab;

    public InterfaceProvider diaperInterface;
    private Vector3 diaperScale;

    public bool log;

    private bool ragdolled;

    public bool wet { get; private set; } = false;
    public bool soaped { get; private set; } = false;
    public bool dirty { get; set; }
    public bool onHands { get; private set; }
    public bool hiding { get; set; }
    public bool cried { get; private set; }

    public GameObject wishCanvas;
    public GameObject wishCanvasLie;
    public GameObject currentCanvas { get; set; }

    private Toy currToy;

    public Transform toyParent;
    private Vector3 toyScale;

    private float toyTimer;

    private float checkWishTimer;

    public Item currItem { get; private set; }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();

    public float playToyTime = 4.5f;

    public ParticleSystem foam;

    private void Awake()
    {
        instance = this;

        animator = GetComponent<Animator>();
        ragdoll = GetComponent<RagdollHelper>();

        rbs = GetComponentsInChildren<Rigidbody>();

        diaperScale = usedDiaperPrefab.transform.localScale;

        currDiaperPrefab = unusedDiaperPrefab;
        diaperInterface.providedInterface = unusedDiaperPrefab.GetComponent<InterfaceCollect>();

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            if (!(d is Wish w)) return;

            //Debug.LogError ("New wish " + w.eventName + " " + checkWishTimer);

            if (w.eventName.Equals("Pampers"))
            {
                wet = true;
                currDiaperPrefab = usedDiaperPrefab;
                diaperInterface.providedInterface = usedDiaperPrefab.GetComponent<InterfaceCollect>();
            }

            if (w.eventName.Equals("Bath"))
            {
                dirty = true;

                var settings = GetComponent<BabySettings>();
                Material mat = settings.body.material;
                float level = mat.GetFloat("_DecalIntensity");
                Updater updater = null;
                updater = Updater.CreateUpdater(() =>
                {
                    if (!dirty)
                    {
                        level = Mathf.Lerp(level, 0, Time.deltaTime);
                        if (level < 0.01f) level = 0.0f;
                    }
                    else
                    {
                        level = Mathf.Lerp(level, 1f, Time.deltaTime);
                    }

                    mat.SetFloat("_DecalIntensity", level);

                    if (!dirty && level == 0) updater.Stop();
                });
            }
        },
        "NewBabyWish");
        listeners.Add(listener);

        if (GameState.Instance().state == GameState.State.Menu)
        {
            wet = true;
            currDiaperPrefab = usedDiaperPrefab;
            diaperInterface.providedInterface = usedDiaperPrefab.GetComponent<InterfaceCollect>();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        currentCanvas = wishCanvas;
        wishCanvasLie.SetActive(false);

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            if (currToy != null)
            {
                toyTimer = 1f;
                if (ShouldPlayEmotion()) checkWishTimer = 1.1f;
                if (GameState.Instance().state == GameState.State.Menu) happy = true;
            }

            happy = false;
        },
        "WishUnSatisfied");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            happy = true;
        },
        "WishSatisfied");
        listeners.Add(listener);
    }

    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);    
    }

    private void Update()
    {
        if (checkWishTimer > 0)
        {
            checkWishTimer -= Time.deltaTime;
            if (checkWishTimer <= 0) CheckWish();
        }

        if (onHands)
        {
            if (Groups.Instance().GetGroup("Button Throw")[0].activeSelf)
                Groups.Instance().SetGroupState("Button Throw", false);
            if (Groups.Instance().GetGroup("Button Put")[0].activeSelf)
                Groups.Instance().SetGroupState("Button Put", false);
        }
    }

    private int emotionCounter;
    private void CheckWish ()
    {
        emotionCounter++;
        if (emotionCounter % 3 == 0 && GameState.Instance().state == GameState.State.Menu && !AdsController.Instance().noAds)
        {
            Time.timeScale = 0f;
            AudioListener.volume = 0f;
            AdsController.Instance().ShowAd(AdsProvider.AdType.skipablevideo, (b) =>
            {
                Time.timeScale = 1f;
                AudioListener.volume = 1f;

                Analytics.Instance().SendEvent("BabyAd");

                Analytics.Instance().SendEvent("af_ads_success");
                Analytics.Instance().SendEvent("af_ads_success_int");

                GameController.Instance().TrackAdsWatched();
            });
        }

        //Debug.LogError("Check wish");

        if (happy) Happy();
        else Cry();
    }

    private bool ShouldPlayEmotion ()
    {
        if (GameState.Instance().state == GameState.State.Menu) return true;

        if (GameState.Instance().state == GameState.State.Challenge) return true;

        if (TaskManager.Instance().currentTask == null ||
            TaskManager.Instance().currentTask.currentWish == null ||
            TaskManager.Instance().currentTask.currentWish.done)
            return false;

        return true;
    }

    private void LateUpdate()
    {
        if (currToy != null)
        {
            {
                currToy.transform.localRotation = Quaternion.Inverse(currToy.posHandler.localRotation);
                Vector3 v = -(currToy.posHandler.position - currToy.transform.position);
                currToy.transform.localPosition = currToy.transform.parent.InverseTransformPoint(currToy.transform.parent.position + v);
            }


            toyTimer -= Time.deltaTime;
            if (toyTimer < 0)
            {
                currToy.transform.SetParent(null);
                currToy.transform.localScale = toyScale;
                currToy.transform.position += 0.3f * (transform.forward + transform.up);

                var rb = currToy.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    rb.isKinematic = false;
                    rb.AddForce(7 * (transform.forward + Vector3.up + Random.Range(-1f, 1f) * transform.right), ForceMode.VelocityChange);
                }

                currToy = null;
                currItem = null;

                animator.SetBool("Toy", false);
            }
        }
        else
        {
            if(toyTimer > 0)
            {
                currItem = null;
                toyTimer = 0;
            }

            toyTimer = playToyTime;
        }
    }

    private void SetPosition ()
    {
        //Debug.LogError ("Here");
        Transform anchor = animator.GetBoneTransform(HumanBodyBones.Hips);
        anchor.localPosition = Vector3.zero;
    }

    private Vector3 anchorLocalPos;
    private Vector3 anchorPos;
    private Quaternion anchorLocalRot;
    private Quaternion anchorRot;

    public void ApplyItem (Item item)
    {
        if (item == null) return;

        var pacifier = item.GetComponent<Pacifier>();
        if (pacifier != null)
        {
            if (myPacifier.activeSelf) return;

            PlayerController.instance.ReleaseItem(() =>
            {
                currItem = item;

                var ds = pacifier.GetComponent<DirtyScript>();
                if (ds != null && !ds.isClean)
                {
                    this.pacifier = pacifier;
                    this.pacifier.transform.SetParent(null);

                    myPacifier.SetActive(true);
                    this.pacifier.gameObject.SetActive(false);

                    Invoke("Spit", 1f);

                    if (ShouldPlayEmotion()) checkWishTimer = 1.1f;
                    EventManager.Instance().TriggerEvent("BabyEvent", "Pacifier", false);
                    if (GameState.Instance().state == GameState.State.Menu) happy = false;
                }
                else
                {
                    this.pacifier = pacifier;
                    this.pacifier.transform.SetParent(null);

                    myPacifier.SetActive(true);
                    this.pacifier.gameObject.SetActive(false);

                    Invoke("Spit", 3f);

                    if (ShouldPlayEmotion()) checkWishTimer = 3.1f;
                    EventManager.Instance().TriggerEvent("BabyEvent", "Pacifier", true);
                    if (GameState.Instance().state == GameState.State.Menu) happy = true;
                }

                Talk();
            });

            return;
        }

        Diaper diaper = item.GetComponent<Diaper>();
        if (diaper != null && !this.diaper.activeSelf)
        {
            Destroy(diaper.gameObject);
            TakeOnDiaper();
        }

        var powder = item.GetComponent<BabyPowder>();
        if (powder != null)
        {
            if (!this.diaper.gameObject.activeSelf) wet = false;
            PlayerController.instance.rightHand.Play(powder.clip);
            powder.Use();
        }

        var loofah = item.GetComponent<Loofah>();
        if (loofah != null)
        {
            PlayerController.instance.rightHand.Play(loofah.clip);
            loofah.Use();
            soaped = true;
            foam.gameObject.SetActive(true);
            var main = foam.main;
            main.loop = true;
            main.simulationSpeed = 1f;
            //main.duration = 2f;
            //main.startLifetime = 5f;
        }

        var bottle = item.GetComponent<BabyBottle>();
        if (bottle != null)
        {
            PlayerController.instance.ReleaseItem(() =>
            {
                if (bottle == null || bottle.pacifier == null) return;

                currItem = item;

                if (!bottle.IsAppropriateForUse())
                {
                    bottle.GetComponent<Rigidbody>().isKinematic = true;
                    bottle.transform.SetParent(transform);
                    bottle.GetComponent<Item>().busy = true;
                    //bottle.pacifier.GetComponent<Item>().busy = true;
                    bottle.transform.SetParent(bottlePlace);
                    bottle.transform.localPosition = Vector3.zero;
                    bottle.transform.localRotation = Quaternion.identity;
                    this.bottle = bottle;
                    Invoke("SpitBottle", 0.5f);
                    if (ShouldPlayEmotion()) checkWishTimer = 0.6f;
                    EventManager.Instance().TriggerEvent("BabyEvent", "MilkBottle", false);
                    if (GameState.Instance().state == GameState.State.Menu) happy = false;
                }
                else
                {
                    bottle.GetComponent<Rigidbody>().isKinematic = true;
                    bottle.transform.SetParent(transform);
                    bottle.GetComponent<Item>().busy = true;
                    //bottle.pacifier.GetComponent<Item>().busy = true;
                    bottle.withFood = false;
                    bottle.withWater = false;
                    bottle.transform.SetParent(bottlePlace);
                    bottle.transform.localPosition = Vector3.zero;
                    bottle.transform.localRotation = Quaternion.identity;
                    this.bottle = bottle;
                    bottle.Use();
                    Invoke("SpitBottle", 3f);
                    if (ShouldPlayEmotion()) checkWishTimer = 3.1f;
                    EventManager.Instance().TriggerEvent("BabyEvent", "MilkBottle", true);
                    if (GameState.Instance().state == GameState.State.Menu) happy = true;
                }

                Talk();
            });
        }

        var toy = item.GetComponent<Toy>();
        if (toy != null && currToy == null)
        {
            PlayerController.instance.ReleaseItem(() =>
            {
                currToy = toy;
                currItem = item;

                {
                    toyScale = currToy.transform.localScale;
                    currToy.transform.SetParent(toyParent/*rightHandIK.boneTransform*/);
                    currToy.transform.localRotation = Quaternion.Inverse(currToy.posHandler.localRotation);
                    Vector3 v = -(currToy.posHandler.position - currToy.transform.position);
                    currToy.transform.localPosition = currToy.transform.parent.InverseTransformPoint(currToy.transform.parent.position + v);
                    Vector3 s = toyParent.lossyScale;
                    s.x = 0.8f * toyScale.x / s.x;
                    s.y = 0.8f * toyScale.y / s.y;
                    s.z = 0.8f * toyScale.z / s.z;
                    currToy.transform.localScale = s;
                }

                var rb = currToy.GetComponent<Rigidbody>();
                if (rb != null) rb.isKinematic = true;

                animator.SetBool("Toy", true);

                if (ShouldPlayEmotion()) checkWishTimer = playToyTime + 0.1f;
                EventManager.Instance().TriggerEvent("BabyEvent", "Toy", currToy);
                if (GameState.Instance().state == GameState.State.Menu) happy = true;

                Talk();
            });
        }
    }

    public void Cartoon (DVDDisc disc)
    {
        if (ShouldPlayEmotion()) checkWishTimer = 0.5f;
        EventManager.Instance().TriggerEvent("BabyEvent", "Cartoons", disc);
        if (GameState.Instance().state == GameState.State.Menu) this.happy = true;

        Talk();
    }

    public void Wash ()
    {
        Debug.Log("Wash");

        if (ShouldPlayEmotion()) checkWishTimer = 0.5f;
        if (soaped) dirty = false;
        EventManager.Instance().TriggerEvent("BabyEvent", "Bath", soaped);

        soaped = false;
        //foam.gameObject.SetActive(true);
        var main = foam.main;        
        main.loop = false;
        main.simulationSpeed = 5f;

        if (GameState.Instance().state == GameState.State.Menu) this.happy = true;

        Talk();
    }

    public void SimonGame(bool result)
    {
        if (!onHands) return;

        if (ShouldPlayEmotion()) checkWishTimer = 0.5f;
        EventManager.Instance().TriggerEvent("BabyEvent", "Simon", result);
        if (GameState.Instance().state == GameState.State.Menu) this.happy = true;

        Talk();
    }

    public void Horse ()
    {
        if (ShouldPlayEmotion()) checkWishTimer = 2.5f;
        EventManager.Instance().TriggerEvent("BabyEvent", "Horse", true);
        if (GameState.Instance().state == GameState.State.Menu) this.happy = true;

        Talk();
    }

    public void HideAndSeek()
    {
        if (ShouldPlayEmotion()) checkWishTimer = 0.5f;
        EventManager.Instance().TriggerEvent("BabyEvent", "Seek", true);
        if (GameState.Instance().state == GameState.State.Menu) this.happy = true;

        Talk();
    }

    public bool happy { get; private set; }
    private void Spit()
    {
        myPacifier.SetActive(false);

        currItem = null;

        if (pacifier == null) return;

        pacifier.gameObject.SetActive(true);

        var rb = pacifier.GetComponent<Rigidbody>();

        if (rb != null)
        {
            rb.isKinematic = false;
            rb.AddForce(3 * transform.forward + 3 * Vector3.up, ForceMode.VelocityChange);
        }
    }

    private BabyBottle bottle;
    private void SpitBottle ()
    {
        currItem = null;

        if (bottle == null) return;

        bottle.transform.SetParent(null);
        bottle.GetComponent<Item>().busy = false;
        //bottle.pacifier.GetComponent<Item>().busy = false;

        var rb = bottle.GetComponent<Rigidbody>();

        if (rb != null)
        {
            rb.isKinematic = false;
            rb.AddForce(3 * transform.forward + 3 * Vector3.up, ForceMode.VelocityChange);
        }
    }

    public void Sad()
    {
        animator.SetTrigger("Sad");
        audioSource.Stop();
        audioSource.PlayOneShot(clipSad);
    }

    public void Happy()
    {
        animator.SetTrigger("Happy");
        audioSource.Stop();
        audioSource.PlayOneShot(clipHappy);

        //wet = true;
        happy = false;

        //Debug.LogError("Happy");
    }

    public void Cry()
    {
        //Debug.LogError("Cry");
        animator.SetTrigger("Cry");
        audioSource.Stop();
        audioSource.PlayOneShot(clipCry);
        cried = true;
    }

    public void Talk ()
    {
        //Debug.LogError("Talk");

        audioSource.clip = clipTalk;
        audioSource.loop = true;
        audioSource.Play();
    }

    public void Ragdoll()
    {
        ragdoll.ragdolled = true;
        ragdolled = true;

        Rigidbody[] rbs = GetComponentsInChildren<Rigidbody>(true);
        for (int i = 0; i < rbs.Length; i++)
        {
            //rbs[i].isKinematic = false;
            rbs[i].velocity = Vector3.zero;
            rbs[i].angularVelocity = Vector3.zero;
        }
    }

    [EditorButton]
    public void Lie()
    {
        ragdoll.ragdolled = false;
        ragdolled = false;
        gameObject.SetActive(true);
        //animator.enabled = true;
        animator.SetInteger("StateMain", 0);
        bool state = currentCanvas.activeSelf;
        currentCanvas = wishCanvasLie;
        wishCanvas.SetActive(false);
        currentCanvas.SetActive(state);

        //Debug.LogError("Lie");
    }

    [EditorButton]
    public void Sit()
    {
        ragdoll.ragdolled = false;
        ragdolled = false;
        gameObject.SetActive(true);
        //animator.enabled = true;
        animator.SetInteger("StateMain", 1);
        //Debug.Log("Children Sit");
        bool state = currentCanvas.activeSelf;
        currentCanvas = wishCanvas;
        wishCanvasLie.SetActive(false);
        currentCanvas.SetActive(state);

        //Debug.LogError("Sit");
    }

    public void OnStartTake ()
    {
        onHands = true;
        btnThrowState = Groups.Instance().GetGroup("Button Throw")[0].activeSelf;
        btnPutState = Groups.Instance().GetGroup("Button Put")[0].activeSelf;
        Groups.Instance().SetGroupState("Button Throw", false);
        Groups.Instance().SetGroupState("Button Put", false);

        Lie();

        EventManager.Instance().TriggerEvent("BabyAboutToTake");

        gameObject.SetActive(false);
    }

    public void BeforeTake ()
    {
        gameObject.SetActive(true);

        SetPosition();
    }

    private bool btnThrowState;
    private bool btnPutState;

    private int place = 0;
    public void Taked ()
    {
        Lie();
        currentCanvas.SetActive(false);
        //Sit();
    }

    public System.Action onReleased;
    public void Released ()
    {
        onHands = false;
        Groups.Instance().SetGroupState ("Button Throw", btnThrowState);
        Groups.Instance().SetGroupState("Button Put", btnPutState);

        //Debug.LogError("Released", currentCanvas);

        currentCanvas.SetActive(true);
        Ragdoll();
        if (onReleased != null) onReleased();
    }

    public void TakeOfDiaper ()
    {
        var newPrefab = Instantiate(currDiaperPrefab);
        newPrefab.transform.SetParent(transform);
        newPrefab.transform.localScale = diaperScale;
        newPrefab.name = currDiaperPrefab.name;

        diaperInterface.providedInterface = newPrefab.GetComponent<InterfaceCollect>();

        currDiaperPrefab.gameObject.SetActive(true);
        currDiaperPrefab.onTaked.RemoveAllListeners();
        currDiaperPrefab.onTaked.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.Off);

        if(currDiaperPrefab == usedDiaperPrefab)
        usedDiaperPrefab = newPrefab;
        if (currDiaperPrefab == unusedDiaperPrefab)
            unusedDiaperPrefab = newPrefab;

        diaper.SetActive(false);

        diaperInterface.gameObject.SetActive(false);

        currDiaperPrefab = unusedDiaperPrefab;
        diaperInterface.providedInterface = unusedDiaperPrefab.GetComponent<InterfaceCollect>();
    }

    public void TakeOnDiaper ()
    {
        diaper.SetActive(true);

        diaperInterface.gameObject.SetActive(true);

        Debug.Log("Take on");

        if (!wet)
        {
            if (ShouldPlayEmotion()) checkWishTimer = 0.2f;
            EventManager.Instance().TriggerEvent("BabyEvent", "Pampers", true);
            if (GameState.Instance().state == GameState.State.Menu) happy = true;
            if (GameState.Instance().state == GameState.State.Menu)
            {
                wet = true;
                currDiaperPrefab = usedDiaperPrefab;
                diaperInterface.providedInterface = usedDiaperPrefab.GetComponent<InterfaceCollect>();
            }
        }
        else
        {
            if (ShouldPlayEmotion()) checkWishTimer = 0.2f;
            EventManager.Instance().TriggerEvent("BabyEvent", "Pampers", false);
            if (GameState.Instance().state == GameState.State.Menu) happy = false;
        }
    }
}
