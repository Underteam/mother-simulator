﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingSmiles : MonoSingleton<FlyingSmiles>
{
    public FlyingCoin fcPrefab;

    private List<Need> needs = new List<Need>();
    private List<float> prevLevels = new List<float>();
    private List<Transform> targets = new List<Transform>();

    protected override void Init()
    {
        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            var need = TaskManager.Instance().GetNeed<NeedForPee>() as Need;
            var bar = TaskManager.Instance().GetBar<NeedForPee>();
            if (need != null)
            {
                var pee = need as NeedForPee;
                int ind = prevLevels.Count;
                pee.onOverflow += () => 
                {
                    Debug.Log("Set prev level " + ind + " " + pee.level);
                    prevLevels[ind] = pee.level;
                };

                needs.Add(need);
                prevLevels.Add(1f);
                if (bar != null) targets.Add(bar.transform);
                else targets.Add(null);
            }

            need = TaskManager.Instance().GetNeed<NeedForFire>() as Need;
            bar = TaskManager.Instance().GetBar<NeedForFire>();
            if (need != null)
            {
                needs.Add(need);
                prevLevels.Add(1f);
                if (bar != null) targets.Add(bar.transform);
                else targets.Add(null);
            }

            need = TaskManager.Instance().GetNeed<DogMood>() as Need;
            bar = TaskManager.Instance().GetBar<DogMood>();
            if (need != null)
            {
                needs.Add(need);
                prevLevels.Add(1f);
                if (bar != null) targets.Add(bar.transform);
                else targets.Add(null);
            }

            need = TaskManager.Instance().GetNeed<DaughterMood>() as Need;
            bar = TaskManager.Instance().GetBar<DaughterMood>();
            if (need != null)
            {
                needs.Add(need);
                prevLevels.Add(1f);
                if (bar != null) targets.Add(bar.transform);
                else targets.Add(null);
            }

            need = TaskManager.Instance().GetNeed<GuestMood>() as Need;
            bar = TaskManager.Instance().GetBar<GuestMood>();
            if (need != null)
            {
                needs.Add(need);
                prevLevels.Add(1f);
                if (bar != null) targets.Add(bar.transform);
                else targets.Add(null);
            }

            need = TaskManager.Instance().GetNeed<MomHungry>() as Need;
            bar = TaskManager.Instance().GetBar<MomHungry>();
            if (need != null)
            {
                needs.Add(need);
                prevLevels.Add(1f);
                if (bar != null) targets.Add(bar.transform);
                else targets.Add(null);
            }

            need = TaskManager.Instance().GetNeed<MomPep>() as Need;
            bar = TaskManager.Instance().GetBar<MomPep>();
            if (need != null)
            {
                needs.Add(need);
                prevLevels.Add(1f);
                if (bar != null) targets.Add(bar.transform);
                else targets.Add(null);
            }

            need = TaskManager.Instance().GetNeed<MomStress>() as Need;
            bar = TaskManager.Instance().GetBar<MomStress>();
            if (need != null)
            {
                needs.Add(need);
                prevLevels.Add(1f);
                if (bar != null) targets.Add(bar.transform);
                else targets.Add(null);
            }
        },
        "TaskStarted");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Update()
    {
        for (int i = 0; i < needs.Count; i++)
        {
            if (prevLevels[i] > needs[i].level) prevLevels[i] = needs[i].level;
            else if (needs[i].level - prevLevels[i] > 0.1f)
            {
                //Debug.Log("Smile " + needs[i] + " " + (needs[i].level - prevLevels[i]), needs[i]);
                int n = (int)((needs[i].level - prevLevels[i] + 0.05f) / 0.1f);
                Run(n, targets[i]);
                prevLevels[i] = needs[i].level;
            }
        }
    }

    public void Run (int num, Transform target)
    {
        //Debug.LogError("Run");
        StartCoroutine(RunCoroutine(num, target));
    }

    private IEnumerator RunCoroutine (int num, Transform target)
    {
        FlyingCoin lastFC = null;

        for (int i = 0; i < num; i++)
        {
            var coin = Instantiate(fcPrefab);
            coin.transform.SetParent(fcPrefab.transform.parent);
            coin.transform.position = fcPrefab.transform.position;
            coin.transform.localScale = fcPrefab.transform.localScale;
            coin.transform.SetParent(fcPrefab.transform.parent.parent);
            coin.gameObject.SetActive(true);
            if (target != null) coin.target = target;
            lastFC = coin;

            yield return new WaitForSeconds(0.1f);
        }

        while (lastFC != null && lastFC.gameObject.activeSelf) yield return null;
    }
}
