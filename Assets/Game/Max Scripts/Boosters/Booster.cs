﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Booster : MonoBehaviour
{
    public Sprite icon;
    public Sprite smallIcon;

    public string description;

    public Wallet.CurrencyType currency;
    public int price;

    public UnityEvent onBuy;

    public string shortDescription;
    public string longDescription;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Buyed ()
    {
        if (onBuy != null) onBuy.Invoke();
    }
}
