﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoffeeBag : MonoBehaviour, IAppropriatanceProvider
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ApplyItem(Item item)
    {
        if (item == null) return;

        var cup = item.GetComponent<CoffeeCup>();

        Debug.Log("Apply " + cup, cup);

        if (cup != null)
        {
            cup.Apply(GetComponent<Item>());
        }
    }

    public bool IsAppropriate(Item item)
    {
        if (item == null) return false;

        var cup = item.GetComponent<CoffeeCup>();

        return cup != null && cup.myBag == null;
    }
}
