﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BigInteger = System.Int32;

public class Wallet : MonoBehaviour {

    public enum CurrencyType
    {
        coins,
        crystals,
        energy,
        real,
        ads,
        keys,
    }

    public string description;

    public CurrencyType currency;

    private static List<Wallet> wallets = new List<Wallet>();

    public System.Action<Wallet> onValueChanged;
    //public System.Action<Wallet> onValueChanged
    //{
    //    get
    //    {
    //        if (!inited) Init();
    //        return _onValueChanged;
    //    }

    //    set
    //    {
    //        _onValueChanged = value;
    //    }
    //}


    [SerializeField]
    //private BigInteger _amount;
    public BigInteger amount;
    //{
    //    get
    //    {
    //        if (!inited) Init();
    //        return _amount;
    //    }

    //    private set
    //    {
    //        _amount = value;
    //    }
    //}

    [HideInInspector]
    public bool developerMode;

    public Sprite currencyIcon;

    public int startAmount;

	// Use this for initialization
	void Awake () {

        
    }

    private void Start()
    {
        if (!inited) Init();
    }

    private bool inited;
    public void Init ()
    {
        if (inited) return;

        if (!wallets.Contains(this)) wallets.Add(this);

        Load();

        inited = true;
    }

    private static List<Wallet> results = new List<Wallet>();
    public static Wallet GetWallet (CurrencyType currency, string description = "")
    {
        results.Clear();

        for(int i = 0; i < wallets.Count; i++)
        {
            if(wallets[i] == null)
            {
                wallets.RemoveAt(i);
                i--;
                continue;
            }

            wallets[i].Init();
            if (wallets[i].currency == currency) results.Add(wallets[i]);
        }

        if (results.Count > 0)
        {
            if (string.IsNullOrEmpty(description)) return results[0];
            for (int i = 0; i < results.Count; i++)
                if (results[i].description.Equals(description)) return results[i];
        }

        results.Clear();

        var list = FindObjectsOfType<Wallet>();
        for(int i = 0; i < list.Length; i++)
        {
            if(list[i].currency == currency)
            {
                wallets.Add(list[i]);
                results.Add(list[i]);
            }
        }

        if (results.Count > 0)
        {
            if (string.IsNullOrEmpty(description)) return results[0];
            for (int i = 0; i < results.Count; i++)
                if (results[i].description.Equals(description)) return results[i];
        }

        GameObject go = new GameObject("Wallet" + currency);

        var wallet = go.AddComponent<Wallet>();
        wallet.currency = currency;
        wallet.description = description;
        wallet.Init();
        wallets.Add(wallet);

        return wallet;
    }

    public bool Spend (BigInteger amount)
    {
        if (!inited) Init();

        if (developerMode) Add(2 * amount);

        //Debug.LogError("----------------------> Spend " + currency + " " + amount);

        if (this.amount < amount) return false;

        this.amount -= amount;

        Save();

        if (onValueChanged != null) onValueChanged(this);

        return true;
    }

    public void Add (BigInteger amount)
    {
        if (!inited) Init();

        //Debug.LogError("----------------------> Add " + currency + " " + amount + " | " + this.amount);

        if (amount <= 0) return;

        this.amount += amount;

        Save();

        if (onValueChanged != null) onValueChanged(this);
    }

    public void Save()
    {
        //Debug.LogError("Save " + currency + " " + description + " " + amount);

        PlayerPrefs.SetString("Wallet_" + currency + "_" + description, this.amount.ToString());
    }

    public void Load()
    {
        string value = "" + startAmount;

        value = PlayerPrefs.GetString("Wallet_" + currency + "_" + description, value);

        int v = 0;
        int.TryParse(value, out v);
        amount = v;

        if (onValueChanged != null) onValueChanged(this);

        //Debug.LogError("Load " + currency + " " + description + " " + amount + " " + value);
    }
}
