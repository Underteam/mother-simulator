﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppsFlyerInit : MonoBehaviour
{
    void Start()
    {
        /* Mandatory - set your AppsFlyer’s Developer key. */
        AppsFlyer.setAppsFlyerKey("upqXCHz86iM7Pe6xT7e8YL");
        /* For detailed logging */
        /* AppsFlyer.setIsDebug (true); */
        #if UNITY_IOS
        /* Mandatory - set your apple app ID
        NOTE: You should enter the number only and not the "ID" prefix */
        AppsFlyer.setAppID ("id1515675190");
        AppsFlyer.trackAppLaunch ();
        #elif UNITY_ANDROID
        /* Mandatory - set your Android package name */
        AppsFlyer.setAppID("com.mothersimulator3d2");
        /* For getting the conversion data in Android, you need to add the "AppsFlyerTrackerCallbacks" listener.*/
        AppsFlyer.init("upqXCHz86iM7Pe6xT7e8YL", "AppsFlyerTrackerCallbacks");
        #endif
    }
}
