﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DailyTaskUI : MonoBehaviour
{
    public Image progressBar;
    public Text lblProgress;
    public Text lblDescription;
    public Text lblReward;

    public DailyTask task { get; private set; }

    private bool ready;

    public Image body;
    public Image crystal;
    public Image btnTake;
    public GameObject btnWatchAd;

    public Sprite bodyActive;
    public Sprite bodyDone;

    public Sprite takeActive;
    public Sprite takeDone;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Remove ()
    {
        if (this.task != null) Destroy(this.task.gameObject);
    }

    public void Set (DailyTask task, int ind, bool load = false)
    {
        if (this.task != null) Destroy(this.task.gameObject);

        this.task = task;

        task.Init(ind, OnUpdated, load);

        //Debug.LogError("Set " + task.evtName + " " + task.requiredAmount);

        OnUpdated (task);
    }

    private void OnUpdated(DailyTask t)
    {
        float p = t.getProgress;

        progressBar.fillAmount = p;

        ready = p >= 1;

        //Debug.LogError("Updated " + task.amount + " " + task.requiredAmount);

        lblDescription.text = Localizer.GetString(task.description, task.description);

        lblProgress.text = Mathf.Min(task.amount, task.requiredAmount) + "/" + task.requiredAmount;

        btnWatchAd.SetActive(false);
        btnTake.gameObject.SetActive(true);

        if (ready)
        {
            body.sprite = bodyDone;
            btnTake.sprite = takeDone;
        }
        else
        {
            body.sprite = bodyActive;
            btnTake.sprite = takeActive;

            if (t.evtName.Equals("taskWatchAd"))
            {
                btnWatchAd.SetActive(true);
                btnTake.gameObject.SetActive(false);
            }
        }

        lblReward.text = "" + task.getReward;

        DailyTasks.Instance().Updated();
    }

    public void Take ()
    {
        if (!ready) return;

        DailyTasks.Instance().Take(this);
    }

    bool waitingForAdEnd;
    public void WatchAd ()
    {
        waitingForAdEnd = true;
        Time.timeScale = 0f;
        AudioListener.volume = 0f;
        AdsController.Instance().ShowAd(AdsProvider.AdType.unskipablevideo, (b) =>
        {
            Time.timeScale = 1f;
            AudioListener.volume = 1f;

            if (!waitingForAdEnd) return;
            waitingForAdEnd = false;

            if (!b) return;


            Analytics.Instance().SendEvent("WatchAdTask", 0);

            Analytics.Instance().SendEvent("af_ads_success");
            Analytics.Instance().SendEvent("af_ads_success_rv");

            GameController.Instance().TrackAdsWatched();

            GMATools.Common.EventManager.Instance().TriggerEvent("taskWatchAd");
        });
    }
}
