﻿using GMATools.Common;
using MoreMountains.NiceVibrations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DailyTasks : MonoSingleton<DailyTasks>
{
    public GameObject panel;

    public VerticalLayoutGroup vlg;

    public RectTransform upperSpace;
    public RectTransform lowerSpace;

    public List<DailyTaskUI> tasks;
    private DailyTaskUI newTask;

    private List<DailyTask> activeTasks = new List<DailyTask>();
    public List<DailyTask> tasksPrefabs;

    public FlyingCoin fcPrefab;
    public Transform fcTarget;

    public GameObject mark;

    protected override void Init()
    {
        for (int i = 0; i < 3; i++)
        {
            bool load = PlayerPrefs.HasKey("DailyTask" + i + "Type");
            DailyTask newTask = null;
            if (load)
            {
                newTask = GetTask(PlayerPrefs.GetString("DailyTask" + i + "Type"));
                //Debug.LogError("1 New task " + newTask.evtName + " " + newTask.startFromDay + " " + GameState.Instance().currentTask);
            }
            else
            {
                newTask = GetRandomTask(null);
                //Debug.LogError("2 New task " + newTask.evtName + " " + newTask.startFromDay + " " + GameState.Instance().currentTask);
            }

            tasks[i].Set(newTask, i, load);
            activeTasks.Add(newTask);
            PlayerPrefs.SetString("DailyTask" + i + "Type", newTask.evtName);
        }

        newTask = tasks[3];

        Updated();

        base.Init();
    }

    private bool taking;
    private List<bool> animating = new List<bool>();

    public void Updated ()
    {
        bool canTake = false;

        for (int i = 0; i < activeTasks.Count; i++)
        {
            if (activeTasks[i].amount >= activeTasks[i].requiredAmount) canTake = true;
        }

        mark.SetActive(canTake && GameState.Instance().state == GameState.State.Menu);
    }
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1)) EventManager.Instance().TriggerEvent(activeTasks[0].evtName);
        if (Input.GetKeyDown(KeyCode.Alpha2)) EventManager.Instance().TriggerEvent(activeTasks[1].evtName);
        if (Input.GetKeyDown(KeyCode.Alpha3)) EventManager.Instance().TriggerEvent(activeTasks[2].evtName);

        if (taking)
        {
            upperHeight -= upperSpeed * Time.unscaledDeltaTime;
            lowerHeight -= lowerSpeed * Time.unscaledDeltaTime;

            if (upperHeight <= -26)
            {
                lowerSpace.SetSiblingIndex(4);
                var size = lowerSpace.sizeDelta;
                size.y = 0;
                lowerSpace.sizeDelta = size;
                task.gameObject.SetActive(true);
                newTask = task;
                upperSpace.gameObject.SetActive(false);
                upperSpace.SetAsLastSibling();
                taking = false;
            }
            else
            {
                var size = upperSpace.sizeDelta;
                size.y = upperHeight;
                upperSpace.sizeDelta = size;

                size = lowerSpace.sizeDelta;
                size.y = lowerHeight;
                lowerSpace.sizeDelta = size;
            }

            vlg.gameObject.SetActive(false);
            vlg.gameObject.SetActive(true);
            vlg.CalculateLayoutInputVertical();
        }
    }

    private float upperHeight;
    private float lowerHeight;
    private float upperSpeed = 400;
    private float lowerSpeed;
    private DailyTaskUI task;
    public void Take (DailyTaskUI task)
    {
        if (taking) return;

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);

        upperSpace.gameObject.SetActive(true);

        var rt = (task.transform as RectTransform);

        upperSpace.sizeDelta = rt.sizeDelta;

        int si = task.transform.GetSiblingIndex();

        upperHeight = rt.rect.height;
        lowerHeight = 0;

        task.transform.SetAsLastSibling();
        task.gameObject.SetActive(false);
        task.Remove();

        lowerSpace.SetSiblingIndex(3);
        upperSpace.SetSiblingIndex(si);

        lowerSpeed = upperSpeed * 25 / upperHeight;

        this.task = task;

        int reward = task.task.getReward;
        Wallet w = Wallet.GetWallet(Wallet.CurrencyType.crystals);
        w.Add(reward);

        animating.Add(true);
        fcPrefab.transform.position = task.crystal.transform.position;
        FlyingCoin.Run(fcPrefab, 20, fcTarget, 0.01f, () => { if (animating.Count > 0) animating.RemoveAt(0); });

        activeTasks.Remove(task.task);
        var newTask = GetRandomTask(task.task);
        //Debug.LogError("New task " + newTask.evtName + " " + newTask.startFromDay + " " + GameState.Instance().currentTask);
        PlayerPrefs.SetString("DailyTask" + task.task.ind + "Type", newTask.evtName);
        this.newTask.Set(newTask, task.task.ind);
        activeTasks.Add(newTask);

        Updated();

        taking = true;
    }

    private DailyTask GetRandomTask(DailyTask prevTask)
    {
        List<string> posibleTasks = new List<string>();

        for (int i = 0; i < tasksPrefabs.Count; i++)
        {
            if (tasksPrefabs[i].startFromDay > GameState.Instance().currentTask) continue;
            posibleTasks.Add(tasksPrefabs[i].evtName);
        }

        if (prevTask != null && !prevTask.canBeRepeated && posibleTasks.Contains(prevTask.evtName)) posibleTasks.Remove(prevTask.evtName);

        for (int i = 0; i < activeTasks.Count; i++)
        {
            if (posibleTasks.Contains(activeTasks[i].evtName)) posibleTasks.Remove(activeTasks[i].evtName);
        }

        //string str = "";
        //for (int i = 0; i < posibleTasks.Count; i++) str += posibleTasks[i] + " ";
        //Debug.LogError("Choose from " + str);

        string evtName = tasksPrefabs[Random.Range(0, tasksPrefabs.Count)].evtName;

        string preferableTask = "taskWatchAd";
        if (posibleTasks.Contains(preferableTask)) evtName = preferableTask;
        else if (posibleTasks.Count > 0) evtName = posibleTasks[Random.Range(0, posibleTasks.Count)];
        else Debug.LogError("No choice");

        for (int i = 0; i < tasksPrefabs.Count; i++)
        {
            if (tasksPrefabs[i].evtName.Equals(evtName)) return Instantiate(tasksPrefabs[i]);
        }

        return Instantiate(tasksPrefabs[Random.Range(0, tasksPrefabs.Count)]);
    }

    private DailyTask GetTask (string evtName)
    {
        if (!string.IsNullOrEmpty(evtName))
        {
            for (int i = 0; i < tasksPrefabs.Count; i++)
            {
                if (tasksPrefabs[i].evtName.Equals(evtName)) return Instantiate(tasksPrefabs[i]);
            }
        }

        return GetRandomTask(null);
    }

    [Sirenix.OdinInspector.Button]
    public void ChangeTask ()
    {
        Take(tasks[1]);
    }

    public void Close ()
    {
        if (taking || animating.Count > 0) return;

        panel.SetActive(false);

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    public void Open ()
    {
        panel.SetActive(true);

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }
}
