﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GMATools.Common;

public class DailyTask : MonoBehaviour
{
    public string description;

    public string evtName;

    public int requiredAmount { get; private set; }

    public int ind { get; set; }

    public int amount { get; private set; }

    public float getProgress { get { return (float)Mathf.Min(amount, requiredAmount) / requiredAmount; } }
    public int getReward { get { return rewardPerAmount * requiredAmount; } }

    private System.Action<DailyTask> onUpdated;

    public Vector2Int posibleAmounts;
    public int rewardPerAmount;

    public bool canBeRepeated = true;

    public int startFromDay = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    private bool inited;
    public void Init (int ind, System.Action<DailyTask> onUpdated, bool load)
    {
        if (inited) return;

        this.ind = ind;
        this.onUpdated = onUpdated;

        if (load)
        {
            requiredAmount = PlayerPrefs.GetInt("DailyTask" + ind + "RA", requiredAmount);
            amount = PlayerPrefs.GetInt("DailyTask" + ind + "A", amount);
        }
        else
        {
            requiredAmount = Random.Range(posibleAmounts.x, posibleAmounts.y + 1);
            amount = 0;

            PlayerPrefs.SetInt("DailyTask" + ind + "RA", requiredAmount);
            PlayerPrefs.SetInt("DailyTask" + ind + "A", 0);
        }

        EventManager.Instance().AddListener(Listener, evtName);

        //Debug.LogError("Init task " + ind + " " + evtName + " " + requiredAmount + " " + load, this);

        inited = true;
    }

    private void Listener (string n, object d)
    {
        //Debug.LogError(n + " " + amount, this);

        amount++;

        PlayerPrefs.SetInt("DailyTask" + ind + "A", amount);

        onUpdated?.Invoke(this);
    }

    private void OnDestroy()
    {
        //Debug.LogError("Destroy", this);
        EventManager.Instance().DetachListener(Listener, evtName);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
