﻿using UnityEngine;
using System;
using System.Collections;

namespace GMATools {
	
	namespace Common {

		public class Updater : MonoBehaviour
		{
			public delegate void UpdateMethod();
			UpdateMethod updateMethod = null;
			
			private float timer = 0;
			private float period = 0;
			public float Period { get { return period; } set {timer += (value - period); } }

			public void SetUpdateMethod(UpdateMethod method, float period = 0) {
				
				updateMethod = method;
				
				Period = period;
			}

			void Start()
            {
				DontDestroyOnLoad (gameObject);
			}

			void Update()
            {
				if (updateMethod != null) {

					if(period > 0) {
					
						timer -= Time.deltaTime;
						
						if(timer <= 0) {
							updateMethod ();
							timer = period;
						}

					} else {

						updateMethod ();
					}
				}
			}

            public void Stop ()
            {
                updateMethod = null;
                Destroy(gameObject);
            }

			void OnLevelWasLoaded(int level) {

				((EventDelegate)"OnGameWasReload").Trigger ();

			}

			public void ExecuteNow() {
				
				if(updateMethod != null) updateMethod();
			}

			public static Updater CreateUpdater(UpdateMethod method, string uName = "", float period = 0) {

				if(uName == "") uName = "Updater";
				GameObject go = new GameObject(uName);
				go.AddComponent(typeof(Updater));
				Updater updater = go.GetComponent<Updater> ();
				updater.SetUpdateMethod(method, period);

				return updater;
			}

            public System.Action onQuit;
            
            private void OnApplicationQuit()
            {
                Debug.LogError("Quiting");
                if (onQuit != null) onQuit();
            }
        }
	}
}
