﻿//C# Unity event manager that uses strings in a hashtable over delegates and events in order to
//allow use of events without knowing where and when they're declared/defined.
//by Billy Fletcher of Rubix Studios
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GMATools {

	namespace Common {

		struct MyEvent {

			public string name;
			public object data;
		}

		public class EventManager
		{

			public delegate void EventHandler(string eventName, object data);

			public bool LimitQueueProcesing = false;
			public float QueueProcessTime = 0.0f;
			
			private static EventManager instance = null;
			public static EventManager Instance () {

				if (instance == null) 
				{
					instance = new EventManager();

					//Debug.Log("Creating EventHandler instance");

					var updater = Updater.CreateUpdater(instance.Update, "EventManagerUpdater");
                    updater.onQuit += () => { instance.Clear(); };
				}
				
				return instance;
			}

			//private Hashtable m_listenerTable = new Hashtable();
			private Hashtable m_listenerTable = new Hashtable();
            //private Dictionary<EventHandler, object> owners = new Dictionary<EventHandler, object>();
            private List<EventHandler> handlers = new List<EventHandler>();
            private Dictionary<string, List<EventHandler>> toDetach = new Dictionary<string, List<EventHandler>>();
			private static Queue queue = new Queue();
			private Queue m_eventQueue = Queue.Synchronized (queue);
			
            public void Clear ()
            {
                m_listenerTable = new Hashtable();
                //owners = new Dictionary<EventHandler, object>();
                handlers = new List<EventHandler>();
                toDetach = new Dictionary<string, List<EventHandler>>();
                queue = new Queue();
                m_eventQueue = Queue.Synchronized(queue);
            }

			public EventHandler AddListener(EventHandler listener, string eventName/*, object owner*/)
			{
				if (listener == null || eventName == null)
				{
					Debug.Log("Event Manager: AddListener failed due to no listener or event name specified.");
                    return null;
				}
				
				if (!m_listenerTable.ContainsKey(eventName)) {
					m_listenerTable.Add(eventName, new ArrayList());
				}
				
				ArrayList listenerList = m_listenerTable[eventName] as ArrayList;
				if (listenerList.Contains(listener))
				{
                    if (toDetach.ContainsKey(eventName) && toDetach[eventName].Contains(listener))
                        toDetach[eventName].Remove(listener);
                    else
                        Debug.Log("Event Manager: Listener: " + listener.GetType().ToString() + " is already in list for event: " + eventName);
                    return listener; //listener already in list
				}

				listenerList.Add(listener);
                //if (!owners.ContainsKey(listener)) owners.Add(listener, owner);

                return listener;
			}

            public EventHandler AddListener(EventHandler listener)
            {
                if (listener == null)
                {
                    Debug.Log("Event Manager: AddListener failed due to no listener or event name specified.");
                    return null;
                }

                if (!handlers.Contains(listener))
                {
                    handlers.Add(listener);
                    //if (!owners.ContainsKey(listener)) owners.Add(listener, owner);
                }

                return listener;

            }

            //Remove a listener from the subscribed to event.
            public bool DetachListener(EventHandler listener, string eventName, bool instant = false)
			{
                if (!m_listenerTable.ContainsKey(eventName))
                    return false;

                ArrayList listenerList = m_listenerTable[eventName] as ArrayList;
                if (!listenerList.Contains(listener))
                    return false;

                if (instant)
                {
                    listenerList.Remove(listener);
                    //if (owners.ContainsKey(listener)) owners.Remove(listener);
                }
                else
                {
                    if (!toDetach.ContainsKey(eventName)) toDetach.Add(eventName, new List<EventHandler>());

                    if (!toDetach[eventName].Contains(listener)) toDetach[eventName].Add(listener);
                }

                return true;
            }

            public void DetachListener(EventHandler listener, bool instant = false)
            {
                foreach (DictionaryEntry entry in m_listenerTable)
                {
                    string eventName = entry.Key as string;

                    ArrayList listenerList = entry.Value as ArrayList;

                    if (!listenerList.Contains(listener)) continue;

                    if (instant)
                    {
                        listenerList.Remove(listener);
                    }
                    else
                    {
                        if (!toDetach.ContainsKey(eventName)) toDetach.Add(eventName, new List<EventHandler>());

                        if (!toDetach[eventName].Contains(listener)) toDetach[eventName].Add(listener);
                    }
                }
            }

            public void DetachListener(string eventName)
            {
                if (m_listenerTable.ContainsKey(eventName)) m_listenerTable.Remove(eventName);
            }

            public bool TriggerEvent(string evtName) {
			
				return TriggerEvent (evtName, null);
			}

            public bool TriggerEvent(string evtName, params object[] list)
            {
                object data = list;
                return TriggerEvent(evtName, data);
            }

            //Trigger the event instantly, this should only be used in specific circumstances,
            //the QueueEvent function is usually fast enough for the vast majority of uses.
            public bool TriggerEvent(string evtName, object data)
			{
				bool result = false;

				string eventName = evtName;

                List<EventHandler> list = new List<EventHandler>();
                if (toDetach.ContainsKey(eventName)) list = toDetach[eventName];

                if (m_listenerTable.ContainsKey(eventName)) {

					ArrayList listenerList = m_listenerTable[eventName] as ArrayList;
					foreach (EventHandler listener in listenerList)
					{
                        if (list.Contains (listener)) continue;
                        //if (owners.ContainsKey(listener) && owners[listener] == null)
                        //{
                        //    DetachListener(listener, evtName);
                        //    continue;
                        //}
						//Debug.Log("Trigger event " + evtName + " at " + listener);
						listener(evtName, data);
					}
					result = true;
				}

                for (int i = 0; i < handlers.Count; i++)
                {
                    if (list.Contains(handlers[i])) continue;
                    //if (owners.ContainsKey(handlers[i]) && owners[handlers[i]] == null)
                    //{
                    //    DetachListener(handlers[i], evtName);
                    //    continue;
                    //}
                    handlers[i](evtName, data);
                }

				if (!result)
				{
					Debug.Log("Event Manager: Event \"" + eventName + "\" triggered has no listeners!");
				}

				return result;
			}
			
			//Inserts the event into the current queue.
			public bool QueueEvent(string evtName, object data)
			{

				if (/*!m_listenerTable.ContainsKey(evtName) && */!m_listenerTable.ContainsKey(evtName))
				{
					Debug.Log("EventManager: QueueEvent failed due to no listeners for event: " + evtName);
					return false;
				}

				MyEvent ev = new MyEvent ();
				ev.name = evtName;
				ev.data = data;

				m_eventQueue.Enqueue(ev);

				return true;
			}
			
			//Every update cycle the queue is processed, if the queue processing is limited,
			//a maximum processing time per update can be set after which the events will have
			//to be processed next update loop.
			public void Update()
			{
                foreach(var kvp in toDetach)
                {
                    string eventName = kvp.Key;
                    ArrayList listenerList = m_listenerTable[eventName] as ArrayList;
                    for(int j = 0; j < kvp.Value.Count; j++)
                    {
                        if (listenerList.Contains(kvp.Value[j])) listenerList.Remove(kvp.Value[j]);
                    }
                }

                toDetach.Clear();

                float timer = 0.0f;
				while (m_eventQueue.Count > 0)
				{
					if (LimitQueueProcesing)
					{
						if (timer > QueueProcessTime)
							return;
					}

					if(m_eventQueue == null) Debug.LogError("??????");

					MyEvent ev = (MyEvent)m_eventQueue.Dequeue();

					try {

						TriggerEvent(ev.name, ev.data);

					} catch (System.Exception e){

						Debug.LogError("Error while Dequeue");
						Debug.Log("Event: " + ev.name);
						//Debug.Log("Data: " + (string)ev.data);
						Debug.Log("Exception " + e.Source);
						Debug.Log("Exception " + e.ToString());
						Debug.Log("Count " + m_eventQueue.Count);//*/

						Time.timeScale = 0.0f;
						//ClientSide.GameController.Instance().SpecialFunction(ev.name, ev.data);
					}
					
					if (LimitQueueProcesing)
						timer += Time.deltaTime;
				}
			}

			~EventManager() {

				//m_listenerTable.Clear();
				m_listenerTable.Clear();
				m_eventQueue.Clear();
                handlers.Clear();
                //owners.Clear();
                toDetach.Clear();
				instance = null;
			}
		}
	}
}