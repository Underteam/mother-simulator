﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct SmartEventHandler {

	private List<System.Action> handlers;

	public static SmartEventHandler operator + (SmartEventHandler a, System.Action b) {
	
		if (a.handlers == null) a.handlers = new List<System.Action> ();

		if (!a.handlers.Contains (b)) a.handlers.Add (b);
		
		return a;
	}

	public static SmartEventHandler operator - (SmartEventHandler a, System.Action b) {

		if (a.handlers == null) a.handlers = new List<System.Action> ();

		if (a.handlers.Contains (b)) a.handlers.Remove (b);

		return a;
	}

	public void Invoke () {
	
		if (handlers == null)
			handlers = new List<System.Action> ();
		
		for (int i = 0; i < handlers.Count; i++) {
			if (handlers [i] == null) {
				handlers.RemoveAt (i);
				i--;
				continue;
			}
			handlers [i] ();
		}
	}
}

public struct SmartEventHandler<T> {

	private List<System.Action<T>> handlers;

	public static SmartEventHandler<T> operator + (SmartEventHandler<T> a, System.Action<T> b) {

		if (a.handlers == null) a.handlers = new List<System.Action<T>> ();

		//Debug.LogError ("Add " + b + " " + a.handlers.Contains (b));
			
		if (!a.handlers.Contains (b)) a.handlers.Add (b);

		return a;
	}

	/*public static SmartEventHandler<T> operator - (SmartEventHandler<T> a, System.Action<T> b) {

		if (a.handlers == null) a.handlers = new List<System.Action<T>> ();

		if (a.handlers.Contains (b)) a.handlers.Remove (b);

		return a;
	}//*/

	public void Invoke (T p) {

		//Debug.LogError ("Invoke " + (handlers == null));

		if (handlers == null)
			handlers = new List<System.Action<T>> ();

		for (int i = 0; i < handlers.Count; i++) {
			//Debug.Log (i + " " + (handlers [i] == null));
			if (handlers [i] == null) {
				handlers.RemoveAt (i);
				i--;
				continue;
			}
			handlers [i] (p);
		}
	}
}
