﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace GMATools {
	
	namespace Common {

		public class EventDelegate {

			/*protected static EventDelegate e = ((EventDelegate)"OnGameWasReload").AddListener(() => {Clear();});
			private static void Clear() {
				
				events.Clear ();
			}//*/

			private static Dictionary<string, EventDelegate> events = new Dictionary<string, EventDelegate>();

			private static EventDelegate empty = new EventDelegate();

			private string name;

			private List<Action> listeners = new List<Action>();

			private EventDelegate() {
			
			}

			private EventDelegate(string name) {

				if(name == null || name == "") return;

				if (events.ContainsKey (name)) return;

				this.name = name;

				events.Add (name, this);
			}

			public void Trigger() {

				if (listeners.Count == 0) {
					Debug.LogWarning("There is no listeners for " + name + " event");
					return;
				}

				for (int i = 0; i < listeners.Count; i++)
					listeners [i] ();
			}

			public EventDelegate AddListener(Action action) {
			
				if (listeners.Contains (action)) {
					Debug.Log("Listener " + action + " already added for " + name + " event");
					return this;
				}

				listeners.Add (action);

				return this;
			}

			public static EventDelegate GetEvent(string eventName) {
				
				if(eventName == null || eventName == "") return empty;
				
				if(events.ContainsKey(eventName)) return events[eventName];
				
				EventDelegate ed = new EventDelegate(eventName);
				
				return ed;
			}
			
			public static implicit operator string (EventDelegate a) {
				
				return a.name;
			}
			
			public static implicit operator EventDelegate (string actionName) {
				
				return GetEvent (actionName);
			}
		}
	}
}
