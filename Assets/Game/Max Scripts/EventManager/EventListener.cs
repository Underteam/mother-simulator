using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

public class EventListener : MonoBehaviour {

	public string eventName;

	public UnityEngine.Events.UnityEvent Listeners;

	void Start () {

		if (eventName != "")
			GMATools.Common.EventManager.Instance ().AddListener (OnEventHeppens, eventName);
	}

    private void OnDestroy()
    {
        if (eventName != "")
            GMATools.Common.EventManager.Instance().DetachListener(OnEventHeppens, eventName);
    }

    void OnEventHeppens(string eventName, object data) {

		Listeners.Invoke ();
	}

	public void Method1() {
	
	}

	public void Method2(int a) {
	
		Debug.Log (a.ToString());
	}

	public void Method3(string s) {
	
		Debug.Log (s);
	}

	public void Method4(object o) {
	
	}

	public int Method5() {
	
		return 0;
	}
}
