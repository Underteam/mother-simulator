﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task : MonoBehaviour
{
    public string taskName;

    public Wish currentWish { get; private set; }

    private int ind = 0;

    public List<Wish> wishes;

    public List<Need> needs;

    public List<GameObject> toInstantiate;

    public bool failed { get; set; }

    public System.Action<Wish> onNextWish;
    public System.Action<Wish> onWishDone;

    public Tutorial tutor;

    public List<Booster> boosters;

    // Start is called before the first frame update
    void Start()
    {
               
    }

    public void StartTask (bool skipTutor)
    {
        //Debug.Log("Start task");

        for (int i = 0; i < toInstantiate.Count; i++)
        {
            if (toInstantiate[i] == null) continue;
            var go = Instantiate(toInstantiate[i]);
        }

        if (!skipTutor)
            TutorialManager.Instance().SetTutorial(tutor);

        var listener = EventManager.Instance().AddListener(OnBabyEvent, "BabyEvent");
        listeners.Add(listener);

        ind = 0;
        failed = false;
        PickNextWish();
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    // Update is called once per frame
    void Update()
    {
        if (!failed)
        {
            for(int i = 0; i < needs.Count; i++)
            {
                if (needs[i].level < 0 && needs[i].critical)
                {
                    failed = true;
                    Debug.Log("Failed because of " + needs[i].description);
                    EventManager.Instance().TriggerEvent("TaskFailed", taskName);
                    break;
                }
            }
        }
    }

    private void PickNextWish ()
    {
        if (currentWish != null)
        {
            //EventManager.Instance().DetachListener(OnBabyEvent, currentWish.eventName);
            //Debug.LogError("Detach");
        }

        currentWish = null;

        while (ind < wishes.Count && !wishes[ind].enabled) ind++;

        if (ind < wishes.Count)
        {
            currentWish = wishes[ind];
            currentWish.Init();
            currentWish.done = false;
            if (onNextWish != null) onNextWish(currentWish);
            ind++;
            //EventManager.Instance().AddListener(OnBabyEvent, currentWish.eventName);
        }
        else
        {
            EventManager.Instance().TriggerEvent("TaskCompleted", taskName);
            PlayerPrefs.SetInt("SkipTutorial" + name, 1);
        }
    }

    private void OnBabyEvent (string name, object data)
    {
        if (this == null) return;

        //Debug.LogError("Baby event happend " + currentWish.eventName, this);

        if (currentWish.done) return;

        bool res = currentWish.Check (name, data);

        if (currentWish != null && !res)
        {
            EventManager.Instance().TriggerEvent("WishUnSatisfied", data);
        }
        else
        {
            EventManager.Instance().TriggerEvent("WishSatisfied", data);
            //Debug.LogError("Wish satisfied " + currentWish.eventName.Equals("Sleep"));
            if (!currentWish.eventName.Equals("Cot")) EventManager.Instance().TriggerEvent("taskBabyWish");
            currentWish.done = true;
            if (onWishDone != null) onWishDone(currentWish);
            TaskManager.Instance().CompleteCurrentWish();
            if (ind < wishes.Count)
                Invoke("PickNextWish", 5.5f);
            else
                PickNextWish();
        }
    }
}
