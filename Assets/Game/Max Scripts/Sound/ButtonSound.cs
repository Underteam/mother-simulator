﻿using MoreMountains.NiceVibrations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSound : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Play ()
    {
        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }
}
