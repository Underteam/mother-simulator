﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSettings : MonoBehaviour
{
    private AudioSource source;

    private float volume;

    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
        if (source != null) volume = source.volume;
    }

    // Update is called once per frame
    void Update()
    {
        if (source == null) return;

        if (SFXPlayer.Instance().volume == 0 && source.volume != 0) source.volume = 0;
        else if (SFXPlayer.Instance().volume > 0 && source.volume != volume) source.volume = volume;
    }
}
