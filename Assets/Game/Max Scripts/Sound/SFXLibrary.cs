﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXLibrary : MonoSingleton<SFXLibrary>
{ 
    public AudioClip doorClip;

    public AudioClip buttonClip;
    
    public AudioClip foodClip;

    public AudioClip waterInClip;

    public AudioClip drinking;

    public AudioClip pee;

    public AudioClip lineCompleted;

    public AudioClip dayFailed;

    public AudioClip dayPassed;

    public AudioClip suck;

    public AudioClip teapot;

    public AudioClip dogFood;

    public AudioClip dogEat;

    public AudioClip dogSad;

    public AudioClip dogFart;

    public AudioClip chatter;

    public AudioClip guestSad;

    public AudioClip typing;

    public AudioClip husbandAngry;

    public AudioClip firewoods;

    public AudioClip powder;

    public AudioClip motherFart;

    public AudioClip countdown;

    public AudioClip horse;

    public AudioClip cartoons;

    public AudioClip shivering;

    public AudioClip clock;
}
