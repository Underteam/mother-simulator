﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimationEventListener : MonoBehaviour
{
    [System.Serializable]
    public class OnEvent : UnityEvent<AnimationEvent> { }
    public OnEvent onEvent;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void OnAnimationEvent(AnimationEvent ev)
    {
        //Debug.Log(ev.stringParameter);
        onEvent?.Invoke(ev);
    }
}
