﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandsCollider : MonoBehaviour
{
    public SphereCollider leftCol;
    public SphereCollider rightCol;

    public Transform leftHand;
    public Transform rightHand;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        leftCol.center = transform.InverseTransformPoint(leftHand.position);
        rightCol.center = transform.InverseTransformPoint(rightHand.position);
    }

    private List<Vector3> normals = new List<Vector3>();
    private List<Vector3> points = new List<Vector3>();

    public bool log;

    private void OnCollisionStay (Collision collision)
    {
        normals.Clear();
        points.Clear();

        if (log) Debug.Log("Stay");

        for (int i = 0; i < collision.contacts.Length; i++)
        {
            if (collision.contacts[i].otherCollider.isTrigger) continue;
            transform.Translate(10 * Time.deltaTime * collision.contacts[i].normal);
            normals.Add(collision.contacts[i].normal);
            points.Add(collision.contacts[i].point);
        }
    }

    private void OnDrawGizmos()
    {
        for(int i = 0; i < points.Count; i++)
        {
            Gizmos.DrawLine(points[i], points[i] + normals[i]);
        }
    }
}
