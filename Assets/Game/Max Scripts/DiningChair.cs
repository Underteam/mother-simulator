﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiningChair : MonoBehaviour
{
    public bool sitting;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnSit ()
    {
        sitting = true;
    }

    public void OnStand ()
    {
        sitting = false;
    }
}
