﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChallengeCard : MonoBehaviour
{
    public Image knob;

    public Text number;

    public Sprite sootherIcon;
    public Sprite numberBG;
    public Sprite usualBG1;
    public Sprite usualBG2;
    public Sprite currentBG;
    public Sprite doneBG;
    public Sprite crystalsReward;

    public Image backGround;

    public Image imgReward;
    public Text rewardAmount;
    public Image doneIcon;

    public Animator anim;
    public GameObject btnTake;

    public enum State
    {
        Done,
        Current,
        Usual,
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Init (int ind, State state, ChallengeReward reward)
    {
        bool taked = PlayerPrefs.GetInt("ChallengeRewardTaked" + ind, 0) == 1;
        
        //Debug.LogError("Init " + ind + " " + state + " " + taked + " " + canBeTaked, this);

        rewardInd = ind;

        btnTake.SetActive(false);
        anim.enabled = false;

        if (state == State.Usual)
        {
            if (ind % 2 == 0)
            {
                backGround.sprite = usualBG2;
            }
            else
            {
                backGround.sprite = usualBG1;
            }

            knob.sprite = numberBG;
            number.gameObject.SetActive(true);
            number.text = "" + (ind);
            doneIcon.gameObject.SetActive(false);
            canBeTaked = false;
        }
        else if (state == State.Current)
        {
            backGround.sprite = currentBG;

            knob.sprite = sootherIcon;
            number.gameObject.SetActive(false);
            doneIcon.gameObject.SetActive(taked);
            canBeTaked = true;

            //Debug.LogError("Set anim1 " + (!taked), this);

            anim.enabled = !taked;
            if (!taked && ind > 0) anim.transform.localScale = Vector3.one;
            btnTake.SetActive(!taked);
        }
        else if (state == State.Done)
        {
            backGround.sprite = doneBG;
            if (ind % 2 == 0)
                backGround.color = Color.white;
            else
                backGround.color = new Color(1, 210f / 255f, 210f/255f);

            knob.sprite = sootherIcon;
            number.gameObject.SetActive(false);
            doneIcon.gameObject.SetActive(taked);
            canBeTaked = true;

            if (!taked && ind > 0)
            {
                anim.enabled = true;
                anim.transform.localScale = Vector3.one;
                btnTake.SetActive(true);
                
            }
        }

        SetReward(reward, ind);
    }

    private void SetReward (ChallengeReward reward, int ind)
    {
        this.reward = reward;
        Sprite sprite = reward.rewardIcon;

        rewardAmount.gameObject.SetActive(false);

        if (reward.item != null && Shop.Instance().IsItemOpened(reward.item))
        {
            bool wasOpened = PlayerPrefs.GetInt("RewardOpened" + ind, 0) == 2;
            if (wasOpened)
            {
                sprite = crystalsReward;
                rewardAmount.gameObject.SetActive(true);
            }
            else
                PlayerPrefs.SetInt("RewardOpened" + ind, 1);
        }

        int amount = reward.rewardAmount;

        imgReward.sprite = sprite;

        rewardAmount.text = "" + amount;
    }

    private ChallengeReward reward;
    private int rewardInd;
    private bool canBeTaked;
    public void Take ()
    {
        //Debug.LogError ("Take " + rewardInd);

        bool taked = PlayerPrefs.GetInt("ChallengeRewardTaked" + rewardInd, 0) == 1;

        if (!canBeTaked) return;

        doneIcon.gameObject.SetActive(true);

        anim.enabled = false;
        anim.transform.localScale = Vector3.one;
        btnTake.SetActive(false);

        PlayerPrefs.SetInt("ChallengeRewardTaked" + rewardInd, 1);

        if (reward == null) return;

        if (reward.item == null || !Shop.Instance().IsItemOpened(reward.item))
        {
            Shop.Instance().OpenItem(reward.item);
            Shop.Instance().ApplyItem(reward.item);
        }
        else
        {
            Wallet w = Wallet.GetWallet(Wallet.CurrencyType.crystals);
            w.Add(reward.rewardAmount);
            Challenges.Instance().CrystalsClaimed(imgReward.transform);
        }

        //for (int i = 0; reward.pack != null && i < reward.pack.items.Count; i++)
        //{
        //    Shop.Instance().OpenItem(reward.pack.items[i]);
        //    Shop.Instance().ApplyItem(reward.pack.items[i]);
        //}
    }
}
