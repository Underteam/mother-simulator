﻿using Facebook.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GMATools.Common;
using MoreMountains.NiceVibrations;

public class Challenges : MonoSingleton<Challenges>
{
    public GameObject panel;

    private int currentLevel;

    private float currentProgress;

    private Wallet keysBalance;

    public ChallengeCard cardPrefab;
    private List<ChallengeCard> challengesCards = new List<ChallengeCard>();
    public List<ChallengeReward> rewards;

    public Text lblCurrentChallengeNumber;

    public Image currentChallengeProgress;
    public Text lblCurrentProgress;
    public Text lblTimeLeft;

    public ChallengeProgress challengesProgress;

    public Text lblKeysBalance;

    public int maxFreeKeys = 10;

    public Sprite flags;

    private System.DateTime expirationTime;

    public double chDuration = 604800;

    protected override void Init()
    {
        keysBalance = Wallet.GetWallet(Wallet.CurrencyType.keys);

        int numChallenges = ChallengeController.MaxLevels();

        {
            var card = Instantiate(cardPrefab);
            card.transform.SetParent(cardPrefab.transform.parent);
            card.transform.localScale = cardPrefab.transform.localScale;

            card.gameObject.SetActive(true);
            challengesCards.Add(card);
        }

        for (int i = 0; i < numChallenges; i++)
        {
            var card = Instantiate(cardPrefab);
            card.transform.SetParent(cardPrefab.transform.parent);
            card.transform.localScale = cardPrefab.transform.localScale;

            card.gameObject.SetActive(true);
            challengesCards.Add(card);
        }

        Apply();

        inited = true;
    }

    private void Drop ()
    {
        PlayerPrefs.SetInt("ChallengeLevel", 0);
        PlayerPrefs.SetInt("TotalPoints", 0);

        double duration = chDuration;
        if (GameController.Instance().developerMode) duration = chDuration / 672;
        expirationTime = System.DateTime.UtcNow;
        expirationTime = expirationTime.AddSeconds(duration);

        PlayerPrefs.SetString("ChallengeExpirationTime", expirationTime.ToString());

        int numChallenges = ChallengeController.MaxLevels();
        for (int i = 0; i <= numChallenges; i++)
        {
            PlayerPrefs.SetInt("ChallengeRewardTaked" + i, 0);
            int ro = PlayerPrefs.GetInt("RewardOpened" + i, 0);
            if (ro > 0) PlayerPrefs.SetInt("RewardOpened" + i, 2);
        }

        Apply();
    }

    public void Apply ()
    {
        currentLevel = PlayerPrefs.GetInt("ChallengeLevel", 0);
        int prevLevelPoints = ChallengeController.GetLevelScore(currentLevel + 0);
        int nextLevelPoints = ChallengeController.GetLevelScore(currentLevel + 1);
        int totalPoints = PlayerPrefs.GetInt("TotalPoints", 0);

        currentProgress = (float)(totalPoints - prevLevelPoints) / (nextLevelPoints - prevLevelPoints);

        //Debug.LogError(currentLevel + " " + prevLevelPoints + " " + nextLevelPoints + " " + totalPoints + " " + currentProgress);

        for (int i = 0; i < challengesCards.Count; i++)
        {
            var card = challengesCards[i];

            ChallengeCard.State state = ChallengeCard.State.Usual;
            if (i == 0) state = ChallengeCard.State.Done;
            else if (i == currentLevel) state = ChallengeCard.State.Current;
            else if (i < currentLevel + 1) state = ChallengeCard.State.Done;

            ChallengeReward reward = null;
            if (i >= 0 && i < rewards.Count) reward = rewards[i];

            card.Init(i, state, reward);
        }

        challengesProgress.progress = (currentLevel + currentProgress + 0.5f) / challengesCards.Count;

        lblCurrentChallengeNumber.text = "" + (currentLevel + 1);
        lblCurrentProgress.text = (totalPoints - prevLevelPoints) + "/" + (nextLevelPoints - prevLevelPoints);
        currentChallengeProgress.fillAmount = currentProgress;
    }

    IEnumerator Start ()
    {
        while (!GameState.Instance().started) yield return null;

        expirationTime = System.DateTime.UtcNow;
        expirationTime = expirationTime.AddSeconds(-1);

        string str = PlayerPrefs.GetString("ChallengeExpirationTime", System.DateTime.UtcNow.ToString());

        System.DateTime.TryParse(str, out expirationTime);

        var span = expirationTime - System.DateTime.UtcNow;

        if (span.TotalSeconds <= 0) Drop();

        if (PlayerPrefs.GetInt("ShowChallengeMenu", 0) == 1)
        {
            string playerPosStr = JsonUtility.ToJson(PlayerController.instance.transform.position);
            playerPosStr = PlayerPrefs.GetString("PlayerPos", playerPosStr);

            string playerRotStr = JsonUtility.ToJson(PlayerController.instance.transform.rotation);
            playerRotStr = PlayerPrefs.GetString("PlayerRot", playerRotStr);

            PlayerController.instance.transform.position = JsonUtility.FromJson<Vector3>(playerPosStr);
            PlayerController.instance.transform.rotation = JsonUtility.FromJson<Quaternion>(playerRotStr);

            Open();

            PlayerPrefs.SetInt("ShowChallengeMenu", 0);
        }
    }

    public void Play ()
    {
        if (keysBalance.amount < 1) return;

        keysBalance.Spend(1);

        EventManager.Instance().TriggerEvent("taskChallenge");

        string playerPosStr = JsonUtility.ToJson(PlayerController.instance.transform.position);
        PlayerPrefs.SetString("PlayerPos", playerPosStr);

        string playerRotStr = JsonUtility.ToJson(PlayerController.instance.transform.rotation);
        PlayerPrefs.SetString("PlayerRot", playerRotStr);

        PlayerPrefs.SetInt("ShowChallengeMenu", 1);

        GameState.Instance().state = GameState.State.Challenge;

        GameController.Instance().LoadScene("Challenge");

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    public void Open ()
    {
        panel.gameObject.SetActive(true);

        Apply();

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    public void Close ()
    {
        panel.gameObject.SetActive(false);

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    private float timer;

    // Update is called once per frame
    void Update()
    {
        challengesProgress.progress = (currentLevel + currentProgress + 0.5f) / challengesCards.Count;

        currentChallengeProgress.fillAmount = Mathf.Clamp(currentProgress, 0f, 1f);

        timer -= Time.unscaledDeltaTime;
        if (timer <= 0)
        {
            timer = 1f;
            
            lblTimeLeft.text = Localizer.GetString("challengeTimer", "До конца периода") + " " + GetTimeLeft();
        }
    }

    private string GetTimeLeft ()
    {
        var span = expirationTime - System.DateTime.UtcNow;

        if (span.TotalSeconds <= 0)
        {
            Drop();
            span = expirationTime - System.DateTime.UtcNow;
        }

        span = span.Add(new System.TimeSpan(0, 0, 59));

        int d = span.Days;
        int h = span.Hours;
        int m = span.Minutes;
        int s = span.Seconds;

        string res = "";

        if (d >= 10) res += d + Localizer.GetString("challengeDays", "d") + " ";
        else if (d > 0) res += d + Localizer.GetString("challengeDays", "d") + " ";

        if (h >= 10) res += h + Localizer.GetString("challengeHours", "d") + " ";
        else res += "0" + h + Localizer.GetString("challengeHours", "d") + " ";

        if (m >= 10) res += m + Localizer.GetString("challengeMins", "d") + " ";
        else res += "0" + m + Localizer.GetString("challengeMins", "d") + " ";

        //if (s >= 10) res += s + "s ";
        //else res += "0" + s + "s ";

        return res;
    }

    private bool waitingForAdEnd;
    private int numCoins;
    private int num;
    private int sum;
    private float coinCost;
    private float accum;
    public void GetKeysForAd ()
    {
        waitingForAdEnd = true;
        Time.timeScale = 0f;
        AudioListener.volume = 0f;
        AdsController.Instance().ShowAd(AdsProvider.AdType.unskipablevideo, (b) =>
        {
            Time.timeScale = 1f;
            AudioListener.volume = 1f;

            if (!waitingForAdEnd) return;
            waitingForAdEnd = false;

            if (!b) return;

            Debug.LogError("Add key");
            keysBalance.Add(1);

            Analytics.Instance().SendEvent("KeysForAd", 0);

            Analytics.Instance().SendEvent("af_ads_success");
            Analytics.Instance().SendEvent("af_ads_success_rv");

            GameController.Instance().TrackAdsWatched();
        });

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    public void GetKeysForCrystals(int i)
    {
        var w = Wallet.GetWallet(Wallet.CurrencyType.crystals);

        int price = 2000;
        int reward = 5;

        if (i == 1)
        {
            price = 6000;
            reward = 20;
        }

        if (w.amount < price) return;

        keysBalance.Add(reward);
        w.Spend(price);

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    public FlyingCoin fcPrefab;
    public Transform fcTarget;
    public void CrystalsClaimed (Transform pos)
    {
        var prefab = Instantiate(fcPrefab);
        prefab.transform.position = pos.position;
        FlyingCoin.Run(prefab, 20, fcTarget, 0.05f, () =>
        {
            Destroy(prefab);
        });
    }
}
