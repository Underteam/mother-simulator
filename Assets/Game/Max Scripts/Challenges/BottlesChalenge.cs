﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BottlesChalenge : Challenge
{
    private List<BabyBottle> counted = new List<BabyBottle>();

    public Transform flyingCanvas;
    public Text flyingText;

    public GameObject tutorPrefab;

    private int mult = 1;

    public int score = 100;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Init()
    {
        if (PlayerPrefs.GetInt("VipAccessActive", 0) == 1) mult = 3;
        else mult = 1;

        counted = new List<BabyBottle>();

        var listener = EventManager.Instance().AddListener(OnBottleReady, "CountBottle");

        flyingCanvas = FindObjectOfType<ToCameraViewer>().transform;
        flyingText = flyingCanvas.GetComponentInChildren<Text>(true);

        //if (PlayerPrefs.GetInt("BottlesTutorShowed", 0) == 0)
        {
            var tutor = Instantiate(tutorPrefab);
            tutor.SetActive(true);
            Time.timeScale = 0f;
            PlayerPrefs.SetInt("BottlesTutorShowed", 1);
            var tracker = tutor.AddComponent<EnableTracker>();
            tracker.onDisable += () =>
            {
                Time.timeScale = 1f;
            };
        }
    }

    private void OnBottleReady (string name, object data)
    {
        if (!(data is BabyBottle bottle)) return;

        CountBottle(bottle);

        //Debug.LogError("Hit " + ChallengeController.Instance().score);
    }

    public void CountBottle (BabyBottle bottle)
    {
        if (bottle == null || !bottle.IsAppropriateForUse()) return;

        if (counted.Contains(bottle)) return;

        counted.Add(bottle);

        flyingText.text = "+" + (mult * score);
        flyingText.gameObject.SetActive(true);
        flyingCanvas.position = bottle.transform.position;

        ChallengeController.Instance().AddScore(score);
    }

    public override IEnumerator Job()
    {
        Init();

        while (true)
        {
            yield return null;
        }
    }

    public override void Done()
    {
        EventManager.Instance().DetachListener(OnBottleReady, "CountBottle");
    }
}
