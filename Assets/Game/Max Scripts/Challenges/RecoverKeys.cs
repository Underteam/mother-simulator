﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecoverKeys : MonoBehaviour
{
    public GameObject panel;

    public Text text1;
    public Text text2;

    private System.DateTime time;
    private float timer;

    public Text lblAmount;

    private Wallet wallet;

    public int numFree = 3;

    private bool recovering;

    public float recoveringPeriod = 7200;//seconds

    // Start is called before the first frame update
    void Start()
    {
        wallet = Wallet.GetWallet(Wallet.CurrencyType.keys);
        wallet.Init();
        wallet.onValueChanged += OnValueChanged;

        if (GameController.Instance().developerMode) recoveringPeriod = recoveringPeriod / 60;

        if (!PlayerPrefs.HasKey("RecoverKeysTime"))
        {
            
        }
        else
        {
            time = System.DateTime.UtcNow.AddSeconds(recoveringPeriod);

            string str = PlayerPrefs.GetString("RecoverKeysTime", time.ToString());

            System.DateTime.TryParse(str, out time);

            var span = time - System.DateTime.UtcNow;

            float seconds = (float)span.TotalSeconds;

            if (seconds <= 0)
            {
                while (seconds <= 0)
                {
                    Recover();
                    seconds += recoveringPeriod;
                }

                time = System.DateTime.UtcNow.AddSeconds(seconds);
                PlayerPrefs.SetString("RecoverKeysTime", time.ToString());
            }
        }

        if (wallet.amount < numFree)
        {
            float secs = 0;
            text2.text = GetTimeLeft(ref secs);
            recovering = true;
        }
        else
        {
            recovering = false;
            panel.SetActive(false);
        }

        lblAmount.text = "" + wallet.amount + "/" + numFree;
    }

    private void StartRecovering ()
    {
        panel.SetActive(true);

        time = System.DateTime.UtcNow.AddSeconds(recoveringPeriod);

        PlayerPrefs.SetString("RecoverKeysTime", time.ToString());

        float secs = 0;
        text2.text = GetTimeLeft(ref secs);

        recovering = true;
    }

    private void StopRecovering ()
    {
        PlayerPrefs.DeleteKey("RecoverKeysTime");
        panel.SetActive(false);
        recovering = false;
    }

    private void Recovering ()
    {
        float secs = 0;
        text2.text = GetTimeLeft(ref secs);

        if (secs <= 0)
        {
            Recover();

            time = System.DateTime.UtcNow.AddSeconds(recoveringPeriod);
            PlayerPrefs.SetString("RecoverKeysTime", time.ToString());
        }
    }

    private void Recover ()
    {
        int amount = wallet.amount;
        int add = 1;// numFree - amount;

        Debug.Log("Recover " + numFree + " " + amount + " " + add);

        if (amount >= numFree) return;

        if (add > 0) wallet.Add(add);
    }

    // Update is called once per frame
    void Update()
    {
        int amount = wallet.amount;
        if (!recovering && amount < numFree) StartRecovering();
        else if (recovering && amount >= numFree) StopRecovering();

        timer += Time.deltaTime;
        if (timer > 1)
        {
            if (recovering) Recovering();
            timer = 0;
        }
    }

    private void OnValueChanged(Wallet w)
    {
        //if (lblAmount == null)
        //{
        //    //Transform tr = transform;
        //    //while (tr != null)
        //    //{
        //    //    Debug.Log(tr.name);
        //    //    tr = tr.parent;
        //    //}
        //}
        //else
            lblAmount.text = "" + w.amount + "/" + numFree;
    }

    private void OnDestroy()
    {
        if (wallet == null) return;

        wallet.onValueChanged -= OnValueChanged;
    }

    private string GetTimeLeft(ref float secs)
    {
        var span = time - System.DateTime.UtcNow;

        secs = (float)span.TotalSeconds;

        if (span.TotalSeconds <= 0)
        {
            return "00" + Localizer.GetString("challengeHours", "h") + "00" + Localizer.GetString("challengeMins", "m");
        }

        span = span.Add(new System.TimeSpan(0, 0, 59));

        int d = span.Days;
        int h = span.Hours;
        int m = span.Minutes;
        int s = span.Seconds;

        string res = "";

        if (d >= 10) res += d + Localizer.GetString("challengeDays", "d") + " ";
        else if (d > 0) res += d + Localizer.GetString("challengeDays", "d") + " ";

        if (h >= 10) res += h + Localizer.GetString("challengeHours", "h") + " ";
        else res += "0" + h + Localizer.GetString("challengeHours", "h") + " ";

        if (d == 0)
        {
            if (m >= 10) res += m + Localizer.GetString("challengeMins", "m") + " ";
            else res += "0" + m + Localizer.GetString("challengeMins", "m") + " ";
        }

        //if (d == 0 && h == 0)
        //{
        //    if (s >= 10) res += s + "s ";
        //    else res += "0" + s + "s ";
        //}

        return res;
    }
}
