﻿using GMATools.Common;
using MoreMountains.NiceVibrations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Leaves : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        var vacuum = other.GetComponent<Vacuum>();
        if (vacuum != null && !vacuum.disabled)
        {
            Destroy(gameObject);
            SFXPlayer.Instance().Play(SFXLibrary.Instance().suck);
            Haptic.Instance().Play(HapticTypes.Warning);

            EventManager.Instance().TriggerEvent("LeavesSucked", this);
        }
    }
}
