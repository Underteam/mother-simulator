﻿using Facebook.Unity;
using MoreMountains.NiceVibrations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChallengeDoneMenu : MonoBehaviour
{
    public Text lblCurrentPoints;
    public Text lblBestPoints;

    public Text challengeLevel;
    public Image progress;

    public FlyingCoin flyingPointsPrefab;
    public FlyingCoin flyingAdsPointsPrefab;

    public RectTransform hidePos;
    public Image prevReward;
    public Image currReward;
    public Image nextReward;
    public Text prevRewardAmount;
    public Text currRewardAmount;
    public Text nextRewardAmount;
    private int currentLevel;

    public AnimationCurve hideCurve;

    public GameObject btnWatchAd;

    public List<ChallengeReward> rewards;

    public Sprite crystalsIcon;

    // Start is called before the first frame update
    void Start()
    {
        prevRewardAmount = prevReward.GetComponentInChildren<Text>(true);
        currRewardAmount = currReward.GetComponentInChildren<Text>(true);
        nextRewardAmount = nextReward.GetComponentInChildren<Text>(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        btnWatchAd.SetActive(true);
        //Show(1000);
    }

    private void ApplyRewardIcon (int ind, Image image, Text text)
    {
        //Debug.LogError("Set reward icon for " + ind, image);
        //Debug.LogError("Set reward text for " + ind, text);

        if (ind >= 0 && ind < rewards.Count)
        {
            if (PlayerPrefs.GetInt("RewardOpened" + ind, 0) == 2)
            {
                image.sprite = crystalsIcon;
                text.gameObject.SetActive(true);
                text.text = "" + rewards[ind].rewardAmount;
                //Debug.LogError("Reward opened " + rewards[ind].rewardAmount, rewards[ind]);
            }
            else
            {
                image.sprite = rewards[ind].rewardIcon;
                text.gameObject.SetActive(false);
                //Debug.LogError("Reward not opened");
            }
        }
        else if (rewards.Count > 0)
        {
            image.sprite = rewards[0].rewardIcon;
            text.gameObject.SetActive(false);
        }
    }

    private void ApplyRewardIcons ()
    {
        //Debug.LogError("Apply rewards icons " + currentLevel);

        ApplyRewardIcon(currentLevel + 0, prevReward, prevRewardAmount);

        ApplyRewardIcon(currentLevel + 1, currReward, currRewardAmount);

        ApplyRewardIcon(currentLevel + 2, nextReward, nextRewardAmount);
    }

    public void Show (int score, bool calcBest)
    {
        gameObject.SetActive(true);

        Debug.LogError("Show " + score + " " + spawnedFCs.Count);

        for (int i = 0; i < spawnedFCs.Count; i++)
        {
            if (spawnedFCs[i] == null) continue;
            Destroy(spawnedFCs[i].gameObject);
        }

        if (animateCoroutine != null) StopCoroutine(animateCoroutine);

        spawnedFCs.Clear();
        int type = ChallengeController.Instance().challengeType;
        //int score = 1000;// ChallengeController.Instance().score;
        int best = PlayerPrefs.GetInt("BestScore" + type, 0);

        if (calcBest && score > best)
        {
            best = score;
            PlayerPrefs.SetInt("BestScore" + type, best);
        }

        lblBestPoints.text = "" + best + " " + Localizer.GetString("challengePoints", "pts.");
        lblCurrentPoints.text = "" + score + " " + Localizer.GetString("challengePoints", "pts.");

        int level = PlayerPrefs.GetInt("ChallengeLevel", 0);//Текущий уровень (какую награду давать)
        Debug.LogError("Current level " + level);

        challengeLevel.text = "" + (level + 1);

        currentLevel = level;

        ApplyRewardIcons();

        int prevLevelPoints = ChallengeController.GetLevelScore(level + 0);
        int nextLevelPoints = ChallengeController.GetLevelScore(level + 1);

        int totalPoints = PlayerPrefs.GetInt("TotalPoints", 0);
        int startPoints = totalPoints;
        int startLevelPoints = prevLevelPoints;
        int startLevel = level;

        float v = (float)(totalPoints - prevLevelPoints) / (nextLevelPoints - prevLevelPoints);
        progress.fillAmount = v;

        List<int> listOfScore = new List<int>();
        List<int> levelsPoints = new List<int>();

        Debug.LogError("Start from: " + level + " " + totalPoints);

        int maxLevel = ChallengeController.MaxLevels() - 1;

        int addScore = score;
        int points = totalPoints;
        while (points + addScore >= nextLevelPoints)
        {
            if (level == maxLevel)
            {
                break;
            }

            level++;

            Debug.LogError("New level at " + nextLevelPoints);

            listOfScore.Add(nextLevelPoints - points);
            levelsPoints.Add(nextLevelPoints);

            addScore -= (nextLevelPoints - points);
            points = nextLevelPoints;
            prevLevelPoints = ChallengeController.GetLevelScore(level + 0);
            nextLevelPoints = ChallengeController.GetLevelScore(level + 1);
        }

        listOfScore.Add(addScore);
        levelsPoints.Add(nextLevelPoints);

        totalPoints += score;
        PlayerPrefs.SetInt("ChallengeLevel", level);
        PlayerPrefs.SetInt("TotalPoints", totalPoints);

        Debug.LogError("Set " + level + " " + totalPoints);

        if (score > 0) animateCoroutine = StartCoroutine(AnimateCoroutine(startLevel, startPoints, startLevelPoints, listOfScore, levelsPoints));
    }

    private Coroutine animateCoroutine;
    private IEnumerator AnimateCoroutine (int startLevel, int startPoints, int startLevelPoints, List<int> listOfScore, List<int> levelsPoints)
    {
        //Debug.LogError("Animate " + startLevel + " " + startPoints);

        yield return new WaitForSeconds(0.5f);

        FlyingCoin lastFC = null;

        int numCoins = 20;

        var fcPrefab = flyingPointsPrefab;

        int prevLevel = startLevelPoints;
        int points = startPoints;

        int totalReward = 0;
        for (int i = 0; i < listOfScore.Count; i++)
        {
            totalReward += listOfScore[i];
        }

        for (int i = 0; i < listOfScore.Count; i++)
        {
            challengeLevel.text = "" + (startLevel + 1);

            float v = (float)(points - prevLevel) / (levelsPoints[i] - prevLevel);
            progress.fillAmount = v;

            int sum = listOfScore[i];
            totalReward -= sum;
            float coinCost = (float)sum / numCoins;
            float accum = 0;

            //Debug.LogError("Animate " + points + " " + prevLevel + " " + levelsPoints[i] + " " + sum);

            for (int j = 0; j < numCoins; j++)
            {
                var coin = Instantiate(fcPrefab);
                coin.transform.SetParent(fcPrefab.transform.parent);
                coin.transform.position = fcPrefab.transform.position;
                coin.transform.localScale = fcPrefab.transform.localScale;
                coin.transform.SetParent(fcPrefab.transform.parent.parent);
                coin.gameObject.SetActive(true);
                lastFC = coin;
                spawnedFCs.Add(coin);

                accum += coinCost;
                int add = (int)accum;
                accum -= add;
                if (j == numCoins - 1) add = sum;
                sum -= add;

                float duration = 0.05f;
                float t = duration;

                while (t > 0)
                {
                    //if (breakAnimations)
                    //{
                    //    Debug.LogError("???");
                    //    yield break;
                    //}

                    t -= Time.deltaTime;

                    int a = (int)Mathf.Lerp(0, add, 1 - t / duration);

                    int s = totalReward + sum + add - a;//rest score to display
                    lblCurrentPoints.text = "" + s + " " + Localizer.GetString("challengePoints", "pts.");

                    v = (float)(points + add - a - prevLevel) / (levelsPoints[i] - prevLevel);

                    progress.fillAmount = Mathf.Max(v, progress.fillAmount);

                    yield return null;
                }

                points += add;
            }

            lblCurrentPoints.text = (totalReward - listOfScore[i]) + " " + Localizer.GetString("challengePoints", "pts.");

            startLevel++;

            prevLevel = levelsPoints[i];

            if (points >= levelsPoints[i])
            {
                //Debug.Log("Change reward " + i + " " + listOfScore.Count + " " + listOfScore[i] + " " + levelsPoints[i] + " " + points);
                StartCoroutine(ChangeRewards());
            }
        }

        lblCurrentPoints.text = "0 " + Localizer.GetString("challengePoints", "pts.");

        while (lastFC != null && lastFC.gameObject.activeSelf) yield return null;

        //Debug.LogError("Done animate");
    }

    private bool changingRewards;

    private IEnumerator ChangeRewards ()
    {
        //Debug.LogError("Change " + changingRewards);

        while (changingRewards) yield return null;

        changingRewards = true;

        float t = 0;
        float speed = 1.5f;

        var nextReward = Instantiate(this.nextReward);
        nextReward.transform.SetParent(this.nextReward.transform.parent);
        nextReward.transform.position = this.nextReward.transform.position;
        nextReward.transform.localScale = this.nextReward.transform.localScale;
        nextReward.gameObject.SetActive(true);

        Vector3 prevRewardPos = prevReward.transform.position;
        Vector3 currRewardPos = currReward.transform.position;
        Vector3 nextRewardPos = nextReward.transform.position;

        while (!breakAnimations && t < 1)
        {
            t += speed * Time.deltaTime;

            float a = hideCurve.Evaluate(t);

            prevReward.transform.position = Vector3.Lerp(prevRewardPos, hidePos.position, a);
            currReward.transform.position = Vector3.Lerp(currRewardPos, prevRewardPos, a);
            nextReward.transform.position = Vector3.Lerp(nextRewardPos, currRewardPos, a);

            yield return null;
        }

        //Debug.LogError("Done change " + breakAnimations);
        breakAnimations = false;

        Destroy(prevReward.gameObject);
        prevReward = currReward;
        currReward = nextReward;
        prevRewardAmount = prevReward.GetComponentInChildren<Text>(true);
        currRewardAmount = currReward.GetComponentInChildren<Text>(true);

        prevReward.transform.position = prevRewardPos;
        currReward.transform.position = currRewardPos;

        currentLevel++;

        ApplyRewardIcons();

        changingRewards = false;
    }

    private List<FlyingCoin> spawnedFCs = new List<FlyingCoin>();
    private bool breakAnimations;

    private bool waitingForAdEnd;
    public void AddPoints ()
    {
        Debug.Log("Add points " + waitingForAdEnd);

        waitingForAdEnd = true;
        Time.timeScale = 0f;
        AudioListener.volume = 0f;

        AdsController.Instance().ShowAd(AdsProvider.AdType.unskipablevideo, (b) =>
        {
            Time.timeScale = 1f;
            AudioListener.volume = 1f;

            if (!waitingForAdEnd) return;
            waitingForAdEnd = false;

            if (!b) return;

            Analytics.Instance().SendEvent("AddPoints", 0, "Points", PlayerPrefs.GetInt("TotalPoints", 0));

            Analytics.Instance().SendEvent("af_ads_success");
            Analytics.Instance().SendEvent("af_ads_success_rv");

            breakAnimations = changingRewards;

            btnWatchAd.SetActive(false);

            Show (1000, false);

            //UnityEditor.EditorApplication.isPaused = true;
        });

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    private void TrackAdsWatched()
    {
        int aw = PlayerPrefs.GetInt("AdsWatched", 0);
        aw++;
        PlayerPrefs.SetInt("AdsWatched", aw);
        if (aw == 10 || aw == 20 || aw == 30 || aw == 40 || aw == 50 || aw == 75 || aw == 100)
        {
            Analytics.Instance().SendEvent("ads_watched_" + aw + "_times");
        }
    }
}
