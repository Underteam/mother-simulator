﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GMATools.Common;

public class DiapersChallenge : Challenge
{
    private List<DiaperUsed> counted = new List<DiaperUsed>();

    public GameObject tutorPrefab;

    private int mult = 1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Init ()
    {
        counted = new List<DiaperUsed>();

        EventManager.Instance().AddListener(OnPampersInTrashbin, "PampersInTrashbin");

        if (PlayerPrefs.GetInt("VipAccessActive", 0) == 1) mult = 3;
        else mult = 1;

        //if (PlayerPrefs.GetInt("DiapersTutorShowed", 0) == 0)
        {
            var tutor = Instantiate(tutorPrefab);
            tutor.SetActive(true);
            Time.timeScale = 0f;
            PlayerPrefs.SetInt("DiapersTutorShowed", 1);
            var tracker = tutor.AddComponent<EnableTracker>();
            tracker.onDisable += () =>
            {
                Time.timeScale = 1f;
            };
        }
    }

    private void OnPampersInTrashbin (string name, object data)
    {
        if (!(data is object[] list)) return;

        if (list.Length < 2) return;

        if (!(list[0] is DiaperUsed diaper)) return;

        if (counted.Contains(diaper)) return;

        if (!(list[1] is TrashBin bin)) return;

        ChallengeBin chBin = bin.GetComponent<ChallengeBin>();

        if (chBin == null) return;

        counted.Add(diaper);

        ChallengeController.Instance().AddScore(chBin.score);
        chBin.bin.ShowText("+" + (mult * chBin.score), true);

        //Debug.LogError("Hit " + ChallengeController.Instance().score);
    }

    public override IEnumerator Job()
    {
        Init();

        while (true)
        {
            yield return null;
        }
    }

    public override void Done()
    {
        EventManager.Instance().DetachListener(OnPampersInTrashbin, "PampersInTrashbin");
    }
}
