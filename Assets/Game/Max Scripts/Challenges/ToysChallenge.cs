﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToysChallenge : Challenge
{
    public ToyWish toyWish;

    public Transform flyingCanvas;
    public Text flyingText;

    private Item baby;

    public GameObject tutorPrefab;

    private int mult = 1;

    public int score = 100;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Init()
    {
        Debug.LogError("Init toys challenge");

        if (PlayerPrefs.GetInt("VipAccessActive", 0) == 1) mult = 3;
        else mult = 1;

        //if (PlayerPrefs.GetInt("ToysTutorShowed", 0) == 0)
        {
            var tutor = Instantiate(tutorPrefab);
            tutor.SetActive(true);
            Debug.LogError("Instantiate tutor", tutor);
            Time.timeScale = 0f;
            PlayerPrefs.SetInt("ToysTutorShowed", 1);
            var tracker = tutor.AddComponent<EnableTracker>();
            tracker.onDisable += () =>
            {
                Time.timeScale = 1f;
            };
        }

        flyingCanvas = FindObjectOfType<ToCameraViewer>().transform;
        flyingText = flyingCanvas.GetComponentInChildren<Text>(true);

        var list = new List<Item>(FindObjectsOfType<Item>());
        List<Item> toys = new List<Item>();

        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].GetComponent<Baby>() != null)
            {
                list.RemoveAt(i);
                i--;
                continue;
            }

            if (list[i].GetComponent<Toy>() != null)
            {
                toys.Add(list[i]);
                list.RemoveAt(i);
                i--;
                continue;
            }

            if (list[i].transform.parent != null && list[i].transform.parent.parent != null)
            {
                list.RemoveAt(i);
                i--;
                continue;
            }
        }

        for (int i = 0; i < toys.Count; i++)
        {
            if (Random.Range(0f, 1f) < 0.3f) continue;
            var pos = toys[i].transform.position;
            int ind = Random.Range(0, list.Count);
            toys[i].transform.position = list[ind].transform.position;
            list[ind].transform.position = pos;
            list.RemoveAt(ind);
        }

        EventManager.Instance().AddListener(OnBabyEvent, "BabyEvent");

        PickNextWish();

        baby = FindObjectOfType<Baby>().GetComponent<Item>();
        baby.busy = true;
    }

    public override void Done()
    {
        EventManager.Instance().DetachListener(OnBabyEvent, "BabyEvent");
    }

    private void OnBabyEvent(string name, object data)
    {
        if (gameObject == null) return;

        if (toyWish.done) return;

        bool res = toyWish.Check(name, data);

        if (toyWish != null && !res)
        {
            EventManager.Instance().TriggerEvent("WishUnSatisfied", data);
        }
        else
        {
            EventManager.Instance().TriggerEvent("WishSatisfied", data);

            flyingText.text = "+" + (mult * score);
            flyingText.gameObject.SetActive(true);
            flyingCanvas.position = baby.transform.position + 0.75f * baby.transform.forward;

            ChallengeController.Instance().AddScore(score);

            toyWish.done = true;

            Invoke("PickNextWish", 1.2f);
        }
    }

    private Toy prevToy;
    private void PickNextWish()
    {
        toyWish.Init();

        int cnt = 0;
        while (toyWish.toy == prevToy && cnt++ < 1000) toyWish.Init();
        prevToy = toyWish.toy;
        toyWish.done = false;

        EventManager.Instance().TriggerEvent("NewBabyWish", toyWish);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override IEnumerator Job()
    {
        Init();

        while (true)
        {
            yield return null;
        }
    }
}
