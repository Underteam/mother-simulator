﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxCloser : MonoBehaviour
{
    private List<BabyBottle> bottles = new List<BabyBottle>();

    public Transform doorLeft;
    public Transform doorRight;

    private float timer;

    private Quaternion closedLeft = Quaternion.Euler(0, 0, 90);
    private Quaternion closedRight = Quaternion.Euler(0, 0, -90);

    private Quaternion openLeft = Quaternion.Euler(0, 0, -145);
    private Quaternion openRight = Quaternion.Euler(0, 0, 145);

    public BoxUncloser uncloser;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (bottles.Count >= 4 && uncloser.bottles.Count == 0)
        {
            if (timer < 0.5f) timer += Time.deltaTime;
            else
            {
                doorLeft.localRotation = Quaternion.Lerp(doorLeft.localRotation, closedLeft, 5 * Time.deltaTime);
                doorRight.localRotation = Quaternion.Lerp(doorRight.localRotation, closedRight, 5 * Time.deltaTime);
            }
        }
        else
        {
            doorLeft.localRotation = Quaternion.Lerp(doorLeft.localRotation, openLeft, 5 * Time.deltaTime);
            doorRight.localRotation = Quaternion.Lerp(doorRight.localRotation, openRight, 5 * Time.deltaTime);

            timer = 0f;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        BabyBottle bottle = other.GetComponent<BabyBottle>();
        if (bottle != null)
        {
            if (!bottles.Contains(bottle)) bottles.Add(bottle);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        BabyBottle bottle = other.GetComponent<BabyBottle>();
        if (bottle != null)
        {
            if (bottles.Contains(bottle)) bottles.Remove(bottle);
        }
    }
}
