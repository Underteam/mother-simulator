﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeavesSpawner : MonoBehaviour
{
    public Vector2 size = new Vector2(10, 10);

    public GameObject trashPrefab;
    public GameObject leavesPrefab;

    public float cellSize = 2;
    public float shiftFactor = 1f;
    public float checkRadius = 0.5f;

    private List<GameObject> spawned = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [Sirenix.OdinInspector.Button]
    public void Spawn ()
    {
        Spawn(10, 50);
    }

    public void Spawn (int numTrash, int numLeaves)
    {
        for (int i = 0; i < spawned.Count; i++) DestroyImmediate(spawned[i]);
        spawned.Clear();

        List<Vector3> cells = new List<Vector3>();

        int n = (int)(size.x / cellSize);
        int m = (int)(size.y / cellSize);

        float x0 = (size.x - n * cellSize) / 2 + cellSize / 2 + (transform.position - transform.forward * size.x / 2).z;
        float y0 = (size.y - m * cellSize) / 2 + cellSize / 2 + (transform.position - transform.right * size.y / 2).x;

        for (int i = 0; i < n; i++)
        {
            float x = x0 + i * cellSize;

            for (int j = 0; j < m; j++)
            {
                float y = y0 + j * cellSize;

                Vector3 v = new Vector3(y, 0, x);
                Vector2 shift = shiftFactor * Random.insideUnitCircle;
                v.x += shift.x;
                v.z += shift.y;
                cells.Add(v);
            }
        }

        for (int i = 0; i < numTrash && cells.Count > 0; i++)
        {
            int ind = Random.Range(0, cells.Count);
            Vector3 v = cells[ind];
            cells.RemoveAt(ind);

            bool appropriate = true;
            var list = Physics.SphereCastAll(v, checkRadius, Vector3.up, 0.01f);
            for (int j = 0; j < list.Length; j++)
            {
                if(!list[j].transform.CompareTag("Floor"))
                {
                    appropriate = false;
                    break;
                }
            }

            if (!appropriate)
            {
                i--;
                continue;
            }

            var trash = Instantiate(trashPrefab);
            spawned.Add(trash);

            trash.transform.position = v;
            //transform.transform.rotation = 
            trash.SetActive(true);
        }

        for (int i = 0; i < numLeaves && cells.Count > 0; i++)
        {
            int ind = Random.Range(0, cells.Count);
            Vector3 v = cells[ind];
            cells.RemoveAt(ind);

            bool appropriate = true;
            var list = Physics.SphereCastAll(v, checkRadius, Vector3.up, 0.01f);
            for (int j = 0; j < list.Length; j++)
            {
                if (!list[j].transform.CompareTag("Floor"))
                {
                    appropriate = false;
                    break;
                }
            }

            if (!appropriate)
            {
                i--;
                continue;
            }

            var leaves = Instantiate(leavesPrefab);
            spawned.Add(leaves);

            leaves.transform.position = v;
            //transform.transform.rotation = 
            leaves.SetActive(true);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        Vector3 p1 = transform.position + transform.forward * size.x / 2 + transform.right * size.y / 2;
        Vector3 p2 = transform.position - transform.forward * size.x / 2 + transform.right * size.y / 2;
        Vector3 p3 = transform.position - transform.forward * size.x / 2 - transform.right * size.y / 2;
        Vector3 p4 = transform.position + transform.forward * size.x / 2 - transform.right * size.y / 2;

        Gizmos.DrawLine(p1, p2);
        Gizmos.DrawLine(p2, p3);
        Gizmos.DrawLine(p3, p4);
        Gizmos.DrawLine(p4, p1);
    }
}
