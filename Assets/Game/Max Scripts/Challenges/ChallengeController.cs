﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using MoreMountains.NiceVibrations;

public class ChallengeController : MonoSingleton<ChallengeController>
{
    public List<Challenge> challenges;

    public Text lblScore;

    private int _score;
    private int score
    {
        get { return _score; }
        set
        {
            _score = value;

            lblScore.text = "" + _score;
        }
    }

    public void AddScore (int s)
    {
        if (PlayerPrefs.GetInt("VipAccessActive", 0) == 1) s *= 3;

        score += s;
    }

    public Text lblTime;

    public float timer = 300;

    private bool running;

    public int challengeType { get; private set; }

    public ChallengeDoneMenu doneMenu;

    public GameObject controlsCanvas;
    public GameObject gameplayMenu;

    public AudioClip music;

    private Challenge challenge;

    protected override void Init()
    {
        if (MainTheme.instance != null) MainTheme.instance.Play(music);

        score = 0;

        int day = GameState.Instance().currentTask + 1;

        List<Challenge> list = new List<Challenge>();

        for (int i = 0; i < challenges.Count; i++)
        {
            //Debug.LogError(challenges[i].startDay + " " + day);
            if (challenges[i].startDay > day) continue;
            list.Add(challenges[i]);
        }

        {
            int ind = PlayerPrefs.GetInt("PrevChallenge", -1);
            if (ind >= 0 && ind < challenges.Count && list.Contains(challenges[ind]) && list.Count > 1)
            {
                list.Remove(challenges[ind]);
            }
        }

        //Debug.LogError(list.Count);
        if (list.Count > 0)
        {
            int ind = Random.Range(0, list.Count);

            challenge = list[ind];// Instantiate(challenges[ind]);

            if (challenge.duration > 0) timer = challenge.duration;

            ind = challenges.IndexOf(challenge);
            PlayerPrefs.SetInt("PrevChallenge", ind);

            for (int i = 0; i < challenge.toInstantiate.Count; i++)
            {
                Instantiate(challenge.toInstantiate[i]);
            }

            StartCoroutine(challenge.Job());
        }

        running = true;

        base.Init();
    }

    private void OnDestroy()
    {
        if(challenge != null) challenge.Done();    
    }

    public LoadingMenu loadingMenu;

    public GameObject pauseMenu;
    public void Pause (bool state)
    {
        pauseMenu.SetActive(state);

        Debug.LogError("Pause " + state);

        if (state) Time.timeScale = 0f;
        else Time.timeScale = 1f;

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    // Update is called once per frame
    void Update()
    {
        if (!running) return;

        timer -= Time.deltaTime;

        lblTime.text = GetTimeStr (timer + 0.5f);

        if (timer <= 0) Finish();
    }

    private void Finish ()
    {
        running = false;

        var player = PlayerController.instance.GetComponent<FPSCharacterController>();
        if (player != null)
        {
            player.disableMovement = true;
            player.disableRotation = true;
        }

        controlsCanvas.SetActive(false);
        gameplayMenu.SetActive(false);

        doneMenu.Show(score, true);
    }

    public void Exit ()
    {
        Time.timeScale = 1f;

        GameState.Instance().state = GameState.State.Menu;

        loadingMenu.Load ("Main");

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    private string GetTimeStr (float time)
    {
        int sec = (int)time % 60;
        time = (time - sec) / 60;
        int min = (int)time % 60;
        time = (time - min) / 60;
        int hrs = (int)time;

        string res = "";
        if (hrs > 0) res += hrs + ":";

        if (min >= 10) res += min + ":";
        else res += "0" + min + ":";

        if (sec >= 10) res += sec;
        else res += "0" + sec;

        return res;
    }

    private static List<int> scoreProgression = new List<int>() { 1500, 2500, 4000, 6000, 8500, 10000, 12500, 15000 };
    private static int constScore = 15000;

    public static int GetLevelScore (int level)
    {
        int score = 0;
        for (int i = 0; i < level; i++)
        {
            if (i < scoreProgression.Count) score += scoreProgression[i];
            else score += constScore;
        }

        return score;

        //return (level) * (level + 1) * 200;
    }

    public static int MaxLevels ()
    {
        return 16;
    }
}
