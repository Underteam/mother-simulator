﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeavesChallenge : Challenge
{
    private LeavesSpawner spawner;

    public Transform flyingCanvas;
    private List<Text> flyingTexts = new List<Text>();

    private Item vacuum;

    public GameObject tutorPrefab;

    private int mult = 1;

    public int score = 10;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Init()
    {
        if (PlayerPrefs.GetInt("VipAccessActive", 0) == 1) mult = 3;
        else mult = 1;

        //if (PlayerPrefs.GetInt("LeavesTutorShowed", 0) == 0)
        {
            var tutor = Instantiate(tutorPrefab);
            tutor.SetActive(true);
            Time.timeScale = 0f;
            PlayerPrefs.SetInt("LeavesTutorShowed", 1);
            var tracker = tutor.AddComponent<EnableTracker>();
            tracker.onDisable += () =>
            {
                Time.timeScale = 1f;
            };
        }

        flyingCanvas = FindObjectOfType<ToCameraViewer>().transform;

        flyingTexts.Clear();
        for (int i = 0; i < 3; i++)
        {
            var canvas = Instantiate(flyingCanvas);
            var flyingText = canvas.GetComponentInChildren<Text>(true);
            flyingTexts.Add(flyingText);
        }
        

        spawner = FindObjectOfType<LeavesSpawner>();
        spawner.Spawn();

        vacuum = FindObjectOfType<Vacuum>().GetComponent<Item>();
        var player = FindObjectOfType<PlayerController>();
        player.Take(vacuum);

        Groups.Instance().SetGroupState("Button Put", false);

        EventManager.Instance().AddListener(LeavesSucked, "LeavesSucked");
    }

    private void ShowText (string text, Vector3 pos)
    {
        for (int i = 0; i < flyingTexts.Count; i++)
        {
            if (!flyingTexts[i].gameObject.activeSelf)
            {
                var flyingText = flyingTexts[i];
                var canvas = flyingText.transform.parent;

                canvas.position = pos;
                flyingText.text = text;
                flyingText.gameObject.SetActive(true);

                return;
            }
        }

        {
            var canvas = Instantiate(flyingCanvas);
            var flyingText = canvas.GetComponentInChildren<Text>(true);
            flyingTexts.Add(flyingText);

            canvas.position = pos;
            flyingText.text = text;
            flyingText.gameObject.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override IEnumerator Job()
    {
        Init();

        while (true)
        {
            yield return null;
        }
    }

    public void LeavesSucked (string eventName, object data)
    {
        var leaves = data as Leaves;

        ShowText("+" + (mult * score), leaves.transform.position + 2 * Vector3.up + vacuum.transform.forward);
        ChallengeController.Instance().AddScore(score);
    }

    public override void Done()
    {
        EventManager.Instance().DetachListener(LeavesSucked, "LeavesSucked");
    }
}
