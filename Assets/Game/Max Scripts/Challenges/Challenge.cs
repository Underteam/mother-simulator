﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Challenge : MonoBehaviour
{
    public List<GameObject> toInstantiate;

    public float duration = 60;

    public int startDay;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual IEnumerator Job ()
    {
        yield return null;
    }
    public virtual void Done ()
    {

    }
}
