﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxUncloser : MonoBehaviour
{
    public List<BabyBottle> bottles { get; set; } = new List<BabyBottle>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        BabyBottle bottle = other.GetComponent<BabyBottle>();
        if (bottle != null)
        {
            if (!bottles.Contains(bottle)) bottles.Add(bottle);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        BabyBottle bottle = other.GetComponent<BabyBottle>();
        if (bottle != null)
        {
            if (bottles.Contains(bottle)) bottles.Remove(bottle);
        }
    }
}
