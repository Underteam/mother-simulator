﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICopyPaster
{
}

[System.Serializable]
public class CopyPaster
{
    public System.Action onCopy;
    public System.Action onPaste;
    public System.Action onLCopy;
    public System.Action onLPaste;
    public System.Action onUndo;

    public CopyPaster(System.Action onCopy, System.Action onPaste, System.Action onLCopy, System.Action onLPaste, System.Action onUndo)
    {
        this.onCopy = onCopy;
        this.onPaste = onPaste;
        this.onLCopy = onLCopy;
        this.onLPaste = onLPaste;
        this.onUndo = onUndo;
    }

    public CopyPaster(ICopyPaster copypaster)
    {

    }

    public void Copy ()
    {
        if (onCopy != null) onCopy();
    }

    public void Paste()
    {
        if (onPaste != null) onPaste();
    }

    public void LCopy()
    {
        if (onLCopy != null) onLCopy();
    }

    public void LPaste()
    {
        if (onLPaste != null) onLPaste();
    }

    public void Undo()
    {
        if (onUndo != null) onUndo();
    }
}
