﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DVDDisc : MonoBehaviour, IItemNameProvider
{
    public string discName;

    public Sprite icon;

    public List<Sprite> cartoon;

    public int cartonID;

    // Start is called before the first frame update
    void Start()
    {
        if (GameState.Instance().state == GameState.State.Game) ShuffleDiscs();
    }

    public string GetItemName ()
    {
        return Localizer.GetString("cartoon" + cartonID, "Cartoon" + cartonID);
    }

    [Sirenix.OdinInspector.Button]
    public void Shuffle ()
    {
        shufled = false;
        ShuffleDiscs();
    }

    private static bool shufled;
    private static void ShuffleDiscs ()
    {
        if (shufled) return;
        shufled = true;

        List<DVDDisc> list = new List<DVDDisc>(FindObjectsOfType<DVDDisc>());

        List<Vector3> positions = new List<Vector3>();
        List<Quaternion> rotations = new List<Quaternion>();

        for (int i = 0; i < list.Count; i++)
        {
            positions.Add(list[i].transform.position);
            rotations.Add(list[i].transform.rotation);
        }

        while (list.Count > 0)
        {
            int ind1 = Random.Range(0, list.Count);
            int ind2 = Random.Range(0, positions.Count);

            list[ind1].transform.position = positions[ind2] + 0.05f * Vector3.up;
            list[ind1].transform.rotation = rotations[ind2];

            list.RemoveAt(ind1);
            positions.RemoveAt(ind2);
            rotations.RemoveAt(ind2);
        }
    }
}
