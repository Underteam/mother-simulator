﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Furs : MonoBehaviour
{
    public List<GameObject> furs;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < furs.Count; i++) if (furs[i] != null) return;

        Destroy(gameObject);
    }
}
