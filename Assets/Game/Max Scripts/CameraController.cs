﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target;

    public Vector3 shift;

    public Quaternion rotShift;

    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(Job());
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (target == null) return;
        transform.position = target.position + shift;
        transform.rotation = target.rotation;
    }

    //IEnumerator Job ()
    //{
    //    while(true)
    //    {
    //        while (target == null) yield return null;

    //        yield return new WaitForEndOfFrame();

    //        transform.position = target.position;
    //        transform.rotation = target.rotation;
    //    }
    //}
}
