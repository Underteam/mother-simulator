﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loofah : MonoBehaviour
{
    public AnimationClip clip;

    private bool use;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Job());
    }

    // Update is called once per frame
    private IEnumerator Job()
    {
        while (true)
        {
            if (use)
            {
                //UnityEditor.EditorApplication.isPaused = true;

                yield return new WaitForEndOfFrame();

                //var particles = Instantiate(powderParticles);
                //particles.transform.position = powderParticles.transform.position;

                //particles.SetActive(true);

                //SFXPlayer.Instance().Play(SFXLibrary.instance.powder);

                use = false;
            }

            yield return null;
        }
    }

    public void Use()
    {
        Invoke("DoUse", 0.45f);
    }

    private void DoUse()
    {
        use = true;
    }
}
