﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogTasksController : MonoBehaviour
{
    public List<Transform> waypoints1;

    public List<Transform> waypoints2;

    public List<Transform> waypoints3;
    private List<Transform> waypoints3Reversed = new List<Transform>();

    public Dog dog;

    private int prevBehaviour = 0;

    public OpenDoorTrigger doorTrigger;

    public int shuffleFromDay = 36;

    // Start is called before the first frame update
    void Start()
    {
        if (GameState.Instance().currentTask + 1 < shuffleFromDay) return;

        for (int i = waypoints3.Count - 1; i >= 0; i--) waypoints3Reversed.Add(waypoints3[i]);

        for(int i = 0; i < GameController.Instance().doorsSettings.Count; i++)
        {
            if (GameController.Instance().doorsSettings[i].name.Equals("LivingRoom"))
                doorTrigger.door = GameController.Instance().doorsSettings[i].doors[0].GetComponentInParent<InteractableDoor>();
        }

        dog.onWishSatisfied += () =>
        {
            prevBehaviour = dog.behaviourType;

            if (dog.behaviourType == 1)
            {
                dog.waypoints = waypoints3;
                dog.SetBehaviourType(3);
            }
            else if(dog.behaviourType == 2)
            {
                dog.waypoints = waypoints3Reversed;
                dog.SetBehaviourType(3);
            }

        };

        dog.onReached += (wp) =>
        {
            if (prevBehaviour == 1 && wp == waypoints3[0])
            {
                dog.waypoints = waypoints2;
                dog.SetBehaviourType(2);
            }
            else if (prevBehaviour == 2 && wp == waypoints3[2])
            {
                dog.waypoints = waypoints1;
                dog.SetBehaviourType(1);
            }
        };
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
