﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;

public class Haptic : MonoSingleton<Haptic>
{
    private bool supported;

    private bool active;

    protected override void Init()
    {
        base.Init();

        MMVibrationManager.iOSInitializeHaptics();

        active = PlayerPrefs.GetInt("HapticEnabled", 1) == 1;

        supported = MMVibrationManager.HapticsSupported() || MMVibrationManager.Android();

        //Debug.LogError("Haptics " + active + " " + supported);
    }

    public void Play (HapticTypes type)
    {
        //Debug.LogError("Play " + type + " " + active + " " + supported);

        if (!supported || !active) return;

        MMVibrationManager.Haptic(type);

        //MMVibrationManager
    }

    protected virtual void OnDisable()
    {
        MMVibrationManager.iOSReleaseHaptics();
    }

    public void Switch ()
    {
        active = !active;
        PlayerPrefs.SetInt("HapticEnabled", active ? 1: 0);
    }

    public void SetState (bool state)
    {
        active = state;
        PlayerPrefs.SetInt("HapticEnabled", active ? 1 : 0);
    }
}
