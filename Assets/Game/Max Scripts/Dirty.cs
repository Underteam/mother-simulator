﻿using MoreMountains.NiceVibrations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dirty : MonoBehaviour
{
    private Collider vacuum;

    private float timer = 0.25f;

    public AudioClip clip;

    // Start is called before the first frame update
    void Start()
    {
        SFXPlayer.Instance().Play(clip, false, true);
    }

    // Update is called once per frame
    void Update()
    {
        if (vacuum != null) timer -= Time.deltaTime;

        if (timer <= 0)
        {
            Destroy(gameObject);
            SFXPlayer.Instance().Play(SFXLibrary.Instance().suck);
            Haptic.Instance().Play(HapticTypes.Warning);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Vacuum>() != null) vacuum = other;

        //bug.Log("Enter " + other + " " + (vacuum != null), other);
    }

    private void OnTriggerExit (Collider other)
    {
        //bug.Log("Enter " + other + " " + (other == vacuum), other);
        if (other == vacuum) vacuum = null;
    }
}
