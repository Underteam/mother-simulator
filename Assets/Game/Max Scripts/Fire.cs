﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    public Transform fire;

    public float scale { get; set; } = 1f;

    public List<Wood> firewoods;

    private float timer;

    public AudioSource sound;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (firewoods.Count == 0)
        {
            if (fineTime > 0)
            {
                scale = 0.1f;
                fire.localScale = scale * Vector3.one;
                fineTime = 0;
            }

            if (poker != null && scale < 1f)
            {
                Vector3 pos = poker.transform.position;
                if (Vector3.Distance(pos, prevPos) > 0.05)
                {
                    scale += Time.deltaTime / 3f;
                    if (scale > 1f) scale = 1f;
                    fire.localScale = scale * Vector3.one;
                }
            }
            else if (scale > 0.1f)
            {
                scale -= Time.deltaTime / 20f;
                if (scale < 0.1f) scale = 0.1f;

                fire.localScale = scale * Vector3.one;
            }
        }
        else
        {
            scale = 1f;
            fire.localScale = scale * Vector3.one;

            if (timer <= 0) timer = 20f;
            else
            {
                if (fineTime > 0)
                {
                    if (fineTime < timer)
                    {
                        timer -= fineTime;
                        fineTime = 0;
                    }
                    else
                    {
                        fineTime -= timer;
                        timer = 0;
                    }
                }
                else
                    timer -= Time.deltaTime;

                if (timer <= 0)
                {
                    Destroy(firewoods[0].gameObject);
                    firewoods.RemoveAt(0);
                }
            }
        }

        sound.volume = Mathf.Lerp(0.5f, 1f, scale);
    }

    private Poker poker;
    private Vector3 prevPos;

    private void OnTriggerEnter (Collider other)
    {
        Poker poker = other.GetComponent<Poker>();

        if (poker != null)
        {
            this.poker = poker;
            prevPos = poker.transform.position;
            return;
        }

        Wood wood = other.GetComponent<Wood>();
        if (wood != null && !firewoods.Contains(wood))
        {
            firewoods.Add(wood);
            SFXPlayer.Instance().Play(SFXLibrary.Instance().firewoods);
        }
    }

    private void OnTriggerExit (Collider other)
    {
        Poker poker = other.GetComponent<Poker>();

        if (poker != null && poker == this.poker) this.poker = null;

        Wood wood = other.GetComponent<Wood>();
        if (wood != null && firewoods.Contains(wood)) firewoods.Remove(wood);
    }

    private float fineTime;
    public void Fine (float time)
    {
        fineTime = time;
    }
}
