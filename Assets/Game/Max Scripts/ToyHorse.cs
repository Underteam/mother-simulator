﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToyHorse : MonoBehaviour
{
    public bool isOn { get; set; }

    private float targetAngle;
    private float angle;

    private MyAudioSource sound;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!isOn) targetAngle = 0;
        else if (targetAngle == 0) targetAngle = 5;

        if (isOn && sound == null)
        {
            sound = SFXPlayer.Instance().Play(SFXLibrary.Instance().horse, true, true);
        }
        else if(!isOn && sound != null)
        {
            sound.Stop();
            sound = null;
        }

        angle = Mathf.Lerp(angle, targetAngle, 5 * Time.deltaTime);

        if (isOn)
        {
            if (targetAngle > 0 && targetAngle - angle < 0.1f) targetAngle = -5;
            else if (targetAngle < 0 && angle - targetAngle < 0.1f) targetAngle = 5;
        }

        Vector3 rot = transform.localRotation.eulerAngles;
        rot.z = angle;

        transform.localRotation = Quaternion.Euler(rot);
    }

    public void BabySat ()
    {
        Baby.instance.Horse();
    }
}
