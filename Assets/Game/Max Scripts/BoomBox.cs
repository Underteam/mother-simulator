﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomBox : MonoBehaviour
{
    public AudioSource myTheme;

    private AudioSource mainTheme;

    public GameObject notes;

    private bool isOn;

    private float storedVolume;

    // Start is called before the first frame update
    void Start()
    {
        mainTheme = null;
    }

    // Update is called once per frame
    void Update()
    {
        if (mainTheme == null) return;

        if (isOn)
        {
            myTheme.volume = Mathf.Lerp(myTheme.volume, 1, 2 * Time.deltaTime);
            mainTheme.volume = Mathf.Lerp(mainTheme.volume, 0, 2 * Time.deltaTime);
        }
        else
        {
            myTheme.volume = Mathf.Lerp(myTheme.volume, 0, 2 * Time.deltaTime);
            mainTheme.volume = Mathf.Lerp(mainTheme.volume, storedVolume, 2 * Time.deltaTime);

            if (Mathf.Abs(mainTheme.volume - storedVolume) < 0.01f && Mathf.Abs(myTheme.volume - 0f) < 0.01f)
            {
                mainTheme = null;
                myTheme.volume = 0;
                mainTheme.volume = storedVolume;
            }
        }
    }

    public void SwitchOn ()
    {
        isOn = true;
        //myTheme.mute = false;
        //mainTheme.mute = true;
        notes.SetActive(true);

        mainTheme = MainTheme.instance.GetComponent<AudioSource>();
        storedVolume = mainTheme.volume;
    }

    public void SwitchOff ()
    {
        isOn = false;
        //myTheme.mute = true;
        //mainTheme.mute = false;
        notes.SetActive(false);
    }
}
