﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class SwipeLook2 : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler {

    //public
    private float minDelta = 1;

    public RectTransform zone;

    public List<UIBlocker> blockers;

    public bool useBlockers;

    // Use this for initialization
    void Start() {

        minDelta = (Screen.width + Screen.height) / 2.0f;
        minDelta /= 20.0f;
        minDelta *= minDelta;
    }

    private bool IsPointInZone(Vector2 point)
    {

        Rect rect = zone.rect;

        //Debug.Log("Is in zone " + point + " " + rect);
        //Debug.Log(zone.position + " " + zone.lossyScale + " " + zone.pivot);

        float x1 = zone.position.x - rect.width * zone.lossyScale.x * zone.pivot.x;
        float x2 = zone.position.x + rect.width * zone.lossyScale.x * (1 - zone.pivot.x);
        float y1 = zone.position.y - rect.height * zone.lossyScale.y * zone.pivot.y;
        float y2 = zone.position.y + rect.height * zone.lossyScale.y * (1 - zone.pivot.y);

        //Debug.Log(x1 + " " + x2 + " " + y1 + " " + y2);

        return point.x > x1 && point.x < x2 && point.y > y1 && point.y < y2;
    }

    private bool waitingTouches = true;
    // Update is called once per frame

    private bool m_Dragging;
    private int m_Id;
    
    public void OnPointerDown(PointerEventData data)
    {
        m_Dragging = true;
        m_Id = data.pointerId;
    }

    void LateUpdate()
    {
        UpdateVirtualAxes(Vector2.zero);
    }

    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        if (PointerOverUI()) return;
        Vector2 pointerDelta = eventData.delta / 10;
        pointerDelta.x *= sens;
        pointerDelta.y *= sens;
        //Debug.Log("Drag " + eventData.delta + " " + pointerDelta.magnitude);
        UpdateVirtualAxes(new Vector2(pointerDelta.x, pointerDelta.y));
    }

    public void OnPointerUp(PointerEventData data)
    {
        m_Dragging = false;
        m_Id = -1;
        UpdateVirtualAxes(Vector3.zero);
    }

    public enum AxisOption
    {
        // Options for which axes to use
        Both, // Use both
        OnlyHorizontal, // Only horizontal
        OnlyVertical // Only vertical
    }

    public float sens = 1;
    public AxisOption axesToUse = AxisOption.Both; // The options for the axes that the still will use
    public bool invertX = false;
    public bool invertY = false;
    public string horizontalAxisName = "Horizontal"; // The name given to the horizontal axis for the cross platform input
    public string verticalAxisName = "Vertical"; // The name given to the vertical axis for the cross platform input

    bool m_UseX; // Toggle for using the x axis
    bool m_UseY; // Toggle for using the Y axis
    InputManager.Axis m_HorizontalVirtualAxis; // Reference to the joystick in the cross platform input
    InputManager.Axis m_VerticalVirtualAxis; // Reference to the joystick in the cross platform input

    void OnEnable()
    {
        CreateVirtualAxes();
    }

    void UpdateVirtualAxes(Vector2 delta)
    {
        //float x = Mathf.Abs(delta.x);
        //float y = Mathf.Abs(delta.y);
        //if (x < 1) x = 1;
        //if (y < 1) y = 1;
        //x = Mathf.Pow (x, 0.25f);
        //y = Mathf.Pow (y, 0.25f);
        //if (x > 1.75f) x = 1.75f;
        //if (y > 1.75f) y = 1.75f;

        //delta.x *= x;
        //delta.y *= y;

        if (m_UseX)
        {
            if (invertX)
                m_HorizontalVirtualAxis.Set(sens * delta.x);
            else
                m_HorizontalVirtualAxis.Set(-sens * delta.x);
        }

        if (m_UseY)
        {
            if (invertY)
                m_VerticalVirtualAxis.Set(-sens * delta.y);
            else
                m_VerticalVirtualAxis.Set(sens * delta.y);
        }
    }

    void CreateVirtualAxes()
    {
        // set axes to use
        m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
        m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);

        // create new axes based on axes to use
        if (m_UseX)
        {
            m_HorizontalVirtualAxis = new InputManager.Axis(horizontalAxisName);
        }
        if (m_UseY)
        {
            m_VerticalVirtualAxis = new InputManager.Axis(verticalAxisName);
        }
    }

    void OnDisable()
    {
        // remove the joysticks from the cross platform input
        if (m_UseX)
        {
            //m_HorizontalVirtualAxis.Remove();
        }
        if (m_UseY)
        {
            //m_VerticalVirtualAxis.Remove();
        }
    }

    public void ToogleInvertX(bool b) {

        invertX = !invertX;
    }

    public void ToogleInvertY(bool b) {

        invertY = !invertY;
    }

    private bool PointerOverUI()
    {
        if (!useBlockers) return false;

        for (int i = 0; i < blockers.Count; i++)
        {
            if (blockers[i] != null && blockers[i].blocked) return true;
        }

        return false;
    }
}
