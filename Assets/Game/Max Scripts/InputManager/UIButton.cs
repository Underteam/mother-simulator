﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class UIButton : MonoBehaviour {

	public KeyCode key;

	public string btnName;

	public bool inEditor;

	// Use this for initialization
	void Start () {
	
		//Debug.LogError ("!!! " + btnName);
		if (btnName == "")
			btnName = key.ToString ();

		InputManager.Instance ().AddButton (btnName);

		EventTrigger trigger = gameObject.AddComponent<EventTrigger> ();

		EventTrigger.Entry down = new EventTrigger.Entry ();
		down.eventID = EventTriggerType.PointerDown;
		down.callback.AddListener ((data) => {
			Down();
		});
		trigger.triggers.Add (down);

		EventTrigger.Entry up = new EventTrigger.Entry ();
		up.eventID = EventTriggerType.PointerUp;
		up.callback.AddListener ((data) => {
			Up();
		});
		trigger.triggers.Add (up);
	}
		
	#if UNITY_EDITOR
	void LateUpdate() {
	
		if (inEditor) {
			if (Input.GetKeyDown(key)/* || Input.GetButtonDown (btnName)*/)
				Down ();

			if (Input.GetKeyUp(key)/* || Input.GetButtonUp (btnName)*/)
				Up ();
		}
	}
	#endif

	private bool down;
	public void Down() {

		down = true;
		InputManager.Instance ().SetButton (btnName, true);
		//Debug.Log ("Button down " + btnName + " " + InputManager.Instance ().GetButton (btnName) + " " + InputManager.Instance ().GetButtonDown (btnName) + " " + InputManager.Instance ().GetButtonUp (btnName));
	}

	public void Up() {

		down = false;
		InputManager.Instance ().SetButton (btnName, false);
		//Debug.Log ("Button up " + btnName + " " + InputManager.Instance ().GetButton (btnName) + " " + InputManager.Instance ().GetButtonDown (btnName) + " " + InputManager.Instance ().GetButtonUp (btnName));
	}

	void OnDisable () {
	
		if (down) Up ();
	}
}
