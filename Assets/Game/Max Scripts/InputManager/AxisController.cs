﻿using UnityEngine;
using System.Collections.Generic;

public class AxisController : MonoBehaviour {

	public string axisName;

	public Vector2 range;

	public float sens;

	public float grav;

	public float val;

	public List<KeyCode> keys;

	// Use this for initialization
	void Start () {
	
		if (axisName == null || axisName == "") {

			Destroy(this);
			return;
		}

		InputManager.Instance ().AddAxis (axisName);
	}
	
	// Update is called once per frame
	void Update () {
	
		int n = keys.Count;

		while(n >= 2) {
			int i1 = n-2;
			int i2 = n-1;
			if(Input.GetKey(keys[i1]))
				val += sens*Time.deltaTime;
			else if(Input.GetKey(keys[i2]))
				val -= sens*Time.deltaTime;
			else 
				val *= 1/(1+grav*grav);

			if(val < range.x) val = range.x;
			if(val > range.y) val = range.y;

			n -= 2;
		}

		InputManager.Instance ().SetAxis (axisName, val);
	}
}
