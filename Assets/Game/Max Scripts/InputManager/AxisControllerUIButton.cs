﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class AxisControllerUIButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	public string axisName;

	public Vector2 range = new Vector2(-1, 1);
	
	public float sens = 1;
	
	public float grav = 1;
	
	public float val = 0;

	public bool pressed;

	public AxisControllerUIButton positive;

	public AxisControllerUIButton negative;

	void Start() {
	
		if (axisName == null || axisName == "") {
			
			Destroy(this);
			return;
		}
		
		InputManager.Instance ().AddAxis (axisName);
	}

	public void OnPointerDown(PointerEventData eventData) {

		pressed = true;
	}

	public void OnPointerUp(PointerEventData eventData) {
	
		pressed = false;
	}

	void Update() {

		if(sens < 0) return;

		if(positive != null && positive.pressed)
			val += positive.sens*Time.deltaTime;
		else if(negative != null && negative.pressed)
			val += negative.sens*Time.deltaTime;
		else
			val *= 1/(1+grav*grav);
		
		if(val < range.x) val = range.x;
		if(val > range.y) val = range.y;

		InputManager.Instance ().SetAxis (axisName, val);
	}
}
