﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AxisControllerScrollbar : MonoBehaviour {

	public string axis;

	public Image bg;

	public Color zeroColor;

	public float deadZone = 0;

	void Start() {
	
		if (bg == null)
			bg = GetComponentInChildren<Image> ();

		if (bg != null)
			bg.color = zeroColor;
	}

	public void HandleInput(float value)
	{
		float val = (value * 2f) - 1f;
		if(val < deadZone && val > -deadZone) val = 0;
		else if(val > 0) val = (val-deadZone)/(1-deadZone);
		else if(val < 0) val = (val+deadZone)/(1-deadZone);

		InputManager.Instance().SetAxis(axis, val);

		if (bg != null) {
			Color c = Color.red;
			if (val < 0)
				c = Color.blue;

			c.a = zeroColor.a;

			Color bgColor = Mathf.Pow (Mathf.Abs (val), 0.5f) * c + (1 - Mathf.Pow (Mathf.Abs (val), 0.5f)) * zeroColor;

			bg.color = bgColor;
		}
	}

}
