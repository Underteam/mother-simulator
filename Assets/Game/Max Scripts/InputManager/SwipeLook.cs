﻿using UnityEngine;
using System.Collections.Generic;

public class SwipeLook : MonoBehaviour
{
    private float minDelta = 1;

    public RectTransform zone;

    public List<GameObject> blockersObjects;

    public List<IBlocker> blockers;

    public bool useBlockers;

    public bool log;

    // Use this for initialization
    void Start() {

        if (blockers == null) blockers = new List<IBlocker>();
        for(int i = 0; i < blockersObjects.Count; i++)
        {
            var blocker = blockersObjects[i].GetComponent<IBlocker>();
            if (blocker != null) blockers.Add(blocker);
        }

        minDelta = (Screen.width + Screen.height) / 2.0f;
        minDelta /= 20.0f;
        minDelta *= minDelta;
    }

    private bool IsPointInZone(Vector2 point)
    {
        Rect rect = zone.rect;

        //Debug.Log("Is in zone " + point + " " + rect);
        //Debug.Log(zone.position + " " + zone.lossyScale + " " + zone.pivot);

        float x1 = zone.position.x - rect.width * zone.lossyScale.x * zone.pivot.x;
        float x2 = zone.position.x + rect.width * zone.lossyScale.x * (1 - zone.pivot.x);
        float y1 = zone.position.y - rect.height * zone.lossyScale.y * zone.pivot.y;
        float y2 = zone.position.y + rect.height * zone.lossyScale.y * (1 - zone.pivot.y);

        //Debug.Log(x1 + " " + x2 + " " + y1 + " " + y2);

        return point.x > x1 && point.x < x2 && point.y > y1 && point.y < y2;
    }

    private bool waitingTouches = true;
    // Update is called once per frame

    public UnityEngine.UI.Text lblInfo;

    private string addInfo;

    private List<int> knownIDs = new List<int>();

    void Update() {

#if UNITY_EDITOR

        if (Input.GetMouseButtonDown(0))
        {
            if(log) Debug.Log("Down 1");
            if (waitingTouches && IsPointInZone(Input.mousePosition))
            {
                if (log) Debug.Log("Down 2");
                waitingTouches = false;
                PointerDown(Input.mousePosition);
            }
        }

        if (Input.GetMouseButton(0) && !waitingTouches)
        {
            Vector2 delta = Input.mousePosition;
            delta -= prevPos;

            if (!moving)
            {
                if (delta.magnitude > 0)
                {
                    moving = true;
                    if (log) Debug.Log("Start drag");
                }
            }
            else
                OnDrag(delta);
            prevPos = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (log) Debug.Log("Up 1 " + waitingTouches + " " + moving);

            if (!waitingTouches)
            {
                if (log) Debug.Log("Up 2");
                PointerUp(Input.mousePosition);
                waitingTouches = true;
            }
        }//*/

 #else

		if (Input.touches.Length > 0) {
			
			if (waitingTouches)
            {
				if (lblInfo != null) lblInfo.text = "Waiting touches " + Input.touches.Length + " | " + addInfo;

				for (int i = 0; i < Input.touches.Length; i++)
                {            
					if (IsPointInZone(Input.touches[i].position) && Input.touches[i].phase == TouchPhase.Began)
                    {
						touch = Input.touches[i];
						PointerDown (touch.position);
						if (lblInfo != null) lblInfo.text = "Found touch " + Input.touches [i].position + " " + Input.touches [i].fingerId + " " + touch.position + " " + touch.fingerId + " | " + addInfo;
                    }
				}
			}
            else 
            {
                bool found = false;
				for (int i = 0; i < Input.touches.Length; i++) {
				
					if (Input.touches [i].fingerId == touch.fingerId && Input.touches[i].phase != TouchPhase.Ended) 
                    {
						found = true;
                        if ((Input.touches[i].position - prevPos).magnitude > 20) addInfo = "" + (Input.touches[i].position - prevPos) + " | " + touch.fingerId;

                        touch = Input.touches [i];
                        if(!moving)
                        {
                            if (touch.deltaPosition.magnitude > 0)
                                moving = true;
                        }
                        else
                            OnDrag (touch.deltaPosition/*touch.position - prevPos*/);
						prevPos = touch.position;
						break;
					}
				}

				if (!found || !IsPointInZone(touch.position)) {
					PointerUp (Vector2.zero);
				} //else
					//if (lblInfo != null) lblInfo.text = "Found " + touch.position;
			}
		
		} else {

			if (lblInfo != null) lblInfo.text = "0 touches " + waitingTouches + " | " + addInfo;

            if (!waitingTouches) PointerUp(Vector2.zero);
			//OnDrag (Vector2.zero);
		}
#endif
    }

    private Touch touch;
    private Vector2 prevPos;
    private bool moving;
    private void PointerDown(Vector2 coord)
    {
        moving = false;
        prevPos = coord;
        waitingTouches = false;
    }

    private void PointerUp(Vector2 coord) {

        moving = false;
        UpdateVirtualAxes(Vector2.zero);
        waitingTouches = true;
    }

    public enum AxisOption
    {
        // Options for which axes to use
        Both, // Use both
        OnlyHorizontal, // Only horizontal
        OnlyVertical // Only vertical
    }

    public float sens = 1;
    public AxisOption axesToUse = AxisOption.Both; // The options for the axes that the still will use
    public bool invertX = false;
    public bool invertY = false;
    public string horizontalAxisName = "Horizontal"; // The name given to the horizontal axis for the cross platform input
    public string verticalAxisName = "Vertical"; // The name given to the vertical axis for the cross platform input

    bool m_UseX; // Toggle for using the x axis
    bool m_UseY; // Toggle for using the Y axis
    InputManager.Axis m_HorizontalVirtualAxis; // Reference to the joystick in the cross platform input
    InputManager.Axis m_VerticalVirtualAxis; // Reference to the joystick in the cross platform input

    void OnEnable()
    {
        CreateVirtualAxes();
    }

    void UpdateVirtualAxes(Vector2 delta)
    {
        if (m_UseX)
        {
            if (invertX)
                m_HorizontalVirtualAxis.Set(sens * delta.x);
            else
                m_HorizontalVirtualAxis.Set(-sens * delta.x);
        }

        if (m_UseY)
        {
            if (invertY)
                m_VerticalVirtualAxis.Set(-sens * delta.y);
            else
                m_VerticalVirtualAxis.Set(sens * delta.y);
        }
    }

    void CreateVirtualAxes()
    {
        // set axes to use
        m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
        m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);

        // create new axes based on axes to use
        if (m_UseX)
        {
            m_HorizontalVirtualAxis = new InputManager.Axis(horizontalAxisName);
        }
        if (m_UseY)
        {
            m_VerticalVirtualAxis = new InputManager.Axis(verticalAxisName);
        }
    }


    public void OnDrag(Vector2 delta)
    {
        if (PointerOverUI()) return;

        if (lblInfo != null) lblInfo.text = "" + delta;

        UpdateVirtualAxes(delta);
    }

    void OnDisable()
    {
        // remove the joysticks from the cross platform input
        if (m_UseX)
        {
            //m_HorizontalVirtualAxis.Remove();
        }
        if (m_UseY)
        {
            //m_VerticalVirtualAxis.Remove();
        }
    }

    public void ToogleInvertX(bool b) {

        invertX = !invertX;
    }

    public void ToogleInvertY(bool b) {

        invertY = !invertY;
    }

    private bool PointerOverUI()
    {
        if (!useBlockers) return false;

        for (int i = 0; i < blockers.Count; i++)
        {
            if (blockers[i] != null && blockers[i].blocked)
            {
                Debug.LogError("Blocked by " + i);
                return true;
            }
        }

        return false;
    }
}
