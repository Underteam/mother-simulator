﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideUI : MonoBehaviour
{
    public List<GameObject> list;

    // Start is called before the first frame update
    void Start()
    {
        //for (int i = 0; i < list.Count; i++) list[i].SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private bool state = true;
    public void Switch ()
    {
        state = !state;

        for (int i = 0; i < list.Count; i++) list[i].SetActive(state);
    }
}
