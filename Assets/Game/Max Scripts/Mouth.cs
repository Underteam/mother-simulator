﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouth : MonoBehaviour
{
    private Food food;

    private float timer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (timer > 0) timer -= Time.deltaTime;

        if (food != null && timer <= 0)
        {
            Destroy(food.gameObject);

            MomHungry need = FindObjectOfType<MomHungry>();
            if (need != null) need.Eat(0.5f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        food = other.GetComponent<Food>();

        if (food != null) timer = 0.5f;
    }

    private void OnTriggerExit(Collider other)
    {
        if (food != null && food == other.GetComponent<Food>()) food = null;
    }
}
