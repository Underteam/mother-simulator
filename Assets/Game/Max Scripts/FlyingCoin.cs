﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlyingCoin : MonoBehaviour {

    public Transform target;
    public float moveSpeed = 2;
    public float amp = 10;
    public float scaleDownDist = 10;

    private Vector3 scale;

    public float w = 0;

    private float length;

    public bool inited;

    private float sign = 1;

    // Use this for initialization
    private void Init()
    {
        if (target == null) return;

        Vector3 dir = target.position - transform.position;
        dir.x /= transform.lossyScale.x;
        dir.y /= transform.lossyScale.y;

        float dist = dir.magnitude;

        moveSpeed = moveSpeed * dist;

        w = 15 / moveSpeed;

        w = Random.Range(0.7f * w, 1.3f * w);

        scale = transform.localScale;

        inited = true;

        if (Random.Range(0f, 1f) < 0.5f) sign = -1;
    }

	// Update is called once per frame
	void Update () {

        if (!inited) Init ();

        if (target != null)
        {
            Vector3 dir = target.position - transform.position;
            float dist = dir.magnitude;
            dir.Normalize();
            Vector3 perp = Vector3.Cross(dir, Vector3.forward);

            float a = Mathf.Clamp(dist, 0, scaleDownDist) / scaleDownDist;

            transform.localScale = scale * a;

            Vector3 speed = dir * moveSpeed;

            float delta = speed.magnitude * Time.unscaledDeltaTime;

            length += delta;

            if (delta > dist)
            {
                Destroy(gameObject);
                //gameObject.SetActive(false);
            }

            transform.position += speed * Time.unscaledDeltaTime + amp * perp * Mathf.Sin(w * length) * a * sign;
        }
    }

    public static GMATools.Common.Updater Run(FlyingCoin prefab, int num, Transform target, float deltaTime, System.Action onFinish)
    {
        var updater = GMATools.Common.Updater.CreateUpdater(() => { });
        updater.StartCoroutine(RunCoroutine(prefab, num, target, deltaTime, () =>
        {
            updater.Stop();
            onFinish?.Invoke();
        }));

        return updater;
    }

    private static IEnumerator RunCoroutine(FlyingCoin prefab, int num, Transform target, float deltaTime, System.Action onFinish)
    {
        FlyingCoin lastFC = null;

        float time = Time.time;

        for (int i = 0; i < num; i++)
        {
            var coin = Instantiate(prefab);
            coin.transform.SetParent(prefab.transform.parent);
            coin.transform.position = prefab.transform.position;
            coin.transform.localScale = prefab.transform.localScale;
            coin.transform.SetParent(prefab.transform.parent.parent);
            coin.gameObject.SetActive(true);
            if (target != null) coin.target = target;
            lastFC = coin;

            //Debug.LogError("Spawn " + (Time.time - time));

            yield return new WaitForSeconds(deltaTime);

            //Debug.LogError("return " + (Time.time - time));
        }

        while (lastFC != null && lastFC.gameObject.activeSelf) yield return null;

        onFinish?.Invoke();
    }
}
