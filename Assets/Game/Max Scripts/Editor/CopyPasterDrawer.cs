﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(CopyPaster))]
public class CopyPasterDrawer : PropertyDrawer
{
    // Draw the property inside the given rect
    public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
    {
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(position, label, property);

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Calculate rects
        Rect rect1 = new Rect(position.x, position.y, 70, position.height);
        Rect rect2 = new Rect(position.x + 80, position.y, 70, position.height);
        Rect rect3 = new Rect(position.x + 160, position.y, 70, position.height);
        Rect rect4 = new Rect(position.x + 240, position.y, 70, position.height);
        Rect rect5 = new Rect(position.x + 320, position.y, 70, position.height);

        if (GUI.Button(rect1, "Copy"))
        {
            CopyPaster copypaster = (CopyPaster)GetTargetObjectOfProperty(property);

            if (copypaster != null)
            {
                copypaster.Copy();
                EditorUtility.SetDirty(property.serializedObject.targetObject);
            }
        }

        if (GUI.Button(rect2, "Paste"))
        {
            CopyPaster copypaster = (CopyPaster)GetTargetObjectOfProperty(property);

            if (copypaster != null)
            {
                copypaster.Paste();
                EditorUtility.SetDirty(property.serializedObject.targetObject);
            }
        }

        if (GUI.Button(rect3, "L Copy"))
        {
            CopyPaster copypaster = (CopyPaster)GetTargetObjectOfProperty(property);

            if (copypaster != null)
            {
                copypaster.LCopy();
                EditorUtility.SetDirty(property.serializedObject.targetObject);
            }
        }

        if (GUI.Button(rect4, "L Paste"))
        {
            CopyPaster copypaster = (CopyPaster)GetTargetObjectOfProperty(property);

            if (copypaster != null)
            {
                copypaster.LPaste();
                EditorUtility.SetDirty(property.serializedObject.targetObject);
            }
        }

        if (GUI.Button(rect5, "Undo"))
        {
            CopyPaster copypaster = (CopyPaster)GetTargetObjectOfProperty(property);

            if (copypaster != null)
            {
                copypaster.Undo();
                EditorUtility.SetDirty(property.serializedObject.targetObject);
            }
        }

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }

    public static object GetTargetObjectOfProperty(SerializedProperty prop)
    {
        if (prop == null) return null;

        var path = prop.propertyPath.Replace(".Array.data[", "[");
        object obj = prop.serializedObject.targetObject;
        var elements = path.Split('.');
        foreach (var element in elements)
        {
            if (element.Contains("["))
            {
                var elementName = element.Substring(0, element.IndexOf("["));
                var index = System.Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
                obj = GetValue_Imp(obj, elementName, index);
            }
            else
            {
                obj = GetValue_Imp(obj, element);
            }
        }
        return obj;
    }

    private static object GetValue_Imp(object source, string name)
    {
        if (source == null)
            return null;
        var type = source.GetType();

        while (type != null)
        {
            var f = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            if (f != null)
                return f.GetValue(source);

            var p = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
            if (p != null)
                return p.GetValue(source, null);

            type = type.BaseType;
        }
        return null;
    }

    private static object GetValue_Imp(object source, string name, int index)
    {
        var enumerable = GetValue_Imp(source, name) as System.Collections.IEnumerable;
        if (enumerable == null) return null;
        var enm = enumerable.GetEnumerator();
        //while (index-- >= 0)
        //    enm.MoveNext();
        //return enm.Current;

        for (int i = 0; i <= index; i++)
        {
            if (!enm.MoveNext()) return null;
        }
        return enm.Current;
    }
}
