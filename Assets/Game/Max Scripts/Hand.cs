﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour
{
    public Animation anim;

    public bool active;

    private Vector3 velocity;
    private Vector3 prevPos;

    // Start is called before the first frame update
    void Start()
    {
        prevPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dir = transform.position - prevPos;
        dir.Normalize();
        prevPos = transform.position;

        velocity = 0.5f * velocity + 0.5f * dir;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!active) return;

        Rigidbody rb = other.attachedRigidbody;
        if(rb != null && !rb.isKinematic)
        {
            rb.AddForce(5 * velocity, ForceMode.VelocityChange);
        }
    }

    public void Play (AnimationClip clip)
    {
        if (anim.GetClip(clip.name) == null) anim.AddClip(clip, clip.name);

        anim.Play(clip.name);
    }

    public void Play(string name)
    {
        anim.Play(name);
    }

    public void Stop ()
    {
        anim.Stop();
    }
}
