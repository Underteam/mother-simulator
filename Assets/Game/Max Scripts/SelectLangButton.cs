﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectLangButton : MonoBehaviour
{
    public Button button;
    public Text text;
    public Image icon;

    public void SetIcon (Sprite sprite)
    {
        //Debug.LogError("Set sprite " + sprite, this);
        icon.sprite = sprite;
    }
}
