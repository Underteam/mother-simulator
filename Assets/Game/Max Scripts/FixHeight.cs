﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixHeight : MonoBehaviour
{
    private float height;
    private Vector3 pos;
    private Quaternion rot;
    // Start is called before the first frame update
    void Start()
    {
        //Debug.LogError("start");
        height = transform.position.y;
        pos = transform.localPosition;
        rot = transform.localRotation;
    }

    private void OnDisable()
    {
        Fix();    
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Fix ()
    {
        //Vector3 pos = transform.position;
        //pos.y = height;
        //transform.position = pos;

        transform.localPosition = pos;
        transform.localRotation = rot;
    }
}
