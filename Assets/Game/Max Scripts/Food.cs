﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour
{
    public Color color = Color.white;

    private bool use;

    public bool dontEat;

    // Start is called before the first frame update
    void Start()
    {
        if (!dontEat)
        {
            var item = GetComponent<Item>();
            item.onTaked.AddListener(new UnityEngine.Events.UnityAction(() =>
            {
                Use();
            }));
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (use)
        {
            var particles = Instantiate(GameController.Instance().foodParticles);

            var module = particles.main;
            module.startColor = color;

            particles.transform.SetParent(null, true);

            particles.gameObject.SetActive(true);

            particles.transform.localScale = Vector3.one;

            particles.transform.position = PlayerController.instance.cam.transform.position + GameController.Instance().particlesDist * PlayerController.instance.cam.transform.forward;
            Destroy(gameObject);

            MomHungry hungry = TaskManager.Instance().GetNeed<MomHungry>();
            if (hungry != null)
            {
                {
                    float l = hungry.level + 0.3f;
                    if (l > 1) l = 1;
                    l = l - hungry.level;
                }

                hungry.level += 0.3f;
                if (hungry.level > 1) hungry.level = 1;
            }

            EventManager.Instance().TriggerEvent("Eating", this);

            use = false;
        }
    }

    public void Use ()
    {
        use = true;

        //Debug.LogError("Use " + this, this);

        SFXPlayer.Instance().Play(SFXLibrary.Instance().foodClip, false, true);
    }
}
