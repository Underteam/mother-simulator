﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Day48Tutorial : Tutorial
{
    private AbilityInterfaceSearcher searcher;

    private int step = 0;

    private bool cakeInHands;

    private GuestMood need;

    public override void Init()
    {
        searcher = FindObjectOfType<AbilityInterfaceSearcher>();

        step = 0;

        cakeInHands = false;

        TaskManager.Instance().displayWishes = true;

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            need = TaskManager.Instance().GetNeed<GuestMood>();
            need.setMarkers = false;
        },
        "TaskStarted");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            var c = i.GetComponent<GuestFood>();
            if (c != null)
            {
                cakeInHands = true;
                if(step == 0 && dish != null) TutorialManager.Instance().FocusOn(dish.transform);
                return;
            }
        },
        "ItemTaked");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            var c = i.GetComponent<GuestFood>();
            if (c != null)
            {
                cakeInHands = false;
                return;
            }
        },
        "ItemDroped");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    public override void Deactivate()
    {
        OnDestroy();
    }

    MainDish dish;

    public Sprite guestImage;

    public override IEnumerator Job()
    {
        var baby = FindObjectOfType<Baby>();
        var babyItem = baby.GetComponent<Item>();
        babyItem.busy = true;

        yield return new WaitForSeconds(0.5f);

        TutorialManager.Instance().ShowGreeting(Localizer.GetString("tutorial48message1", "Привет, подруга! Я зашла к тебе в гости!"), guestImage);

        var guest = FindObjectOfType<Guest>();
        guest.timer = 0;

        dish = FindObjectOfType<MainDish>();
        var cakes = FindObjectsOfType<GuestFood>();

        var btnAct = Groups.Instance().GetGroup("Button Act")[0].GetComponent<RectTransform>();
        var btnDrop = Groups.Instance().GetGroup("Button Throw")[0].GetComponent<RectTransform>();
        TutorialManager.Instance().SetFingerPos(btnAct);
        TutorialManager.Instance().SetLongFingerPos(btnDrop);

        yield return new WaitForSeconds(0.5f);

        TutorialManager.Instance().FocusOn(cakes[0].transform);

        while (step == 0 || step == 1)
        {
            if (cakeInHands) step = 1;
            else step = 0;

            if (step == 0)
            {
                TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial48note1", "Tap to put"));
                if (cakes.Length > 0)
                {
                    TutorialManager.Instance().UnHightlightAll();
                    TutorialManager.Instance().HightlightObject(cakes[0].gameObject, 0.3f, TutorialManager.Instance().icons["take"].sprites);
                    bool active = false;
                    for (int i = 0; !active && i < cakes.Length; i++)
                        active = active || ActivateFinger<InterfaceCollect>(cakes[i].gameObject, false);
                    Groups.Instance().SetGroupState("Finger", active);
                }
            }
            else
            {
                TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial48note2", "Tap to put"));
                TutorialManager.Instance().UnHightlightAll();
                TutorialManager.Instance().HightlightObject(dish.gameObject, 0.15f, TutorialManager.Instance().icons["interact"].sprites);
                ActivateFinger<AbilityInterfaceApplyItem>(dish.gameObject);
            }

            if (dish.food != null) step = 2;

            yield return null;
        }

        babyItem.busy = false;

        TutorialManager.Instance().UnHightlightAll();
        TutorialManager.Instance().HideFinger();

        yield return new WaitForSeconds(3f);

        need.setMarkers = true;

        while (true) yield return null;
    }

    private bool ActivateFinger<T>(GameObject go, bool setstate = true) where T : AbilityInterface
    {
        if (searcher.IsThereAnyOf<T>(go))
        {
            if (setstate) Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            if (setstate) Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }
}
