﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Day31Tutorial : Tutorial
{
    private AbilityInterfaceSearcher searcher;

    private int step = 0;

    private bool vacuumInHands;

    private List<Dirty> dirties = new List<Dirty>();
    private List<Furs> furs = new List<Furs>();

    public List<Sprite> sprites;

    public override void Init()
    {
        searcher = FindObjectOfType<AbilityInterfaceSearcher>();

        step = 0;
        vacuumInHands = false;

        TaskManager.Instance().displayWishes = true;

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            var v = i.GetComponent<Vacuum>();
            if (v != null)
            {
                vacuumInHands = true;

                dirties.Clear();
                dirties.AddRange(FindObjectsOfType<Dirty>());
                furs.Clear();
                for (int j = 0; j < dirties.Count; j++)
                {
                    var fur = dirties[j].GetComponentInParent<Furs>();
                    if (fur != null)
                    {
                        if (!furs.Contains(fur)) furs.Add(fur);
                        continue;
                    }
                    TutorialManager.Instance().HightlightObject(dirties[j].gameObject, 0.5f, sprites).stickToEdge = false;
                }

                for (int j = 0; j < furs.Count; j++)
                {
                    //TutorialManager.Instance().HightlightObject(furs[i].gameObject, 0.5f, null).stickToEdge = false;
                }

                return;
            }
        },
        "ItemTaked");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            vacuumInHands = false;

            Debug.LogError("Droped");

            Item i = (Item)d;
            if (i == null) return;

            for (int j = 0; j < dirties.Count; j++)
            {
                if (dirties[j] == null) continue;
                TutorialManager.Instance().UnHightlightObject(dirties[j].gameObject);
            }

            for (int j = 0; j < furs.Count; j++)
            {
                if (furs[j] == null) continue;
                TutorialManager.Instance().UnHightlightObject(furs[j].gameObject);
            }
        },
        "ItemDroped");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    public override void Deactivate()
    {
        OnDestroy();
    }

    public override IEnumerator Job()
    {
        var baby = FindObjectOfType<Baby>();
        var babyItem = baby.GetComponent<Item>();
        //babyItem.busy = true;

        yield return new WaitForSeconds(0.5f);

        var vacuum = FindObjectOfType<Vacuum>();

        TutorialManager.Instance().HightlightObject(vacuum.gameObject, 2f, TutorialManager.Instance().icons["interact"].sprites);

        var btnAct = Groups.Instance().GetGroup("Button Act")[0].GetComponent<RectTransform>();
        var btnDrop = Groups.Instance().GetGroup("Button Throw")[0].GetComponent<RectTransform>();
        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial31note1", "Tap to take"));
        TutorialManager.Instance().SetFingerPos(btnAct);
        TutorialManager.Instance().SetLongFingerPos(btnDrop);

        yield return new WaitForSeconds(0.5f);

        TutorialManager.Instance().FocusOn(vacuum.transform);

        while (step == 0)
        {
            ActivateFinger<InterfaceCollect>(vacuum.gameObject);

            if (vacuumInHands) step = 1;

            yield return null;
        }

        //babyItem.busy = false;

        TutorialManager.Instance().UnHightlightObject(vacuum.gameObject);
        TutorialManager.Instance().HideFinger();

        while (true) yield return null;
    }

    private bool ActivateFinger<T>(GameObject go) where T : AbilityInterface
    {
        if (searcher.IsThereAnyOf<T>(go))
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }
}
