﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Day13Tutorial : Tutorial
{
    private AbilityInterfaceSearcher searcher;

    private int step = 0;

    private bool coffeBagInHands;
    private bool coffeCupInHands;
    private bool wrongItem;

    private MomPep need;

    public bool log;

    private TutorialManager tm;

    public override void Init()
    {
        searcher = FindObjectOfType<AbilityInterfaceSearcher>();

        tm = TutorialManager.Instance();

        step = 0;
        coffeBagInHands = false;
        coffeCupInHands = false;
        wrongItem = false;

        TaskManager.Instance().displayWishes = true;

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;
            need = TaskManager.Instance().GetNeed<MomPep>();
            need.level = 0.4f;
            need.instantSet = true;
            need.setMarkers = false;
        },
        "TaskStarted");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            if (step == 0)
            {
                var b = i.GetComponent<CoffeeBag>();
                if (b != null)
                {
                    coffeBagInHands = true;
                    return;
                }
                else
                {
                    wrongItem = true;
                }
            }

            if (step == 2)
            {
                var c = i.GetComponent<CoffeeCup>();
                if (c != null)
                {
                    coffeCupInHands = true;
                    return;
                }
                else
                {
                    wrongItem = true;
                }
            }

            if (step == 4)
            {
                var c = i.GetComponent<CoffeeCup>();
                if (c != null)
                {
                    coffeCupInHands = true;
                    return;
                }
            }
        },
        "ItemTaked");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            wrongItem = false;

            var b = i.GetComponent<CoffeeBag>();
            if (b != null)
            {
                coffeBagInHands = false;
                return;
            }

            var c = i.GetComponent<CoffeeCup>();
            if (c != null)
            {
                coffeCupInHands = false;
                return;
            }
        },
        "ItemDroped");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    public override void Deactivate()
    {
        OnDestroy();
    }

    private CoffeeBag GetCoffeeBag ()
    {
        var player = PlayerController.instance.transform;

        List<CoffeeBag> coffeeBags = new List<CoffeeBag>(FindObjectsOfType<CoffeeBag>());
        coffeeBags.Sort((a, b) =>
        {
            float d1 = Vector3.Distance(a.transform.position, player.position);
            float d2 = Vector3.Distance(b.transform.position, player.position);

            return d1.CompareTo(d2);
        });

        return coffeeBags[0];
    }

    public override IEnumerator Job()
    {
        var baby = FindObjectOfType<Baby>();
        var babyItem = baby.GetComponent<Item>();
        babyItem.busy = true;

        yield return new WaitForSeconds(0.5f);

        var needBar = TaskManager.Instance().GetBar<MomPep>();

        var player = PlayerController.instance.transform;

        List<CoffeeBag> coffeeBags = new List<CoffeeBag>(FindObjectsOfType<CoffeeBag>());

        List<CoffeeCup> coffeeCups = new List<CoffeeCup>(FindObjectsOfType<CoffeeCup>());
        coffeeCups.Sort((a, b) =>
        {
            float d1 = Vector3.Distance(a.transform.position, player.position);
            float d2 = Vector3.Distance(b.transform.position, player.position);

            return -d1.CompareTo(d2);
        });
        for (int i = 1; i < coffeeCups.Count; i++) coffeeCups[i].gameObject.SetActive(false);

        var coffeeBag = GetCoffeeBag();
        var coffeeCup = coffeeCups[0];
        var teapot = FindObjectOfType<Teapot>();
        var switcher = teapot.GetComponentInChildren<AbilityInterfaceApplyItem>(true);

        var btnAct = Groups.Instance().GetGroup("Button Act")[0].GetComponent<RectTransform>();
        var btnDrop = Groups.Instance().GetGroup("Button Throw")[0].GetComponent<RectTransform>();
        var btnPut = Groups.Instance().GetGroup("Button Put")[0].GetComponent<RectTransform>();
        TutorialManager.Instance().SetFingerPos(btnAct);
        TutorialManager.Instance().SetLongFingerPos(btnDrop);

        yield return new WaitForSeconds(0.5f);

        TutorialManager.Instance().FocusOn(coffeeBag.transform);

        GameObject targetObject = null;
        System.Type targetInterface = null;
        float targetHeight = 1f;
        bool hide = false;

        while (step < 6)
        {
            if (step == 0)//взять пакетик
            {
                TutorialManager.Instance().UnHightlightAll();

                if (coffeeBag == null) coffeeBag = GetCoffeeBag();
                targetObject = coffeeBag.gameObject;
                targetInterface = typeof(InterfaceCollect);
                targetHeight = 0.15f;
                hide = false;

                while (step == 0)
                {
                    if (wrongItem)
                    {
                        TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
                        TutorialManager.Instance().ShowFinger(btnPut);
                        TutorialManager.Instance().UnHightlightObject(targetObject);

                        if (PlayerController.instance.currentItem != null)
                        {
                            var cup = PlayerController.instance.currentItem.GetComponent<CoffeeCup>();
                            if (cup != null && cup.coffee.isItCoffee)
                            {
                                coffeCupInHands = true;
                                wrongItem = false;
                                step = 3;
                            }
                        }
                    }
                    else
                    {
                        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial13note1", "Tap to put"));

                        bool state = false;
                        for (int i = 0; !state && i < coffeeBags.Count; i++)
                        {
                            if (coffeeBags[i] == null) continue;
                            state = state || ActivateFinger<InterfaceCollect>(coffeeBags[i].gameObject, false);
                        }
                        Groups.Instance().SetGroupState("Finger", state);

                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["take"].sprites).hideWhenVisible = hide;
                    }

                    if (coffeBagInHands) step = 1;

                    yield return null;
                }
            }

            if (step == 1)//положить в кружку
            {
                TutorialManager.Instance().UnHightlightAll();

                targetObject = coffeeCup.gameObject;
                targetInterface = typeof(InterfaceCollect);
                targetHeight = 0.2f;
                hide = false;

                while (step == 1)
                {
                    if (wrongItem)
                    {
                        TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
                        TutorialManager.Instance().ShowFinger(btnPut);
                        TutorialManager.Instance().UnHightlightObject(targetObject);
                    }
                    else
                    {
                        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial13note2", "Tap to put"));
                        ActivateFinger(targetInterface, targetObject);

                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["interact"].sprites).hideWhenVisible = hide;
                    }

                    if (coffeeCup.coffee.isItCoffee) step = 2;
                    else if (!coffeBagInHands) step = 0;

                    yield return null;
                }
            }

            if (step == 2)//взять кружку
            {
                TutorialManager.Instance().UnHightlightAll();

                targetObject = coffeeCup.gameObject;
                targetInterface = typeof(InterfaceCollect);
                targetHeight = 0.2f;
                hide = false;

                while (step == 2)
                {
                    if (wrongItem)
                    {
                        TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
                        TutorialManager.Instance().ShowFinger(btnPut);
                        TutorialManager.Instance().UnHightlightObject(targetObject);
                    }
                    else
                    {
                        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial13note3", "Tap to put"));
                        ActivateFinger(targetInterface, targetObject);

                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["take"].sprites).hideWhenVisible = hide;
                    }

                    if (coffeCupInHands) step = 3;
                    else if (!coffeeCup.coffee.isItCoffee) step = 0;

                    yield return null;
                }
            }

            if (step == 3)//включить кофе
            {
                TutorialManager.Instance().UnHightlightAll();

                targetObject = switcher.gameObject;
                targetInterface = typeof(AbilityInterfaceApplyItem);
                targetHeight = 0.3f;
                hide = false;

                while (step == 3)
                {
                    if (wrongItem)
                    {
                        TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
                        TutorialManager.Instance().ShowFinger(btnPut);
                        TutorialManager.Instance().UnHightlightObject(targetObject);
                    }
                    else
                    {
                        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial13note4", "Tap to put"));
                        ActivateFinger(targetInterface, targetObject);

                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["milkon"].sprites).hideWhenVisible = hide;
                    }

                    if (teapot.isOn) step = 4;

                    yield return null;
                }
            }

            if (step == 4)//налить кофе
            {
                TutorialManager.Instance().UnHightlightAll();
                TutorialManager.Instance().HideFinger();

                targetObject = switcher.gameObject;
                targetInterface = typeof(AbilityInterfaceApplyItem);
                targetHeight = 0.3f;
                hide = false;

                while (step == 4)
                {
                    //if (wrongItem)
                    //{
                    //    TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
                    //    TutorialManager.Instance().ShowFinger(btnPut);
                    //    TutorialManager.Instance().UnHightlightObject(targetObject);
                    //}
                    //else
                    //{
                    //    TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial13note5", "Tap to put"));
                    //    ActivateFinger(targetInterface, targetObject);

                    //    TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["interact"].sprites).hideWhenVisible = hide;
                    //}

                    if (!coffeCupInHands)
                    {
                        if (!coffeeCup.coffee.isItCoffee) step = 0;
                        else step = 2;
                    }
                    else if (coffeeCup.coffee.level > 0.5f) step = 5;
                     
                    yield return null;
                }
            }

            if (step == 5)//выпить кофе
            {
                TutorialManager.Instance().UnHightlightAll();
                TutorialManager.Instance().HideFinger();

                Groups.Instance().SetGroupState("SwipeFinger", true);
                float prevLevel = need.level;
                //bool active = true;

                //bool canFinish = false;
                //PlayerController.instance.cam.transform.forward.y > 0.5f;

                while (step == 5)
                {
                    //if (active && need.level > prevLevel)
                    //{
                    //    active = false;
                    //    TutorialManager.Instance().SetGroupState("SwipeFinger", false);
                    //}

                    prevLevel = need.level;

                    if (need.level > 0.95f)
                    {
                        Debug.LogError("Finish at " + need.level);
                        step = 6;
                    }
                    else if (!coffeCupInHands)
                    {
                        if (!coffeeCup.coffee.isItCoffee) step = 0;
                        else step = 2;
                    }
                    else
                    {
                        if (!coffeeCup.coffee.isItCoffee)
                        {
                            wrongItem = true;
                            step = 0;
                        }
                    }

                    yield return null;
                }

                Groups.Instance().SetGroupState("SwipeFinger", false);
            }

            yield return null;
        }

        babyItem.busy = false;

        TutorialManager.Instance().UnHightlightAll();
        TutorialManager.Instance().HideFinger();

        for (int i = 0; i < coffeeCups.Count; i++) coffeeCups[i].gameObject.SetActive(true);

        need.setMarkers = true;

        while (true) yield return null;
    }

    private bool ActivateFinger<T>(GameObject go, bool setstate = true) where T : AbilityInterface
    {
        if (searcher.IsThereAnyOf<T>(go))
        {
            if (setstate) Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            if (setstate) Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }

    private bool ActivateFinger(System.Type type, GameObject go)
    {
        if (searcher.IsThereAnyOf(type, go))
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }
}
