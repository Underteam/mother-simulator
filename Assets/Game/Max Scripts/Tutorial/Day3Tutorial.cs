﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Day3Tutorial : Tutorial
{
    private AbilityInterfaceSearcher searcher;

    private int step = 0;

    private bool bottleInHands;
    private bool wrongItem = false;
    private bool babyOnHands;

    private TutorialManager tm;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public override void Init()
    {
        searcher = FindObjectOfType<AbilityInterfaceSearcher>();

        tm = TutorialManager.Instance();

        step = 0;
        bottleInHands = false;
        wrongItem = false;
        babyOnHands = false;

        //Debug.LogError("Tutorial day 3");

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            //Debug.LogError("Item taked " + i, i);

            if (step == 0)
            {
                var b = i.GetComponent<BabyBottle>();
                if (b != null)
                {
                    bottleInHands = true;
                    return;
                }
                else
                {
                    wrongItem = true;
                }
            }

            if (step == 5)
            {
                Baby b = i.GetComponent<Baby>();
                if (b != null)
                {
                    babyOnHands = true;
                    return;
                }
                else
                {
                    wrongItem = true;
                }
            }
        },
        "ItemTaked");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            babyOnHands = true;
        },
        "BabyAboutToTake");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            wrongItem = false;
            bottleInHands = false;
            babyOnHands = false;
        },
        "ItemDroped");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;
        },
        "WishSatisfied");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    public override void Deactivate()
    {
        OnDestroy();
    }

    private BabyBottle bottle;

    public override IEnumerator Job()
    {
        var player = PlayerController.instance;
        var baby = FindObjectOfType<Baby>();
        var babyItem = baby.GetComponent<Item>();
        babyItem.busy = true;

        while (TaskManager.Instance().currentTask.currentWish.done)
        {
            yield return null;
        }

        Wish w = TaskManager.Instance().currentTask.currentWish;
        while (w.box == null) yield return null;
        WishBox wb = w.box;
        wb.SetNumVisibleSublines(0);

        yield return new WaitForSeconds(0.5f);

        var bottles = FindObjectsOfType<BabyBottle>();
        for (int i = 1; i < bottles.Length; i++) bottles[i].gameObject.SetActive(false);
        bottle = bottles[0];// FindObjectOfType<BabyBottle>();
        var pacifiers = FindObjectsOfType<BottlePacifier>();
        for (int i = 1; i < pacifiers.Length; i++) pacifiers[i].gameObject.SetActive(false);
        var pacifier = pacifiers[0];// FindObjectOfType<BottlePacifier>();
        var waterTap = Groups.Instance().GetGroup("KitchenWaterTap")[0].GetComponent<Switcher>();
        var sink = Groups.Instance().GetGroup("KitchenSink")[0].GetComponent<Washbasin>();

        var ds1 = bottle.GetComponent<DirtyScript>();
        var ds2 = pacifier.GetComponent<DirtyScript>();

        var btnAct = Groups.Instance().GetGroup("Button Act")[0].GetComponent<RectTransform>();
        var btnDrop = Groups.Instance().GetGroup("Button Throw")[0].GetComponent<RectTransform>();
        var btnPut = Groups.Instance().GetGroup("Button Put")[0].GetComponent<RectTransform>();
        TutorialManager.Instance().SetFingerPos(btnAct);
        TutorialManager.Instance().SetLongFingerPos(btnDrop);

        yield return new WaitForSeconds(0.5f);

        TutorialManager.Instance().FocusOn(bottle.transform);

        GameObject targetObject = null;
        System.Type targetInterface = null;
        float targetHeight = 1f;
        bool hide = false;
        //Sprite sprite = null;
        int numLines = 1;

        while (step < 7)
        {
            if (step == 0)//взять бутылку
            {
                TutorialManager.Instance().UnHightlightAll();

                numLines = Mathf.Max(numLines, 1);
                wb.SetNumVisibleSublines(numLines);

                targetObject = bottle.gameObject;
                targetInterface = typeof(InterfaceCollect);
                targetHeight = 0.2f;
                hide = false;

                while (step == 0)
                {
                    if (wrongItem)
                    {
                        TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
                        TutorialManager.Instance().ShowFinger(btnPut);
                        TutorialManager.Instance().UnHightlightObject(targetObject);
                    }
                    else
                    {
                        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial03note1", "Tap to put"));
                        ActivateFinger(targetInterface, targetObject);

                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["take"].sprites).hideWhenVisible = hide;
                    }

                    if (bottleInHands) step = 1;

                    yield return null;
                }
            } 

            if (step == 1)//помыть бутылку
            {
                TutorialManager.Instance().UnHightlightAll();
                TutorialManager.Instance().HideFinger();

                numLines = Mathf.Max(numLines, 2);
                wb.SetNumVisibleSublines(numLines);

                var water = waterTap.transform.parent.GetComponentInChildren<AbilityInterfaceApplyItem>(true);

                targetObject = waterTap.gameObject;
                targetInterface = typeof(AbilityInterfaceApplyItem);
                targetHeight = 0.25f;
                hide = false;

                while (step == 1)
                {
                    TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial03note6", "Tap to put"));
                    ActivateFinger(targetInterface, targetObject);

                    TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["wateron"].sprites).hideWhenVisible = hide;

                    if (!bottleInHands) step = 0;
                    else if (bottle.IsClean()) step = 2;

                    yield return null;
                }
            }

            if (step == 2)//налить молоко
            {
                TutorialManager.Instance().UnHightlightAll();
                TutorialManager.Instance().HideFinger();

                numLines = Mathf.Max(numLines, 3);
                wb.SetNumVisibleSublines(numLines);

                var milkpot = FindObjectOfType<Milkpot>();
                var switcher = milkpot.GetComponentInChildren<AbilityInterfaceApplyItem>();

                targetObject = switcher.gameObject;
                targetInterface = typeof(AbilityInterfaceApplyItem);
                targetHeight = 0.4f;
                hide = false;

                while (step == 2)
                {
                    TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial03note3", "Tap to put"));
                    ActivateFinger(targetInterface, targetObject);

                    TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["milkon"].sprites).hideWhenVisible = hide;

                    if (bottle.withFood) step = 3;
                    else if (!bottleInHands) step = 0;

                    yield return null;
                }
            }

            if (step == 3)//отдать бутылку
            {
                TutorialManager.Instance().UnHightlightAll();
                TutorialManager.Instance().HideFinger();

                numLines = Mathf.Max(numLines, 4);
                wb.SetNumVisibleSublines(numLines);

                targetObject = baby.gameObject;
                targetInterface = typeof(AbilityInterfaceApplyItem);
                targetHeight = 1.1f;
                hide = true;

                var bottleItem = bottle.GetComponent<Item>();

                TutorialManager.Instance().FocusOn(baby.transform, true);

                while (step == 3)
                {
                    if (wrongItem)
                    {
                        TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
                        TutorialManager.Instance().ShowFinger(btnPut);
                        TutorialManager.Instance().UnHightlightObject(targetObject);
                    }
                    else
                    {
                        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial03note7", "Tap to put"));
                        ActivateFinger(targetInterface, targetObject);

                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["interact"].sprites).hideWhenVisible = hide;
                    }
                    
                    if (!bottleInHands) step = 0;
                    else if (!bottle.IsAppropriateForUse()) step = 1;

                    if (bottleItem.busy) step = 4;

                    yield return null;
                }
            }

            if (step == 4)
            {
                babyItem.busy = false;

                TutorialManager.Instance().UnHightlightAll();
                TutorialManager.Instance().HideFinger();

                while (TaskManager.Instance().currentTask.currentWish == null || !TaskManager.Instance().currentTask.currentWish.done) yield return null;//ждем завершение текущего

                while (TaskManager.Instance().currentTask.currentWish == null || TaskManager.Instance().currentTask.currentWish.done) yield return null;//ждем начала следующего

                step = 5;
            }

            if (step == 5)//взять пацана
            {
                TutorialManager.Instance().UnHightlightAll();
                TutorialManager.Instance().HideFinger();

                targetObject = Baby.instance.gameObject;
                targetInterface = typeof(InterfaceCollect);
                targetHeight = 1.1f;
                hide = true;

                while (step == 5)
                {
                    {
                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["take"].sprites).hideWhenVisible = hide;
                    }

                    if (babyOnHands)
                    {
                        //if (table != null) TutorialManager.Instance().FocusOn(table.transform);
                        step = 6;
                    }

                    yield return null;
                }
            }

            if (step == 6)//положить на стол
            {
                TutorialManager.Instance().UnHightlightAll();
                TutorialManager.Instance().HideFinger();

                var table = FindObjectOfType<ChangingTable>();

                targetObject = table.gameObject;
                targetInterface = typeof(AbilityInterfaceApplyItem);
                targetHeight = 1.7f;
                hide = false;

                while (step == 6)
                {
                    {
                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["interact"].sprites).hideWhenVisible = hide;
                    }

                    if (table.baby != null)
                    {
                        step = 7;
                    }
                    else if (!babyOnHands) step = 5;

                    yield return null;
                }
            }

            yield return null;
        }

        TutorialManager.Instance().UnHightlightAll();
        TutorialManager.Instance().HideFinger();

        while (true) yield return null;
    }

    private bool ActivateFinger<T>(GameObject go, bool setstate = true) where T : AbilityInterface
    {
        if (searcher.IsThereAnyOf<T>(go))
        {
            if (setstate) Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            if (setstate) Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }

    private bool ActivateFinger(System.Type type, GameObject go)
    {
        if (searcher.IsThereAnyOf(type, go))
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }
}
