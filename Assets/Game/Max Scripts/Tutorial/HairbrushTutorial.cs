﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HairbrushTutorial : Tutorial
{
    private AbilityInterfaceSearcher searcher;

    private int step = 0;

    public List<Sprite> sprites;

    private bool wrongItem;
    private bool brushInHands;

    public Sprite daughterImage;

    public override void Init()
    {
        searcher = FindObjectOfType<AbilityInterfaceSearcher>();

        step = 0;
        brushInHands = false;
        wrongItem = false;

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;
        },
        "TaskStarted");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            Hairbrush h = i.GetComponent<Hairbrush>();
            if (h != null)
            {
                brushInHands = true;

                return;
            }
            else
            {
                if (step == 0) wrongItem = true;
            }
        },
        "ItemTaked");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            wrongItem = false;
            brushInHands = false;
        },
        "ItemDroped");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;
        },
        "WishSatisfied");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    public override void Deactivate()
    {
        OnDestroy();
    }

    void Start()
    {

    }

    public override IEnumerator Job()
    {
        yield return new WaitForSeconds(0.5f);

        var baby = FindObjectOfType<Baby>();
        var babyItem = baby.GetComponent<Item>();
        babyItem.busy = true;

        var need = TaskManager.Instance().GetNeed<DaughterMood>();
        need.level = 0.22f;
        var daughter = FindObjectOfType<Daughter>();
        daughter.angryTimer = 0.1f;

        TutorialManager.Instance().ShowGreeting(Localizer.GetString("tutorial21message1", "Привет, дорогая! Я вернулся домой из командировки!"), daughterImage);

        yield return new WaitForSeconds(0.1f);

        var player = PlayerController.instance;

        var btnAct = Groups.Instance().GetGroup("Button Act")[0].GetComponent<RectTransform>();
        var btnDrop = Groups.Instance().GetGroup("Button Throw")[0].GetComponent<RectTransform>();
        var btnPut = Groups.Instance().GetGroup("Button Put")[0].GetComponent<RectTransform>();
        TutorialManager.Instance().SetFingerPos(btnAct);

        Hairbrush brush = FindObjectOfType<Hairbrush>();

        GameObject targetObject = null;
        System.Type targetInterface = null;
        float targetHeight = 1f;
        bool hide = false;

        TutorialManager tm = TutorialManager.Instance();

        while (step < 2)
        {
            if (step == 0)//взять расческу
            {
                TutorialManager.Instance().UnHightlightAll();

                targetObject = brush.gameObject;
                targetInterface = typeof(InterfaceCollect);
                targetHeight = 0.2f;
                hide = false;

                while (step == 0)
                {
                    if (wrongItem)
                    {
                        TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
                        TutorialManager.Instance().ShowFinger(btnPut);
                        TutorialManager.Instance().UnHightlightObject(targetObject);
                    }
                    else
                    {
                        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial21note1", "Tap to put"));
                        ActivateFinger(targetInterface, targetObject);

                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["take"].sprites).hideWhenVisible = hide;
                    }

                    if (brushInHands)
                    {
                        step = 1;
                    }

                    yield return null;
                }
            }

            if (step == 1)//сделать прическу
            {
                TutorialManager.Instance().UnHightlightAll();

                targetObject = daughter.gameObject;
                targetInterface = typeof(AbilityInterfaceApplyItem);
                targetHeight = 1.8f;
                hide = false;

                while (step == 1)
                {
                    {
                        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial21note2", "Tap to put"));
                        ActivateFinger(targetInterface, targetObject);

                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["take"].sprites).hideWhenVisible = hide;
                    }

                    if (daughter.changing)
                    {
                        step = 2;
                    }
                    else if (!brushInHands)
                    {
                        step = 0;
                    }

                    yield return null;
                }
            }

            yield return null;
        }

        babyItem.busy = false;

        TutorialManager.Instance().HideFinger();
        TutorialManager.Instance().UnHightlightAll();

        while (true) yield return null;
    }

    private bool ActivateFinger<T>(GameObject go) where T : AbilityInterface
    {
        if (searcher.IsThereAnyOf<T>(go))
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }

    private bool ActivateFinger(System.Type type, GameObject go)
    {
        if (searcher.IsThereAnyOf(type, go))
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }
}
