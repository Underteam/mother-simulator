﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Day35Tutorial : Tutorial
{
    private AbilityInterfaceSearcher searcher;

    private int step = 0;

    private bool vacuumInHands;

    public List<Sprite> sprites;

    private DogMood need;

    public override void Init()
    {
        searcher = FindObjectOfType<AbilityInterfaceSearcher>();

        step = 0;
        vacuumInHands = false;

        TaskManager.Instance().displayWishes = true;

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;
            var dog = FindObjectOfType<Dog>();
            dog.Sleep();
            need = TaskManager.Instance().GetNeed<DogMood>();
            need.setMarkers = false;
        },
        "TaskStarted");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            var v = i.GetComponent<Vacuum>();
            if (v != null)
            {
                vacuumInHands = true;
                if (step == 0 && carpet != null) TutorialManager.Instance().FocusOn(carpet.transform, true);
                return;
            }
        },
        "ItemTaked");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            var v = i.GetComponent<Vacuum>();
            if (v != null)
            {
                vacuumInHands = false;
                return;
            }
        },
        "ItemDroped");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    public override void Deactivate()
    {
        OnDestroy();
    }

    DogCarpet carpet;
    public Sprite dogImage;

    public override IEnumerator Job()
    {
        var baby = FindObjectOfType<Baby>();
        var babyItem = baby.GetComponent<Item>();
        //babyItem.busy = true;

        yield return new WaitForSeconds(0.5f);

        TutorialManager.Instance().ShowGreeting(Localizer.GetString("tutorial35message1", "Гав-гав"), dogImage);

        var vacuum = FindObjectOfType<Vacuum>();
        carpet = FindObjectOfType<DogCarpet>();

        var btnAct = Groups.Instance().GetGroup("Button Act")[0].GetComponent<RectTransform>();
        var btnDrop = Groups.Instance().GetGroup("Button Throw")[0].GetComponent<RectTransform>();
        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial35note1", "Tap to take"));
        TutorialManager.Instance().SetFingerPos(btnAct);
        TutorialManager.Instance().SetLongFingerPos(btnDrop);

        yield return new WaitForSeconds(0.5f);

        TutorialManager.Instance().FocusOn(vacuum.transform);

        while (step == 0 || step == 1)
        {
            if (vacuumInHands) step = 1;
            else step = 0;

            if (step == 0)
            {
                TutorialManager.Instance().UnHightlightObject(carpet.gameObject);
                TutorialManager.Instance().HightlightObject(vacuum.gameObject, 2f, TutorialManager.Instance().icons["take"].sprites);
                ActivateFinger<InterfaceCollect>(vacuum.gameObject);
            }
            else
            {
                TutorialManager.Instance().HideFinger();
                TutorialManager.Instance().UnHightlightObject(vacuum.gameObject);
                TutorialManager.Instance().HightlightObject(carpet.gameObject, 0.5f, sprites);
            }

            if (carpet.furs == null) step = 2;

            yield return null;
        }

        babyItem.busy = false;

        TutorialManager.Instance().UnHightlightAll();
        TutorialManager.Instance().HideFinger();

        yield return new WaitForSeconds(3f);

        need.setMarkers = true;

        while (true) yield return null;
    }

    private bool ActivateFinger<T>(GameObject go) where T : AbilityInterface
    {
        if (searcher.IsThereAnyOf<T>(go))
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }
}
