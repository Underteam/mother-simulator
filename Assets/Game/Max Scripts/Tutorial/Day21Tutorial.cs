﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Day21Tutorial : Tutorial
{
    private AbilityInterfaceSearcher searcher;

    private int step = 0;

    private bool woodInHands;
    private bool babyInHands;

    public bool log;

    private NeedForFire need;

    private TutorialManager tm;

    public override void Init()
    {
        searcher = FindObjectOfType<AbilityInterfaceSearcher>();

        tm = TutorialManager.Instance();

        step = 0;
        woodInHands = false;
        babyInHands = false;

        TaskManager.Instance().displayWishes = true;

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;
            need = TaskManager.Instance().GetNeed<NeedForFire>();
            need.level = 1f;
            need.instantSet = true;
            need.setMarker = false;
        },
        "TaskStarted");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            var b = i.GetComponent<Baby>();
            if (b != null)
            {
                babyInHands = true;
                return;
            }

            var w = i.GetComponent<Wood>();
            if (w != null)
            {
                woodInHands = true;
                return;
            }
        },
        "ItemTaked");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            babyInHands = false;
            woodInHands = false;
        },
        "ItemDroped");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    public override void Deactivate()
    {
        OnDestroy();
    }

    public override IEnumerator Job()
    {
        var baby = FindObjectOfType<Baby>();
        var babyItem = baby.GetComponent<Item>();
        //babyItem.busy = true;

        var fireplace = FindObjectOfType<Fire>();
        fireplace.scale = 0.11f;

        yield return new WaitForSeconds(0.5f);

        var woods = FindObjectOfType<Woodpile>();
        var wood = FindObjectOfType<Wood>();

        RectTransform rt = Groups.Instance().GetGroup("Button Throw")[0].GetComponent<RectTransform>();

        var btnAct = Groups.Instance().GetGroup("Button Act")[0].GetComponent<RectTransform>();
        var btnDrop = Groups.Instance().GetGroup("Button Throw")[0].GetComponent<RectTransform>();

        TutorialManager.Instance().SetFingerPos(btnAct);

        while (step == 0 || step == 1)
        {
            if (step == 0)
            {
                if (woodInHands) step = 1;
            }
            else if (step == 1)
            {
                if (!woodInHands)
                {
                    yield return new WaitForSeconds(0.5f);
                    if (fireplace.firewoods.Count > 0) step = 2;
                    else if (!woodInHands) step = 0;
                }
            }

            if (step == 0)
            {
                TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial25note1", "Tap to take"));
                TutorialManager.Instance().UnHightlightObject(fireplace.gameObject);
                TutorialManager.Instance().HightlightObject(woods.gameObject, 0.5f, tm.icons["take"].sprites);
                bool activate = ActivateFinger<InterfaceProvider>(woods.gameObject, false);
                activate = activate || ActivateFinger<InterfaceCollect>(wood.gameObject, false);
                Groups.Instance().SetGroupState("Finger", activate);
            }
            else if (step == 1)
            {
                TutorialManager.Instance().finger.Set(btnDrop, Localizer.GetString("tutorial25note2", "Hold to throw"));
                TutorialManager.Instance().UnHightlightObject(woods.gameObject);
                TutorialManager.Instance().HightlightObject(fireplace.gameObject, 0.5f, tm.icons["aim"].sprites);
                if (ActivateFinger<InterfaceInfo>(fireplace.gameObject, false))
                {
                    TutorialManager.Instance().ShowFinger(rt);
                }
                else
                {
                    TutorialManager.Instance().HideFinger();
                }
            }

            if (fireplace.firewoods.Count > 0) step = 2;

            yield return null;
        }

        babyItem.busy = false;

        TutorialManager.Instance().UnHightlightAll();
        TutorialManager.Instance().HideFinger();

        need.setMarker = true;

        yield return new WaitForSeconds(1f);

        while (true) yield return null;
    }

    private bool ActivateFinger<T>(GameObject go, bool setstate = true) where T : AbilityInterface
    {
        if (searcher.IsThereAnyOf<T>(go))
        {
            if (setstate) Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            if (setstate) Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }
}
