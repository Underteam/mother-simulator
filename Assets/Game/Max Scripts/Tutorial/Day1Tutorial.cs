﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GMATools.Common;

public class Day1Tutorial : Tutorial
{
    public bool log;

    private AbilityInterfaceSearcher searcher;

    private class Blocker : IBlocker
    {
        public bool blocked { get; set; }
    }

    private Blocker blocker = new Blocker();
    private SwipeLook looker;

    private Baby baby;
    private bool babyInHands;
    private bool wrongItem;
    private bool pacifierInHands;

    private TutorialManager tm;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public override void Init()
    {
        looker = FindObjectOfType<SwipeLook>();

        searcher = FindObjectOfType<AbilityInterfaceSearcher>();

        tm = TutorialManager.Instance();

        baby = Baby.instance;
        baby.GetComponent<Item>().busy = true;

        wrongItem = false;
        babyInHands = false;
        pacifierInHands = false;

        step = 0;

        //TaskManager.Instance().displayWishes = false;

        Groups.Instance().SetGroupState("Button Throw", false);
        //TutorialManager.Instance().SetGroupState("Button Put", false);

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            if (step == 0)
            {
                Pacifier p = i.GetComponent<Pacifier>();
                if (p != null)
                {
                    pacifierInHands = true;
                    visiblePacifier = p.gameObject;
                    return;
                }
                else
                {
                    wrongItem = true;
                }
            }

            if (step == 5)
            {
                var b = i.GetComponent<Baby>();
                if (b != null)
                {
                    babyInHands = true;
                    return;
                }
            }
        },
        "ItemTaked");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            babyInHands = true;
        },
        "BabyAboutToTake");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            wrongItem = false;
            babyInHands = false;
            pacifierInHands = false;
        },
        "ItemDroped");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            if (step == 3)
            {
                step = 4;
            }

            if (step == 5)
            {
                step = 6;
            }
        },
        "WishSatisfied");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    public override void Deactivate()
    {
        OnDestroy();
    }

    private int _step = 0;
    private int prevStep = -1;
    private int step
    {
        get { return _step; }
        set
        {
            _step = value;

            if (_step > prevStep)
            {
                prevStep = _step;
                Analytics.Instance().SendEvent("Task1Step" + _step);
            }
        }
    }

    private GameObject visiblePacifier;

    private Coroutine coroutine;
    private void FocusOn (Transform target, bool yaxis = false)
    {
        if (coroutine != null) PlayerController.instance.StopCoroutine(coroutine);
        coroutine = PlayerController.instance.StartCoroutine(FocusOnCoroutine(target, yaxis));
    }

    public string info;
    private IEnumerator FocusOnCoroutine (Transform target, bool yaxis)
    {
        if (!looker.blockers.Contains(blocker)) looker.blockers.Add(blocker);
        blocker.blocked = true;

        var fps = PlayerController.instance.GetComponent<FPSCharacterController>();
        fps.smooth = false;
        Transform cam = fps.cam.transform;

        yield return null;

        float maxSpeed = 4f;

        while (true)
        {
            Vector3 dir = target.position - cam.transform.position;
            float dot = Vector3.Dot(dir, cam.forward);
            dir -= dot * cam.forward;
            float xspeed = Vector3.Dot(dir, cam.right);
            float yspeed = Vector3.Dot(dir, Vector3.up);

            if (dot < 0)
            {
                yspeed = 0;
                xspeed = Mathf.Sign(xspeed) * maxSpeed;
            }

            info = xspeed + " " + yspeed;

            if (Mathf.Abs(xspeed) > maxSpeed) xspeed = Mathf.Sign(xspeed) * maxSpeed;
            if (Mathf.Abs(yspeed) > maxSpeed) yspeed = Mathf.Sign(yspeed) * maxSpeed;

            InputManager.Instance().SetAxis(looker.horizontalAxisName, xspeed);
            if (yaxis) InputManager.Instance().SetAxis(looker.verticalAxisName, yspeed);
            else yspeed = 0;

            if (Mathf.Abs(xspeed) < 0.05f && Mathf.Abs(yspeed) < 0.05f) break;

            yield return null;
        }

        blocker.blocked = false;
        InputManager.Instance().SetAxis(looker.horizontalAxisName, 0);
        InputManager.Instance().SetAxis(looker.verticalAxisName, 0);
        fps.smooth = true;

        coroutine = null;
    }

    private WishBox wb;

    public override IEnumerator Job()
    {
        var player = PlayerController.instance;

        Wish w = TaskManager.Instance().currentTask.currentWish;
        while (w.box == null) yield return null;
        wb = w.box;
        //wb.InsertSubline(1, Localizer.GetString("tutorial01short2", "Включить воду"));
        wb.SetNumVisibleSublines(0);

        yield return new WaitForSeconds(0.5f);

        {
            wb.SetNumVisibleSublines(1);
        };


        List<Pacifier> pacifiers = new List<Pacifier>();

        var btnAct = Groups.Instance().GetGroup("Button Act")[0].GetComponent<RectTransform>();
        var btnPut = Groups.Instance().GetGroup("Button Put")[0].GetComponent<RectTransform>();
        var btnDrop = Groups.Instance().GetGroup("Button Throw")[0].GetComponent<RectTransform>();


        if (!looker.blockers.Contains(blocker)) looker.blockers.Add(blocker);
        blocker.blocked = true;
        PlayerController.instance.GetComponent<FPSCharacterController>().smooth = false;

        step = -1;

        while (visiblePacifier == null)
        {
            if (pacifiers.Count == 0)
            {
                pacifiers.AddRange(FindObjectsOfType<Pacifier>());
            }

            visiblePacifier = null;

            for (int i = 0; i < pacifiers.Count; i++)
            {
                if (pacifiers[i] == null)
                {
                    pacifiers.RemoveAt(i);
                    i--;
                }
                else
                {
                    Vector3 dir = pacifiers[i].transform.position - player.transform.position;
                    dir.y = 0;
                    dir.Normalize();
                    float dot = Vector3.Dot(dir, player.transform.forward);
                    if (dot > 0.9f)
                    {
                        visiblePacifier = pacifiers[i].gameObject;
                        FocusOn(visiblePacifier.transform);
                    }
                }
            }

            if (visiblePacifier == null)
            {
                InputManager.Instance().SetAxis(looker.horizontalAxisName, 4f);
            }

            yield return null;
        }

        step = 0;

        var waterTap = Groups.Instance().GetGroup("KitchenWaterTap")[0];
        var water = waterTap.transform.parent.GetComponentInChildren<Water>(true);
        //System.Func<bool> Checker = () =>
        //{
        //    return water.gameObject.activeInHierarchy;
        //};

        GameObject targetObject = null;
        System.Type targetInterface = null;
        float targetHeight = 1f;
        bool hide = false;

        int numLines = 1;

        while (step < 4)
        {
            if (step == 0)//взять соску
            {
                TutorialManager.Instance().UnHightlightAll();
                TutorialManager.Instance().HideFinger();

                numLines = Mathf.Max(numLines, 1);
                wb.SetNumVisibleSublines(numLines);

                targetObject = visiblePacifier;
                targetInterface = typeof(InterfaceCollect);
                targetHeight = 0.15f;
                hide = true;

                while (step == 0)
                {
                    if (wrongItem)
                    {
                        TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
                        TutorialManager.Instance().ShowFinger(btnPut);
                        TutorialManager.Instance().UnHightlightObject(targetObject);
                    }
                    else
                    {
                        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial01note1", "Tap to put"));
                        ActivateFinger(targetInterface, targetObject);

                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["take"].sprites).hideWhenVisible = hide;
                    }

                    if (pacifierInHands)
                    {
                        step = 1;
                    }

                    yield return null;
                }
            }

            if (step == 1)//включить воду
            {
                TutorialManager.Instance().UnHightlightAll();
                TutorialManager.Instance().HideFinger();

                numLines = Mathf.Max(numLines, 2);
                wb.SetNumVisibleSublines(numLines);

                var switcher = waterTap.GetComponentInChildren<Switcher>();

                //var checker = w.GetComponent<PacifierChecker>();
                
                //if (!checker.stepsCheckers.Contains(Checker)) checker.stepsCheckers.Insert(1, Checker);

                TutorialManager.Instance().HightlightObject(waterTap, 0.3f, tm.icons["wateron"].sprites).hideWhenVisible = false;

                TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial01note3", "Tap to switch on"));

                //FocusOn(switcher.transform, true);

                while (step == 1)
                {
                    ActivateFinger<AbilityInterfaceApplyItem>(switcher.gameObject);

                    if (water.gameObject.activeInHierarchy) step = 2;
                    if (!pacifierInHands) step = 0;

                    yield return null;
                }
            }

            if (step == 2)//помыть соску
            {
                TutorialManager.Instance().UnHightlightAll();
                TutorialManager.Instance().HideFinger();

                numLines = Mathf.Max(numLines, 3);
                wb.SetNumVisibleSublines(numLines);

                //TutorialManager.Instance().HightlightObject(water.gameObject, 0.15f, tm.icons["wash"].sprites).hideWhenVisible = false;

                DirtyScript ds = null;
                TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial01note3", "Hold to wash"));

                //FocusOn(water.transform, true);

                while (step == 2)
                {
                    if (visiblePacifier == null) step = 0;
                    else if (ds == null) ds = visiblePacifier.GetComponent<DirtyScript>();

                    ActivateFinger<AbilityInterfaceApplyItem>(water.gameObject);

                    if (ds != null && ds.isClean) step = 3;

                    if (!water.gameObject.activeInHierarchy) step = 1;

                    if (!pacifierInHands) step = 0;

                    yield return null;
                }
            }

            if (step == 3)//отдать соску
            {
                TutorialManager.Instance().UnHightlightAll();
                TutorialManager.Instance().HideFinger();

                numLines = Mathf.Max(numLines, 4);
                wb.SetNumVisibleSublines(numLines);

                TutorialManager.Instance().HightlightObject(baby.gameObject, 1.1f, tm.icons["interact"].sprites).hideWhenVisible = true;

                TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial01note4", "Tap to give"));

                FocusOn(baby.transform, true);

                while (step == 3)
                {
                    ActivateFinger<AbilityInterfaceApplyItem>(baby.gameObject);

                    if (!visiblePacifier.activeSelf) step = 4;
                    else if (!pacifierInHands) step = 0;

                    yield return null;
                }
            }

            yield return null;
        }

        //Debug.LogError ("Done " + step);

        TutorialManager.Instance().UnHightlightAll();
        TutorialManager.Instance().HideFinger();

        TaskManager.Instance().CompleteWish(0);

        Cot cot = FindObjectOfType<Cot>();

        //Debug.LogError("Step " + step);

        while (step == 4)
        {
            if (TaskManager.Instance().currentTask.currentWish.eventName.Equals("Sleep")) step = 5;
            yield return null;
        }

        baby.GetComponent<Item>().busy = false;

        //Debug.LogError("Step " + step);

        while (step == 5)
        {
            if (!babyInHands)
            {
                TutorialManager.Instance().UnHightlightObject(cot.gameObject);
                TutorialManager.Instance().HightlightObject(baby.gameObject, 1.1f, tm.icons["interact"].sprites).hideWhenVisible = true;
                TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial01note5", "Tap to put"));
                ActivateFinger<InterfaceCollect>(baby.gameObject);
            }
            else
            {
                TutorialManager.Instance().UnHightlightObject(baby.gameObject);
                TutorialManager.Instance().HightlightObject(cot.gameObject, 1.1f, tm.icons["interact"].sprites).hideWhenVisible = false;
                TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial01note6", "Tap to put"));
                ActivateFinger<AbilityInterfaceApplyItem>(cot.gameObject);
            }

            yield return null;
        }

        TutorialManager.Instance().UnHightlightAll();
        TutorialManager.Instance().HideFinger();

        while (true) yield return null;
    }

    private bool ActivateFinger(GameObject go, PlayerController player, float d, float h = 0f)
    {
        Vector3 dir = go.transform.position + h * Vector3.up - player.cam.transform.position;
        float dist = dir.magnitude;
        dir.Normalize();
        float dot = Vector3.Dot(dir, player.cam.transform.forward);

        if (dot > d && dist < 2.5f)
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }

    private bool ActivateFinger<T>(GameObject go) where T : AbilityInterface
    {
        if (searcher.IsThereAnyOf<T>(go))
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }

    private bool ActivateFinger(System.Type type, GameObject go)
    {
        if (searcher.IsThereAnyOf(type, go))
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }
}
