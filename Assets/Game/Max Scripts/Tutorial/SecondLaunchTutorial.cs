﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondLaunchTutorial : Tutorial
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public override void Init()
    {
        Debug.LogWarning("Second Launch Tutorial");

        Groups.Instance().SetGroupState("StartHint", false);
    }

    public override IEnumerator Job()
    {
        Debug.LogWarning("Switch on hint");

        Groups.Instance().SetGroupState("ShopHint", true);

        var shop = GameController.Instance().shop;

        var wallet = Wallet.GetWallet(Wallet.CurrencyType.crystals);
        
        var excl = FindObjectOfType<ShopExclamation>();
        if (excl != null) excl.Check(wallet);

        Debug.LogWarning("Wait");

        while (!shop.shopPanel.gameObject.activeSelf) yield return null;

        int am = wallet.amount;

        Groups.Instance().GetGroup("ShopHint")[0].SetActive(false);

        while (am == wallet.amount) yield return null;

        PlayerPrefs.SetInt("FirstLaunch", 0);

        Groups.Instance().SetGroupState("ShopHint", false);

        if (excl != null) excl.Check(wallet);

        while (shop.gameObject.activeSelf) yield return null;

        yield return new WaitForSeconds(5f);

        Groups.Instance().SetGroupState("StartHint", true);

        var list = Groups.Instance().GetGroup("StartPanel");
        var panel = list[0];

        while (!panel.activeSelf)
        {
            if (panel.activeSelf)
            {
                Groups.Instance().SetGroupState("StartHint", false);
            }

            yield return null;
        }

        Groups.Instance().SetGroupState("StartHint", false);

        while (true) yield return null;
    }
}
