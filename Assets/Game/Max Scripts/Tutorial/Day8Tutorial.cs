﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Day8Tutorial : Tutorial
{
    private AbilityInterfaceSearcher searcher;

    private int step = 0;

    private NeedForHygien need;

    public override void Init()
    {
        searcher = FindObjectOfType<AbilityInterfaceSearcher>();

        step = 0;

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;
            need = TaskManager.Instance().GetNeed<NeedForHygien>();
            need.level = 0.099f;
            need.instantSet = true;
        },
        "TaskStarted");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    public override void Deactivate()
    {
        OnDestroy();
    }

    void Start()
    {

    }

    public override IEnumerator Job()
    {
        yield return new WaitForSeconds(0.5f);

        bool wait = true;

        var player = PlayerController.instance;
        var needBar = TaskManager.Instance().GetBar<NeedForHygien>();
        //var anim = needBar.gameObject.AddComponent<ScaleAnimation>();

        TutorialManager.Instance().tutorialPanel.collapse = true;

        var message = Localizer.GetString("tutorial08message1", "Грязные предметы пачкают руки. Не забывайте мыть руки водой, иначе сами будете пачкать всё вокруг.");
        TutorialManager.Instance().ShowMessage(message);
        TutorialManager.Instance().tutorialPanel.onHided = () => wait = false;
        TutorialManager.Instance().tutorialPanel.collapse = false;

        wait = true;

        while (wait) yield return null;

        var waterTap = Groups.Instance().GetGroup("KitchenWaterTap")[0];
        var water = waterTap.transform.parent.GetComponentInChildren<Water>(true);
        TutorialManager.Instance().HightlightObject(waterTap, 0.5f, null);

        var btnAct = Groups.Instance().GetGroup("Button Act")[0].GetComponent<RectTransform>();
        var btnDrop = Groups.Instance().GetGroup("Button Throw")[0].GetComponent<RectTransform>();
        TutorialManager.Instance().SetFingerPos(btnAct);
        TutorialManager.Instance().SetLongFingerPos(btnAct);

        yield return new WaitForSeconds(0.5f);

        TutorialManager.Instance().FocusOn(waterTap.transform);

        while (step == 0)
        {
            if (!water.gameObject.activeInHierarchy)
            {
                TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial08note1", "Tap to switch on"));
                ActivateFinger<Switcher>(waterTap.gameObject);
            }
            else
            {
                TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial08note2", "Hold to wash"));
                ActivateFinger<AbilityInterfaceApplyItem>(water.gameObject);
            }

            if (need.level > 0.5f) step = 1;

            yield return null;
        }

        Groups.Instance().SetGroupState("LongFinger", false);
        Groups.Instance().SetGroupState("Finger", false);
        TutorialManager.Instance().UnHightlightAll();

        //anim.Delete();

        while (true) yield return null;
    }

    private bool ActivateFinger(GameObject go, PlayerController player, float d, float h = 0f, bool setstate = true)
    {
        Vector3 dir = go.transform.position + h * Vector3.up - player.cam.transform.position;
        float dist = dir.magnitude;
        dir.Normalize();
        float dot = Vector3.Dot(dir, player.cam.transform.forward);

        if (dot > d && dist < 2.5f)
        {
            if (setstate) Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            if (setstate) Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }

    private bool ActivateFinger<T>(GameObject go) where T : AbilityInterface
    {
        if (searcher.IsThereAnyOf<T>(go))
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }
}
