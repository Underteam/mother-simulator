﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Day11Tutorial : Tutorial
{
    private AbilityInterfaceSearcher searcher;

    private int _step = 0;
    private int prevStep = -1;
    private int step
    {
        get { return _step; }
        set
        {
            _step = value;

            if (_step > prevStep)
            {
                prevStep = _step;
                Analytics.Instance().SendEvent("Task11Step" + _step);
            }
        }
    }

    private Toy toy;
    private Baby baby;

    private bool toyInHands;
    private bool wrongItem;
    private bool bottleInHands;
    private bool babyInHands;

    private TutorialManager tm;

    public override void Init()
    {
        searcher = FindObjectOfType<AbilityInterfaceSearcher>();

        tm = TutorialManager.Instance();

        step = 0;
        toyInHands = false;
        wrongItem = false;
        bottleInHands = false;
        babyInHands = false;

        //TaskManager.Instance().displayWishes = false;
        baby = Baby.instance;
        baby.GetComponent<Item>().busy = true;

        Groups.Instance().SetGroupState("Button Throw", false);

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            if (step == 0)
            {
                var t = i.GetComponent<Toy>();
                if (t != null)
                {
                    if (t == toy)
                    {
                        TutorialManager.Instance().UnHightlightObject(toy.gameObject);
                        TutorialManager.Instance().HightlightObject(baby.gameObject, 1.1f, tm.icons["interact"].sprites).hideWhenVisible = true;
                        toyInHands = true;
                        if(wb != null) wb.SetNumVisibleSublines(2);
                    }
                    else
                    {
                        wrongItem = true;
                    }

                    return;
                }

                wrongItem = true;
            }

            if (step == 3)
            {
                var b = i.GetComponent<Baby>();
                if (b != null)
                {
                    babyInHands = true;

                    return;
                }

                wrongItem = true;
            }

            if (step == 5)
            {
                var b = i.GetComponent<BabyBottle>();
                if (b != null)
                {
                    bottleInHands = true;
                    return;
                }
            }
        },
        "ItemTaked");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            babyInHands = true;
        },
        "BabyAboutToTake");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            toyInHands = false;
            wrongItem = false;
            babyInHands = false;
            bottleInHands = false;

            if (step == 0) TutorialManager.Instance().UnHightlightObject(baby.gameObject);
        },
        "ItemDroped");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            if (step == 0) step = 1;

            if (step == 1) step = 2;

            if (step == 3) step = 4;
        },
        "WishSatisfied");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    public override void Deactivate()
    {
        OnDestroy();
    }

    private WishBox wb;
    public override IEnumerator Job()
    {
        Wish w = TaskManager.Instance().currentTask.currentWish;
        while (w.box == null) yield return null;
        wb = w.box;
        wb.SetNumVisibleSublines(0);

        WishDrawer wd1 = Baby.instance.wishCanvas.GetComponent<WishDrawer>();
        WishDrawer wd2 = Baby.instance.wishCanvasLie.GetComponent<WishDrawer>();

        var toyWish = TaskManager.Instance().GetWish<ToyWish>()[0];
        if (toyWish != null)
        {
            toyWish.SetToy(GameObject.Find("ToyBear2").GetComponent<Toy>());
            toy = toyWish.toy;
            wd1.SetWish(toyWish);
            wd2.SetWish(toyWish);
        }

        yield return new WaitForSeconds(0.5f);

        { 
            wb.SetNumVisibleSublines(1);
        };

        yield return new WaitForSeconds(0.5f);

        TutorialManager.Instance().FocusOn(toy.transform);

        var btnAct = Groups.Instance().GetGroup("Button Act")[0].GetComponent<RectTransform>();
        var btnPut = Groups.Instance().GetGroup("Button Put")[0].GetComponent<RectTransform>();

        while (step == 0)
        {
            if (!toyInHands)
            {
                if (toyWish == null) toyWish = TaskManager.Instance().GetWish<ToyWish>()[0];
                else if (toy == null)
                {
                    if (toyWish != null)
                    {
                        toyWish.SetToy(GameObject.Find("ToyBear2").GetComponent<Toy>());
                        toy = toyWish.toy;
                        wd1.SetWish(toyWish);
                        wd2.SetWish(toyWish);
                    }
                }
                else
                {
                    if (wrongItem)
                    {
                        TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
                        TutorialManager.Instance().ShowFinger(btnPut);
                    }
                    else
                    {
                        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial11note1", "Tap to put"));
                        ActivateFinger<InterfaceCollect>(toy.gameObject);

                        TutorialManager.Instance().HightlightObject(toy.gameObject, 0.35f, tm.icons["take"].sprites).hideWhenVisible = false;
                    }
                }
            }
            else
            {
                TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial11note2", "Tap to put"));
                ActivateFinger<InterfaceCollect>(baby.gameObject);
            }

            yield return null;
        }

        TutorialManager.Instance().UnHightlightObject(toy.gameObject);
        TutorialManager.Instance().UnHightlightObject(baby.gameObject);
        TutorialManager.Instance().HideFinger();

        while (TaskManager.Instance().currentTask.currentWish.done) yield return null;

        wb = null;

        step = 0;
        toyWish = TaskManager.Instance().GetWish<ToyWish>()[1];
        if (toyWish != null)
        {
            toyWish.SetToy(GameObject.Find("Airplane").GetComponent<Toy>());
            toy = toyWish.toy;
            wd1.SetWish(toyWish);
            wd2.SetWish(toyWish);
        }

        if (PlayerController.instance.currentItem != null)
        {
            if (PlayerController.instance.currentItem.gameObject != toy.gameObject)
            {
                wrongItem = true;
                toyInHands = false;
            }
            else
            {
                wrongItem = false;
                toyInHands = true;
                TutorialManager.Instance().UnHightlightAll();
                TutorialManager.Instance().HightlightObject(baby.gameObject, 1.1f, tm.icons["interact"].sprites).hideWhenVisible = true;
            }
        }
        else
        {
            wrongItem = false;
            toyInHands = false;
        }

        while (step == 0)
        {
            if (!toyInHands)
            {
                if (toyWish == null) toyWish = TaskManager.Instance().GetWish<ToyWish>()[1];
                else if (toy == null)
                {
                    if (toyWish != null)
                    {
                        toyWish.SetToy(GameObject.Find("Airplane").GetComponent<Toy>());
                        toy = toyWish.toy;
                        wd1.SetWish(toyWish);
                        wd2.SetWish(toyWish);
                    }
                }
                else
                {
                    if (wrongItem)
                    {
                        TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
                        TutorialManager.Instance().ShowFinger(btnPut);
                    }
                    else
                    {
                        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial11note1", "Tap to put"));
                        ActivateFinger<InterfaceCollect>(toy.gameObject);

                        TutorialManager.Instance().HightlightObject(toy.gameObject, 0.35f, tm.icons["take"].sprites).hideWhenVisible = false;
                    }
                }
            }
            else
            {
                TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial11note2", "Tap to put"));
                ActivateFinger<InterfaceCollect>(baby.gameObject);
            }

            yield return null;
        }

        TutorialManager.Instance().UnHightlightObject(toy.gameObject);
        TutorialManager.Instance().UnHightlightObject(baby.gameObject);
        TutorialManager.Instance().HideFinger();

        {
            TutorialManager.Instance().UnHightlightAll();
            TutorialManager.Instance().HideFinger();

            while (TaskManager.Instance().currentTask.currentWish == null || !TaskManager.Instance().currentTask.currentWish.done) yield return null;//ждем завершение текущего

            while (TaskManager.Instance().currentTask.currentWish == null || TaskManager.Instance().currentTask.currentWish.done) yield return null;//ждем начала следующего

            TutorialManager.Instance().UnHightlightAll();
            TutorialManager.Instance().HideFinger();

            var bottle = FindObjectOfType<BabyBottle>();
            var targetHeight = 0.2f;
            var hide = false;

            step = 5;

            while (step == 5)
            {
                if (bottleInHands)
                {
                    TutorialManager.Instance().UnHightlightObject(bottle.gameObject);
                    step = 6;
                }
                else
                {
                    TutorialManager.Instance().HightlightObject(bottle.gameObject, targetHeight, tm.icons["take"].sprites).hideWhenVisible = hide;
                }

                yield return null;
            }
        }

        TutorialManager.Instance().UnHightlightAll();
        TutorialManager.Instance().HideFinger();

        baby.GetComponent<Item>().busy = false;

        //Cot cot = FindObjectOfType<Cot>();

        //while (true)
        //{
        //    if (TaskManager.Instance().currentTask.currentWish.eventName.Equals("Sleep")) break;
        //    yield return null;
        //}

        //baby.GetComponent<Item>().busy = false;

        //Debug.LogError("Step " + step);

        //step = 3;
        //while (step == 3)
        //{
        //    if (wrongItem)
        //    {
        //        TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
        //        TutorialManager.Instance().ShowFinger(btnPut);
        //    }
        //    else
        //    {
        //        if (!babyInHands)
        //        {
        //            TutorialManager.Instance().UnHightlightObject(cot.gameObject);
        //            TutorialManager.Instance().HightlightObject(baby.gameObject, 1.1f, tm.icons["interact"].sprites).hideWhenVisible = true;
        //            TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial11note3", "Tap to put"));
        //            ActivateFinger<InterfaceCollect>(baby.gameObject);
        //        }
        //        else
        //        {
        //            TutorialManager.Instance().UnHightlightObject(baby.gameObject);
        //            TutorialManager.Instance().HightlightObject(cot.gameObject, 1.1f, tm.icons["interact"].sprites).hideWhenVisible = false;
        //            TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial11note4", "Tap to put"));
        //            ActivateFinger<AbilityInterfaceApplyItem>(cot.gameObject);
        //        }
        //    }

        //    yield return null;
        //}

        //TutorialManager.Instance().UnHightlightAll();
        //TutorialManager.Instance().HideFinger();

        while (true) yield return null;
    }

    private bool ActivateFinger<T>(GameObject go) where T : AbilityInterface
    {
        if (searcher.IsThereAnyOf<T>(go))
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }
}
