﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Day6Tutorial : Tutorial
{
    private AbilityInterfaceSearcher searcher;

    private int step = 0;

    private NeedForPee need;

    public override void Init()
    {
        searcher = FindObjectOfType<AbilityInterfaceSearcher>();

        step = 0;

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;
            need = TaskManager.Instance().GetNeed<NeedForPee>();
            need.level = 0.25f;
            need.instantSet = true;
            need.setMarkers = false;
        },
        "TaskStarted");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    public override void Deactivate()
    {
        OnDestroy();
    }

    void Start()
    {
        
    }

    public List<Sprite> sprites;

    public override IEnumerator Job()
    {
        yield return new WaitForSeconds(0.5f);

        var needBar = TaskManager.Instance().GetBar<NeedForPee>();

        var toilet = FindObjectOfType<Toilet>();
        var cap = toilet.cap;
        var sp = toilet.GetComponent<SitPoint>();

        TutorialManager.Instance().HightlightObject(sp.targetPoint.gameObject, 0.75f, TutorialManager.Instance().icons["interact"].sprites);

        var btnAct = Groups.Instance().GetGroup("Button Act")[0].GetComponent<RectTransform>();
        var btnDrop = Groups.Instance().GetGroup("Button Throw")[0].GetComponent<RectTransform>();
        TutorialManager.Instance().SetFingerPos(btnAct);
        TutorialManager.Instance().SetLongFingerPos(btnDrop);

        yield return new WaitForSeconds(0.5f);

        TutorialManager.Instance().FocusOn(toilet.transform);

        while (step == 0)
        {
            if (!cap.isOn)
            {
                TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial06note1", "Tap to open"));
                ActivateFinger<Switcher>(cap.gameObject);
            }
            else
            {
                TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial06note2", "Tap to sit"));
                ActivateFinger<SitPoint>(toilet.gameObject);
            }

            if (need.onToilet) step = 1;

            yield return null;
        }

        TutorialManager.Instance().HideFinger();
        TutorialManager.Instance().UnHightlightAll();

        while (step == 1)
        {
            if (need.level >= 0.4f) step = 2;

            yield return null;
        }

        need.setMarkers = true;

        while (true) yield return null;
    }

    private bool ActivateFinger<T>(GameObject go) where T : AbilityInterface
    {
        if (searcher.IsThereAnyOf<T>(go))
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }
}
