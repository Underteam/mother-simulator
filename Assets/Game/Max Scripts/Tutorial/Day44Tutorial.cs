﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Day44Tutorial : Tutorial
{
    private AbilityInterfaceSearcher searcher;

    private int step = 0;
    
    private bool somethingInHands;

    private MomStress need;

    public override void Init()
    {
        searcher = FindObjectOfType<AbilityInterfaceSearcher>();

        step = 0;
        somethingInHands = false;

        TaskManager.Instance().displayWishes = true;

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;
            need = TaskManager.Instance().GetNeed<MomStress>();
            need.level = 0.2f;
            need.instantSet = true;
        },
        "TaskStarted");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            somethingInHands = true;
        },
        "ItemTaked");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            somethingInHands = false;
        },
        "ItemDroped");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    public override void Deactivate()
    {
        OnDestroy();
    }

    public override IEnumerator Job()
    {
        var baby = FindObjectOfType<Baby>();
        var babyItem = baby.GetComponent<Item>();
        babyItem.busy = true;

        yield return new WaitForSeconds(0.5f);

        RectTransform rt = Groups.Instance().GetGroup("Button Throw")[0].GetComponent<RectTransform>();

        var phone = Groups.Instance().GetGroup("Telephone")[0];
        TutorialManager.Instance().HightlightObject(phone, 0.5f, TutorialManager.Instance().icons["take"].sprites);

        var btnAct = Groups.Instance().GetGroup("Button Act")[0].GetComponent<RectTransform>();
        var btnDrop = Groups.Instance().GetGroup("Button Throw")[0].GetComponent<RectTransform>();
        TutorialManager.Instance().SetLongFingerPos(btnDrop);

        yield return new WaitForSeconds(0.5f);

        TutorialManager.Instance().FocusOn(phone.transform);

        while (step == 0 || step == 1)
        {
            if (somethingInHands) step = 1;
            else step = 0;

            if (step == 0)
            {
                TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial44note1", "Tap to put"));
                TutorialManager.Instance().SetFingerPos(btnAct);
                ActivateFinger<InterfaceCollect>(null);
            }
            else
            {
                TutorialManager.Instance().finger.Set(btnDrop, Localizer.GetString("tutorial44note2", "Tap to put"));
                TutorialManager.Instance().UnHightlightObject(phone);
                TutorialManager.Instance().ShowFinger(btnDrop);
            }

            if (need.level > 0.25f) step = 2;

            yield return null;
        }

        babyItem.busy = false;

        TutorialManager.Instance().UnHightlightAll();
        TutorialManager.Instance().HideFinger();

        yield return new WaitForSeconds(1f);

        while (true) yield return null;
    }

    private bool ActivateFinger<T>(GameObject go) where T : AbilityInterface
    {
        if (searcher.IsThereAnyOf<T>(go))
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }
}
