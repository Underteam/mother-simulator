﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GMATools.Common;

public class Day2Tutorial : Tutorial
{
    public bool log;

    private AbilityInterfaceSearcher searcher;

    private bool babyOnHands;
    private bool powderInHands;
    private bool pampersInHands;
    private bool usedDiaperInHands;
    private bool pacifierInHands;

    private bool wrongItem;

    private TutorialManager tm;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public override void Init()
    {
        Debug.LogError("Init " + name);

        searcher = FindObjectOfType<AbilityInterfaceSearcher>();

        tm = TutorialManager.Instance();

        step = 0;
        babyOnHands = false;
        powderInHands = false;
        pampersInHands = false;
        usedDiaperInHands = false;
        wrongItem = false;

        //TaskManager.Instance().displayWishes = false;

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            if (step == 0)
            {
                Baby b = i.GetComponent<Baby>();
                if (b != null)
                {
                    babyOnHands = true;
                    return;
                }
                else
                {
                    wrongItem = true;
                }
            }

            if (step == 2)
            {
                DiaperUsed used = i.GetComponent<DiaperUsed>();
                Baby b = i.GetComponent<Baby>();
                if (b != null)
                {
                    babyOnHands = true;
                    return;
                }
                else if (used != null)
                {
                    usedDiaperInHands = true;
                    return;
                }
                else
                {
                    wrongItem = true;
                }
            }

            if (step == 4)
            {
                BabyPowder p = i.GetComponent<BabyPowder>();
                Baby b = i.GetComponent<Baby>();
                if (b != null)
                {
                    babyOnHands = true;
                    return;
                }
                else if (p != null)
                {
                    powderInHands = true;
                    return;
                }
                else
                {
                    wrongItem = true;
                }
            }

            if (step == 6)
            {
                Diaper diaper = i.GetComponent<Diaper>();
                Baby b = i.GetComponent<Baby>();
                if (b != null)
                {
                    babyOnHands = true;
                    return;
                }
                else if (diaper != null)
                {
                    pampersInHands = true;
                    return;
                }
                else
                {
                    wrongItem = true;
                }
            }

            if (step == 8)
            {
                Pacifier p = i.GetComponent<Pacifier>();
                if (p != null)
                {
                    pacifierInHands = true;
                }
            }
        },
        "ItemTaked");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            babyOnHands = true;
        },
        "BabyAboutToTake");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            wrongItem = false;
            pacifierInHands = false;

            Baby b = i.GetComponent<Baby>();

            if (b != null)
            {
                babyOnHands = false;
                return;
            }

            BabyPowder p = i.GetComponent<BabyPowder>();
            if (p != null)
            {
                powderInHands = false;
                return;
            }

            Diaper diaper = i.GetComponent<Diaper>();
            if (diaper != null)
            {
                pampersInHands = false;
                return;
            }

            DiaperUsed used = i.GetComponent<DiaperUsed>();
            if (used != null)
            {
                usedDiaperInHands = false;
                return;
            }
        },
        "ItemDroped");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            if (step == 8)
            {
                step = 9;
            }
        },
        "WishSatisfied");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    public override void Deactivate()
    {
        OnDestroy();
    }

    private int step = 0;
    private ChangingTable table;

    public override IEnumerator Job()
    {
        var player = PlayerController.instance;
        var baby = FindObjectOfType<Baby>();
        table = FindObjectOfType<ChangingTable>();

        //Debug.LogError("Here1");
        Wish w = TaskManager.Instance().currentTask.currentWish;
        while (w.box == null) yield return null;
        WishBox wb = w.box;
        wb.InsertSubline (0, Localizer.GetString("wishPampersSub1_1", "Уложить на стол"));
        wb.InsertSubline (2, Localizer.GetString("wishPampersSub1_2", "Выбросить подгузник"));
        wb.SetNumVisibleSublines(0);
        //Debug.LogError("Here2");

        var checker = w.GetComponent<PampersChecker>();
        checker.stepsCheckers.Insert(0, () =>
        {
            return table.baby != null;
        });

        bool stepFinished = false;
        checker.stepsCheckers.Insert(2, () =>
        {
            return stepFinished;
        });

        yield return new WaitForSeconds(0.5f);

        var btnAct = Groups.Instance().GetGroup("Button Act")[0].GetComponent<RectTransform>();
        var btnPut = Groups.Instance().GetGroup("Button Put")[0].GetComponent<RectTransform>();
        var btnThrow = Groups.Instance().GetGroup("Button Throw")[0].GetComponent<RectTransform>();

        GameObject targetObject = null;
        System.Type targetInterface = null;
        float targetHeight = 1f;
        bool hide = false;
        
        int numLines = 1;

        while (step < 8)
        {
            if (step == 0)//взять пацана
            {
                TutorialManager.Instance().UnHightlightAll();

                numLines = Mathf.Max(numLines, 1);
                wb.SetNumVisibleSublines(numLines);

                targetObject = Baby.instance.gameObject;
                targetInterface = typeof(InterfaceCollect);
                targetHeight = 1.1f;
                hide = true;

                while (step == 0)
                {
                    if (wrongItem)
                    {
                        TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
                        TutorialManager.Instance().ShowFinger(btnPut);
                        TutorialManager.Instance().UnHightlightObject(targetObject);
                    }
                    else
                    {
                        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial02note1", "Tap to put"));
                        ActivateFinger (targetInterface, targetObject);

                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["take"].sprites).hideWhenVisible = hide;
                    }

                    if (babyOnHands)
                    {
                        if (table != null) TutorialManager.Instance().FocusOn(table.transform);
                        step = 1;
                    }

                    yield return null;
                }
            }

            if (step == 1)//положить на стол
            {
                TutorialManager.Instance().UnHightlightAll();

                numLines = Mathf.Max(numLines, 1);
                wb.SetNumVisibleSublines(numLines);

                targetObject = table.gameObject;
                targetInterface = typeof(AbilityInterfaceApplyItem);
                targetHeight = 1.7f;
                hide = false;

                while (step == 1)
                {
                    if (wrongItem)
                    {
                        TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
                        TutorialManager.Instance().ShowFinger(btnPut);
                        TutorialManager.Instance().UnHightlightObject(targetObject);
                    }
                    else
                    {
                        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial02note2", "Tap to put"));
                        ActivateFinger(targetInterface, targetObject);

                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["interact"].sprites).hideWhenVisible = hide;
                    }

                    if (table.baby != null) step = 2;
                    else if (!babyOnHands) step = 0;

                    yield return null;
                }
            }

            if (step == 2)//снять памперс
            {
                TutorialManager.Instance().UnHightlightAll();

                numLines = Mathf.Max(numLines, 2);
                wb.SetNumVisibleSublines(numLines);

                InterfaceProvider usedDiaper2 = baby.GetComponentInChildren<InterfaceProvider>(true);

                targetObject = usedDiaper2.gameObject;
                targetInterface = typeof(InterfaceProvider);
                targetHeight = 0.15f;
                hide = false;

                while (step == 2)
                {
                    if (wrongItem)
                    {
                        TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
                        TutorialManager.Instance().ShowFinger(btnPut);
                        TutorialManager.Instance().UnHightlightObject(targetObject);
                    }
                    else
                    {
                        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial02note3", "Tap to put"));
                        ActivateFinger(targetInterface, targetObject);

                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["take"].sprites).hideWhenVisible = hide;
                    }

                    if (!usedDiaper2.gameObject.activeSelf) step = 3;
                    if (babyOnHands) step = 1;

                    yield return null;
                }
            }

            if (step == 3)//выбросить памперс
            {
                TutorialManager.Instance().UnHightlightAll();

                numLines = Mathf.Max(numLines, 3);
                wb.SetNumVisibleSublines(numLines);

                var bin = FindObjectOfType<TrashBin>();

                targetObject = bin.gameObject;
                targetInterface = typeof(InterfaceProvider);
                targetHeight = 0.95f;
                hide = false;

                while (step == 3)
                {
                    if (wrongItem)
                    {
                        TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
                        TutorialManager.Instance().ShowFinger(btnPut);
                        TutorialManager.Instance().UnHightlightObject(targetObject);
                    }
                    else
                    {
                        TutorialManager.Instance().finger.Set(btnThrow, Localizer.GetString("tutorial02note4", "Tap to put"));

                        float dot = Vector3.Dot(player.cam.transform.forward, (bin.transform.position + 0.5f * Vector3.up - player.cam.transform.position).normalized);
                        if(dot > 0.9f)
                            Groups.Instance().SetGroupState("Finger", true);
                        else
                            Groups.Instance().SetGroupState("Finger", false);

                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["aim"].sprites).hideWhenVisible = hide;
                    }

                    if (!usedDiaperInHands) step = 4;

                    yield return null;
                }
            }

            if (step == 4)//взять присыпку
            {
                stepFinished = true;

                TutorialManager.Instance().UnHightlightAll();

                numLines = Mathf.Max(numLines, 4);
                wb.SetNumVisibleSublines(numLines);

                var powder2 = FindObjectOfType<BabyPowder>();

                targetObject = powder2.gameObject;
                targetInterface = typeof(InterfaceCollect);
                targetHeight = 0.3f;
                hide = false;

                while (step == 4)
                {
                    if (wrongItem)
                    {
                        TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
                        TutorialManager.Instance().ShowFinger(btnPut);
                        TutorialManager.Instance().UnHightlightObject(targetObject);
                    }
                    else
                    {
                        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial02note5", "Tap to put"));
                        ActivateFinger(targetInterface, targetObject);

                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["take"].sprites).hideWhenVisible = hide;
                    }

                    if (powderInHands) step = 5;
                    if (!baby.wet) step = 6;
                    if (babyOnHands) step = 1;

                    yield return null;
                }
            }

            if (step == 5)//посыпать
            {
                TutorialManager.Instance().UnHightlightAll();

                numLines = Mathf.Max(numLines, 4);
                wb.SetNumVisibleSublines(numLines);

                targetObject = baby.gameObject;
                targetInterface = typeof(InterfaceCollect);
                targetHeight = 0.3f;
                hide = true;

                while (step == 5)
                {
                    if (wrongItem)
                    {
                        TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
                        TutorialManager.Instance().ShowFinger(btnPut);
                    }
                    else
                    {
                        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial02note6", "Tap to put"));
                        ActivateFinger(targetInterface, targetObject);

                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["interact"].sprites).hideWhenVisible = hide;
                    }

                    if (!baby.wet) step = 6;
                    if (!powderInHands) step = 4;

                    yield return null;
                }
            }

            if (step == 6)//взять памперс
            {
                TutorialManager.Instance().UnHightlightAll();

                numLines = Mathf.Max(numLines, 5);
                wb.SetNumVisibleSublines(numLines);

                List<Diaper> listOfDiapers2 = new List<Diaper>(FindObjectsOfType<Diaper>());
                listOfDiapers2.Sort((a, b) =>
                {
                    return a.transform.position.z.CompareTo(b.transform.position.z);
                });
                var pampers2 = listOfDiapers2[0];

                targetObject = pampers2.gameObject;
                targetInterface = typeof(InterfaceCollect);
                targetHeight = 0.2f;
                hide = false;
                wrongItem = powderInHands;

                while (step == 6)
                {
                    if (wrongItem)
                    {
                        TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
                        TutorialManager.Instance().ShowFinger(btnPut);
                        TutorialManager.Instance().UnHightlightObject(targetObject);
                    }
                    else
                    {
                        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial02note8", "Tap to put"));
                        ActivateFinger(targetInterface, targetObject);

                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["take"].sprites).hideWhenVisible = hide;
                    }

                    if (babyOnHands) step = 1;
                    if (pampersInHands) step = 7;

                    yield return null;
                }
            }

            if (step == 7)//одеть памперс
            {
                TutorialManager.Instance().UnHightlightAll();

                numLines = Mathf.Max(numLines, 5);
                wb.SetNumVisibleSublines(numLines);

                targetObject = baby.gameObject;
                targetInterface = typeof(InterfaceCollect);
                targetHeight = 0.3f;
                hide = true;

                while (step == 7)
                {
                    if (wrongItem)
                    {
                        TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
                        TutorialManager.Instance().ShowFinger(btnPut);
                    }
                    else
                    {
                        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial02note9", "Tap to put"));
                        ActivateFinger(targetInterface, targetObject);

                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["interact"].sprites).hideWhenVisible = hide;
                    }

                    if (baby.diaperInterface.gameObject.activeSelf) step = 8;
                    else if (!pampersInHands) step = 6;

                    yield return null;
                }
            }

            if (step == 8)//взять соску
            {
                TutorialManager.Instance().UnHightlightAll();
                TutorialManager.Instance().HideFinger();

                var pacifier = FindObjectOfType<Pacifier>();
                if (pacifier != null) targetObject = pacifier.gameObject;
                else targetObject = null;
                targetInterface = typeof(AbilityCollect);
                targetHeight = 0.15f;
                hide = false;

                while (TaskManager.Instance().currentTask.currentWish == null || TaskManager.Instance().currentTask.currentWish.done) yield return null;

                while (step == 8)
                {
                    
                    if (pacifierInHands)
                    {
                        TutorialManager.Instance().UnHightlightObject(targetObject);
                    }
                    else
                    {
                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["take"].sprites).hideWhenVisible = hide;
                    }

                    yield return null;
                }
            }

            yield return null;
        }

        TutorialManager.Instance().UnHightlightAll();
        TutorialManager.Instance().HideFinger();

        while (true) yield return null;
    }

    private bool ActivateFinger<T>(GameObject go) where T : AbilityInterface
    {
        if (searcher.IsThereAnyOf<T>(go))
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }

    private bool ActivateFinger (System.Type type, GameObject go)
    {
        if (searcher.IsThereAnyOf(type, go))
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }
}
