﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Day41Tutorial : Tutorial
{
    private AbilityInterfaceSearcher searcher;

    private int step = 0;

    public Sprite guestImage;

    private GuestMood need;

    public override void Init()
    {
        searcher = FindObjectOfType<AbilityInterfaceSearcher>();

        step = 0;

        TaskManager.Instance().displayWishes = true;

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            need = TaskManager.Instance().GetNeed<GuestMood>();
            need.setMarkers = false;
        },
        "TaskStarted");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;
        },
        "ItemTaked");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;
        },
        "ItemDroped");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    public override void Deactivate()
    {
        OnDestroy();
    }

    public override IEnumerator Job()
    {
        yield return new WaitForSeconds(0.5f);

        TutorialManager.Instance().ShowGreeting(Localizer.GetString("tutorial41message1", "Привет, подруга! Я зашла к тебе в гости!"), guestImage);

        yield return new WaitForSeconds(0.1f);

        var guest = FindObjectOfType<Guest>();
        guest.timer = 0;

        var chair = FindObjectOfType<DiningChair>();

        TutorialManager.Instance().HightlightObject(chair.gameObject, 0.9f, TutorialManager.Instance().icons["interact"].sprites);

        var btnAct = Groups.Instance().GetGroup("Button Act")[0].GetComponent<RectTransform>();
        var btnDrop = Groups.Instance().GetGroup("Button Throw")[0].GetComponent<RectTransform>();
        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial41note1", "Tap to put"));
        TutorialManager.Instance().SetFingerPos(btnAct);
        TutorialManager.Instance().SetLongFingerPos(btnDrop);

        yield return new WaitForSeconds(0.5f);

        TutorialManager.Instance().FocusOn(chair.transform);

        while (step == 0)
        {
            ActivateFinger<SitPoint>(chair.gameObject);

            if (chair.sitting) step = 1;

            yield return null;
        }

        TutorialManager.Instance().UnHightlightAll();
        TutorialManager.Instance().HideFinger();

        need.setMarkers = true;

        while (true) yield return null;
    }

    private bool ActivateFinger<T>(GameObject go) where T : AbilityInterface
    {
        if (searcher.IsThereAnyOf<T>(go))
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }
}
