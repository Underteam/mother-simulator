﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Day32Tutorial : Tutorial
{
    private AbilityInterfaceSearcher searcher;

    private int step = 0;

    private bool foodInHands;

    public Sprite dogImage;

    private DogMood need;

    public override void Init()
    {
        searcher = FindObjectOfType<AbilityInterfaceSearcher>();

        step = 0;
        foodInHands = false;

        TaskManager.Instance().displayWishes = true;

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;
            var dog = FindObjectOfType<Dog>();
            dog.Hungry();
            need = TaskManager.Instance().GetNeed<DogMood>();
            need.setMarkers = false;
        },
        "TaskStarted");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            var f = i.GetComponent<DogFood>();
            if (f != null)
            {
                foodInHands = true;
                if (step == 0 && bowl != null) TutorialManager.Instance().FocusOn(bowl.transform);
                return;
            }
        },
        "ItemTaked");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            var f = i.GetComponent<DogFood>();
            if (f != null)
            {
                foodInHands = false;
                return;
            }
        },
        "ItemDroped");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    public override void Deactivate()
    {
        OnDestroy();
    }

    DogDish bowl;

    public override IEnumerator Job()
    {
        var baby = FindObjectOfType<Baby>();
        var babyItem = baby.GetComponent<Item>();
        babyItem.busy = true;

        yield return new WaitForSeconds(0.5f);

        TutorialManager.Instance().ShowGreeting(Localizer.GetString("tutorial32message1", "Гав-гав"), dogImage);

        yield return new WaitForSeconds(0.1f);

        var player = PlayerController.instance;

        List<DogFood> foods = new List<DogFood>(FindObjectsOfType<DogFood>());
        foods.Sort((a, b) =>
        {
            float d1 = Vector3.Distance(a.transform.position, player.transform.position);
            float d2 = Vector3.Distance(b.transform.position, player.transform.position);
            return d1.CompareTo(d2);
        });
        var food = foods[0];
        bowl = FindObjectOfType<DogDish>();

        var btnAct = Groups.Instance().GetGroup("Button Act")[0].GetComponent<RectTransform>();
        var btnDrop = Groups.Instance().GetGroup("Button Throw")[0].GetComponent<RectTransform>();
        TutorialManager.Instance().SetFingerPos(btnAct);
        TutorialManager.Instance().SetLongFingerPos(btnDrop);

        yield return new WaitForSeconds(0.5f);

        TutorialManager.Instance().FocusOn(food.transform);

        while (step == 0 || step == 1)
        {
            if (foodInHands) step = 1;
            else step = 0;

            if (step == 0)
            {
                TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial32note1", "Tap to take"));
                ActivateFinger<InterfaceCollect>(food.gameObject);
                TutorialManager.Instance().UnHightlightObject(bowl.gameObject);
                TutorialManager.Instance().HightlightObject(food.gameObject, 0.15f, TutorialManager.Instance().icons["take"].sprites);
            }
            else
            {
                TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial32note2", "Tap to put"));
                ActivateFinger<AbilityInterfaceApplyItem>(bowl.gameObject);
                TutorialManager.Instance().UnHightlightObject(food.gameObject);
                TutorialManager.Instance().HightlightObject(bowl.gameObject, 0.25f, TutorialManager.Instance().icons["interact"].sprites);
            }

            if (bowl.food != null) step = 2;

            yield return null;
        }

        babyItem.busy = false;

        TutorialManager.Instance().UnHightlightAll();
        TutorialManager.Instance().HideFinger();

        yield return new WaitForSeconds(2f);

        need.setMarkers = true;

        while (true) yield return null;
    }

    private bool ActivateFinger<T>(GameObject go) where T : AbilityInterface
    {
        if (searcher.IsThereAnyOf<T>(go))
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }
}
