﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialManager : MonoSingleton<TutorialManager>
{
    [System.Serializable]
    public class ObjectGroup
    {
        public string name;
        public List<GameObject> objects;

        public CopyPaster copypaster;

        public ObjectGroup()
        {
            copypaster = new CopyPaster(Copy, null, null, null, null);
        }

        public void Copy ()
        {
            Groups.copiedGroup = this;
        }
    }

    public List<ObjectGroup> groups;

    public TutorialPanel tutorialPanel;

    public Text tutorialText;
    public Text shortMessageText;
    public GameObject shortMessageBG;

    private Tutorial tutor;

    public GameObject arrow;

    public Finger finger;
    public Finger longFinger;

    public GameObject pointer;

    public List<WorldToCanvas> allMarkers;
    private List<GameObject> highlightedObjects = new List<GameObject>();
    private List<WorldToCanvas> objectsMarkers = new List<WorldToCanvas>();

    public GreetingPanel greetingPanel;

    [System.Serializable]
    public class SpritePack
    {
        public List<Sprite> sprites;
    }

    [ShowInInspector]
    public Dictionary<string, SpritePack> icons;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Job());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetTutorial(Tutorial t)
    {
        if (tutor != null) tutor.Deactivate();

        tutor = t;

        if (tutor != null)
        {
            tutor.Init();
            if (tutor.somethingNew) SetGroupState("BtnSkip", false);
        }
    }

    private IEnumerator Job ()
    {
        while(true)
        {
            if (tutor != null) yield return tutor.Job();

            yield return null;
        }
    }

    public void ShowMessage(string text, string shortMessage = "")
    {
        tutorialPanel.Show();
        tutorialText.text = text;
        shortMessageText.text = shortMessage;
        if (string.IsNullOrEmpty(shortMessage)) shortMessageBG.SetActive(false);
        else shortMessageBG.SetActive(true);
    }

    public void HideMessage ()
    {
        tutorialPanel.Hide();
    }

    public void HightlightObjectOld (GameObject go, float height = 0.5f)
    {
        if (go == null) arrow.SetActive(false);
        else
        {
            arrow.SetActive(true);

            arrow.transform.position = go.transform.position + height * Vector3.up;
        }

    }

    public void UnHightlightAll ()
    {
        //Debug.LogError("UnhighlightAll");
        highlightedObjects.Clear();
        objectsMarkers.Clear();
    }

    public void UnHightlightObject(GameObject go)
    {
        int ind = highlightedObjects.IndexOf(go);
        if (ind >= 0)
        {
            highlightedObjects.RemoveAt(ind);
            objectsMarkers.RemoveAt(ind);
        }

        //Debug.Log("UnHighlight " + go + " " + ind, go);
    }

    public bool log;

    private void LateUpdate()
    {
        if (allMarkers == null) return;

        for (int i = 0; i < allMarkers.Count; i++)
        {
            if (allMarkers[i] == null)
            {
                allMarkers.RemoveAt(i);
                i--;
                continue;
            }

            if (allMarkers[i].target == null && objectsMarkers.Contains(allMarkers[i]))
            {
                int ind = objectsMarkers.IndexOf(allMarkers[i]);
                objectsMarkers.RemoveAt(ind);
                highlightedObjects.RemoveAt(ind);
            }
            
            if (log)
            {
                Debug.Log("Check " + allMarkers[i] + " " + (allMarkers[i].target == null), allMarkers[i]);
                Debug.Log("Contains " + objectsMarkers.Contains(allMarkers[i]) + " active " + allMarkers[i].gameObject.activeSelf + " hiding " + allMarkers[i].hiding, allMarkers[i].target);
            }

            if (!allMarkers[i].gameObject.activeSelf || allMarkers[i].hiding) continue;

            if (!objectsMarkers.Contains (allMarkers[i]))
            {
                allMarkers[i].Hide();
            }
        }
    }

    public WorldToCanvas HightlightObject (GameObject go, float height, List<Sprite> sprites)
    {
        if (go == null) return null;

        //Debug.Log("Highlight " + go, go);

        WorldToCanvas marker = null;

        int ind = highlightedObjects.IndexOf(go);
        if (ind >= 0) marker = objectsMarkers[ind];
        else
        {
            for (int i = 0; i < allMarkers.Count; i++)
            {
                if ((allMarkers[i].target == null || !highlightedObjects.Contains (allMarkers[i].target.gameObject)) && !allMarkers[i].hiding)
                {
                    marker = allMarkers[i];
                    break;
                }
            }

            if (marker == null)
            {
                marker = Instantiate(allMarkers[0]);
                marker.transform.SetParent(allMarkers[0].transform.parent);
                marker.transform.localScale = Vector3.one;
                allMarkers.Add(marker);
            }

            highlightedObjects.Add(go);
            objectsMarkers.Add(marker);
        }

        marker.gameObject.SetActive(true);

        marker.SetSprites(sprites);
        marker.target = go.transform;
        marker.shift.y = height;

        //Debug.Log("Return marker " + marker, marker);

        return marker;
    }

    //public void ShowPointer(Vector3 pos, Quaternion rot)
    //{
    //    pointer.transform.position = pos;
    //    pointer.transform.rotation = rot;

    //    pointer.SetActive(true);
    //}

    //public void HidePointer()
    //{
    //    pointer.SetActive(false);
    //}

    public void SetFingerPos (RectTransform rt, Vector3 shift = new Vector3())
    {
        finger.rt.position = rt.position + shift;
    }

    public void ShowFinger(RectTransform rt, Vector3 shift = new Vector3())
    {
        if (!rt.gameObject.activeInHierarchy) return;

        finger.rt.position = rt.position + shift;
        finger.gameObject.SetActive(true);
    }

    public void HideFinger ()
    {
        finger.gameObject.SetActive(false);
    }

    public void SetLongFingerPos(RectTransform rt, Vector3 shift = new Vector3())
    {
        longFinger.rt.position = rt.position + shift;
    }

    public void ShowLongFinger(RectTransform rt, Vector3 shift = new Vector3())
    {
        longFinger.rt.position = rt.position + shift;
        longFinger.gameObject.SetActive(true);
    }

    public void HideLongFinger()
    {
        longFinger.gameObject.SetActive(false);
    }

    private void SetGroupState (string group, bool state)
    {
        for(int i = 0; i < groups.Count; i++)
        {
            if(groups[i].name.Equals(group))
            {
                for (int j = 0; j < groups[i].objects.Count; j++)
                {
                    if (groups[i].objects[j].activeSelf != state) groups[i].objects[j].SetActive(state);
                }
            }
        }
    }

    private List<GameObject> GetGroup(string name)
    {
        for (int i = 0; i < groups.Count; i++)
        {
            if (groups[i].name.Equals(name))
            {
                return groups[i].objects;
            }
        }

        return null;
    }

    private class Blocker : IBlocker
    {
        public bool blocked { get; set; }
    }

    private Blocker blocker = new Blocker();
    private SwipeLook looker;
    private Joystick2 walker;

    private Coroutine coroutine;
    public void FocusOn (Transform target, bool yaxis = false)
    {
        if (looker == null) looker = FindObjectOfType<SwipeLook>();
        if (walker == null) walker = FindObjectOfType<Joystick2>();

        if (coroutine != null) StopCoroutine(coroutine);
        coroutine = StartCoroutine(FocusOnCoroutine(target, yaxis));
    }

    public void FocusOn (Transform target, bool yaxis, Vector3 shift, System.Action onFocus = null)
    {
        if (looker == null) looker = FindObjectOfType<SwipeLook>();
        if (walker == null) walker = FindObjectOfType<Joystick2>();

        if (coroutine != null) StopCoroutine(coroutine);
        //coroutine = StartCoroutine(FocusOnCoroutine(target, yaxis));
        var fps = PlayerController.instance.GetComponent<FPSCharacterController>();
        fps.smooth = false;
        fps.SetTarget(target);
        fps.targetShift = shift;
        fps.onLookAtTarget -= OnLookAtTarget;
        fps.onLookAtTarget += OnLookAtTarget;
        this.onFocus = onFocus;
    }

    private System.Action onFocus;
    private void OnLookAtTarget (Transform target)
    {
        var fps = PlayerController.instance.GetComponent<FPSCharacterController>();
        fps.smooth = true;
        fps.SetTarget(null);

        if (onFocus != null) onFocus();
    }

    public string info;
    private IEnumerator FocusOnCoroutine(Transform target, bool yaxis)
    {
        if (!looker.blockers.Contains(blocker)) looker.blockers.Add(blocker);
        if (!walker.blockers.Contains(blocker)) walker.blockers.Add(blocker);

        blocker.blocked = true;

        var fps = PlayerController.instance.GetComponent<FPSCharacterController>();
        fps.smooth = false;
        //fps.disableMovement = true;
        Transform cam = fps.cam.transform;

        yield return null;

        float maxSpeed = 3f;
        float minSpeed = 0.5f;

        while (true)
        {
            Vector3 dir = target.position - cam.transform.position;
            float dot = Vector3.Dot(dir, cam.forward);
            float dot2 = -Vector3.Dot(cam.transform.forward, Vector3.up);
            dir -= dot * cam.forward;
            float xspeed = Vector3.Dot(dir, cam.right);
            float yspeed = Vector3.Dot(dir, Vector3.up);

            if (dot < 0)
            {
                yspeed = 0;
                xspeed = Mathf.Sign(xspeed) * maxSpeed;
            }

            info = xspeed + " " + yspeed + " " + dot2 + " | ";

            if (Mathf.Abs(xspeed) > maxSpeed) xspeed = Mathf.Sign(xspeed) * maxSpeed;
            //else xspeed = Mathf.Sign(xspeed) * Mathf.Sqrt(Mathf.Abs(xspeed));

            if (Mathf.Abs(yspeed) > maxSpeed) yspeed = Mathf.Sign(yspeed) * maxSpeed;
            //else yspeed = Mathf.Sign(yspeed) * Mathf.Sqrt(Mathf.Abs(yspeed));

            info += xspeed + " " + yspeed + " | ";

            InputManager.Instance().SetAxis(looker.horizontalAxisName, xspeed);
            if (yaxis) InputManager.Instance().SetAxis(looker.verticalAxisName, yspeed);
            else
            {
                yspeed = dot2;
                if (Mathf.Abs(yspeed) > maxSpeed) yspeed = Mathf.Sign(yspeed) * maxSpeed;
                else if(Mathf.Abs(yspeed) < 0.05f) yspeed = 0;
                InputManager.Instance().SetAxis(looker.verticalAxisName, yspeed);

                info += " | " + yspeed;
            }

            if (Mathf.Abs(xspeed) < 0.07f && (Mathf.Abs(yspeed) < 0.07f || !yaxis)) break;

            yield return null;
        }

        blocker.blocked = false;
        InputManager.Instance().SetAxis(looker.horizontalAxisName, 0);
        InputManager.Instance().SetAxis(looker.verticalAxisName, 0);
        fps.smooth = true;
        //fps.disableMovement = false;

        coroutine = null;
    }

    public void ShowGreeting (string greeting, Sprite sprite)
    {
        greetingPanel.Show(greeting, sprite);
    }

    private void OnDestroy()
    {
        if (tutor != null) tutor.Deactivate();
    }
}
