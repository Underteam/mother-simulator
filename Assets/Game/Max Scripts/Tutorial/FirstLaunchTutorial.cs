﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GMATools.Common;
using UnityEngine.EventSystems;

public class FirstLaunchTutorial : Tutorial
{
    public bool log;

    private AbilityInterfaceSearcher searcher;

    // Start is called before the first frame update
    void Start()
    {

    }

    public override void Init()
    {
        Debug.LogError("Init");

        step = 0;

        searcher = FindObjectOfType<AbilityInterfaceSearcher>();

        Groups.Instance().SetGroupState("Button Throw", false);
        Groups.Instance().SetGroupState("Button Put", false);
        Groups.Instance().SetGroupState("ShopAndCrystals", false);
        Groups.Instance().SetGroupState("BtnStart", false);

        //TaskManager.Instance().dayProgress.transform.SetParent(TaskManager.Instance().dayProgress.transform.parent.parent);
        //TaskManager.Instance().dayProgress.transform.SetAsFirstSibling();
        //TaskManager.Instance().dayProgress.lblDay.text = Localizer.GetString("tutorial", "Обучение");
        //TaskManager.Instance().dayProgress.imgState.gameObject.SetActive(false);

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            if (i.gameObject == visiblePacifier)
            {
                step = 1;
                return;
            }

            Baby b = i.GetComponent<Baby>();

            if (b != null && step == 5)
            {
                step = 6;
                return;
            }

            Groups.Instance().SetGroupState("Button Throw", true);
        },
        "ItemTaked");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    public override void Deactivate()
    {
        OnDestroy();
    }

    private int step = 0;
    private GameObject visiblePacifier;

    public override IEnumerator Job()
    {
        var player = PlayerController.instance;
        var baby = FindObjectOfType<Baby>();

        yield return new WaitForSeconds(0.5f);

        Groups.Instance().SetGroupState("Finger", false);

        var list = Groups.Instance().GetGroup("StartPanel");
        var panel = list[0];
        Destroy(panel.GetComponent<EventTrigger>());
        list[2].gameObject.SetActive(false);

        Groups.Instance().SetGroupState("LookAroundHint", true);

        Quaternion q = player.cam.transform.rotation;
        float ang = 0;

        while (step == 0)
        { 
            ang += Quaternion.Angle(q, player.cam.transform.rotation);
            q = player.cam.transform.rotation;

            if (ang > 45) step = 1;

            yield return null;
        }

        Groups.Instance().SetGroupState("LookAroundHint", false);

        yield return new WaitForSeconds(0.5f);

        Groups.Instance().SetGroupState("JoystickHint", true);
        Groups.Instance().SetGroupState("Rotator", true);

        Vector3 startPos = player.transform.position;

        while (step == 1)
        {
            float dist = Vector3.Distance(startPos, player.transform.position);

            if (dist > 1.0f) step = 2;

            yield return null;
        }

        Groups.Instance().SetGroupState("JoystickHint", false);
        Groups.Instance().SetGroupState("Rotator", false);
        //TutorialManager.Instance().HidePointer();

        yield return new WaitForSeconds(0.5f);

        Groups.Instance().SetGroupState("BtnStart", true);
        Groups.Instance().SetGroupState("StartHint", true);

        RectTransform rt1 = Groups.Instance().GetGroup("Button Act")[0].GetComponent<RectTransform>();
        RectTransform rt2 = list[1].GetComponent<RectTransform>();
        list[1].gameObject.AddComponent<ScaleAnimation>();

        while (step == 2)
        {
            if (panel.activeSelf)
            {
                TutorialManager.Instance().finger.Set(rt2, "");
                TutorialManager.Instance().ShowFinger(rt2);
                Groups.Instance().SetGroupState("StartHint", false);

                step = 3;
            }

            yield return null;
        }

        while (true) yield return null;
    }

    private bool ActivateFinger<T> () where T : AbilityInterface 
    {
        if(searcher.IsThereAnyOf<T>())
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }
}
