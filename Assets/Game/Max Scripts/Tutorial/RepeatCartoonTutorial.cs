﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatCartoonTutorial : Tutorial
{
    private AbilityInterfaceSearcher searcher;

    private int step = 0;

    private TutorialManager tm;

    private bool wrongItem;
    private bool babyOnHands;

    private BabySitPoint sofa;

    public override void Init()
    {
        searcher = FindObjectOfType<AbilityInterfaceSearcher>();

        tm = TutorialManager.Instance();

        step = 0;
        babyOnHands = false;
        wrongItem = false;

        var allSPs = FindObjectsOfType<BabySitPoint>();
        for (int i = 0; i < allSPs.Length; i++)
        {
            if (allSPs[i].description.Equals("Sofa")) sofa = allSPs[i];
        }

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            Baby b = i.GetComponent<Baby>();
            if (b != null)
            {
                babyOnHands = true;

                if (step != 1) step = 2;

                return;
            }
            else
            {
                if (step == 1) wrongItem = true;
            }
        },
        "ItemTaked");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            babyOnHands = true;
        },
        "BabyAboutToTake");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            wrongItem = false;
            babyOnHands = false;
        },
        "ItemDroped");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;
        },
        "WishSatisfied");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    public override void Deactivate()
    {
        OnDestroy();
    }

    public override IEnumerator Job()
    {
        var player = PlayerController.instance;
        var baby = FindObjectOfType<Baby>();

        while (TaskManager.Instance().currentTask.currentWish.done)
        {
            yield return null;
        }

        Wish w = TaskManager.Instance().currentTask.currentWish;
        while (w.box == null) yield return null;
        WishBox wb = w.box;
        wb.SetNumVisibleSublines(0);

        yield return new WaitForSeconds(0.5f);

        var btnAct = Groups.Instance().GetGroup("Button Act")[0].GetComponent<RectTransform>();
        var btnDrop = Groups.Instance().GetGroup("Button Throw")[0].GetComponent<RectTransform>();
        var btnPut = Groups.Instance().GetGroup("Button Put")[0].GetComponent<RectTransform>();
        TutorialManager.Instance().SetFingerPos(btnAct);
        TutorialManager.Instance().SetLongFingerPos(btnDrop);

        yield return new WaitForSeconds(0.5f);

        GameObject targetObject = null;
        System.Type targetInterface = null;
        float targetHeight = 1f;
        bool hide = false;

        while (step < 3)
        {
            if (step == 0)
            {
                TutorialManager.Instance().UnHightlightAll();
                TutorialManager.Instance().HideFinger();

                while (TaskManager.Instance().currentTask.currentWish == null || !TaskManager.Instance().currentTask.currentWish.done) yield return null;//ждем завершение текущего

                while (TaskManager.Instance().currentTask.currentWish == null || TaskManager.Instance().currentTask.currentWish.done) yield return null;//ждем начала следующего

                step = 1;
            }

            if (step == 1)//взять пацана
            {
                TutorialManager.Instance().UnHightlightAll();

                targetObject = Baby.instance.gameObject;
                targetInterface = typeof(InterfaceCollect);
                targetHeight = 1.1f;
                hide = true;

                while (step == 1)
                {
                    if (wrongItem)
                    {
                        //TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
                        //TutorialManager.Instance().ShowFinger(btnPut);
                        TutorialManager.Instance().UnHightlightObject(targetObject);
                    }
                    else
                    {
                        //TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorialCartoonsnote1", "Tap to put"));
                        //ActivateFinger(targetInterface, targetObject);

                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["take"].sprites).hideWhenVisible = hide;
                    }

                    if (babyOnHands)
                    {
                        step = 2;
                    }

                    yield return null;
                }
            }

            if (step == 2)//посадить на диван
            {
                TutorialManager.Instance().UnHightlightAll();

                targetObject = sofa.gameObject;
                targetInterface = typeof(AbilityInterfaceApplyItem);
                targetHeight = 0.6f;
                hide = false;

                while (step == 2)
                {
                    {
                        //TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorialCartoonsnote2", "Tap to put"));
                        //ActivateFinger(targetInterface, targetObject);

                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["take"].sprites).hideWhenVisible = hide;
                    }

                    if (sofa.baby != null)
                    {
                        step = 3;
                    }
                    else if (!babyOnHands)
                    {
                        step = 1;
                    }

                    yield return null;
                }
            }

            yield return null;
        }

        TutorialManager.Instance().UnHightlightAll();
        TutorialManager.Instance().HideFinger();

        while (true) yield return null;
    }

    private bool ActivateFinger<T>(GameObject go, bool setstate = true) where T : AbilityInterface
    {
        if (searcher.IsThereAnyOf<T>(go))
        {
            if (setstate) Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            if (setstate) Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }

    private bool ActivateFinger(System.Type type, GameObject go)
    {
        if (searcher.IsThereAnyOf(type, go))
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }
}
