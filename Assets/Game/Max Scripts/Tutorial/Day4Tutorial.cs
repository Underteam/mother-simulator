﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Day4Tutorial : Tutorial
{
    private AbilityInterfaceSearcher searcher;

    private int step = 0;

    private MomHungry need;

    public List<Sprite> sprites;

    private bool wrongItem;
    private bool babyOnHands;
    private bool bottleInHands;

    public override void Init()
    {
        Debug.LogError("Init day 4 tutorial", this);

        //var need = TaskManager.Instance().GetNeed<MomHungry>();
        //need.setMarkers = false;

        searcher = FindObjectOfType<AbilityInterfaceSearcher>();

        step = 0;
        babyOnHands = false;
        wrongItem = false;
        bottleInHands = false;

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            Debug.LogError("Day4Tutorial TaskStarted " + (this == null), this);
            if (this == null) return;
            need = TaskManager.Instance().GetNeed<MomHungry>();
            need.level = 0.2f;
            need.instantSet = true;
            need.setMarkers = false;
        },
        "TaskStarted");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            if (step == 2)
            {
                Baby b = i.GetComponent<Baby>();
                if (b != null)
                {
                    babyOnHands = true;
                    return;
                }
                else
                {
                    wrongItem = true;
                }
            }

            if (step == 5)
            {
                var b = i.GetComponent<Pacifier>();
                if (b != null)
                {
                    bottleInHands = true;
                    return;
                }
                else
                {
                    wrongItem = true;
                }
            }
        },
        "ItemTaked");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            babyOnHands = true;
        },
        "BabyAboutToTake");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            wrongItem = false;
            babyOnHands = false;
            bottleInHands = false;
        },
        "ItemDroped");
        listeners.Add(listener);

        int eatingCounter = 0;
        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            eatingCounter++;

            if (step == 0 && eatingCounter >= 1) step = 1;
        },
        "Eating");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            if (step == 5)
            {
                step = 6;
            }
        },
        "WishSatisfied");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    public override void Deactivate ()
    {
        OnDestroy();
    }

    void Start()
    {

    }

    public override IEnumerator Job()
    {
        yield return new WaitForSeconds(0.5f);

        var baby = FindObjectOfType<Baby>();
        var babyItem = baby.GetComponent<Item>();
        babyItem.busy = true;

        var needBar = TaskManager.Instance().GetBar<MomHungry>();

        Groups.Instance().SetGroupState("ScaleHint", true);
        Groups.Instance().SetGroupState("BoostersIcons", false);

        var allFood = new List<Food>(FindObjectsOfType<Food>());

        var player = PlayerController.instance;

        var btnAct = Groups.Instance().GetGroup("Button Act")[0].GetComponent<RectTransform>();
        var btnDrop = Groups.Instance().GetGroup("Button Throw")[0].GetComponent<RectTransform>();
        var btnPut = Groups.Instance().GetGroup("Button Put")[0].GetComponent<RectTransform>();
        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial04note1", "Tap to eat"));
        TutorialManager.Instance().SetFingerPos(btnAct);

        var dish = GameObject.Find("SuperDonut");

        TutorialManager.Instance().HightlightObject(dish.gameObject, 0.3f, sprites);

        yield return new WaitForSeconds(0.5f);

        TutorialManager.Instance().FocusOn(dish.transform);

        Food currentFood = null;

        while (step == 0)
        {
            allFood.Sort((a, b) =>
            {
                float dist1 = Vector3.Distance(a.transform.position, player.transform.position);
                float dist2 = Vector3.Distance(b.transform.position, player.transform.position);
                return dist1.CompareTo(dist2);
            });

            if (currentFood != null)
            {
                if (!ActivateFinger<InterfaceCollect>(currentFood.gameObject)) currentFood = null;
            }
            else for (int i = 0; i < allFood.Count; i++)
            {
                if (ActivateFinger<InterfaceCollect>(allFood[i].gameObject))
                {
                    currentFood = allFood[i];
                    break;
                }
            }

            yield return null;
        }

        babyItem.busy = false;

        TutorialManager.Instance().HideFinger();
        TutorialManager.Instance().UnHightlightAll();

        while (step == 1)
        {
            if (need.level >= 0.4f) step = 2;

            yield return null;
        }

        Groups.Instance().SetGroupState("ScaleHint", false);
        Groups.Instance().SetGroupState("BoostersIcons", true);

        need.setMarkers = true;

        TutorialManager.Instance().HideFinger();
        TutorialManager.Instance().UnHightlightAll();

        while (true) yield return null;
    }

    private bool ActivateFinger<T>(GameObject go) where T : AbilityInterface
    {
        if (searcher.IsThereAnyOf<T>(go))
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }

    private bool ActivateFinger(System.Type type, GameObject go)
    {
        if (searcher.IsThereAnyOf(type, go))
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }
}
