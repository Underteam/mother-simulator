﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideNSeekTutorial : Tutorial
{
    private AbilityInterfaceSearcher searcher;

    private int step = 0;

    public List<Sprite> sprites;

    private bool wrongItem;
    private bool babyOnHands;

    private BabySitPoint carpet;

    public override void Init()
    {
        searcher = FindObjectOfType<AbilityInterfaceSearcher>();

        step = 0;
        babyOnHands = false;
        wrongItem = false;

        var allSPs = FindObjectsOfType<BabySitPoint>();
        for (int i = 0; i < allSPs.Length; i++)
        {
            if (allSPs[i].description.Equals("HNS1")) carpet = allSPs[i];
        }

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;
        },
        "TaskStarted");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            Baby b = i.GetComponent<Baby>();
            if (b != null)
            {
                babyOnHands = true;

                if (step != 0) step = 1;

                return;
            }
            else
            {
                if (step == 0 || step == 2) wrongItem = true;
            }
        },
        "ItemTaked");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            babyOnHands = true;
        },
        "BabyAboutToTake");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            wrongItem = false;
            babyOnHands = false;
        },
        "ItemDroped");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;
        },
        "WishSatisfied");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    public override void Deactivate()
    {
        OnDestroy();
    }

    void Start()
    {

    }

    public override IEnumerator Job()
    {
        yield return new WaitForSeconds(0.5f);

        var player = PlayerController.instance;

        var btnAct = Groups.Instance().GetGroup("Button Act")[0].GetComponent<RectTransform>();
        var btnDrop = Groups.Instance().GetGroup("Button Throw")[0].GetComponent<RectTransform>();
        var btnPut = Groups.Instance().GetGroup("Button Put")[0].GetComponent<RectTransform>();
        TutorialManager.Instance().SetFingerPos(btnAct);

        GameObject targetObject = null;
        System.Type targetInterface = null;
        float targetHeight = 1f;
        bool hide = false;

        TutorialManager tm = TutorialManager.Instance();

        while (step < 3)
        {
            if (step == 0)//взять пацана
            {
                TutorialManager.Instance().UnHightlightAll();

                targetObject = Baby.instance.gameObject;
                targetInterface = typeof(InterfaceCollect);
                targetHeight = 1.1f;
                hide = true;

                while (step == 0)
                {
                    if (wrongItem)
                    {
                        TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
                        TutorialManager.Instance().ShowFinger(btnPut);
                        TutorialManager.Instance().UnHightlightObject(targetObject);
                    }
                    else
                    {
                        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial02note1", "Tap to put"));
                        ActivateFinger(targetInterface, targetObject);

                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["take"].sprites).hideWhenVisible = hide;
                    }

                    if (babyOnHands)
                    {
                        step = 1;
                    }

                    yield return null;
                }
            }

            if (step == 1)//посадить на коврик
            {
                TutorialManager.Instance().UnHightlightAll();

                targetObject = carpet.gameObject;
                targetInterface = typeof(AbilityInterfaceApplyItem);
                targetHeight = 0.3f;
                hide = false;

                while (step == 1)
                {
                    {
                        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorialseeknote2", "Tap to put"));
                        ActivateFinger(targetInterface, targetObject);

                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["take"].sprites).hideWhenVisible = hide;
                    }

                    if (carpet.baby != null)
                    {
                        Debug.LogError("Continue");
                        step = 2;
                    }
                    else if (!babyOnHands)
                    {
                        step = 0;
                    }

                    yield return null;
                }
            }

            if (step == 2)//найти
            {
                TutorialManager.Instance().UnHightlightAll();
                TutorialManager.Instance().HideFinger();

                yield return new WaitForSeconds(2f);

                targetObject = Baby.instance.gameObject;
                targetInterface = typeof(InterfaceCollect);
                targetHeight = 1.1f;
                hide = true;

                while (step == 2)
                {
                    if (wrongItem)
                    {
                        TutorialManager.Instance().finger.Set(btnPut, Localizer.GetString("wrongItem", "Tap to put"));
                        TutorialManager.Instance().ShowFinger(btnPut);
                        TutorialManager.Instance().UnHightlightObject(targetObject);
                    }
                    else
                    {
                        TutorialManager.Instance().finger.Set(btnAct, Localizer.GetString("tutorial02note1", "Tap to put"));
                        ActivateFinger(targetInterface, targetObject);

                        TutorialManager.Instance().HightlightObject(targetObject, targetHeight, tm.icons["take"].sprites).hideWhenVisible = hide;
                    }

                    if (babyOnHands)
                    {
                        step = 3;
                    }

                    yield return null;
                }
            }

            yield return null;
        }

        TutorialManager.Instance().HideFinger();
        TutorialManager.Instance().UnHightlightAll();

        while (true) yield return null;
    }

    private bool ActivateFinger<T>(GameObject go) where T : AbilityInterface
    {
        if (searcher.IsThereAnyOf<T>(go))
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }

    private bool ActivateFinger(System.Type type, GameObject go)
    {
        if (searcher.IsThereAnyOf(type, go))
        {
            Groups.Instance().SetGroupState("Finger", true);
            return true;
        }
        else
        {
            Groups.Instance().SetGroupState("Finger", false);
            return false;
        }
    }
}
