﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public Transform spawnPoint;

    public GameObject prefab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) Spawn();
    }

    public void Spawn ()
    {
        var o = Instantiate(prefab);

        o.transform.position = spawnPoint.position + 0.5f * spawnPoint.forward;
        o.transform.rotation = Quaternion.Euler(Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f));

        var rb = o.GetComponent<Rigidbody>();

        if (rb != null)
        {
            rb.AddForce(2f * spawnPoint.forward);
        }
    }
}
