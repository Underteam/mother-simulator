﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainDish : MonoBehaviour, IAppropriatanceProvider
{
    public Transform foodPoint;

    public GuestFood food;

    // Start is called before the first frame update
    void Start()
    {
        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            var f = i.GetComponent<GuestFood>();
            if (food != null && f != null && f.gameObject == food)
            {
                food = null;
            }
        },
        "ItemTaked");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnApply(Item item)
    {
        if (this.food != null) return;
        if (item == null) return;

        GuestFood food = item.GetComponent<GuestFood>();

        if (food == null) return;

        this.food = Instantiate(food);
        Destroy(food.gameObject);
        this.food.gameObject.SetActive(true);
        this.food.transform.SetParent(null);
        this.food.transform.position = foodPoint.position;
        this.food.transform.rotation = foodPoint.rotation;
    }

    public bool IsAppropriate(Item item)
    {
        if (item == null) return false;
        return item.GetComponent<GuestFood>() != null;
    }
}
