﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimonGame : MonoBehaviour
{
    public List<GameObject> buttons;

    public List<AudioClip> sounds;

    public List<int> sequence { get; set; } = new List<int>();

    private int sequenceLength = 4;

    public bool playing { get; set; }

    public int phase { get; set; }

    public bool wrong { get; set; }

    public GameObject btnPlay;

    public GameObject portrait;

    public List<GameObject> game;

    public int startFrom;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        while (!GameState.Instance().started) yield return null;

        if (GameState.Instance().currentTask > startFrom || (GameState.Instance().currentTask == startFrom && GameState.Instance().state == GameState.State.Game))
        {
            portrait.SetActive(false);
            for (int i = 0; i < game.Count; i++)
            {
                game[i].SetActive(true);
            }
        }
        else
        {
            portrait.SetActive(true);
            for (int i = 0; i < game.Count; i++)
            {
                game[i].SetActive(false);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseUpAsButton()
    {
        Play();
    }

    public void Play ()
    {
        if (GameState.Instance().state == GameState.State.Game)
        {
            if (PlayerController.instance.currentItem == null || PlayerController.instance.currentItem.GetComponent<Baby>() == null) return;
        }

        playing = true;
        wrong = false;
        choice = -1;

        StopAllCoroutines();
        for (int i = 0; i < buttons.Count; i++) buttons[i].SetActive(false);
        GenerateSequence();
        StartCoroutine(PlaySequence());
    }

    private void GenerateSequence()
    {
        sequence.Clear();
        for (int i = 0; i < sequenceLength; i++)
        {
            sequence.Add(Random.Range(0, buttons.Count));
        }
    }

    private IEnumerator PlaySequence ()
    {
        phase = 1;

        yield return new WaitForSeconds(0.5f);

        for (int i = 0; i < sequence.Count; i++)
        {
            buttons[sequence[i]].SetActive(true);
            SFXPlayer.Instance().Play(sounds[sequence[i]], false, true);
            yield return new WaitForSeconds(0.8f);
            buttons[sequence[i]].SetActive(false);
            yield return new WaitForSeconds(0.5f);
        }

        StartCoroutine(CheckSequence((r) =>
        {
            Baby.instance.SimonGame(r);
        }));
    }

    private IEnumerator CheckSequence (System.Action<bool> onFinish)
    {
        phase = 2;

        bool result = true;

        while (sequence.Count > 0)
        {
            if (choice >= 0)
            {
                result = result && sequence[0] == choice;
                if (!result) wrong = true;
                choice = -1;
                sequence.RemoveAt(0);
            }

            if (GameState.Instance().state == GameState.State.Game && wrong) break;

            yield return null;
        }

        if (GameState.Instance().state == GameState.State.Game)
        {
            if (PlayerController.instance.currentItem == null || PlayerController.instance.currentItem.GetComponent<Baby>() == null)
            {
                playing = false;
                yield break;
            }
        }

        if (onFinish != null) onFinish(result);

        phase = 0;

        playing = false;
    }

    private int choice = -1;
    public void Choice (int ch)
    {
        if (phase == 1) return;

        if (phase == 2) choice = ch;

        StartCoroutine(HighlightButton(ch));
    }

    private IEnumerator HighlightButton(int b)
    {
        if (b < 0 || b >= buttons.Count) yield break;

        buttons[b].SetActive(true);
        SFXPlayer.Instance().Play(sounds[b], false, true);

        yield return new WaitForSeconds(0.5f);

        buttons[b].SetActive(false);
    }
}
