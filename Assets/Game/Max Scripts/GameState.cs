﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState : MonoSingleton<GameState>
{
    public enum State
    {
        Menu,
        Game,
        Challenge,
    }

    public State state = State.Menu;

    public bool started;

    public int currentTask = 0;

    public List<Booster> appliedBoosters = new List<Booster>();

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
