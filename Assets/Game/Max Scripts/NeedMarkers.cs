﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeedMarkers : MonoSingleton<NeedMarkers>
{
    public List<WorldToCanvas> allMarkers;
    private List<GameObject> highlightedObjects = new List<GameObject>();
    private List<WorldToCanvas> objectsMarkers = new List<WorldToCanvas>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void UnHightlightAll()
    {
        highlightedObjects.Clear();
        objectsMarkers.Clear();
    }

    public void UnHightlightObject(GameObject go)
    {
        int ind = highlightedObjects.IndexOf(go);

        if (ind >= 0)
        {
            highlightedObjects.RemoveAt(ind);
            objectsMarkers.RemoveAt(ind);
        }
    }

    public WorldToCanvas HightlightObject(GameObject go, float height, List<Sprite> sprites)
    {
        if (go == null) return null;

        WorldToCanvas marker = null;

        int ind = highlightedObjects.IndexOf(go);
        if (ind >= 0) marker = objectsMarkers[ind];
        else
        {
            for (int i = 0; i < allMarkers.Count; i++)
            {
                if ((allMarkers[i].target == null || !highlightedObjects.Contains(allMarkers[i].target.gameObject)) && !allMarkers[i].hiding)
                {
                    marker = allMarkers[i];
                    break;
                }
            }

            if (marker == null)
            {
                marker = Instantiate(allMarkers[0]);
                marker.transform.SetParent(allMarkers[0].transform.parent);
                marker.transform.localScale = Vector3.one;
                allMarkers.Add(marker);
            }

            highlightedObjects.Add(go);
            objectsMarkers.Add(marker);
        }

        marker.gameObject.SetActive(true);

        marker.SetSprites(sprites);
        marker.target = go.transform;
        marker.shift.y = height;

        marker.stickToEdge = false;

        return marker;
    }

    private void LateUpdate()
    {
        for (int i = 0; i < allMarkers.Count; i++)
        {
            if (allMarkers[i] == null)
            {
                allMarkers.RemoveAt(i);
                i--;
                continue;
            }

            if (allMarkers[i].target == null && objectsMarkers.Contains(allMarkers[i]))
            {
                int ind = objectsMarkers.IndexOf(allMarkers[i]);
                objectsMarkers.RemoveAt(ind);
                highlightedObjects.RemoveAt(ind);
            }

            if (!allMarkers[i].gameObject.activeSelf || allMarkers[i].hiding) continue;

            if (!objectsMarkers.Contains(allMarkers[i]))
            {
                allMarkers[i].Hide();
            }
        }
    }
}
