﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Item : MonoBehaviour
{
    [System.Serializable]
    public class HandleInfo : ICopyPaster
    {
        public static HandleInfo copied;
        public static Dictionary<HandleInfo, HandleInfo> localCopied;
        private HandleInfo prevValue;

        public HandleInfo()
        {
            copypaster = new CopyPaster(Copy, Paste, LocalCopy, LocalPaste, Undo);
        }

        public enum Phase
        {
            Take,
            Hold,
            Use,
            None,
        }

        public enum Target
        {
            LeftIK,
            RightIK,
            Item,
            LeftBend,
            RightBend,
            LeftShoulder,
            RightShoulder,
        }

        public enum Parent
        {
            None,
            Root,
            LeftHand,
            RightHand,
            Item,
            Base,
        }

        public Phase phase;
        public Target target;
        public Parent parent;

        public bool setPos;
        public bool setRot;

        public Transform posHandler;
        public Transform rotHandler;

        public Vector3 targetPos;
        public Vector3 targetRot;

        public CopyPaster copypaster;

        private void Copy()
        {
            copied = new HandleInfo();
            CopyTo(copied);

            Debug.Log("Copy");
        }

        private void Paste()
        {
            prevValue = new HandleInfo();
            CopyTo(prevValue);

            CopyFrom(copied);

            Debug.Log("Paste");
        }

        private void LocalCopy ()
        {
            var copied = new HandleInfo();
            CopyTo(copied);

            if(localCopied == null) localCopied = new Dictionary<HandleInfo, HandleInfo>();
            if (!localCopied.ContainsKey(this)) localCopied.Add(this, copied);
            else localCopied[this] = copied;

            Debug.Log("Local Copy " + localCopied[this] != null);
        }

        private void LocalPaste()
        {
            prevValue = new HandleInfo();
            CopyTo(prevValue);

            if (localCopied == null) localCopied = new Dictionary<HandleInfo, HandleInfo>();
            HandleInfo copied = null;
            if (localCopied.ContainsKey(this)) copied = localCopied[this];

            CopyFrom(copied);

            Debug.Log("Local Paste " + localCopied.ContainsKey(this));
        }

        private void Undo()
        {
            CopyFrom(prevValue);
            prevValue = null;
        }

        private void CopyTo(HandleInfo info)
        {
            if (info == null) return;

            info.phase = phase;
            info.target = target;
            info.parent = parent;

            info.setPos = setPos;
            info.setRot = setRot;

            info.posHandler = posHandler;
            info.rotHandler = rotHandler;

            info.targetPos = targetPos;
            info.targetRot = targetRot;
        }

        private void CopyFrom(HandleInfo info)
        {
            if (info == null) return;

            info.CopyTo(this);
        }

        public HandleInfo Clone()
        {
            var clone = new HandleInfo();

            this.CopyTo(clone);

            return clone;
        }
    }

    public enum Target
    {
        None,
        LeftIK,
        RightIK,
        Item,
    }

    public List<HandleInfo> handlies;

    public Target applyInterfaceTarget;

    public UnityEvent onStartTake;
    public UnityEvent onBeforeTake;
    public UnityEvent onTaked;
    public UnityEvent onReleased;

    public bool busy { get; set; }

    public bool blockHands;
    public bool skipTakeAnimation = true;
    public bool animateReachout = true;
    public bool animateThrow = true;

    public float grip = 0.7f;

    public Vector3 startPos { get; set; }
    public Quaternion startRot { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        storedScale = transform.lossyScale;

        startPos = transform.position;
        startRot = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnStartTake ()
    {
        if (onStartTake != null) onStartTake.Invoke();
    }

    public void OnBeforeTake ()
    {
        if (onBeforeTake != null) onBeforeTake.Invoke();
    }

    public void OnTaked()
    {
        if (onTaked != null) onTaked.Invoke();
    }

    public void OnReleased()
    {
        if (onReleased != null) onReleased.Invoke();
    }

    public HandleInfo GetInfo(HandleInfo.Phase phase, HandleInfo.Target target)
    {
        for (int i = 0; i < handlies.Count; i++)
        {
            if (handlies[i].phase == phase && handlies[i].target == target) return handlies[i];
        }

        return null;
    }

    private Vector3 storedScale;
    public void RestoreScale ()
    {
        Vector3 s = transform.lossyScale;

        Vector3 a = new Vector3(storedScale.x / s.x, storedScale.y / s.y, storedScale.z / s.z);

        s = transform.localScale;
        s.Scale(a);

        transform.localScale = s;
    }

    void OnTriggerEnter(Collider othrer)
    {
        if (othrer.GetComponent<AutoPlaceTrigger>() != null)
        {
            transform.position = startPos;
            transform.rotation = startRot;
            var rbs = GetComponentsInChildren<Rigidbody>();
            for(int i = 0; i < rbs.Length; i++)
            {
                rbs[i].velocity = Vector3.zero;
                rbs[i].angularVelocity = Vector3.zero;
            }
        }
    }
}