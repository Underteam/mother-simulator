﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoffeeCup : MonoBehaviour, IAppropriatanceProvider
{
    //private List<Item> knownItems = new List<Item>();

    public CoffeeBag myBag { get; private set; }

    public Transform targetPoint;

    public Coffee coffee;

    public DirtyScript ds;

    public bool isClean { get { return ds == null || ds.isClean; } }

    // Start is called before the first frame update
    void Start()
    {
        coffee.SetState(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (myBag != null)
        {
            myBag.transform.localPosition = targetPoint.localPosition;
            myBag.transform.localRotation = targetPoint.localRotation;
        }
        else if(coffee.level < 0.01f)
        {
            coffee.SetState(false);
            coffee.level = 0;
        }

        if (waterSound != null && waterCollider == null && waterSound.isPlaying)
        {
            waterSound.Stop();
            waterSound = null;
        }
    }

    public bool IsAppropriate(Item item)
    {
        if (item == null) return false;

        //if (knownItems.Contains(item)) return true;

        if (item.GetComponent<CoffeeBag>() != null)
        {
            //knownItems.Add(item);
            
            return myBag == null;
        }

        return false;
    }

    public void Apply (Item item)
    {
        Debug.Log("Apply " + item, item);

        if (item == null) return;

        if (myBag != null) return;

        CoffeeBag bag = item.GetComponent<CoffeeBag>();

        if (bag == null) return;

        if (PlayerController.instance.currentItem == item)
        {
            PlayerController.instance.ReleaseItem(() =>
            {
                DoApply(bag);
            });
        }
        else
        {
            DoApply(bag);
        }
    }

    private void DoApply(CoffeeBag bag)
    {
        myBag = Instantiate(bag);

        Destroy(myBag.GetComponent<Rigidbody>());
        Destroy(myBag.GetComponent<Collider>());

        Destroy(bag.gameObject);

        myBag.transform.SetParent(transform);
        myBag.transform.localPosition = targetPoint.localPosition;
        myBag.transform.localRotation = targetPoint.localRotation;

        coffee.SetState(true);

        if (myBag != null && coffee.level > 0.2f)
        {
            Destroy(myBag.gameObject);
            myBag = null;
        }
    }

    private Collider waterCollider;
    private MyAudioSource waterSound;
    private void OnTriggerEnter(Collider other)
    {
        BoiledWater water = other.GetComponent<BoiledWater>();
        if (water != null)
        {
            waterCollider = other;
        }
    }

    private void OnTriggerStay (Collider other)
    {
        if (!PlayerController.instance.handReachedOut) return;

        if (waterCollider != null && other == waterCollider)
        {
            if (waterSound == null)
                waterSound = SFXPlayer.Instance().Play(SFXLibrary.Instance().waterInClip, false, true);

            if (coffee.level < 1) coffee.level += 0.5f * Time.deltaTime;

            if (myBag != null && coffee.level > 0.2f)
            {
                Destroy(myBag.gameObject);
                myBag = null;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (waterCollider != null && other == waterCollider)
        {
            waterCollider = null;
            if (waterSound != null) waterSound.Stop();
            waterSound = null;
        }
    }

    public bool log;

    public System.Action onHit;

    private MyAudioSource drinkingSound;
    private bool applied;
    private void FixedUpdate()
    {
        if (!coffee.particles.gameObject.activeSelf)
        {
            if (drinkingSound != null)
            {
                drinkingSound.Stop();
                drinkingSound = null;
            }

            return;
        }

        bool hit = Vector3.Dot(transform.up, Vector3.down) > -0.5f;

        if (hit)
        {
            var pep = TaskManager.Instance().GetNeed<MomPep>();
            hit = true;
            if (pep != null && pep.level < 1 && coffee.isItCoffee)
            {
                pep.level = 1f;
            }

            if (!applied)
            {
                var pee = TaskManager.Instance().GetNeed<NeedForPee>();
                if (pee != null)
                {
                    pee.AddSeconds(-0.35f / pee.speed, true);
                    if (pee.level < 0) pee.level = 0;
                }
                applied = true;
            }
        }
        else
            applied = false;

        if (hit && coffee.level > 0 && drinkingSound == null)
            drinkingSound = SFXPlayer.Instance().Play(SFXLibrary.Instance().drinking, true, true);
        else if (drinkingSound != null && (!hit || coffee.level <= 0))
        {
            drinkingSound.Stop();
            drinkingSound = null;
        }
    }
}
