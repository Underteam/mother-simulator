﻿using Facebook.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SessionTimeTracker : MonoBehaviour
{
    private static SessionTimeTracker instance;

    private int sessionID;
    private float time;
    private int TimeThreshold = 10;

    private void Awake()
    {
        //Debug.LogError("Awake " + (instance != null), instance);

        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    IEnumerator Start()
    {
        //Debug.LogError("Start " + (instance != null), instance);

        while (!FB.IsInitialized) yield return null;

        //bool writeSessionTime = true;

        if (PlayerPrefs.HasKey("SessionID"))
        {
            sessionID = PlayerPrefs.GetInt("SessionID");
            int time = PlayerPrefs.GetInt("SessionTime", 0);

            //var eventParams = new Dictionary<string, object>();
            //eventParams["fb_session_id"] = sessionID;
            //eventParams["fb_time"] = "unknown";
            //FB.LogAppEvent("SessionTime", time, eventParams);

            Analytics.Instance().SendEvent("SessionTime", time, "fb_session_id", sessionID);

            //if (PlayerPrefs.HasKey("LastSessionStarted"))
            //{
            //    string lastTime = PlayerPrefs.GetString("LastSessionStarted");
            //    System.DateTime t = System.DateTime.UtcNow;
            //    if (System.DateTime.TryParse(lastTime, out t))
            //    {
            //        var span = System.DateTime.UtcNow - t;
            //        if (span.TotalMinutes > 5)
            //        {

            //        }
            //    }
            //}
        }
        else
        {
            sessionID = 0;
        }

        sessionID++;
        PlayerPrefs.SetInt("SessionID", sessionID);
        PlayerPrefs.SetInt("SessionTime", 0);
        //if (writeSessionTime) PlayerPrefs.SetString("LastSessionStarted", System.DateTime.UtcNow.ToLongTimeString());
    }

    // Update is called once per frame
    void Update()
    {
        TrackInGameTime();
    }

    private void TrackInGameTime()
    {
        time += Time.unscaledDeltaTime;
        if (time > TimeThreshold)
        {
            PlayerPrefs.SetInt("SessionTime", TimeThreshold);
            TimeThreshold += 10;
            //Debug.Log("Write " + sessionID + " " + TimeThreshold);
        }
    }
}
