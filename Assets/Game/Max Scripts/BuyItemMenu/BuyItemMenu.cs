﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyItemMenu : MonoBehaviour
{
    public GameObject panel;

    public Image itemIcon;
    public Text lblPrice;

    public Image backLight;

    public Sprite unboughtLight;
    public Sprite boughtLight;

    private ShopItem item;

    private bool refused;

    public GameObject btnBuy;
    public GameObject btnBought;

    //private bool showed;

    private void Awake()
    {
        
    }

    // Start is called before the first frame update
    IEnumerator Start()
    {
        //showed = false;

        while (!GameState.Instance().started) yield return null;

        refused = PlayerPrefs.GetInt("BuyItemRefused", 0) == 1;

        var wallet = Wallet.GetWallet(Wallet.CurrencyType.crystals);
        //wallet.onValueChanged += (w) =>
        //{
        //    Check(w);
        //};

        Check(wallet);
    }

    private void Check(Wallet wallet)
    {
        if (GameState.Instance().state != GameState.State.Menu) return;

        var list = Shop.Instance().GetUnbuyedItems(ShopItem.PriceType.Crystals);

        Debug.Log("Found " + list.Count + " items. Refused " + refused);

        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].price > wallet.amount)
            {
                list.RemoveAt(i);
                i--;
            }
        }

        backLight.sprite = unboughtLight;
        btnBuy.SetActive(true);
        btnBought.SetActive(false);

        if (list.Count > 0)
        {
            item = list[Random.Range(0, list.Count)];

            itemIcon.sprite = item.image;
            lblPrice.text = "" + item.price;

            if (!refused)
            {
                //showed = true;
                MenuQueue.Instance().Show(panel, 1);
            }
            else panel.SetActive(false);
        }
        else
        {
            refused = false;
            PlayerPrefs.SetInt("BuyItemRefused", 0);
            panel.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Refuse ()
    {
        if (btnBuy.activeSelf) PlayerPrefs.SetInt("BuyItemRefused", 1);
        panel.SetActive(false);

        refused = PlayerPrefs.GetInt("BuyItemRefused", 0) == 1;
        Debug.LogError("Refused " + refused);
    }

    public void Buy ()
    {
        backLight.sprite = boughtLight;
        btnBuy.SetActive(false);
        btnBought.SetActive(true);

        Shop.Instance().ApplyItem(item);
        Shop.Instance().ApplyItem(item);
    }
}
