﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;
using GMATools.Common;

public class Husband : MonoBehaviour, IAppropriatanceProvider
{
    public Animator anim;

    public FullBodyBipedIK ikController;

    public Transform ikTarget;

    private float targetWeight;
    private float weight;

    public Vector3 shift;

    public Transform mouth;

    private float holdTimer;

    public AbilityInterfaceApplyItem applyInterface;

    private HusbandMood need;

    public float calmTimer = 10;

    public bool angry { get; private set; }

    private float angryTimer;

    public GameObject canvasWants;

    public AudioSource typing;

    void Awake ()
    {
        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            need = FindObjectOfType<HusbandMood>();
        },
        "TaskStarted");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    // Start is called before the first frame update
    void Start()
    {
        applyInterface.description = Localizer.GetString("Husband", "Муж");
    }

    // Update is called once per frame
    void Update()
    {
        weight = Mathf.Lerp(weight, targetWeight, 5 * Time.deltaTime);
        ikController.solver.GetEffector(FullBodyBipedEffector.LeftHand).positionWeight = weight;
        ikController.solver.GetEffector(FullBodyBipedEffector.LeftHand).rotationWeight = weight;

        if (currCup != null)
        {
            ikTarget.transform.position = currCup.transform.position + shift;

            if (weight > 0.99f && drinkingSound == null)
            {
                Debug.LogError("Here");
                drinkingSound = SFXPlayer.Instance().Play(SFXLibrary.Instance().drinking, false, true);
            }

            if (!hold)
            {
               
            }
            else
            {
                currCup.transform.position = Vector3.Lerp(currCup.transform.position, mouth.position, 5 * Time.deltaTime);
                currCup.transform.rotation = Quaternion.Lerp(currCup.transform.rotation, mouth.rotation, 5 * Time.deltaTime);

                holdTimer -= Time.deltaTime;
                if (angry && currCup.coffee.isItCoffee)
                {
                    angry = false;
                    need.husbandAngry = false;
                }

                if (holdTimer <= 0)
                {
                    hold = false;
                    targetWeight = 0f;

                    var rbs = currCup.GetComponentsInChildren<Rigidbody>();
                    for (int i = 0; i < rbs.Length; i++)
                    {
                        rbs[i].isKinematic = false;
                        rbs[i].velocity = 3 * transform.forward + 3 * Vector3.up + Random.Range(-3f, 3f) * transform.right;
                    }

                    currCup.transform.SetParent(null);
                    currCup.GetComponent<Item>().busy = false;
                    currCup.coffee.level = 0f;
                    calmTimer = 30f;

                    currCup = null;

                    if (drinkingSound != null) drinkingSound.Stop();
                    drinkingSound = null;

                    typing.mute = false;
                }
            }
        }
        else
        {
            if (fineTime > 0)
            {
                if (fineTime < calmTimer)
                {
                    calmTimer -= fineTime;
                    fineTime = 0;
                }
                else
                {
                    fineTime -= calmTimer;
                    calmTimer = 0;
                }
            }

            if (calmTimer > 0) calmTimer -= Time.deltaTime;
            else
            {
                if (fineTime > 0 && need != null)
                {
                    need.level -= fineTime / 100f;
                    fineTime = 0;
                }

                angry = true;
                need.husbandAngry = true;
            }
        }

        if (angry)
        {
            if (angryTimer > 0) angryTimer -= Time.deltaTime;
            else
            {
                anim.SetTrigger("Angry");
                if (angrySound == null && !GameController.Instance().completed) angrySound = SFXPlayer.Instance().Play(SFXLibrary.Instance().husbandAngry, true, true);
                typing.mute = true;
                Invoke("ContinueTyping", 2f);
                angryTimer = Random.Range(3f, 5f);
            }
        }

        if (angry && !canvasWants.activeSelf) canvasWants.SetActive(true);
        else if (!angry && canvasWants.activeSelf) canvasWants.SetActive(false);
    }

    private MyAudioSource angrySound;
    private void ContinueTyping ()
    {
        typing.mute = false;
    }

    public CoffeeCup currCup { get; private set; }
    private MyAudioSource drinkingSound;
    public void TakeItem (Item item)
    {
        if (item == null) return;

        if (currCup != null) return;

        Debug.LogError("Take " + item, item);

        currCup = item.GetComponent<CoffeeCup>();
        if (currCup == null) return;

        item.busy = true;

        targetWeight = 1f;
        hold = false;
        weight = 0f;
        holdTimer = 4f;

        typing.mute = true;
        CancelInvoke("ContinueTyping");
        if (angrySound != null && angrySound.clip == SFXLibrary.Instance().husbandAngry) angrySound.Stop();
        angrySound = null;

        if (!currCup.coffee.isItCoffee || !currCup.isClean)
        {
            holdTimer = 0.5f;
            if (need != null) need.AddSeconds(-5);
        }
        //else holdTimer = 1f + currCup.coffee.level * 4f;

        //GetComponent<Animator>().SetLayerWeight(2, 0.7f);

        item.onReleased.AddListener(new UnityEngine.Events.UnityAction(OnItemReleased));
        PlayerController.instance.ReleaseItem();
    }

    private bool hold = false;
    private void OnItemReleased ()
    {
        currCup.GetComponent<Item>().onReleased.RemoveListener(new UnityEngine.Events.UnityAction(OnItemReleased));

        var rbs = currCup.GetComponentsInChildren<Rigidbody>();
        for(int i = 0; i < rbs.Length; i++)
        {
            rbs[i].isKinematic = true;
        }
        
        weight = targetWeight;
        currCup.transform.SetParent(transform);
        currCup.transform.position = applyInterface.targetPoint.position;
        currCup.transform.rotation = applyInterface.targetPoint.rotation;
        ikTarget.transform.position = currCup.transform.position + shift;

        hold = true;
    }

    public bool IsAppropriate(Item item)
    {
        if (item == null) return false;

        return item.GetComponent<CoffeeCup>() != null;
    }

    private float fineTime;
    public void Fine(float time)
    {
        fineTime = time;
    }

    private void OnDisable()
    {
        if (drinkingSound != null) drinkingSound.Stop();
        if (angrySound != null) angrySound.Stop();
    }
}
