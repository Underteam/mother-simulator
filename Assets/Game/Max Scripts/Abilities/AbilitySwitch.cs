﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AbilitySwitch : Ability
{
    [System.Serializable]
    public class OnInteracted : UnityEvent<AbilityInterface> { }
    public OnInteracted onInteracted;
    
    protected override bool StartInteraction()
    {
        if (currInterface == null) return false;

        //Debug.Log ("[AbilityCollect] StartInteraction with " + currInterface);

        (currInterface as AbilityInterface).StartInteraction(this, OnInteractedWith);

        return true;
    }

    protected override void OnInteractedWith(AbilityInterface interf)
    {
        if (onInteracted != null) onInteracted.Invoke(interf);
    }
}
