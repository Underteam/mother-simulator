﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InterfaceCollect : AbilityInterface {

	public UnityEvent onCollect;

	public override System.Type AbilityType ()
	{
		return typeof(AbilityCollect);
	}

	public override void StartInteraction (Ability ability, System.Action<AbilityInterface> onInteracted)
	{
		if (!(ability is AbilityCollect)) return;

		//Debug.Log("[InterfaceCollect] Collect " + this + " " + gameObject.activeSelf, this);
		if (onCollect != null) onCollect.Invoke ();

        if (onInteracted != null) onInteracted (this);
    }
}
