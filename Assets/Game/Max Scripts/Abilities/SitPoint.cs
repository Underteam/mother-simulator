﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SitPoint : AbilityInterface
{
    public List<Transform> accessPoints;

    public Transform targetPoint;

    public UnityEvent onSit;
    public UnityEvent onStand;

    public bool buisy;

    // Update is called once per frame
    void Update()
    {
        
    }

    public override System.Type AbilityType()
    {
        return typeof(AbilitySit);
    }

    public override void StartInteraction(Ability ability, System.Action<AbilityInterface> onInteracted)
    {
        if (!(ability is AbilitySit) || buisy) return;

        if (onSit != null) onSit.Invoke();

        if (onInteracted != null) onInteracted(this);
    }

    public void Stand ()
    {
        if (onStand != null) onStand.Invoke();
    }
}
