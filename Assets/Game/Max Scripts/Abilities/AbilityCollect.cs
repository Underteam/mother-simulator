﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AbilityCollect : Ability {

    [System.Serializable]
    public class OnInteracted : UnityEvent<AbilityInterface> { }

    public OnInteracted onInteracted;

	// Use this for initialization
	void Start () {
		
	}

	public void Found () {
	
		//Debug.Log ("Found " + this);
	}

	public void Lost () {
	
		//Debug.Log ("Lost " + this);
	}

	protected override bool StartInteraction ()
	{
		if (currInterface == null) return false;

        if (!currInterface.enabled || !currInterface.gameObject.activeInHierarchy) return false;

		(currInterface as AbilityInterface).StartInteraction (this, OnInteractedWith);

        return true;
	}

    protected override void OnInteractedWith(AbilityInterface interf)
    {
        if (onInteracted != null) onInteracted.Invoke(interf);
    }
}
