﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityInterfaceSearcher : MonoSingleton<AbilityInterfaceSearcher>
{
	public List<Ability> abilities;

	private List<AbilityInterface> interfaces = new List<AbilityInterface>();

	private List<AbilityInterface> foundInterfaces = new List<AbilityInterface> ();

	public float length = 5;
	public float radius = 0.01f;

	public Text lblInfo;
    public GameObject dot;

    public GameObject btnAct;

	// Use this for initialization
	void Start () {

		this.abilities.Sort ((x, y) => 
        {
			return x.priority.CompareTo (y.priority);
		});
	}

	void Update () {

        bool used = false;
        bool use = false;

		for (int i = 0; i < abilities.Count; i++) {

            bool skip = true;
            for (int j = 0; j < abilities[i].activationKeys.Count && skip; j++)
                skip = abilities[i].activationKeys[j] == KeyCode.None;

            skip = skip && abilities[i].activationKey == KeyCode.None;
            skip = skip && !InputManager.Instance().GetButtonDown("Act");

            if (skip) continue;

            use = InputManager.Instance().GetButtonDown("Act");
            use = use || InputManager.Instance().GetButtonDown (abilities[i].activationKey);

            for (int j = 0; j < abilities[i].activationKeys.Count && !use; j++)
                use = InputManager.Instance().GetButtonDown (abilities[i].activationKeys[j]);

            if (use)
            {
                bool b = abilities[i].Use();
                //Debug.Log("Try use " + abilities[i] + " " + b);
                used = used || b;
            }
        }

        //if (use) Debug.Log("Used " + used + " " + foundInterfaces.Count + " " + (foundInterfaces.Count > 0 ? "" + foundInterfaces[0] : ""));

        if (use && !used && foundInterfaces.Count > 0)
        {
            for (int i = 0; i < foundInterfaces.Count; i++)
            {
                if (foundInterfaces[i] != null && foundInterfaces[i].gameObject.activeInHierarchy)
                {
                    //SFXPlayer.Instance().Play(SFXLibrary.instance.accessDenied);
                    break;
                }
            }
        }

        if (btnAct != null)
        {
            info = "not appropriate";

            bool isAppropriateInterface = false;
            if (PlayerController.instance.currentItem == null)
            {
                for (int i = 0; i < foundInterfaces.Count; i++)
                {
                    var ci = foundInterfaces[i] as InterfaceCollect;
                    if (ci == null && foundInterfaces[i] is InterfaceProvider ip)
                    {
                        ci = ip.providedInterface as InterfaceCollect;
                    }

                    if (ci != null)
                    {
                        info = "1 appropriate for " + ci;
                        isAppropriateInterface = true;
                        break;
                    }
                }
            }
            //else
            {
                for (int i = 0; i < foundInterfaces.Count; i++)
                {
                    var ai = foundInterfaces[i] as AbilityInterfaceApplyItem;
                    var sw = foundInterfaces[i] as Switcher;
                    var sp = foundInterfaces[i] as SitPoint;
                    if (sw != null || sp != null || ai != null && ai.IsAppropriateItem(PlayerController.instance.currentItem))
                    {
                        info = "2 appropriate for " + ai;
                        isAppropriateInterface = true;
                        break;
                    }
                }
            }

            if (isAppropriateInterface && !btnAct.activeSelf) btnAct.SetActive(true);
            else if (!isAppropriateInterface && btnAct.activeSelf) btnAct.SetActive(false);
            //dot.SetActive(!btnAct.activeSelf);
        }
    }

    public string info;
    public bool log;

    public List<Collider> chits = new List<Collider>();
    public List<float> dists = new List<float>();
    private List<RaycastHit> list = new List<RaycastHit>();
    void FixedUpdate () {

        chits.Clear();
        dists.Clear();
        list.Clear();

		RaycastHit[] hits = Physics.SphereCastAll (transform.position, radius, transform.forward, length);
		int k = 0;
        list.AddRange(hits);
        list.Sort((x, y) => {
            return x.distance.CompareTo(y.distance);
        });

        for(int i = 0; i < list.Count; i++)
        {
            if (log) Debug.Log("Check " + list[i].transform + " " + IsParentOf(transform, list[i].transform) + " " + IsChildOf(transform.parent, list[i].transform) + " " + (list[i].collider.isTrigger && list[i].collider.GetComponent<AbilityInterface>() == null), list[i].transform);

            if (IsParentOf(transform, list[i].transform) || IsChildOf(transform.parent, list[i].transform) || !list[i].transform.gameObject.activeInHierarchy)
            {
                list.RemoveAt(i);
                i--;
            }
            else
            {
                if (list[i].collider.isTrigger && list[i].collider.GetComponent<AbilityInterface>() == null)
                {
                    list.RemoveAt(i);
                    i--;
                }
            }
        }

        if (list.Count > 1) list.RemoveRange(1, list.Count - 1);

        if (list.Count > 0) hitPoint = list[0].point;

		for (int i = 0; i < list.Count; i++) {

            chits.Add(list[i].collider);
            dists.Add(list[i].distance);

			AbilityInterface[] newInterfaces = list[i].collider.GetComponents<AbilityInterface> ();
            if (newInterfaces.Length == 0) newInterfaces = list[i].collider.GetComponentsInParent<AbilityInterface>();

            if (newInterfaces.Length == 0)
            {
                if (!list[i].collider.isTrigger)
                    break;//если коллайдер перекрывает
                else
                    continue;
            }

			for (int j = 0; j < newInterfaces.Length; j++) {

                if (!newInterfaces[j].enabled) continue;

                //if (log) Debug.Log(newInterfaces[j] + " " + interfaces.Contains(newInterfaces[j]));
				if (k < foundInterfaces.Count) foundInterfaces [k] = newInterfaces[j];
				else foundInterfaces.Add(newInterfaces[j]);
				k++;

				if (interfaces.Contains (newInterfaces[j])) continue;

				interfaces.Add (newInterfaces[j]);
				for (int l = 0; l < abilities.Count; l++)
                {
                    //Debug.Log("found " + newInterfaces[j] + " | " + newInterfaces.Length + " " + list.Count, newInterfaces[j]);
					abilities [l].OnInterfaceFound (newInterfaces[j]);
				}
			}
		}

		if (k < foundInterfaces.Count) foundInterfaces.RemoveRange (k, foundInterfaces.Count - k);

		for (int i = interfaces.Count-1; i >= 0; i--) {
		
			if(!foundInterfaces.Contains(interfaces[i])) {
				for (int j = 0; j < abilities.Count; j++)
					abilities [j].OnInterfaceLost (interfaces[i]);
				interfaces.RemoveAt (i);
			}
		}

		if (lblInfo != null) {
			if (foundInterfaces.Count > 0)
				lblInfo.text = string.IsNullOrEmpty(foundInterfaces[0].description) ? foundInterfaces [0].name : foundInterfaces[0].description;
			else
				lblInfo.text = "";
		}
	}

	public void AddAbility (Ability ability) {
	
		if (ability == null) return;

		if (!abilities.Contains (ability)) {
			int i = 0;
			while (i < abilities.Count && abilities [i].priority > ability.priority) i++;
			if (i < abilities.Count)
				abilities.Insert (i, ability);
			else
				abilities.Add (ability);
		}
	}

	public void RemoveAbility (Ability ability) {
	
		if (!this.abilities.Contains (ability))
			return;

		abilities.Remove (ability);

		for (int i = 0; i < interfaces.Count; i++)
			ability.OnInterfaceLost (interfaces [i]);
	}

    private bool IsChildOf(Transform parent, Transform child)
    {
        Transform p = child;

        while (p != null)
        {
            if (p == parent) return true;
            p = p.parent;
        }

        return false;
    }

    private bool IsParentOf(Transform child, Transform parent)
    {
        Transform p = child;

        while (p != null)
        {
            if (p == parent) return true;
            p = p.parent;
        }

        return false;
    }

    public bool IsThereAnyOf<T> (GameObject go = null) where T : AbilityInterface
    {
        for(int i = 0; i < interfaces.Count; i++)
        {
            if (interfaces[i] is T)
            {
                if (go == null)
                    return true;

                if (go.GetComponent<T>() == interfaces[i])
                    return true;
            }
        }

        return false;
    }

    public bool IsThereAnyOf(System.Type type, GameObject go = null)
    {
        for (int i = 0; i < interfaces.Count; i++)
        {
            if (interfaces[i].GetType().Equals(type))
            {
                if (go == null)
                    return true;

                var component = go.GetComponent(type);
                if (component == null) return false;

                if (component.gameObject == interfaces[i].gameObject)
                    return true;
            }
        }

        return false;
    }

    private Vector3 hitPoint;
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(hitPoint, 0.01f);
    }
}
