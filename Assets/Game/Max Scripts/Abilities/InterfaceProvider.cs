﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterfaceProvider : AbilityInterface
{
    public AbilityInterface providedInterface;

    public override void StartInteraction(Ability ability, Action<AbilityInterface> onInteracted)
    {
        if (providedInterface == null) return;

        providedInterface.StartInteraction(ability, onInteracted);
    }

    public override Type AbilityType()
    {
        if (providedInterface == null) return base.AbilityType();

        return providedInterface.AbilityType();
    }

    public override void Interact()
    {
        if (providedInterface == null) return;

        providedInterface.Interact();
    }
}
