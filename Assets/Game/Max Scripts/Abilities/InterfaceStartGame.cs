﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InterfaceStartGame : AbilityInterface
{
    public UnityEvent onInteract;

    void Start ()
    {
        description = Localizer.GetString("startnewday", description);
    }

    public override System.Type AbilityType()
    {
        return typeof(AbilityCollect);
    }

    public override void StartInteraction(Ability ability, System.Action<AbilityInterface> onInteracted)
    {
        if (!(ability is AbilityCollect)) return;

        //Debug.Log("[InterfaceCollect] Collect " + this + " " + gameObject.activeSelf, this);
        if (onInteract != null) onInteract.Invoke();
    }
}
