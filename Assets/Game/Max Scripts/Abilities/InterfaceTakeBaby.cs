﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterfaceTakeBaby : AbilityInterface
{
    public Transform leftHandler;
    public Transform rightHandler;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override System.Type AbilityType()
    {
        return typeof(AbilityTakeBaby);
    }

    public override void StartInteraction(Ability ability, System.Action<AbilityInterface> onInteracted)
    {
        if (!(ability is AbilityTakeBaby)) return;

        if (onInteracted != null) onInteracted(this);
    }
}
