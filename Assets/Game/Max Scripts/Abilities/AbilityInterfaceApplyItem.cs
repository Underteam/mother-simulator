﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public interface IAppropriatanceProvider
{
    bool IsAppropriate(Item item);
}

public class AbilityInterfaceApplyItem : AbilityInterface
{
    public Transform targetPoint;

    [System.Serializable]
    public class OnApply : UnityEvent<Item> { }
    public OnApply onApply;

    public GameObject appropriatanceProvider;
    private IAppropriatanceProvider _appropriatanceProvider;

    protected override void Init()
    {
        base.Init();

        if (appropriatanceProvider != null) _appropriatanceProvider = appropriatanceProvider.GetComponent<IAppropriatanceProvider>();
    }

    public override System.Type AbilityType()
    {
        return typeof(AbilityApplyItem);
    }

    public override void StartInteraction(Ability ability, System.Action<AbilityInterface> onInteracted)
    {
        //Debug.LogError("StartInteraction " + ability + " " + (onInteracted != null), ability);

        if (!(ability is AbilityApplyItem)) return;

        if (onInteracted != null) onInteracted(this);
    }

    public void Apply (Item item)
    {
        //Debug.LogError("Apply " + item, item);

        if (onApply != null) onApply.Invoke(item);
    }

    public bool IsAppropriateItem(Item item)
    {
        if (_appropriatanceProvider != null) return _appropriatanceProvider.IsAppropriate(item);

        return true;
    }
}
