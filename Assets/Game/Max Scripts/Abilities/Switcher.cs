﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Switcher : AbilityInterface {

    public bool isOn;

    public UnityEvent onSwitchOn;

    public UnityEvent onSwitchOff;

    public List<Switcher> dependents;

    public Transform targetPoint;

    public bool locked;

    public AudioClip sound;

    public SpriteRenderer icon;

    protected override void Init()
    {
        base.Init();

        CheckDependencies();

        if (isOn)
        {
            if (onSwitchOn != null) onSwitchOn.Invoke();
        }
        else
        {
            if (onSwitchOff != null) onSwitchOff.Invoke();
        }
    }
	
    public void CheckDependencies ()
    {
        for (int i = 0; i < dependents.Count; i++)
        {
            if (!dependents[i].dependents.Contains(this)) dependents[i].dependents.Add(this);

            for(int j = 0; j < dependents[i].dependents.Count; j++)
            {
                if (dependents[i].dependents[j] != this && !dependents.Contains(dependents[i].dependents[j])) dependents.Add(dependents[i].dependents[j]);
            }
        }
    }

	// Update is called once per frame
	void Update () {
		
	}

    public override Type AbilityType()
    {
        return typeof(AbilitySwitch);
    }

    public override void StartInteraction(Ability ability, System.Action<AbilityInterface> onInteracted)
    {
        if (!(ability is AbilitySwitch)) return;

        //Switch();

        if (onInteracted != null) onInteracted(this);
    }

    public void Switch ()
    {
        if (!enabled) return;

        if (locked)
        {
            GameController.Instance().RoomClosed(this);
            return;
        }

        isOn = !isOn;

        if (sound != null) SFXPlayer.Instance().Play(sound);

        for (int i = 0; i < dependents.Count; i++) dependents[i].isOn = isOn;

        if (isOn)
        {
            if (onSwitchOn != null) onSwitchOn.Invoke();
        }
        else
        {
            if (onSwitchOff != null) onSwitchOff.Invoke();
        }
    }
}
