﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AbilityTakeBaby : Ability
{
    [System.Serializable]
    public class OnInteracted : UnityEvent<AbilityInterface> { }

    public OnInteracted onInteracted;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override bool StartInteraction()
    {
        if (currInterface == null) return false;

        if (!currInterface.enabled || !currInterface.gameObject.activeInHierarchy) return false;

        (currInterface as AbilityInterface).StartInteraction(this, OnInteractedWith);

        return true;
    }

    protected override void OnInteractedWith(AbilityInterface interf)
    {
        if (onInteracted != null) onInteracted.Invoke(interf);
    }
}
