﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AbilitySit : Ability
{
    //public Animator anim;

    private SitPoint sitPoint;

    private bool sitting;

    private Transform point1;

    private Transform point2;

    private int phase = 0;
    private float timer = 0f;

    private float timer2;

    //[System.Serializable]
    //public class OnInteracted : UnityEvent<AbilityInterface> { }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Update()
    {
        if (sitting)
        {
            if (phase == 0)
            {
                transform.position = Vector3.Lerp(transform.position, point1.position, 5 * Time.deltaTime);
                transform.rotation = Quaternion.Lerp(transform.rotation, point1.rotation, 5 * Time.deltaTime);
                timer += Time.deltaTime;
                if (timer >= 0.5f) phase = 1;
            }

            if (phase == 1)
            {
                transform.position = Vector3.Lerp(transform.position, point2.position, 2 * Time.deltaTime);
                transform.rotation = Quaternion.Lerp(transform.rotation, point2.rotation, 2 * Time.deltaTime);

                float horizontal = InputManager.Instance().GetAxis("Horizontal");
                float vertical = InputManager.Instance().GetAxis("Vertical");
                if (Mathf.Abs(horizontal) + Mathf.Abs(vertical) > 0) timer2 += Time.deltaTime;
                else timer2 = 0;

                if (timer2 > 0.5f)
                {
                    phase = 2;
                    timer = 0;
                }
            }

            if (phase == 2)
            {
                transform.position = Vector3.Lerp(transform.position, point1.position, 5 * Time.deltaTime);
                transform.rotation = Quaternion.Lerp(transform.rotation, point1.rotation, 5 * Time.deltaTime);
                timer += Time.deltaTime;
                if (timer >= 0.5f)
                {
                    phase = 3;
                    //anim.SetBool("Sit", false);
                    if (sitPoint != null) sitPoint.Stand();
                }
            }

            if (phase == 3)
            {
                timer += Time.deltaTime;
                if (timer >= 0.5f)
                {
                    phase = 4;
                    //anim.GetComponent<FPS_WithBody.FPSWBController>().disableMovement = false;
                    GetComponent<FPSCharacterController>().disableMovement = false;
                }
            }
        }
    }

    protected override bool StartInteraction()
    {
        if (currInterface == null) return false;

        if (!currInterface.enabled || !currInterface.gameObject.activeInHierarchy) return false;

        (currInterface as AbilityInterface).StartInteraction(this, OnInteractedWith);

        return true;
    }

    protected override void OnInteractedWith(AbilityInterface interf)
    {
        sitPoint = interf as SitPoint;

        if (sitPoint != null)
        {
            var list = sitPoint.accessPoints;
            point1 = sitPoint.transform;
            float min = float.MaxValue;
            for (int i = 0; i < list.Count; i++)
            {
                float dist = Vector3.Distance(list[i].position, transform.position);
                if(dist < min)
                {
                    min = dist;
                    point1 = list[i];
                }
            }

            point2 = sitPoint.targetPoint;

            //anim.SetBool ("Sit", true);
            sitting = true;
            phase = 0;
            timer = 0;
            timer2 = 0;

            //anim.GetComponent<FPS_WithBody.FPSWBController>().disableMovement = true;
            GetComponent<FPSCharacterController>().disableMovement = true;

            Debug.LogError("Sit");
        }
    }

    public void Stand ()
    {
        //Debug.LogError ("Stand");

        if (!sitting || phase == 2) return;

        phase = 2;
        timer = 0;
        if (sitPoint != null) sitPoint.Stand();
    }
}
