﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleKeeper : MonoBehaviour
{
    public Vector3 scale;

    // Start is called before the first frame update
    void Start()
    {
        //Debug.LogError("Start", this);
    }

    private void Awake()
    {
        if (scale.sqrMagnitude < 0.001f) scale = transform.lossyScale;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [Sirenix.OdinInspector.Button]
    public void RestoreScale ()
    {
        Vector3 s = transform.lossyScale;

        Vector3 a = new Vector3(scale.x / s.x, scale.y / s.y, scale.z / s.z);

        s = transform.localScale;
        s.Scale(a);

        transform.localScale = s;

        //Debug.LogError ("Restored " + 100 * transform.lossyScale);
    }
}
