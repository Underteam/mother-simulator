﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Ability : MonoBehaviour {

    public List<AbilityInterface> interfaces { get; private set; } = new List<AbilityInterface>();

	public AbilityInterface currInterface;

	public UnityEvent onInterfaceFound;

	public UnityEvent onInterfaceLost;

	public KeyCode activationKey;

    public List<KeyCode> activationKeys;

	protected bool interacting = false;

	public bool needInteraface = true;

	[SerializeField]
	private bool _active;
	public bool active {
	
		get {
			return _active;
		}

		set {
			if (_active && !value)
				OnInterfaceLost (currInterface);

			if (!_active && value)
				OnInterfaceFound (currInterface);

			_active = value;
		}
	}

	public int priority;

	void Start () {
	
		if(!needInteraface && active) OnInterfaceFound (currInterface);
	}

    private void Update()
    {
        OnUpdate();    
    }

    protected virtual void OnUpdate ()
    {
        for(int i = 0; i < interfaces.Count; i++)
        {
            if(interfaces[i] == null)
            {
                interfaces.RemoveAt(i);
                i--;
            }
        }

        if (currInterface == null && interfaces.Count > 0) currInterface = interfaces[0];
    }

    public bool Use () {

		//Debug.Log ("Use " + this + " " + currInterface);

		return StartInteraction ();
	}

	protected virtual void Interacting () {
	
	}

	protected virtual bool StartInteraction () {

		if (currInterface == null) return false;
		
		currInterface.StartInteraction (this, OnInteractedWith);

        return true;
	}

	public void OnInterfaceFound (AbilityInterface interf) {
		
		if (interf == null) return;

		if (interf.AbilityType () != this.GetType ()) return;

		if (!interfaces.Contains (interf)) interfaces.Add (interf);

		if (currInterface == null)
        {
			if (needInteraface) currInterface = interf;
			if (onInterfaceFound != null) onInterfaceFound.Invoke ();
		}
	}

	public void OnInterfaceLost (AbilityInterface interf) {

		//Debug.Log ("Lost " + interf + " " + interfaces.Count);

		if (interf == null) return;
		if (interfaces.Contains (interf)) interfaces.Remove (interf);

		//Debug.Log ("Continue " + (interf == currInterface));

		if (interf != currInterface) return;

		//Debug.Log ("Continue " + interfaces.Count);

		if (interfaces.Count > 0) {
			if (needInteraface) currInterface = interfaces [0];
		} else {
			if (needInteraface) currInterface = null;
			if (onInterfaceLost != null) {
				onInterfaceLost.Invoke ();
			}
		}
	}

    protected virtual void OnInteractedWith(AbilityInterface interf)
    {

    }
}
