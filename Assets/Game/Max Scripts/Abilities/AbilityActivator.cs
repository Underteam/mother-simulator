﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Ability))]
public class AbilityActivator : MonoBehaviour {

	public string activationButton;

	public KeyCode key;

	private Ability ability;

	void Awake () {
	
		ability = GetComponent<Ability> ();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (InputManager.Instance ().GetButtonDown (activationButton) || Input.GetKeyDown(key)) {
			//Debug.Log ("Pressed " + activationButton);
			ability.Use ();
		}
	}
}
