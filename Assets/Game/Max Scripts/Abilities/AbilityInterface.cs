﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IItemNameProvider
{
    string GetItemName();
}

public class AbilityInterface : MonoBehaviour {

    public string description;

    public bool inited { get; protected set; }

	void Start ()
	{
		Init();
	}

	protected virtual void Init ()
	{
        //string sName = "item" + name;
        //if (sName.Equals("wash") || description.Equals("Помыть")) Debug.LogError("!!Get " + sName + " " + description, this);

        var provider = GetComponent<IItemNameProvider>();
        if (provider != null) description = provider.GetItemName();
        else description = Localizer.GetString("item" + name, description);
        inited = true;

        //Debug.LogError("Init " + name + " " + description, this);
	}

	public virtual void Interact () {
	
	}

	public virtual void StartInteraction (Ability ability, System.Action<AbilityInterface> onInteracted) {
	
	}

	public virtual System.Type AbilityType () {

		return typeof(Ability);
	}
}
