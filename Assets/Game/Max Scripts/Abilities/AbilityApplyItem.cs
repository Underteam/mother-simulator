﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityApplyItem : Ability
{
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    protected override void Interacting()
    {
        base.Interacting();
    }

    protected override bool StartInteraction()
    {
        if (currInterface == null) return false;

        if (!currInterface.enabled || !currInterface.gameObject.activeInHierarchy) return false;

        (currInterface as AbilityInterface).StartInteraction(this, OnInteractedWith);

        return true;
    }

    protected override void OnInteractedWith (AbilityInterface interf)
    {
        if (!(interf is AbilityInterfaceApplyItem ai)) return;

        ai.Apply(null);
    }
}
