﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterfaceInfo : AbilityInterface
{
    public string localizationKey;

    private List<AbilityInterface> toLocalize = new List<AbilityInterface>();

    public bool log;

    void Start()
    {
        toLocalize.AddRange(GetComponents<AbilityInterface>());
    }

    protected override void Init()
    {
        
    }

    void Update ()
    {
        if (inited) return;

        if (!Localizer.Inited()) return;

        for (int i = 0; i < toLocalize.Count; i++)
        {
            if (toLocalize[i] == this)
            {
                toLocalize[i].description = Localizer.GetString(localizationKey, description);
                toLocalize.RemoveAt(i);
                i--;
                continue;
            }

            if (!toLocalize[i].inited) continue;
            //if (localizationKey.Equals("wash") || description.Equals("Помыть")) Debug.LogError("!!Get " + localizationKey + " " + description, this);
            toLocalize[i].description = Localizer.GetString(localizationKey, description);
            toLocalize.RemoveAt(i);
            i--;
        }

        if (toLocalize.Count == 0) inited = true;
    }
}
