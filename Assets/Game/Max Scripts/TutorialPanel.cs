﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialPanel : MonoBehaviour
{
    public Animator anim;

    public bool collapse;

    public RectTransform panel;
    public RectTransform startPos;
    public RectTransform endPos;
    private bool collapsing;

    public System.Action onHided;

    private float coolDown;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (coolDown > 0) coolDown -= Time.unscaledDeltaTime;
    }

    private void LateUpdate()
    {
        if (collapsing)
        {
            float dist = Vector3.Distance(panel.position, endPos.position);
            float s = 15 / (1 + dist / startDist);
            panel.position = Vector3.Lerp(panel.position, endPos.position, s * Time.unscaledDeltaTime);
            //Debug.LogError("Collapsing " + panel.position + " -> " + endPos.position + " " + Vector3.Distance(panel.position, endPos.position));
            
            if (dist < 0.2f)
            {
                anim.enabled = true;
                gameObject.SetActive(false);
                collapsing = false;
            }
            else
                panel.localScale = (dist / startDist) * Vector3.one;
        }
    }

    private void OnEnable()
    {
        Time.timeScale = 0f;
        coolDown = 0.8f;
    }

    private void OnDisable()
    {
        Time.timeScale = 1f;
        if (onHided != null) onHided();
    }

    private float startDist;
    public void Hide ()
    {
        if (coolDown > 0) return;
        if (collapsing) return;

        //Debug.Log("Hide");
        //UnityEditor.EditorApplication.isPaused = true;

        if (collapse)
        {
            collapsing = true;
            anim.enabled = false;
            startDist = Vector3.Distance(panel.position, endPos.position);
            //anim.SetTrigger("Collapse");
        }
        else anim.SetTrigger("Hide");
    }

    public void Show ()
    {
        panel.position = startPos.position;
        panel.localScale = Vector3.one;

        gameObject.SetActive(true);
        coolDown = 0.8f;
    }

    public void OnEvent (AnimationEvent ev)
    {
        if (string.IsNullOrEmpty(ev.stringParameter)) return;

        if (ev.stringParameter.Equals("Hided"))
        {
            gameObject.SetActive(false);
        }

        if (ev.stringParameter.Equals("Collapsed"))
        {
            gameObject.SetActive(false);
        }
    }
}
