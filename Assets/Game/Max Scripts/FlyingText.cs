﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlyingText : MonoBehaviour
{
    public Text text;

    public System.Action onDisappear;

    public float speed = 100;

    public float time = 1f;

    public float scale = 2f;

    private float timer;

    public AnimationCurve alphaCurve;

    // Start is called before the first frame update
    void Start()
    {
        if (time < 0.1f) time = 0.1f;

        transform.localScale = Vector3.one;// Vector3.zero;
        var c = text.color;
        c.a = 0;
        text.color = c;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += speed * Vector3.up * Time.unscaledDeltaTime;

        timer += Time.deltaTime;

        float a = timer / time;

        //transform.localScale = a * scale * Vector3.one;
        var c = text.color;
        c.a = alphaCurve.Evaluate(a);
        c.a = Mathf.Clamp(c.a, 0, 1);
        text.color = c;

        if (timer > time)
        {
            gameObject.SetActive(false);
            onDisappear?.Invoke();
        }
    }
}
