﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AgeSettings : MonoBehaviour
{
    public Text lblYear;
    public Text lblYearLeft;
    public Text lblYearRight;

    private bool left;

    private bool right;

    private float timer = 0;

    private float nextTime = 0.5f;

    public int myYear = 2000;

    public int minYear = 1900;

    public int maxYear = 2020;

    public GameObject dots;

    public Button btnOk;

    private bool inited;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Init ()
    {
        if (PlayerPrefs.HasKey("AgeSettings"))
        {
            int age = PlayerPrefs.GetInt("AgeSettings", 12);

            GameController.Instance().StartCoroutine(SetAgeCoroutine(age < 13));

            return;
        }

        gameObject.SetActive(true);

        maxYear = System.DateTime.UtcNow.Year;

        dots.SetActive(true);
        lblYear.gameObject.SetActive(false);

        lblYearLeft.text = "" + (myYear);
        lblYearRight.text = "" + (myYear + 1);

        btnOk.interactable = false;
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.unscaledDeltaTime;

        if ((left || right) && timer <= 0)
        {
            if (left) myYear--;
            else if (right) myYear++;

            myYear = Mathf.Clamp(myYear, minYear, maxYear);

            Apply();

            timer = nextTime;
            if (nextTime > 0.25f) nextTime -= 0.1f;
        }
    }

    public void Apply ()
    {
        lblYear.text = "" + myYear;

        lblYearLeft.text = "" + (myYear - 1);
        lblYearRight.text = "" + (myYear + 1);

        dots.SetActive(false);
        lblYear.gameObject.SetActive(true);

        btnOk.interactable =  true;
    }

    public void LeftDown ()
    {
        left = true;
        right = false;

        if (dots.activeSelf) myYear++;

        timer = 0;
        nextTime = 0.5f;
    }

    public void LeftUp ()
    {
        left = false;
    }

    public void RightDown ()
    {
        right = true;
        left = false;

        timer = 0;
        nextTime = 0.5f;
    }

    public void RightUp ()
    {
        right = false;
    }

    public void Ok ()
    {
        int age = System.DateTime.UtcNow.Year - myYear - 1;

        PlayerPrefs.SetInt("AgeSettings", age);

        //Debug.LogError("Age: " + age + " " + (age < 13));

        this.age = age;
        GameController.Instance().StartCoroutine(SetAgeCoroutine(age < 13));

        gameObject.SetActive(false);
    }

    private int age;
    public IEnumerator SetAgeCoroutine (bool state)
    {
        Debug.Log("Wait for sdk");

        while (!MaxSdk.IsInitialized()) yield return null;

        //Debug.Log("Set age " + state);

        Analytics.Instance().SendEvent("SetAge", age, "Age", age);

        MaxSdk.SetIsAgeRestrictedUser(state);//младше 13 лет
    }
}
