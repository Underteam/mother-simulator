﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bath : MonoBehaviour, IAppropriatanceProvider
{
    public Transform sitPoint;

    public Baby baby { get; set; }

    public WaterLevel water;
    public GameObject fallingWater;

    public GameObject showerParticles;

    private bool withDiaper;

    // Start is called before the first frame update
    void Start()
    {
        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            if (baby != null && i.GetComponent<Baby>() == baby)
            {
                var settings = baby.GetComponent<BabySettings>();
                if (settings != null) settings.ClothesOn();
                if (withDiaper) baby.diaper.SetActive(true);

                baby = null;
                water.Down();
                fallingWater.SetActive(false);
            }
        },
        "ItemTaked");
        listeners.Add(listener);

    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    public void SitBaby(Item baby)
    {
        if (baby == null) return;

        this.baby = baby.GetComponent<Baby>();
        Debug.Log("Sit " + this.baby, this.baby);

        if (this.baby != null)
        {
            this.baby.onReleased = () =>
            {
                this.baby.transform.position = sitPoint.position;
                this.baby.transform.rotation = sitPoint.rotation;
                this.baby.Sit();
                this.baby.onReleased = null;
                EventManager.Instance().TriggerEvent("BabyBath");
                water.Up();
                fallingWater.SetActive(true);

                var settings = baby.GetComponent<BabySettings>();
                if (settings != null) settings.ClothesOff();
                withDiaper = this.baby.diaper.activeSelf;
                this.baby.diaper.SetActive(false);

                showerParticles.SetActive(false);
            };

            PlayerController.instance.ReleaseItem();
        }
    }

    public bool IsAppropriate(Item item)
    {
        if (item == null) return false;
        return item.GetComponent<Baby>() != null;
    }

    public void Shower (bool state)
    {
        showerParticles.SetActive (state);

        if (baby != null && showerParticles.activeSelf) baby.Wash();
    }
}
