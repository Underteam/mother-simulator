﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;

public class BabyTaker : MonoBehaviour
{
    public Baby baby;

    public FullBodyBipedIK ikControl;

    public Transform leftHand;
    public Transform rightHand;

    private Transform leftTarget;
    private Transform rightTarget;

    public Transform leftHandHandlePos;
    public Transform rightHandHandlePos;
    public Transform babyPos;

    private float targetWeight;
    private float currWeight;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        currWeight = Mathf.Lerp(currWeight, targetWeight, 5f * Time.deltaTime);
        
        ikControl.solver.GetEffector(FullBodyBipedEffector.LeftHand).positionWeight = currWeight;
        ikControl.solver.GetEffector(FullBodyBipedEffector.RightHand).positionWeight = currWeight;
    }

    public void Take (AbilityInterface babyInterface)
    {
        InterfaceTakeBaby baby = (babyInterface as InterfaceTakeBaby);
        if (baby == null) return;

        Vector3 v1 = rightHand.position - leftHand.position;
        Vector3 v2 = baby.rightHandler.position - baby.leftHandler.position;

        this.baby = baby.GetComponent<Baby>();

        StartCoroutine(TakeCoroutine(baby.leftHandler, baby.rightHandler));
    }

    private IEnumerator TakeCoroutine (Transform leftHandler, Transform rightHandler)
    {
        yield return null;

        targetWeight = 1f;

        while (currWeight < targetWeight - 0.05f)
        {
            leftHand.position = leftHandler.position;
            rightHand.position = rightHandler.position;

            yield return null;
        }

        baby.transform.SetParent(transform);
        Debug.Log("Set parent " + transform, transform);
        baby.Lie();

        while (true)
        {
            leftHand.position = Vector3.Lerp(leftHand.position, leftHandHandlePos.position, 5 * Time.deltaTime);
            rightHand.position = Vector3.Lerp(rightHand.position, rightHandHandlePos.position, 5 * Time.deltaTime);
            leftHand.rotation = Quaternion.Lerp(leftHand.rotation, leftHandHandlePos.rotation, 5 * Time.deltaTime);
            rightHand.rotation = Quaternion.Lerp(rightHand.rotation, rightHandHandlePos.rotation, 5 * Time.deltaTime);

            baby.transform.position = Vector3.Lerp(baby.transform.position, babyPos.position, 5 * Time.deltaTime);
            baby.transform.rotation = Quaternion.Lerp(baby.transform.rotation, babyPos.rotation, 5 * Time.deltaTime);

            yield return null;
        }
    }
}
