﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GMATools
{
    public class Rotate : MonoBehaviour
    {
        public Vector3 speed;

        public bool unscaledTime;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            transform.Rotate (speed * (unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime));
        }
    }
}
