﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialCases : MonoBehaviour
{
    [System.Serializable]
    public class Case
    {
        public string name;
        public int openFromDay;
        public List<GameObject> objects;
    }

    public List<Case> cases;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return null;

        if (GameState.Instance().state != GameState.State.Menu) yield break;

        for (int i = 0; i < cases.Count; i++)
        {
            if (GameState.Instance().currentTask >= cases[i].openFromDay)
            {
                for(int j = 0; j < cases[i].objects.Count; j++)
                {
                    cases[i].objects[j].SetActive(true);
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
