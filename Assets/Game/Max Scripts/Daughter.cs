﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Daughter : MonoBehaviour, IAppropriatanceProvider
{
    public Transform targetPos;

    public float camRot;
    private Quaternion targetRot;

    private FPSCharacterController player;

    private bool playing;

    public AnimationClip handLeft;
    public AnimationClip handRight;

    public List<GameObject> hairs;
    private int currHair = -1;

    public bool changing { get; private set; }

    private DaughterMood need;
    public float angryTimer { get; set; } = 5f;

    public GameObject particles;

    public Animator anim;

    public AudioClip callSFX;
    public AudioClip happySFX;

    private float callTimer;
    private float talkTimer;

    void Awake ()
    {
        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            need = FindObjectOfType<DaughterMood>();
        },
        "TaskStarted");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);

    }

    // Start is called before the first frame update
    void Start()
    {
        targetRot = Quaternion.Euler(camRot, 0, 0);

        ChangeHair();
        angryTimer = 5f;
    }

    public void ChangeHair ()
    {
        List<int> posibleHairs = new List<int>();
        for (int i = 0; i < hairs.Count; i++) posibleHairs.Add(i);

        if (posibleHairs.Count < 1) return;

        if (posibleHairs.Contains(currHair) && posibleHairs.Count > 1) posibleHairs.Remove(currHair);

        for (int i = 0; i < hairs.Count; i++) hairs[i].SetActive(false);

        currHair = posibleHairs[Random.Range(0, posibleHairs.Count)];

        hairs[currHair].SetActive(true);

        var p = Instantiate(particles, particles.transform.parent);
        p.transform.localPosition = particles.transform.localPosition;
        p.transform.localRotation = particles.transform.localRotation;
        p.transform.localScale = particles.transform.localScale;
        p.SetActive(true);

        angryTimer = 30f;
    }

    public string info;
    private float timer;
    void Update()
    {
        info = "null";

        if (talkTimer > 0)
        {
            talkTimer -= Time.deltaTime;
            if (talkTimer <= 0)
            {
                anim.SetBool("Talk", false);
            }
        }

        if (player != null)
        {
            float horizontal = InputManager.Instance().GetAxis("Horizontal");
            float vertical = InputManager.Instance().GetAxis("Vertical");
            if (Mathf.Abs(horizontal) + Mathf.Abs(vertical) > 0) timer += Time.deltaTime;
            else timer = 0;

            if (timer > 0.2f)
            {
                changing = false;
                AllowMovement();
                timer = 0;
                return;
            }

            SetTargetPos();

            player.transform.position = Vector3.Lerp(player.transform.position, targetPos.position, 5 * Time.deltaTime);
            player.transform.rotation = Quaternion.Lerp(player.transform.rotation, targetPos.rotation, 5 * Time.deltaTime);
            player.pivot.transform.localRotation = Quaternion.Lerp(player.pivot.transform.localRotation, targetRot, 5 * Time.deltaTime);

            Vector3 dir = targetPos.position - player.transform.position;
            float d1 = dir.magnitude;
            if (d1 > 0.1f) d1 = Vector3.Dot(dir.normalized, dir);
            float d2 = Quaternion.Angle(player.transform.rotation, targetPos.rotation) / 10f;

            info = d1 + " " + d2 + " " + Mathf.Max(d1, d2);

            if (!playing && Mathf.Max(d1, d2) < 0.1f)
            {
                playing = true;

                Debug.LogError("Play " + d1 + " " + d2 + " " + Mathf.Max(d1, d2));

                PlayerController.instance.leftHand.Play(handLeft);
                PlayerController.instance.rightHand.Play(handRight);
                PlayerController.instance.handReachedOut = true;
                Invoke("AllowMovement", 1.1f);
            }
        }

        if (need != null)
        {
            if (angryTimer > 0)
            {
                angryTimer -= Time.deltaTime;
                need.daughterAngry = false;
            }
            else if (!need.daughterAngry)
            {
                SFXPlayer.Instance().Play(callSFX);
                anim.SetBool("Talk", true);
                talkTimer = 1f;
                need.daughterAngry = true;
                callTimer = 10;
            }

            if (need.daughterAngry)
            {
                callTimer -= Time.deltaTime;
                if (callTimer <= 0)
                {
                    SFXPlayer.Instance().Play(callSFX);
                    anim.SetBool("Talk", true);
                    talkTimer = 1f;
                    callTimer = 10;
                }
            }
        }
    }

    private void AllowMovement()
    {
        playing = false;
        player = null;
        if (PlayerController.instance == null) return;
        PlayerController.instance.handReachedOut = false;
        PlayerController.instance.leftHand.Stop();
        PlayerController.instance.rightHand.Stop();
        PlayerController.instance.GetComponent<FPSCharacterController>().disableMovement = false;
        PlayerController.instance.GetComponent<FPSCharacterController>().disableRotation = false;

        if (changing)
        {
            ChangeHair();
            anim.SetTrigger("Happy");
            SFXPlayer.Instance().Play(happySFX);
            anim.SetBool("Talk", true);
            talkTimer = 1f;
        }

        changing = false;
    }

    public void ApplyItem(Item item)
    {
        Debug.Log("Apply " + item, item);

        if (changing) return;

        if (item == null || item.GetComponent<Hairbrush>() == null) return;

        if (PlayerController.instance == null) return;

        SetTargetPos();

        player = PlayerController.instance.GetComponent<FPSCharacterController>();
        player.disableMovement = true;
        player.disableRotation = true;

        changing = true;
    }

    private void SetTargetPos ()
    {
        Vector3 dir = transform.position - PlayerController.instance.transform.position;
        dir.y = 0;
        float dist = dir.magnitude;
        dir.Normalize();

        Vector3 pos = PlayerController.instance.transform.position;
        if (dist > 2.0f)
        {
            pos = transform.position - 2.0f * dir;
            pos.y = 0;// targetPos.position.y;
        }
        targetPos.position = pos;
        targetPos.rotation = Quaternion.LookRotation(dir);
    }

    public bool IsAppropriate(Item item)
    {
        if (item != null) return false;

        if (PlayerController.instance == null) return false;

        return true;
    }
}
