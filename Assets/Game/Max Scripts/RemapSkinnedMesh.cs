﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemapSkinnedMesh : MonoBehaviour
{
    public SkinnedMeshRenderer copyFrom;

    public List<SkinnedMeshRenderer> copyTo;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [Sirenix.OdinInspector.Button]
    public void Remap ()
    {
        for (int i = 0; i < copyTo.Count; i++)
        {
            copyTo[i].rootBone = copyFrom.rootBone;

            List<Transform> list = new List<Transform>(copyFrom.bones);
            copyTo[i].bones = list.ToArray();
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(copyTo[i]);
#endif
        }
    }

}
