﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetStartPos : MonoBehaviour
{
    public Vector3 startPos;
    public Vector3 startRot;

    // Start is called before the first frame update
    void Start()
    {
        var item = GetComponent<Item>();
        if (item == null) return;

        item.startPos = startPos;
        item.startRot = Quaternion.Euler(startRot);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
