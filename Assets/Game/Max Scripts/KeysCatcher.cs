﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeysCatcher : MonoSingleton<KeysCatcher>
{
    [TextArea]
    public string text;

    private List<string> keys = new List<string>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void AddKey(string key, string val)
    {
        if (keys.Contains(key)) return;

        keys.Add(key);

        text += key + "\t" + val + "\n\r";
    }
}
