﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Clicker : MonoBehaviour
{
    private static Clicker instance;

    public static Clicker Instance ()
    {
        return instance;
    }

    private Vector3 startPoint;

    public Camera cam;

    public float maxDist;

    public List<GameObject> blockersObjects;

    private List<IBlocker> blockers;

    public bool log;

    private void Awake()
    {
        instance = this;    
    }

    // Start is called before the first frame update
    void Start()
    {
        if (blockers == null) blockers = new List<IBlocker>();
        for (int i = 0; i < blockersObjects.Count; i++)
        {
            if (blockersObjects[i] == null) continue;
            var blocker = blockersObjects[i].GetComponent<IBlocker>();
            if (blocker != null) blockers.Add(blocker);
        }
    }

    public List<GameObject> skip;

    public List<GameObject> blockers2;

    private bool PointerOverUI()
    {
        List<RaycastResult> results = new List<RaycastResult>();
        var data = new PointerEventData(EventSystem.current);
        data.position = Input.mousePosition;

        EventSystem.current.RaycastAll(data, results);
        for (int i = 0; i < results.Count; i++)
        {
            if (!skip.Contains(results[i].gameObject))
            {
                //Debug.Log("Blocked by " + i, results[i].gameObject);
                return true;
            }
        }

        for (int i = 0; i < blockers.Count; i++)
        {
            if (blockers[i] != null && blockers[i].blocked)
            {
                //Debug.Log("Blocked by " + i);
                return true;
            }
        }

        return false;
    }

    private bool skipThisTap;
    public void PointerDown ()
    {
        //Debug.LogError("Pointer down " + skipThisTap);

        startPoint = Input.mousePosition;

        if (drop)
            drop = false;
        else
            down = true;

        if (skipThisTap) down = false;
        skipThisTap = false;

        //Debug.LogError("Set " + down);
    }

    public bool down { get; set; }
    public bool drop { get; set; }

    public void PointerUp()
    {
        //Debug.LogError("Pointer up " + PointerOverUI());

        if (PointerOverUI()) return;

        //Debug.LogError("Check " + down);

        if (!down) return;

        var delta = Input.mousePosition - startPoint;

        if (delta.magnitude < 3)
        {
            var ray = cam.ScreenPointToRay(Input.mousePosition);

            List<RaycastHit> list = new List<RaycastHit>(Physics.SphereCastAll(ray, 0.01f));

            list.Sort((a, b) =>
            {
                return a.distance.CompareTo(b.distance);
            });

            for (int i = 0; i < list.Count; i++)
            {
                var interf = list[i].transform.GetComponent<AbilityInterface>();
                if (interf == null) interf = list[i].transform.GetComponentInParent<AbilityInterface>();

                if (interf != null)
                {
                    //Debug.Log("Found " + interf, interf);
                    float dist = list[i].distance;
                    Vector3 shift = list[i].point - list[i].transform.position;
                    TutorialManager.Instance().FocusOn(list[i].transform, true, shift, () =>
                    {
                        if (dist <= maxDist) InputManager.Instance().SetButton("ReachOut", true);
                    });

                    break;
                }
                else
                {
                    if (!list[i].collider.isTrigger)
                    {
                        //Debug.Log("Break at " + list[i].transform, list[i].transform);
                        break;
                    }
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //Debug.LogError("Mouse down");

            List<RaycastResult> results = new List<RaycastResult>();
            var data = new PointerEventData(EventSystem.current);
            data.position = Input.mousePosition;

            EventSystem.current.RaycastAll(data, results);
            //Debug.LogError("Found " + results.Count + " results");
            for (int i = 0; i < results.Count; i++)
            {
                if (blockers2.Contains(results[i].gameObject))
                {
                    skipThisTap = true;
                    //Debug.LogError("Skip");
                }
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            skipThisTap = false;
        }

        if (Input.GetMouseButtonDown(0)) PointerDown();

        if (Input.GetMouseButtonUp(0)) PointerUp();
    }

    private void LateUpdate()
    {
        InputManager.Instance().SetButton("ReachOut", false);
    }
}
