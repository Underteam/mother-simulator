﻿using MoreMountains.NiceVibrations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    public GameObject panel;

    public GameObject mainPanel;
    public GameObject languagesPanel;
    public GameObject btnBack;

    public GameObject closer;

    private bool showed;

    public GameObject btnRestore;

    public List<Setting> settings;

    public AudioSource mainTheme;

    public GameObject btnPlus;
    public GameObject btnMinus;

    private bool loaded;

    public Text lblLanguage;

    public Image imgLanguage;

    public List<SelectLangButton> btnSelectLanguage;

    // Start is called before the first frame update
    void Start()
    {
        var list = Localizer.GetLanguages();
        //Debug.LogError("Languages:");
        for (int i = 0; i < list.Count && i < btnSelectLanguage.Count; i++)
        {
            int ind = i;
            Sprite sprite = Localizer.GetLanguageIcon(list[i]);
            string langName = Localizer.GetString("languageName", list[i], list[i]);
            //Debug.LogError(list[i] + " " + langName, sprite);
            btnSelectLanguage[i].text.text = langName;
            btnSelectLanguage[i].SetIcon(sprite);
            btnSelectLanguage[i].button.onClick.AddListener(() =>
            {
                SetLanguage(ind);
            });
        }
        //languageName

        btnRestore.SetActive(true);

#if UNITY_ANDROID
        btnRestore.SetActive(false);
#endif

        if (!GameController.Instance().developerMode)
        {
            if (btnPlus != null) btnPlus.SetActive(false);
            if (btnMinus != null) btnMinus.SetActive(false);
        }

        for (int i = 0; i < settings.Count; i++) settings[i].Load(true);

        var lang = PlayerPrefs.GetString("SelectedLanguage", Application.systemLanguage.ToString());

        lblLanguage.text = Localizer.GetString("languageName", lang);
        imgLanguage.sprite = Localizer.GetLanguageIcon(lang);
        //Debug.LogError("-----------------> Get language icon " + lang);
        loaded = true;
    }

    public void SetMusicState (bool state)
    {
        if (mainTheme == null) mainTheme = FindObjectOfType<MainTheme>().GetComponent<AudioSource>();

        if (state)
        {
            mainTheme.volume = 0.5f;
        }
        else
        {
            mainTheme.volume = 0.0f;
        }

        if (!loaded) return;

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    public void SetSFXState (bool state)
    {
        if (state)
        {
            SFXPlayer.Instance().volume = 1f;
        }
        else
        {
            SFXPlayer.Instance().volume = 0f;
        }

        if (!loaded) return;

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    public void SetVibroState (bool state)
    {
        Haptic.Instance().SetState(state);

        if (!loaded) return;

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    public void SetNotificationsState (bool state)
    {
        NotificationManager.Instance().SetNotificationsState(state);

        if (!loaded) return;

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    public void SetAdsState (bool state)
    {
        Debug.Log("Set gdpr " + state);

        StartCoroutine(SetAdsSettingsCoroutine(state));

        if (!loaded) return;

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    private IEnumerator SetAdsSettingsCoroutine (bool state)
    {
        Debug.Log("Wait for sdk");

        while (!MaxSdk.IsInitialized()) yield return null;

        Debug.Log("Set settings " + state);

        if (state)
        {
            MaxSdk.SetDoNotSell(false);
            MaxSdk.SetHasUserConsent(true);
        }
        else
        {
            MaxSdk.SetDoNotSell(true);
            MaxSdk.SetHasUserConsent(false);
        }
    }

    public void Switch ()
    {
        showed = !showed;

        panel.SetActive(showed);
        closer.SetActive(showed);

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);

        if (GameController.Instance().developerMode)
        {
            if (btnPlus != null) btnPlus.SetActive(showed);
            if (btnMinus != null) btnMinus.SetActive(showed);
        }
    }

    public void SetLanguage (int ind)
    {
        try
        {
            var languages = Localizer.GetLanguages();
            var language = Localizer.Language();

            Debug.LogError(languages.Count + " " + language);

            if (ind < 0 || ind >= languages.Count) return;

            language = languages[ind];

            PlayerPrefs.SetString("SelectedLanguage", language);

            UnityEngine.SceneManagement.SceneManager.LoadScene(2);
        }
        catch (System.Exception e)
        {
            Debug.LogError("Error: " + e.ToString());
        }
    }

    public void OpenLanguages ()
    {
        languagesPanel.SetActive(true);
        mainPanel.SetActive(false);
        btnBack.SetActive(true);
    }

    public void CloseLanguages ()
    {
        languagesPanel.SetActive(false);
        mainPanel.SetActive(true);
        btnBack.SetActive(false);
    }
}
