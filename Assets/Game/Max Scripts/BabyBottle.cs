﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabyBottle : MonoBehaviour, IAppropriatanceProvider
{
    public Transform pacifierPlace;

    public bool withFood;
    public bool withWater;
    public bool withPacifier { get { return pacifier != null; } }

    private float timer;

    public DirtyScript ds;

    public GameObject milkLevel;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float dot = Vector3.Dot(transform.up, Vector3.up);
        if (pacifier == null && timer > 0 && dot < 0.1f)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)//молоко вылилось
            {
                //if (withWater) withFood = false;
                //withWater = false;
                //milkLevel.SetActive(false);
            }
        }
        else timer = 0.25f;

        if (waterSound != null && milk == null && waterSound.isPlaying)
        {
            waterSound.Stop();
            waterSound = null;
        }

        //if (!ds.isClean && pacifier != null && pacifier.ds.isClean) pacifier.ds.Dirty();
    }

    private Milk milk;
    private MyAudioSource waterSound;
    private void OnTriggerEnter (Collider other)
    {
        if (!PlayerController.instance.handReachedOut) return;

        bool ready = IsAppropriateForUse();

        Milk milk = other.GetComponent<Milk>();
        if (milk != null && (pacifier == null || pacifier.transform.parent != transform))
        {
            withWater = true;
            milkLevel.SetActive(true);
            withFood = true;
            this.milk = milk;
            waterSound = SFXPlayer.Instance().Play(SFXLibrary.Instance().waterInClip, true, true);
        }

        //Debug.LogError("Check " + ready + " " + IsAppropriateForUse());
        if (!ready && IsAppropriateForUse())
        {
            EventManager.Instance().TriggerEvent("BottleReady", this);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Milk milk = other.GetComponent<Milk>();
        //Debug.LogError("Exit " + (milk != null));
        if (milk != null)
        {
            if (milk == this.milk)
            {
                this.milk = null;
                if (waterSound != null) waterSound.Stop();
                waterSound = null;
            }
        }
    }

    public bool IsAppropriate(Item item)
    {
        if (item == null) return false;

        return item.GetComponent<BottlePacifier>() != null || item.GetComponent<BabyFood>() != null;
    }

    public BottlePacifier pacifier;
    public void ApplyItem (Item item)
    {
        if (item == null) return;

        var pacifier = item.GetComponent<BottlePacifier>();

        Debug.Log("Apply " + pacifier + " " + (this.pacifier == null), pacifier);

        if (pacifier != null && this.pacifier == null)
        {
            this.pacifier = pacifier;
            item.onReleased.AddListener(new UnityEngine.Events.UnityAction(OnItemReleased));
            PlayerController.instance.ReleaseItem();
        }

        var food = item.GetComponent<BabyFood>();

        if (food != null)
        {
            PlayerController.instance.rightHand.Play(food.clip);
            food.Use();
            if (this.pacifier == null) withFood = true;
        }
    }

    private void OnItemReleased ()
    {
        var pacifier = this.pacifier;
        this.pacifier = null;

        if (pacifier == null) return;

        pacifier.GetComponent<Item>().onReleased.RemoveListener(new UnityEngine.Events.UnityAction(OnItemReleased));

        PutOnPacifier(pacifier);
    }

    public void PutOnPacifier (BottlePacifier pacifier)
    {
        //Debug.Log("Put on pacifier" + (this.pacifier == null), pacifier);

        if (this.pacifier != null) return;

        bool ready = IsAppropriateForUse();

        this.pacifier = pacifier;

        pacifier.transform.SetParent(transform);

        pacifier.transform.position = pacifierPlace.position;
        pacifier.transform.rotation = pacifierPlace.rotation;

        pacifier.bottle = this;

        pacifier.Applied();

        //Debug.LogError("Check " + ready + " " + IsAppropriateForUse());
        if (!ready && IsAppropriateForUse())
        {
            EventManager.Instance().TriggerEvent("BottleReady", this);
        }
    }

    public void FixPacifier ()
    {
        if (pacifier == null) return;
        pacifier.transform.position = pacifierPlace.position;
        pacifier.transform.rotation = pacifierPlace.rotation;
    }

    public void PacifierRemoved()
    {
        pacifier = null;
    }

    [Sirenix.OdinInspector.Button]
    public void Check()
    {
        Debug.LogError(IsClean() + " " + IsAppropriateForUse());
    }

    public bool IsClean ()
    {
        if (ds != null && !ds.isClean) return false;

        if (pacifier != null && pacifier.ds != null && !pacifier.ds.isClean) return false;

        return true;
    }

    public bool IsAppropriateForUse ()
    {
        if (!withWater) return false;
        if (!withFood) return false;
        if (pacifier == null) return false;

        if (ds != null && !ds.isClean) return false;

        if (pacifier.ds != null && !pacifier.ds.isClean) return false;

        return true;
    }

    public void Use ()
    {
        withWater = false;
        withFood = false;
        milkLevel.SetActive(false);
    }
}
