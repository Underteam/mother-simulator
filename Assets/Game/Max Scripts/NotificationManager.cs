﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
#if UNITY_ANDROID
using Unity.Notifications.Android;
#elif UNITY_IOS
using Unity.Notifications.iOS;
#endif

public class NotificationManager : MonoSingleton<NotificationManager>
{
    public string notificationId = "test_notification";

    private void Start()
    {

#if UNITY_ANDROID

        AndroidNotificationCenter.Initialize();
        
        AndroidNotificationChannel defaultNotificationChannel = new AndroidNotificationChannel()
        {
            Id = "default_channel",
            Name = "Default Channel",
            Description = "For Generic notifications",
            Importance = Importance.Default,
        };

        AndroidNotificationCenter.RegisterNotificationChannel(defaultNotificationChannel);

        AndroidNotificationCenter.NotificationReceivedCallback receivedNotificationHandler = delegate (AndroidNotificationIntentData data)
        {
            var msg = "Notification received : " + data.Id + "\n";
            msg += "\n Notification received: ";
            msg += "\n .Title: " + data.Notification.Title;
            msg += "\n .Body: " + data.Notification.Text;
            msg += "\n .Channel: " + data.Channel;
            Debug.Log(msg);
        };

        AndroidNotificationCenter.OnNotificationReceived += receivedNotificationHandler;

        var notificationIntentData = AndroidNotificationCenter.GetLastNotificationIntent();

        if (notificationIntentData != null)//если приложение было открыто нажатием на уведомление
        {
            Debug.Log("App was opened with notification! " + notificationIntentData.Id + " " + notificationIntentData.Notification.Title);
        }
        else
        {
            Debug.Log("App was open without notification");
        }

        AndroidNotificationCenter.CancelAllDisplayedNotifications();
        AndroidNotificationCenter.CancelAllNotifications();
        AndroidNotificationCenter.CancelAllScheduledNotifications();

        SendNotification();

#elif UNITY_IOS

        iOSNotificationCenter.OnRemoteNotificationReceived += recievedNotification =>
        {
            Debug.Log("Recieved notification " + recievedNotification.Identifier + "!");
        };

        iOSNotification notificationIntentData = iOSNotificationCenter.GetLastRespondedNotification();

        if (notificationIntentData != null)
        {
            Debug.Log("App was opened with notification!");
        }
        
        iOSNotificationCenter.RemoveAllDeliveredNotifications();
        iOSNotificationCenter.RemoveAllScheduledNotifications();

        SendNotification();
#endif
    }

    public List<string> keys;

    public void SendNotification ()
    {
        if (!isOn) return;

        string textKey = "notificationBaby";
        if (keys.Count > 0) textKey = keys[UnityEngine.Random.Range(0, keys.Count)];

#if UNITY_ANDROID
        AndroidNotification notification = new AndroidNotification()
        {
            Title = Localizer.GetString("notificationTitle", "Return to the game"),
            Text = Localizer.GetString(textKey, "Return to the game"),
            SmallIcon = "app_icon_small",
            LargeIcon = "app_icon_large",
            FireTime = System.DateTime.Now.AddHours(24),
        };

        notification.RepeatInterval = new TimeSpan(24, 0, 0);

        int identifier = AndroidNotificationCenter.SendNotification(notification, "default_channel");

#elif UNITY_IOS

        iOSNotificationTimeIntervalTrigger timeTrigger = new iOSNotificationTimeIntervalTrigger()
        {
            TimeInterval = new TimeSpan(24, 0, 0),
            Repeats = true,
        };

        iOSNotification notification = new iOSNotification()
        {
            Identifier = "test_notification",
            Title = Localizer.GetString("notificationTitle", "Return to the game"),
            Subtitle = "",
            Body = Localizer.GetString(textKey, "Return to the game"),
            ShowInForeground = true,
            ForegroundPresentationOption = (PresentationOption.Alert | PresentationOption.Sound),
            CategoryIdentifier = "category_a",
            ThreadIdentifier = "thread1",
            Trigger = timeTrigger,
        };

        iOSNotificationCenter.ScheduleNotification(notification);
#endif
    }

    private bool isOn = true;
    public void SetNotificationsState (bool state)
    {
        isOn = state;

        {
#if UNITY_ANDROID
            AndroidNotificationCenter.CancelAllDisplayedNotifications();
            AndroidNotificationCenter.CancelAllNotifications();
            AndroidNotificationCenter.CancelAllScheduledNotifications();
#elif UNITY_IOS
            iOSNotificationCenter.RemoveAllDeliveredNotifications();
            iOSNotificationCenter.RemoveAllScheduledNotifications();
#endif
        }

        if (isOn) SendNotification();
    }

    private void OnApplicationPause(bool pause)
    {

#if UNITY_ANDROID

        //if (AndroidNotificationCenter.CheckScheduledNotificationStatus(identifier) == NotificationStatus.Scheduled)
        //{
        //    //If the player has left the game and the game is not running. Send them a new notification
        //    AndroidNotification newNotification = new AndroidNotification()
        //    {
        //        Title = "Reminder Notification!",
        //        Text = "You've paused Unity Royale!",
        //        SmallIcon = "app_icon_small",
        //        LargeIcon = "app_icon_large",
        //        FireTime = System.DateTime.Now
        //    };

        //    // Replace the currently scheduled notification with a new notification.
        //    AndroidNotificationCenter.UpdateScheduledNotification(identifier, newNotification, "default_channel");
        //}
        //else if (AndroidNotificationCenter.CheckScheduledNotificationStatus(identifier) == NotificationStatus.Delivered)
        //{
        //    //Remove the notification from the status bar
        //    AndroidNotificationCenter.CancelNotification(identifier);
        //}
        //else if (AndroidNotificationCenter.CheckScheduledNotificationStatus(identifier) == NotificationStatus.Unknown)
        //{
        //    AndroidNotification notification = new AndroidNotification()
        //    {
        //        Title = "Test Notification!",
        //        Text = "This is a test notification!",
        //        SmallIcon = "app_icon_small",
        //        LargeIcon = "app_icon_large",
        //        FireTime = System.DateTime.Now.AddSeconds(10),
        //    };

        //    //Try sending it again
        //    identifier = AndroidNotificationCenter.SendNotification(notification, "default_channel");
        //}

#elif UNITY_IOS

        ////If it's scheduled remove it when the app is paused
        //iOSNotificationCenter.RemoveScheduledNotification(notificationId);

        ////if it's already been delivered, remove the notification from the notification center
        //iOSNotificationCenter.RemoveDeliveredNotification(notificationId);

#endif
    }
}
