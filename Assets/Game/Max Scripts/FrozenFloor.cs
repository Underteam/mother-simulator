﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrozenFloor : MonoBehaviour
{
    public Renderer rend;

    private Material mat;

    private float targetAlpha;
    private float alpha;
    private Color col;

    private NeedForFire need;

    private FPSCharacterController player;

    private MyAudioSource sound;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        mat = rend.material;
        col = mat.color;
        alpha = col.a;

        while (!GameState.Instance().started) yield return null;

        need = TaskManager.Instance().GetNeed<NeedForFire>();

        player = PlayerController.instance.GetComponent<FPSCharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (need == null) targetAlpha = 0;
        else
        {
            float ff = Mathf.Clamp(1 - need.level / 0.2f, 0f, 1f);
            targetAlpha = ff;
            player.frozenFactor = ff;
        }

        if (targetAlpha >= 1f && sound == null)
        {
            sound = SFXPlayer.Instance().Play(SFXLibrary.Instance().shivering, true, true);
        }
        else if(targetAlpha < 1 && sound != null)
        {
            sound.Stop();
            sound = null;
        }

        alpha = Mathf.Lerp(alpha, targetAlpha, 1 * Time.deltaTime);

        col.a = alpha;

        mat.color = col;
    }
}
