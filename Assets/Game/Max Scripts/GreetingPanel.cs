﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GreetingPanel : MonoBehaviour
{
    public Animator anim;

    public System.Action onHided;

    private float coolDown;

    public Text greeting;

    public Image image;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (coolDown > 0) coolDown -= Time.unscaledDeltaTime;
    }

    private void OnEnable()
    {
        Time.timeScale = 0f;
        coolDown = 0.8f;
    }

    private void OnDisable()
    {
        Debug.LogError ("Hided");
        Time.timeScale = 1f;
        if (onHided != null) onHided();
    }

    private float startDist;
    public void Hide()
    {
        if (coolDown > 0) return;

        coolDown = 1f;

        anim.SetTrigger("Hide");
    }

    public void Show (string greeting, Sprite sprite)
    {
        gameObject.SetActive(true);
        coolDown = 0.8f;

        this.greeting.text = greeting;

        if (sprite != null) image.sprite = sprite;
    }

    public void OnEvent (AnimationEvent ev)
    {
        Debug.LogError("Event " + ev.stringParameter);

        if (string.IsNullOrEmpty(ev.stringParameter)) return;

        if (ev.stringParameter.Equals("Hided"))
        {
            gameObject.SetActive(false);
        }
    }
}
