﻿using GMATools.Common;
using RootMotion.FinalIK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;

//Quaternion LocalRotation = Quaternion.Inverse(Target.transform.rotation) * WorldRotation; 
//this will transform WorldRotation to Target's local space

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;

    public Camera cam;

    //[System.Serializable]
    //public class IKTarget
    //{
    //    public Item.HandleInfo.Target targetType;
    //    public Vector3 pos { get; set; }
    //    public Vector3 rot { get; set; }
    //    public bool setPos { get; set; }
    //    public bool setRot { get; set; }
    //    public Transform target;
    //    public float posWeight { get; set; }
    //    public float rotWeight { get; set; }
    //}

    private Vector3 itemPos;
    private Quaternion itemRot;
    private bool setItemPos;
    private bool setItemRot;

    private Vector3 leftIKPos;
    private Quaternion leftIKRot;
    private bool setLeftIKPos;
    private bool setLeftIKRot;

    private Vector3 rightIKPos;
    private Quaternion rightIKRot;
    private bool setRightIKPos;
    private bool setRightIKRot;

    //private Vector3 rightBendIKPos;
    //private Quaternion rightBendIKRot;
    //private bool setRightBendIKPos;
    //private bool setRightBendIKRot;

    private float leftIKPosWeight;
    private float leftIKRotWeight;
    private float rightIKPosWeight;
    private float rightIKRotWeight;
    //private float rightBendIKWeight;

    private float leftIKTargetPosWeight;
    private float leftIKTargetRotWeight;
    private float rightIKTargetPosWeight;
    private float rightIKTargetRotWeight;
    //private float rightBendIKTargetWeight;

    public FullBodyBipedIK ikControler;

    public Transform leftIK;
    public Transform rightIK;
    //public Transform rightBendIK;
    public Transform root;

    //public List<IKTarget> targets;

    public Transform leftHandBone;
    public Transform rightHandBone;

    public Item currentItem { get; private set; }
    private Vector3 currItemScale;

    public Animator anim;

    public bool _reachOut;
    private bool reachout;
    //public bool drop;

    private bool release;

    public bool log;

    private float speed = 10f;

    //private float throwTimer;

    //public Transform leftHandler;
    //public Transform rightHandler;

    public GameObject btnReachOut;
    public GameObject btnDrop;
    public GameObject btnTake;

    public bool dirty { get; set; }

    private float dirtyTimer;

    private float[] smoothVelocities = new float[4] { 0, 0, 0, 0 };
    private Vector3[] smoothVectors = new Vector3[3] { Vector3.zero, Vector3.zero, Vector3.zero };

    private Vector3 leftHandPos;
    private Vector3 rightHandPos;
    private Quaternion leftHandRot;
    private Quaternion rightHandRot;

    public Transform reachoutTarget;
    private Quaternion reachoutTargetLocalRotation;
    public Transform dropTarget;

    public SwipeLook swipeLook;

    public Renderer rend;

    private NeedForHygien hygien;
    private MomStress stress;

    public AudioSource sound;

    private void Awake()
    {
        instance = this;

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            //TODO перенести в GameController
            NeedForPee pee = TaskManager.Instance().GetNeed<NeedForPee>();
            hygien = TaskManager.Instance().GetNeed<NeedForHygien>();
            MomHungry hungry = TaskManager.Instance().GetNeed<MomHungry>();
            stress = TaskManager.Instance().GetNeed<MomStress>();

            if (pee != null && hygien != null)
            {
                pee.onOverflow += () =>
                {
                    hygien.AddDirty(0.5f);
                };
            }

            if (hungry != null)
            {
                SpeedPenalty.Instance().AddPenaltyProvider(() =>
                {
                    return hungry != null & hungry.level <= 0;
                });
            }
        },
        "TaskStarted");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);

    }

    // Start is called before the first frame update
    void Start()
    {
        InputManager.Instance().AddButton("Act");

        leftHandPos = leftIK.localPosition;
        rightHandPos = rightIK.localPosition;
        leftHandRot = leftIK.localRotation;
        rightHandRot = rightIK.localRotation;

        //anim = GetComponent<Animator>();
        if (leftHandBone == null) leftHandBone = anim.GetBoneTransform(HumanBodyBones.LeftHand);
        if (rightHandBone == null) rightHandBone = anim.GetBoneTransform(HumanBodyBones.RightHand);

        reachoutTargetLocalRotation = reachoutTarget.localRotation;

        if (swipeLook == null) swipeLook = FindObjectOfType<SwipeLook>();
    }

    private float delta = -1;
    private float timer;

    private float decalLevel = 0;

    // Update is called once per frame
    void Update()
    {
        if (hygien != null)
        {
            if (log) Debug.LogError(dirty + " " + hygien.level + " " + decalLevel);

            if (!dirty && hygien.level <= 0.1f)
            {
                dirty = true;
            }
            else if(dirty && hygien.level > 0.1f)
            {
                dirty = false;
            }
        }
        else dirty = false;

        if (dirty && decalLevel < 0.99f)
        {
            decalLevel = Mathf.Lerp(decalLevel, 1f, Time.deltaTime);
            if (decalLevel >= 0.99f) decalLevel = 1;
            rend.material.SetFloat("_DecalIntensity", decalLevel);
        }
        else if (!dirty && decalLevel > 0.01f)
        {
            decalLevel = Mathf.Lerp(decalLevel, 0f, Time.deltaTime);
            if (decalLevel <= 0.01f) decalLevel = 0;
            rend.material.SetFloat("_DecalIntensity", decalLevel);
        }

        if (dirty && currentItem != null)
        {
            if (log) Debug.Log("Here " + dirtyTimer);
            if (dirtyTimer > 0)
            {
                dirtyTimer -= Time.deltaTime;
                if (dirtyTimer <= 0)
                {
                    var ds = currentItem.GetComponent<DirtyScript>();
                    if(ds != null) ds.Dirty();
                }
            }
            else dirtyTimer = 1f;
        }
        else if (hygien != null && waterCollider != null)
        {
            if (hygien.level < 1) hygien.level += 0.5f * Time.deltaTime;
        }

        //if (InputManager.Instance().GetButtonUp ("Drop")) PutItem();
        //else if (InputManager.Instance().GetButton ("Drop")) throwTimer += Time.deltaTime;
        //else throwTimer = 0;
        //drop = throwTimer >= 0.3f;
        if (InputManager.Instance().GetButtonDown("Put")) PutItem(true);
        else if (InputManager.Instance().GetButtonDown("Drop")) PutItem(false);

        CheckForApplyInterface();

        float v = anim.GetFloat("Forward");
        if (currentItem != null && currentItem.blockHands) v = 0;
        leftIKTargetPosWeight = (1f - v);
        rightIKTargetPosWeight = (1f - v);

        leftIKPosWeight = Mathf.SmoothDamp(leftIKPosWeight, leftIKTargetPosWeight, ref smoothVelocities[0], 1f / (speed-2)/* * Time.deltaTime*/);
        leftIKRotWeight = Mathf.SmoothDamp(leftIKRotWeight, leftIKTargetRotWeight, ref smoothVelocities[1], 1f / (speed-2)/* * Time.deltaTime*/);

        rightIKPosWeight = Mathf.SmoothDamp(rightIKPosWeight, rightIKTargetPosWeight, ref smoothVelocities[2], 1f / (speed-2)/* * Time.deltaTime*/);
        rightIKRotWeight = Mathf.SmoothDamp(rightIKRotWeight, rightIKTargetRotWeight, ref smoothVelocities[3], 1f / (speed-2)/* * Time.deltaTime*/);

        //rightBendIKWeight = Mathf.Lerp(rightBendIKWeight, rightBendIKTargetWeight, (speed - 2) * Time.deltaTime);

        ikControler.solver.GetEffector(FullBodyBipedEffector.LeftHand).positionWeight = leftIKPosWeight;
        ikControler.solver.GetEffector(FullBodyBipedEffector.RightHand).positionWeight = rightIKPosWeight;

        ikControler.solver.GetEffector(FullBodyBipedEffector.LeftHand).rotationWeight = leftIKRotWeight;
        ikControler.solver.GetEffector(FullBodyBipedEffector.RightHand).rotationWeight = rightIKRotWeight;

        //ikControler.solver.GetBendConstraint(FullBodyBipedChain.RightArm).weight = rightBendIKWeight;

        delta = -1;

        if (currentItem != null)
        {
            if (log) Debug.Log("Here");
            if (setItemPos) currentItem.transform.localPosition = currentItem.skipTakeAnimation ? itemPos : Vector3.SmoothDamp(currentItem.transform.localPosition, itemPos, ref smoothVectors[0], 1f / speed/* * Time.deltaTime*/);
            if (setItemRot) currentItem.transform.localRotation = currentItem.skipTakeAnimation ? itemRot : Quaternion.Lerp(currentItem.transform.localRotation, itemRot, speed * Time.deltaTime);
            delta = Mathf.Max(delta, (currentItem.transform.localPosition - itemPos).magnitude);
        }

        if (leftIK != null)
        {
            if (setLeftIKPos) leftIK.localPosition = Vector3.Lerp(leftIK.localPosition, leftIKPos, speed * Time.deltaTime);// Vector3.SmoothDamp(leftIK.localPosition, leftIKPos, ref smoothVectors[1], 1f / speed);
            if (setLeftIKRot) leftIK.localRotation = Quaternion.Lerp(leftIK.localRotation, leftIKRot, speed * Time.deltaTime);
            delta = Mathf.Max(delta, (leftIK.localPosition - leftIKPos).magnitude);
        }

        if (rightIK != null)
        {
            if (setRightIKPos) rightIK.localPosition = Vector3.Lerp(rightIK.localPosition, rightIKPos, speed * Time.deltaTime);// Vector3.SmoothDamp(rightIK.localPosition, rightIKPos, ref smoothVectors[2], 1f / speed);
            if (setRightIKRot) rightIK.localRotation = Quaternion.Lerp(rightIK.localRotation, rightIKRot, speed * Time.deltaTime);
            delta = Mathf.Max(delta, (rightIK.localPosition - rightIKPos).magnitude);
        }

        //if (rightBendIK != null)
        //{
        //    if (setRightBendIKPos) rightBendIK.localPosition = Vector3.Lerp(rightBendIK.localPosition, rightBendIKPos, speed * Time.deltaTime);
        //    if (setRightBendIKRot) rightBendIK.localRotation = Quaternion.Lerp(rightBendIK.localRotation, rightBendIKRot, speed * Time.deltaTime);
        //}

        //if (currentItem != null)
        //{
        //    btnReachOut.SetActive(true);
        //    //btnDrop.SetActive(true);
        //    btnTake.SetActive(false);
        //}
        //else
        //{
        //    btnReachOut.SetActive(false);
        //    //btnDrop.SetActive(true);
        //    btnTake.SetActive(true);
        //}

        {
            //if (!reachout && _reachOut)
            //{
            //    reachout = true;
            //}

            if (InputManager.Instance().GetButton("ReachOut"))
            {
                if (timer < 0.2f) timer += Time.deltaTime;
                //else if (!reachout)
                //{
                //    reachout = true;
                //}
            }
            else timer = 0;

            if (InputManager.Instance().GetButtonUp("ReachOut"))
            {
                if (reachout)
                {
                    reachout = false;
                }
                else if (currentItem != null)
                {
                    if (applyInterface != null && applyInterface.enabled)
                    {
                        EventManager.Instance().TriggerEvent("ItemApplied", currentItem, applyInterface);
                        applyInterface.Apply(currentItem);
                        Haptic.Instance().Play(HapticTypes.LightImpact);
                    }
                    else InputManager.Instance().SetButton("Act", true);
                }
                else
                {
                    //collectAbility.Use();
                    //Debug.LogError("Act");
                    InputManager.Instance().SetButton("Act", true);
                }
            }
        }
    }

    //int cnt = 0;
    private void LateUpdate()
    {
        UseItemInfo();
        InputManager.Instance().SetButton("Act", false);

        if (reachout && !Input.GetMouseButton(0)) InputManager.Instance().SetButton("ReachOut", false);
    }

    private IEnumerator UseItemInfoCoroutine ()
    {
        while(true)
        {
            yield return null;

            yield return new WaitForEndOfFrame();

            UseItemInfo();
        }
    }

    Item.HandleInfo leftHandInfo = null;
    Item.HandleInfo rightHandInfo = null;
    Item.HandleInfo itemInfo = null;
    //Item.HandleInfo rightBendInfo = null;

    private void UseItemInfo()
    {
        if (log) Debug.Log("UseItemInfo " + (leftHandInfo != null) + " " + (rightHandInfo != null) + " " + (itemInfo != null && currentItem != null));

        if (leftHandInfo != null)
        {
            if (leftHandInfo.setPos)
            {
                if (leftHandInfo.posHandler != null)
                    leftIKPos = leftIK.parent != null ? leftIK.parent.InverseTransformPoint(leftHandInfo.posHandler.position) : leftHandInfo.posHandler.position;
                else
                    leftIKPos = leftHandInfo.targetPos;

                setLeftIKPos = true;
                leftIKTargetPosWeight = 1f;
            }
            else
            {
                setLeftIKPos = true;
                leftIKPos = leftHandPos;
                leftIKTargetPosWeight = 1f;
            }

            if (leftHandInfo.setRot)
            {
                if (leftHandInfo.rotHandler != null)
                    leftIKRot = Quaternion.Inverse(leftIK.parent.rotation) * leftHandInfo.rotHandler.rotation;// leftHandInfo.rotHandler.localRotation;
                else
                    leftIKRot = Quaternion.Euler(leftHandInfo.targetRot);

                setLeftIKRot = true;
                leftIKTargetRotWeight = 1f;
            }
            else
            {
                setLeftIKRot = true;
                leftIKRot = leftHandRot;
                leftIKTargetRotWeight = 1f;
            }
        }
        else
        {
            setLeftIKPos = true;
            leftIKPos = leftHandPos;
            setLeftIKRot = true;
            leftIKRot = leftHandRot;
            leftIKTargetPosWeight = 1f;
            leftIKTargetRotWeight = 1f;
        }

        if (rightHandInfo != null)
        {
            if (rightHandInfo.setPos)
            {
                if (rightHandInfo.posHandler != null)
                    rightIKPos = rightIK.parent != null ? rightIK.parent.InverseTransformPoint(rightHandInfo.posHandler.position) : rightHandInfo.posHandler.position;
                else
                    rightIKPos = rightHandInfo.targetPos;

                setRightIKPos = true;
                rightIKTargetPosWeight = 1f;
            }
            else
            {
                setRightIKPos = true;
                rightIKPos = rightHandPos;
                rightIKTargetPosWeight = 1f;
            }

            if (rightHandInfo.setRot)
            {
                if (rightHandInfo.rotHandler != null)
                    rightIKRot = Quaternion.Inverse(rightIK.parent.rotation) * rightHandInfo.rotHandler.rotation;//rightHandInfo.rotHandler.localRotation;
                else
                    rightIKRot = Quaternion.Euler(rightHandInfo.targetRot);

                setRightIKRot = true;
                rightIKTargetRotWeight = 1f;
            }
            else
            {
                setRightIKRot = true;
                rightIKRot = rightHandRot;
                rightIKTargetRotWeight = 1f;
            }
        }
        else
        {
            setRightIKPos = true;
            rightIKPos = rightHandPos;
            setRightIKRot = true;
            rightIKRot = rightHandRot;
            rightIKTargetPosWeight = 1f;
            rightIKTargetRotWeight = 1f;
        }

        if (itemInfo != null && currentItem != null)
        {
            if (itemInfo.setRot)
            {
                if (itemInfo.rotHandler != null)
                {
                    itemRot = Quaternion.Inverse(itemInfo.rotHandler.localRotation);
                }
                else
                    itemRot = Quaternion.Euler(itemInfo.targetRot);

                setItemRot = true;
            }
            else
            {
                setItemRot = false;
            }

            if (itemInfo.setPos)
            {
                if (itemInfo.posHandler != null)//нужно сделать так чтобы handler находился в 0 родителя
                {
                    Vector3 v = -(itemInfo.posHandler.position - currentItem.transform.position);
                    itemPos = currentItem.transform.parent != null ? currentItem.transform.parent.InverseTransformPoint(currentItem.transform.parent.position + v) : v;
                    //itemPos = currentItem.transform.parent != null ? currentItem.transform.parent.InverseTransformPoint(itemInfo.posHandler.position) : itemInfo.posHandler.position;
                }
                else
                    itemPos = itemInfo.targetPos;

                setItemPos = true;
            }
            else
            {
                setItemPos = false;
            }
        }
        else
        {
            setItemPos = false;
            setItemRot = false;
        }

        if (reachout)
        {
            if (currentItem == null || currentItem.animateReachout)
            {
                if (currentItem != null) reachoutTarget.localRotation = rightIKRot;
                rightIKPos = reachoutTarget.localPosition;
                rightIKRot = reachoutTarget.localRotation;
            }
        }

        //if (drop)
        //{
        //    if (currentItem == null || currentItem.animateReachout)
        //    {
        //        rightIKPos = rightIK.parent.InverseTransformPoint(dropTarget.position);
        //        rightIKRot = dropTarget.localRotation;
        //    }
        //}

        //if (drop || reachout)
        //    swipeLook.useBlockers = false;
        //else
            //swipeLook.useBlockers = true;
    }

    private Item.HandleInfo.Phase currPhase = Item.HandleInfo.Phase.None;

    private IEnumerator SetPhase(Item.HandleInfo.Phase phase)
    {
        if (currentItem == null) yield break;

        //var temp = currApplyInterface;
        //if (currApplyInterface != null)
        //{
        //    OnApplyInterfaceLost();
        //}

        leftHandInfo = currentItem.GetInfo(phase, Item.HandleInfo.Target.LeftIK);
        rightHandInfo = currentItem.GetInfo(phase, Item.HandleInfo.Target.RightIK);
        itemInfo = currentItem.GetInfo(phase, Item.HandleInfo.Target.Item);
        //rightBendInfo = currentItem.GetInfo(phase, Item.HandleInfo.Target.RightBend);
        //yield return new WaitForEndOfFrame();

        if (leftHandInfo != null)
        {
            if (leftHandInfo.parent == Item.HandleInfo.Parent.None) leftIK.SetParent(null, true);
            else if (leftHandInfo.parent == Item.HandleInfo.Parent.Root) leftIK.SetParent(root, true);
            else if (leftHandInfo.parent == Item.HandleInfo.Parent.LeftHand) leftIK.SetParent(leftHandBone, true);
            else if (leftHandInfo.parent == Item.HandleInfo.Parent.RightHand) leftIK.SetParent(rightHandBone, true);
            else if (leftHandInfo.parent == Item.HandleInfo.Parent.Item) leftIK.SetParent(currentItem.transform, true);
            else if (leftHandInfo.parent == Item.HandleInfo.Parent.Base) leftIK.SetParent(transform, true);

            leftIKPos = leftIK.localPosition;
            leftIKRot = leftIK.localRotation;
        }

        if (rightHandInfo != null)
        {
            if (rightHandInfo.parent == Item.HandleInfo.Parent.None) rightIK.SetParent(null, true);
            else if (rightHandInfo.parent == Item.HandleInfo.Parent.Root) rightIK.SetParent(root, true);
            else if (rightHandInfo.parent == Item.HandleInfo.Parent.LeftHand) rightIK.SetParent(leftHandBone, true);
            else if (rightHandInfo.parent == Item.HandleInfo.Parent.RightHand) rightIK.SetParent(rightHandBone, true);
            else if (rightHandInfo.parent == Item.HandleInfo.Parent.Item) rightIK.SetParent(currentItem.transform, true);
            else if (rightHandInfo.parent == Item.HandleInfo.Parent.Base) rightIK.SetParent(transform, true);

            rightIKPos = rightIK.localPosition;
            rightIKRot = rightIK.localRotation;
        }

        //if (rightBendInfo != null)
        //{
        //    if (rightBendInfo.parent == Item.HandleInfo.Parent.None) rightBendIK.SetParent(null, true);
        //    else if (rightBendInfo.parent == Item.HandleInfo.Parent.Root) rightBendIK.SetParent(anim.transform, true);
        //    else if (rightBendInfo.parent == Item.HandleInfo.Parent.LeftHand) rightBendIK.SetParent(leftHandBone, true);
        //    else if (rightBendInfo.parent == Item.HandleInfo.Parent.RightHand) rightBendIK.SetParent(rightHandBone, true);
        //    else if (rightBendInfo.parent == Item.HandleInfo.Parent.Item) rightBendIK.SetParent(currentItem.transform, true);

        //    rightBendIKPos = rightBendIK.localPosition;
        //    rightBendIKRot = rightBendIK.localRotation;
        //}

        if (itemInfo != null)
        {
            currItemScale = currentItem.transform.localScale;
            //Debug.Log("Item pos " + currentItem.transform.position + " " + currentItem.transform.localPosition + " | " + rightHandBone.position);
            if (itemInfo.parent == Item.HandleInfo.Parent.None) currentItem.transform.SetParent(null, true);
            else if (itemInfo.parent == Item.HandleInfo.Parent.Root) currentItem.transform.SetParent(root, true);
            else if (itemInfo.parent == Item.HandleInfo.Parent.LeftHand) currentItem.transform.SetParent(leftHandBone, true);
            else if (itemInfo.parent == Item.HandleInfo.Parent.RightHand) currentItem.transform.SetParent(rightHandBone, true);
            else if (itemInfo.parent == Item.HandleInfo.Parent.Item) currentItem.transform.SetParent(currentItem.transform, true);
            else if (itemInfo.parent == Item.HandleInfo.Parent.Base) currentItem.transform.SetParent(transform, true);
            //Debug.Log("Item pos " + currentItem.transform.position + " " + currentItem.transform.localPosition + " | " + rightHandBone.position);

            itemPos = currentItem.transform.localPosition;
            itemRot = currentItem.transform.localRotation;

            Vector3 scale = new Vector3(currItemScale.x / currentItem.transform.lossyScale.x,
                                        currItemScale.y / currentItem.transform.lossyScale.y,
                                        currItemScale.z / currentItem.transform.lossyScale.z);

            scale.Scale(currentItem.transform.localScale);
            currentItem.transform.localScale = scale;
        }

        currPhase = phase;

        //Debug.Log("Set phase " + phase);
    }

    public Hand rightHand;
    public Hand leftHand;
    public bool handReachedOut;

    bool waitForTake;
    public void OnAnimationEvent (AnimationEvent ev)
    {
        if (ev.stringParameter == "Take")
            waitForTake = false;

        if (ev.stringParameter == "Finish")
            rightHand.active = false;
    }

    public void Take (AbilityInterface interf)
    {
        //Debug.LogError("Here " + (takeItemCoroutine == null) + " " + (currentItem == null), currentItem);

        if (takeItemCoroutine != null) return;

        var item = interf.GetComponent<Item>();

        if (item != null && !item.busy)
        {
            TakeItem(item);
        }
        else
        {

        }
    }

    public void Take(Item item)
    {
        if (takeItemCoroutine != null) return;

        if (item != null && !item.busy)
        {
            TakeItem(item);
        }
        else
        {

        }
    }

    private Coroutine takeItemCoroutine;
    private void TakeItem(Item item)
    {
        if (Baby.instance != null && item == Baby.instance.currItem) return;
        if (takeItemCoroutine != null) return;
        takeItemCoroutine = StartCoroutine(TakeItemCoroutine(item));
    }

    public bool wait;
    private bool taking;
    private IEnumerator TakeItemCoroutine(Item item)
    {
        yield return null;

        //Debug.Log("Take " + item, item);

        if (item == null)
        {
            takeItemCoroutine = null;
            yield break;
        }

        taking = true;

        currentItem = item;

        currentItem.OnStartTake();

        sound.PlayOneShot(sound.clip);
        Haptic.Instance().Play(HapticTypes.LightImpact);

        if (currentItem.skipTakeAnimation)
        {
            rightHand.active = true;

            rightHand.Play("Take");

            waitForTake = true;

            float wftTimer = 1f;
            while (waitForTake && wftTimer > 0)
            {
                wftTimer -= Time.deltaTime;
                yield return null;
            }

            anim.SetFloat("Grip", currentItem.grip);

            rightHand.active = false;
        }
        else
        {
            Item.HandleInfo.Phase phase = Item.HandleInfo.Phase.Take;
            yield return SetPhase(phase);

            anim.SetFloat("Grip", currentItem.grip);

            while (wait) yield return null;

            yield return new WaitForSeconds(0.8f * 5 / speed);

            yield return new WaitForEndOfFrame();
        }

        //Debug.Log("BeforeTake");

        item.OnBeforeTake();

        release = false;

        yield return SetPhase(Item.HandleInfo.Phase.Hold);

        var rb = item.GetComponent<Rigidbody>();
        if (rb != null) rb.isKinematic = true;

        if (dirty)
        {
            var ds = item.GetComponent<DirtyScript>();
            if(ds != null) ds.Dirty();
        }
        else
        {
            var ds = item.GetComponent<DirtyScript>();
            if (ds != null && !ds.isClean)
            {
                //Debug.LogError("Dirty is " + ds, ds);
                var need = TaskManager.Instance().GetNeed<NeedForHygien>();
                if (need != null) need.AddDirty(0.301f);
            }
        }

        item.OnTaked();
        EventManager.Instance().TriggerEvent("ItemTaked", item);

        //Debug.Log("Taked " + item + " " + release, item);

        reachout = false;
        taking = false;

        while (!release && currentItem != null)
        {
            yield return null;
        }

        Vector3 leftPos = root.InverseTransformPoint(leftIK.position);
        Vector3 rightPos = root.InverseTransformPoint(rightIK.position);
        Quaternion leftRot = Quaternion.Inverse(root.rotation) * leftIK.rotation;
        Quaternion rightRotRot = Quaternion.Inverse(root.rotation) * rightIK.rotation;

        //Debug.Log("Release");

        if (release && currentItem != null)
        {
            yield return new WaitForEndOfFrame();

            currentItem.transform.SetParent(null, true);
            currentItem.transform.localScale = currItemScale;

            rb = currentItem.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.isKinematic = false;
                rb.velocity = Vector3.zero;
                rb.angularVelocity = Vector3.zero;
            }

            currentItem.OnReleased();
            onReleased?.Invoke();
            onReleased = null;

            EventManager.Instance().TriggerEvent("ItemDroped", currentItem);

            currentItem = null;
        }

        //Debug.LogError("Here");
        leftIK.SetParent(root);
        rightIK.SetParent(root);
        leftIK.localPosition = leftPos;
        rightIK.localPosition = rightPos;
        leftIK.localRotation = leftRot;
        rightIK.localRotation = rightRotRot;

        leftHandInfo = null;
        rightHandInfo = null;
        itemInfo = null;
        //rightBendInfo = null;

        rightIKPos = rightIK.localPosition;
        leftIKPos = leftIK.localPosition;

        takeItemCoroutine = null;
        currentItem = null;
        reachoutTarget.localRotation = reachoutTargetLocalRotation;
        dirtyTimer = 1f;
        //currApplyInterface = null;
        reachout = false;

        anim.SetFloat("Grip", 0.0f);

        currPhase = Item.HandleInfo.Phase.None;
    }

    private Vector3 reachoutTargetPoint;
    private void FixedUpdate()
    {
        Ray ray = new Ray(cam.transform.position, cam.transform.forward);
        var hits = Physics.RaycastAll(ray, 3f);

        Vector3 closestPoint = cam.transform.position + 0.8f * cam.transform.forward + 0.2f * Vector3.down;
        float min = 0.8f;
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].collider.isTrigger || IsChildOf(transform, hits[i].transform)) continue;
            if (hits[i].distance < min)
            {
                min = hits[i].distance;
                closestPoint = hits[i].point - 0.2f * cam.transform.forward;
                if (log) Debug.Log("Found " + hits[i].transform + " " + hits[i].distance, hits[i].transform);
            }
        }

        reachoutTargetPoint = closestPoint;

        reachoutTargetPoint = cam.transform.position + 0.5f * cam.transform.forward;
    }

    private bool IsChildOf(Transform parent, Transform child)
    {
        Transform p = child;

        while (p != null)
        {
            if (p == parent) return true;
            p = p.parent;
        }

        return false;
    }

    public AbilityCollect collectAbility;
    public AbilityApplyItem applyAbility;
    private AbilityInterfaceApplyItem applyInterface;
    private void CheckForApplyInterface()
    {
        applyInterface = null;

        var list = applyAbility.interfaces;

        if (log) Debug.Log("Count " + list.Count);

        for (int i = 0; i < list.Count; i++)
        {
            var ai = list[i] as AbilityInterfaceApplyItem;

            if (log && ai != null) Debug.Log("Check " + ai + " " + ai.IsAppropriateItem(currentItem), ai);
            if (ai != null && ai.IsAppropriateItem(currentItem))
            {
                applyInterface = ai;
                return;
            }
        }

        //applyInterface = myApplyItemInterface;
    }

    public System.Action onReleased;
    public void ReleaseItem(System.Action onReleased = null)
    {
        //Debug.Log("Release");
        this.onReleased = onReleased;
        release = true;
    }

    public void Switch (AbilityInterface interf)
    {
        //Debug.LogError("Switch " + interf, interf);

        Switcher sw = interf as Switcher;

        if (sw != null)
        {
            //Reachout(sw.targetPoint, (b) => { if (b) sw.Switch(); });
            sw.Switch();
        }
    }

    private Coroutine reachoutCoroutine;
    public void Reachout (Transform target, System.Action<bool> result)
    {
        if (reachoutCoroutine != null) return;
        reachoutCoroutine = StartCoroutine(ReachoutCoroutine(target, result));
    }

    private IEnumerator ReachoutCoroutine (Transform target, System.Action<bool> result)
    {
        yield return null;

        bool success = false;

        if (target == null)
        {
            if (result != null) result(success);
            reachoutCoroutine = null;
            yield break;
        }

        if (currentItem == null)
        {
            rightHandInfo = new Item.HandleInfo();
            rightHandInfo.phase = Item.HandleInfo.Phase.Take;
            rightHandInfo.posHandler = target;
            rightHandInfo.setPos = true;

            yield return new WaitForSeconds(1f * 5 / speed);
            yield return new WaitForEndOfFrame();

            float dist = Vector3.Distance(rightIK.position, rightHandBone.position);

            Debug.Log("Here " + dist);

            if (dist < 0.15f)
            {
                success = true;
            }
            else
            {
                success = false;
            }

            rightHandInfo = null;
        }
        else
        {
            if (leftHandInfo == null || !leftHandInfo.setPos)
            {
                bool isNull = false;
                if(leftHandInfo == null)
                {
                    isNull = true;
                    leftHandInfo = new Item.HandleInfo();
                }

                Transform storedTarget = leftHandInfo.posHandler;
                leftHandInfo.posHandler = target;
                leftHandInfo.setPos = true;

                yield return new WaitForSeconds(1f * 5 / speed);
                yield return new WaitForEndOfFrame();

                float dist = Vector3.Distance(leftIK.position, leftHandBone.position);

                Debug.Log("Here " + dist);

                if (dist < 0.15f)
                {
                    success = true;
                }
                else
                {
                    success = false;
                }

                leftHandInfo.setPos = false;
                leftHandInfo.posHandler = storedTarget;
                if(isNull) leftHandInfo = null;
            }
            else if (rightHandInfo == null || !rightHandInfo.setPos)
            {
                bool isNull = false;
                if (rightHandInfo == null)
                {
                    isNull = true;
                    rightHandInfo = new Item.HandleInfo();
                }

                Transform storedTarget = rightHandInfo.posHandler;
                rightHandInfo.posHandler = target;
                rightHandInfo.setPos = true;

                yield return new WaitForSeconds(1f * 5 / speed);
                yield return new WaitForEndOfFrame();

                float dist = Vector3.Distance(rightIK.position, rightHandBone.position);

                Debug.Log("Here " + dist);

                if (dist < 0.15f)
                {
                    success = true;
                }
                else
                {
                    success = false;
                }

                rightHandInfo.setPos = false;
                rightHandInfo.posHandler = storedTarget;
                if (isNull) rightHandInfo = null;
            }
        }

        if (result != null) result(success);
        reachoutCoroutine = null;
    }

    private bool putting;
    private void PutItem (bool put)
    {
        if (taking || putting || currentItem == null) return;

        if (put/*throwTimer < 0.3f*/) 
            StartCoroutine(PutItemCoroutine());
        else
            StartCoroutine(ThrowItemCoroutine(maxSpeedTime/*throwTimer*/));
    }

    private IEnumerator PutItemCoroutine ()
    {
        putting = true;

        yield return null;

        release = true;

        while (currentItem != null) yield return null;

        putting = false;
    }

    private Item thrownItem;
    private Vector3 hitPoint;
    private Vector3 startPos;

    private float maxSpeedTime = 2f;
    private float maxThrowSpeed = 25;
    private IEnumerator ThrowItemCoroutine (float timer)
    {
        //Debug.Log("Try to throw " + currentItem + " " + (rightHandInfo != null), currentItem);

        putting = true;

        yield return null;

        float k = (timer - 0.3f) / (maxSpeedTime - 0.3f);
        float speed = Mathf.Lerp(1f, maxThrowSpeed, k);

        float storedSpeed = this.speed;
        this.speed = 20f;

        Vector3 velocity = speed * (transform.forward + 0.5f * Vector3.up).normalized;

        var ray = new Ray(cam.transform.position, cam.transform.forward);
        RaycastHit hit = new RaycastHit();
        bool hitted = false;
        var hits = Physics.RaycastAll(ray);
        float minDist = float.MaxValue;
        for(int i = 0; i < hits.Length; i++)
        {
            if (IsChildOf(transform, hits[i].transform)) continue;
            float dist = hits[i].distance;
            if(dist < minDist)
            {
                hit = hits[i];
                minDist = dist;
                hitted = true;
            }
        }

        //Debug.Log("Set release");

        release = true;

        var item = currentItem;

        while (currentItem != null) yield return null;

        //Debug.Log("Released");

        yield return new WaitForFixedUpdate();

        thrownItem = null;

        if (hitted)
        {
            //Debug.Log("Hitted " + hit.point);
            thrownItem = item;
            hitPoint = hit.point;
            startPos = item.transform.position;

            Vector3 p = hit.point - item.transform.position;
            float y = p.y;
            float x = Mathf.Sqrt(p.sqrMagnitude - y * y);
            float g = 9.81f;
            float t = 0.1f;
            float a = x * t - y;

            if (a > 0)
            {
                float v = Mathf.Sqrt(g * x * x * (1 + t * t) / (2 * a));
                if (v > speed) v = speed;

                velocity = p;
                velocity.y = 0;
                velocity.y = t * velocity.magnitude;
                velocity = velocity.normalized * v;
            }
            else
                velocity = (hit.point - item.transform.position).normalized * maxThrowSpeed;
        }

        var rbs = item.GetComponentsInChildren<Rigidbody>();

        positions.Clear();
        directions.Clear();

        for(int i = 0; i < rbs.Length; i++)
        {
            rbs[i].velocity = velocity;

            positions.Add(rbs[i].transform.position);
            directions.Add(velocity);
        }

        sound.PlayOneShot(sound.clip);

        if (stress != null)
        {
            float toAdd = 0.15f + k * 0.15f;
            stress.level += toAdd;
            if (k > 0.99f) stress.AddSeconds(1);
            if (stress.level > 1) stress.level = 1;
        }

        this.speed = storedSpeed;

        _reachOut = false;

        putting = false;
    }

    private void OnDrawGizmos()
    {
        if (currPhase == Item.HandleInfo.Phase.Use)
        {
            Gizmos.DrawSphere(reachoutTargetPoint, 0.05f);

            Vector3 p1 = currentItem.transform.position;
            Vector3 p2 = reachoutTargetPoint;// rightHandBone.position;
            //var rend = currentItem.GetComponent<Renderer>();
            //if (rend != null) p1 = rend.bounds.center;
            var shift = p2 - p1;

            Gizmos.DrawSphere(reachoutTargetPoint + shift, 0.02f);
        }

        Gizmos.color = Color.red;
        for (int i = 0; i < positions.Count; i++)
        {
            Gizmos.DrawLine(positions[i], positions[i] + directions[i]);
        }

        if (thrownItem != null)
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(startPos, hitPoint);
        }
    }

    private List<Vector3> positions = new List<Vector3>();
    private List<Vector3> directions = new List<Vector3>();

    private Collider waterCollider;
    public void OnTriggerEnter(Collider other)
    {
        var water = other.GetComponent<Water>();
        if(water != null)
        {
            waterCollider = other;
            //Debug.LogError("Water");
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other == waterCollider) waterCollider = null;
    }
}
