﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdmixController : MonoBehaviour
{
    public AudioSource sound;

    public Renderer rend;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [Sirenix.OdinInspector.Button]
    public void SwitchAudio ()
    {
        sound.enabled = !sound.enabled;
    }

    [Sirenix.OdinInspector.Button]
    public void SwitchVideo ()
    {
        rend.enabled = !rend.enabled;
    }
}
