﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottlePacifier : MonoBehaviour, IAppropriatanceProvider
{
    public Rigidbody rb;
    public Collider col;

    public BabyBottle bottle;

    public DirtyScript ds;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Applied ()
    {
        rb.isKinematic = true;
        col.isTrigger = true;
    }

    public void Taked ()
    {
        if (bottle != null) bottle.PacifierRemoved();
        //col.isTrigger = false;
    }

    public void ApplyItem(Item item)
    {
        if (item == null) return;

        //Debug.Log("Apply " + item + " on " + this);

        var bottle = item.GetComponent<BabyBottle>();
        if (bottle == null) return;

        if (this.bottle != null) this.bottle.PacifierRemoved();

        bottle.PutOnPacifier(this);
    }

    public bool IsAppropriate(Item item)
    {
        if (item == null) return false;

        return item.GetComponent<BabyBottle>() != null;
    }
}
