﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementDisabler : MonoBehaviour
{
    public List<string> axesToDisable;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnEnable ()
    {
        //Debug.LogError("Disable movements", this);
        //InputManager.Instance().disabled = true;
        for (int i = 0; i < axesToDisable.Count; i++)
            InputManager.Instance().SetAxisState(axesToDisable[i], false);
        if (PlayerController.instance == null) return;
        PlayerController.instance.GetComponent<FPSCharacterController>().disableMovement = true;
        PlayerController.instance.GetComponent<FPSCharacterController>().disableRotation = true;
    }

    void OnDisable ()
    {
        //Debug.LogError("Enable movements");
        //InputManager.Instance().disabled = false;
        for (int i = 0; i < axesToDisable.Count; i++)
            InputManager.Instance().SetAxisState(axesToDisable[i], true);
        if (PlayerController.instance == null) return;
        PlayerController.instance.GetComponent<FPSCharacterController>().disableMovement = false;
        PlayerController.instance.GetComponent<FPSCharacterController>().disableRotation = false;
    }
}
