﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Milkpot : MonoBehaviour
{
    public bool isOn;

    public UnityEvent onSwitchOn;

    public UnityEvent onSwitchOff;

    public AudioClip sound;

    public Transform targetPos;

    public float camRot;
    private Quaternion targetRot;

    private FPSCharacterController player;

    public AnimationClip leftHandClip;
    public AnimationClip rightHandClip;
    
    private bool playing;

    private BabyBottle bottle;

    // Start is called before the first frame update
    void Start()
    {
        targetRot = Quaternion.Euler(camRot, 0, 0);

        if (isOn)
        {
            if (onSwitchOn != null) onSwitchOn.Invoke();
        }
        else
        {
            if (onSwitchOff != null) onSwitchOff.Invoke();
        }
    }

    private float timer;
    void Update()
    {
        if (player != null)
        {
            float horizontal = InputManager.Instance().GetAxis("Horizontal");
            float vertical = InputManager.Instance().GetAxis("Vertical");
            if (Mathf.Abs(horizontal) + Mathf.Abs(vertical) > 0) timer += Time.deltaTime;
            else timer = 0;

            if (timer > 0.2f)
            {
                //AllowMovement();
                timer = 0;
                return;
            }

            player.transform.position = Vector3.Lerp(player.transform.position, targetPos.position, 5 * Time.deltaTime);
            player.transform.rotation = Quaternion.Lerp(player.transform.rotation, targetPos.rotation, 5 * Time.deltaTime);
            player.pivot.transform.localRotation = Quaternion.Lerp(player.pivot.transform.localRotation, targetRot, 5 * Time.deltaTime);

            float d1 = Vector3.Distance(player.transform.position, targetPos.position);
            float d2 = Quaternion.Angle(player.transform.rotation, targetPos.rotation) / 10f;

            if (!playing && Mathf.Max(d1, d2) < 0.1f)
            {
                playing = true;
                var listener = PlayerController.instance.leftHand.GetComponent<AnimationEventListener>();
                if (listener != null) listener.onEvent.AddListener(OnAnimationEvent);
                PlayerController.instance.rightHand.Play(rightHandClip);
                PlayerController.instance.leftHand.Play(leftHandClip);
                PlayerController.instance.handReachedOut = true;
                Invoke("AllowMovement", 2.2f);
            }
        }
    }

    private void OnAnimationEvent (AnimationEvent evnt)
    {
        if (evnt.stringParameter.Equals("TakeOffPacifier"))
        {
            bottle.pacifier.transform.SetParent(PlayerController.instance.leftHandBone);
            bottle.pacifier.transform.localPosition = new Vector3(-0.1817f, 0.1236f, -0.2498f);
            bottle.pacifier.transform.localRotation = Quaternion.Euler(180f, 10f, -180f);
            bottle.GetComponent<ScaleKeeper>().RestoreScale();
        }

        if (evnt.stringParameter.Equals("TakeOnPacifier"))
        {
            bottle.pacifier.transform.SetParent(bottle.transform);
            bottle.FixPacifier();
            bottle.GetComponent<ScaleKeeper>().RestoreScale();
        }
    }

    private void AllowMovement ()
    {
        playing = false;
        player = null;
        if (PlayerController.instance == null) return;
        PlayerController.instance.handReachedOut = false;
        PlayerController.instance.leftHand.Stop();
        PlayerController.instance.rightHand.Stop();
        PlayerController.instance.GetComponent<FPSCharacterController>().disableMovement = false;
        PlayerController.instance.GetComponent<FPSCharacterController>().disableRotation = false;

        var listener = PlayerController.instance.leftHand.GetComponent<AnimationEventListener>();
        if (listener != null) listener.onEvent.RemoveListener(OnAnimationEvent);
        bottle = null;

        Switch();
    }

    public void SwitchOn()
    {
        if (isOn) return;
        SFXPlayer.Instance().Play(SFXLibrary.Instance().teapot);
        isOn = true;
    }

    public void SwitchOff()
    {
        if (!isOn) return;
        SFXPlayer.Instance().Play(SFXLibrary.Instance().teapot);
        isOn = false;
    }

    public void Switch()
    {
        isOn = !isOn;

        SFXPlayer.Instance().Play(SFXLibrary.Instance().teapot);

        if (isOn)
        {
            if (onSwitchOn != null) onSwitchOn.Invoke();
        }
        else
        {
            if (onSwitchOff != null) onSwitchOff.Invoke();
        }
    }

    public void ApplyItem (Item item)
    {
        Debug.LogError("Apply item " + item, item);

        if (player != null) return;

        if (item == null)
        {
            Switch();
            return;
        }

        var bottle = item.GetComponent<BabyBottle>();

        if (bottle == null || !isOn)
        {
            Switch();
            //return;
        }

        if (bottle != null) this.bottle = bottle;
        else return;

        if (PlayerController.instance == null) return;

        player = PlayerController.instance.GetComponent<FPSCharacterController>();
        player.disableMovement = true;
        player.disableRotation = true;
    }
}
