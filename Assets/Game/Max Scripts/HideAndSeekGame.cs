﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HideAndSeekGame : MonoBehaviour
{
    [System.Serializable]
    public class HidePoints
    {
        public int openDay;
        public List<Transform> points;
    }

    public List<HidePoints> hidePoints;

    private Blinker blinker;

    public bool playing { get; private set; }

    public AnimationClip leftHand;
    public AnimationClip rightHand;

    private float shutTimer;

    private FPSCharacterController player;

    private Color c = new Color(0, 0, 0, 0);
    private bool shutting;

    // Start is called before the first frame update
    void Start()
    {
        blinker = FindObjectOfType<Blinker>();
    }

    // Update is called once per frame
    void Update()
    {
        if (shutting && shutTimer > 0)
        {
            shutTimer -= Time.deltaTime; 

            c.a = Mathf.Lerp(1, 0, shutTimer - 0.05f);
            GameController.Instance().shutter.color = c;
            if (shutTimer <= 0)
            {
                Shutted();
            }
        }
        
        if (!shutting && shutTimer > 0)
        {
            shutTimer -= Time.deltaTime;

            c.a = Mathf.Lerp(0, 1, shutTimer);
            GameController.Instance().shutter.color = c;
            if (shutTimer <= 0)
            {
                if (player == null) player = PlayerController.instance.GetComponent<FPSCharacterController>();
                player.disableMovement = false;
                player.disableRotation = false;

                PlayerController.instance.leftHand.Stop();
                PlayerController.instance.rightHand.Stop();
            }
        }
    }

    public void Play ()
    {
        if (playing) return;

        playing = true;

        //blinker.onBlinked = () =>
        //{
        //    Debug.LogError("Hide");

        //    var point = hidePoints[0].points[Random.Range(0, hidePoints[0].points.Count)];

        //    Baby.instance.transform.position = point.position;
        //    Baby.instance.transform.rotation = point.rotation;
        //    Baby.instance.currentCanvas.SetActive(false);

        //    EventManager.Instance().AddListener(OnBabyTaked, "ItemTaked");
        //};

        //blinker.blinking = true;

        SFXPlayer.Instance().Play(SFXLibrary.Instance().countdown);

        if (player == null) player = PlayerController.instance.GetComponent<FPSCharacterController>();
        player.disableMovement = true;
        player.disableRotation = true;

        PlayerController.instance.leftHand.Play(leftHand);
        PlayerController.instance.rightHand.Play(rightHand);

        shutting = true;
        shutTimer = 1f;
    }

    private void Shutted ()
    {
        List<Transform> posiblePoints = new List<Transform>();
        for (int i = 0; i < hidePoints.Count; i++)
        {
            if (hidePoints[i].openDay > GameState.Instance().currentTask) continue;
            for (int j = 0; j < hidePoints[i].points.Count; j++)
                posiblePoints.Add(hidePoints[i].points[j]);
        }
        var point = posiblePoints[Random.Range(0, posiblePoints.Count)];

        Baby.instance.transform.position = point.position;
        Baby.instance.transform.rotation = point.rotation;
        Baby.instance.currentCanvas.SetActive(false);

        EventManager.Instance().AddListener(OnBabyTaked, "ItemTaked");

        shutting = false;
        Invoke("OpenEyes", 1f);
    }

    private void OpenEyes ()
    {
        shutTimer = 1f;
    }

    private void OnBabyTaked (string eventName, object data)
    {
        Item i = data as Item;
        if (i == null) return;

        if (i.GetComponent<Baby>() != null)
        {
            EventManager.Instance().DetachListener(OnBabyTaked, "ItemTaked");
            Baby.instance.HideAndSeek();
            playing = false;
        }
    }

    private void OnDestroy()
    {
        EventManager.Instance().DetachListener(OnBabyTaked, "ItemTaked");
    }
}
