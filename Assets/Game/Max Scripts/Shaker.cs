﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shaker : MonoBehaviour
{
    public bool active;

    private MomStress stress;

    public Image red;

    public List<Image> veins;

    private Color color = new Color(1, 1, 1);

    public float level = -1f;

    private CameraController cam;

    private Vector3 camPos;
    private Quaternion camRot;

    public float ampMult = 0.5f;

    private void Awake()
    {
        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            stress = FindObjectOfType<MomStress>();
        },
        "TaskStarted");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);

    }

    // Start is called before the first frame update
    void Start()
    {
        cam = PlayerController.instance.cam.GetComponent<CameraController>();
        camPos = cam.target.localPosition;
        camRot = cam.target.localRotation;
    }

    private Vector3 shift;

    // Update is called once per frame
    void LateUpdate()
    {
        if (cam == null) return;

        active = stress != null || level > 0;// && stress.level <= 0;

        if (active)
        {
            if (!red.gameObject.activeSelf) red.gameObject.SetActive(true);

            if (stress != null) level = stress.level;

            color.a = Mathf.Clamp ((0.3f - level) / 0.3f, 0, 1);
            red.color = color;
            for (int i = 0; i < veins.Count; i++) veins[i].color = color;

            Vector3 v1 = cam.transform.right;
            Vector3 v2 = cam.transform.up;

            float amp = color.a * ampMult;
            cam.shift = amp * Random.Range(-0.05f, 0.05f) * v1 + amp * Random.Range(-0.05f, 0.05f) * v2;
            //cam.target.localPosition = camPos + amp * Random.Range(-0.05f, 0.05f) * v1 + amp * Random.Range(-0.05f, 0.05f) * v2;
            cam.target.localRotation = camRot;
            cam.target.localRotation *= Quaternion.Euler(amp * Random.Range(-3f, 3f), amp * Random.Range(-3f, 3f), 0);
        }
        else if (red.gameObject.activeSelf)
        {
            red.gameObject.SetActive(false);

            cam.shift = Vector3.zero;
            cam.target.localPosition = camPos;
            cam.target.localRotation = camRot;
        }
    }
}
