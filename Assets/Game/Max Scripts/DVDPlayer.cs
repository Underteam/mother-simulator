﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DVDPlayer : MonoBehaviour
{
    public Image screen;

    public DVDDisc disc { get; private set; }

    public BabySitPoint sitPoint;

    private float timer;
    private int frame;

    public Sprite startImage;
    private float duration;

    public AudioSource admixSound;

    public Renderer admixRend;

    public bool disableAdmixSound;

    private bool admixLoaded;

    // Start is called before the first frame update
    void Start()
    {
        disableAdmixSound = disableAdmixSound && PlayerPrefs.GetInt("SFXState", 0) == 1;
        admixSound.enabled = !disableAdmixSound;
    }

    // Update is called once per frame
    void Update()
    {
        if (disc != null)
        {
            timer -= Time.deltaTime;
            if (timer < 0)
            {
                timer = 0.3f;

                if (disc.cartoon.Count > 0)
                {
                    frame = (frame + 1) % disc.cartoon.Count;
                    screen.sprite = disc.cartoon[frame];
                }
            }

            duration -= Time.deltaTime;
            if (duration <= 0)
            {
                disc = null;
                screen.sprite = startImage;
                admixSound.enabled = !disableAdmixSound;
                admixRend.enabled = admixLoaded;
            }
        }
    }

    public void ApplyItem(Item item)
    {
        var disc = item.GetComponent<DVDDisc>();

        if (disc == null) return;

        this.disc = disc;

        if (disc.cartoon.Count > 0)
            screen.sprite = disc.cartoon[0];

        PlayerController.instance.ReleaseItem(() =>
        {
            if (item != null) item.gameObject.SetActive(false);
        });

        duration = 5f;
        SFXPlayer.Instance().Play(SFXLibrary.Instance().cartoons, false, true);
        admixSound.enabled = false;
        admixRend.enabled = false;

        if (sitPoint.baby != null) sitPoint.baby.Cartoon(disc);
    }

    public void Check ()
    {
        Debug.LogError("Check " + (disc != null) + " " + (sitPoint.baby != null));
        if (disc != null && sitPoint.baby != null) sitPoint.baby.Cartoon(disc);
    }

    public void SetAdmixState(bool s)
    {
        admixLoaded = s;
    }
}
