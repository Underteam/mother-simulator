﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dog : MonoBehaviour
{
    public Animation anim;

    public AnimationClip idle;
    public AnimationClip walk;
    public AnimationClip lieDown;
    public AnimationClip lieing;
    public AnimationClip sleeping;
    public AnimationClip standUp;
    public AnimationClip eating;
    public AnimationClip shitting;

    public List<Transform> waypoints;
    private int ind;

    private Transform _targetPoint;
    private Transform targetPoint { get { return _targetPoint; } set { _targetPoint = value; Debug.LogError("Set targetPoint", value); } }

    public Transform waitingPoint;
    private Transform feedingPoint;
    public Transform waitForSleepPoint;
    public Transform sleepingPoint;

    private bool reached;

    public float speed = 1.2f;

    private Vector3 correction;

    public Transform nose;

    private float hungryTimer = 20f;
    private float sleepTimer = 20f;

    public GameObject fur;

    public GameObject pooPrefab;
    public GameObject peePrefab;
    public GameObject furPrefab;

    private bool wannaShit;
    private float shitTimer;

    private int jobType;

    private DogDish dish;

    public int behaviourType;

    public GameObject canvasWants;
    public Image imgWant;
    public Sprite eat;
    public Sprite sleep;

    private DogMood need;

    public System.Action onWishSatisfied;
    public System.Action<Transform> onReached;

    private void Awake()
    {
        Play(lieing);

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            need = TaskManager.Instance().GetNeed<DogMood>();

            if (behaviourType == 1)
                job = Job1();
            else if (behaviourType == 2)
                job = Job2();
            else if (behaviourType == 3)
                job = Walking();

            StartCoroutine(job);
        },
        "TaskStarted");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    private IEnumerator job;
    public void SetBehaviourType (int type)
    {
        Debug.LogError("Set behaviour " + type + " " + behaviourType);

        if (type == behaviourType) return;

        behaviourType = type;

        if (job != null) StopCoroutine(job);
        //StopAllCoroutines();

        if (behaviourType == 1)
            job = Job1();
        else if(behaviourType == 2)
            job = Job2();
        else if(behaviourType == 3)
            job = Walking();

        StartCoroutine(job);
    }

    // Update is called once per frame
    void Update()
    {
        if (wannaShit)
        {
            if (!isShitting) StartCoroutine(Shitting());
        }
        else if (!reached && targetPoint != null)
        {
            Vector3 dir = targetPoint.position - transform.position;
            dir.y = 0;
            float dist = dir.magnitude;
            dir.Normalize();
            float delta = speed * Time.deltaTime;
            if (dist < delta)
            {
                reached = true;
                Debug.LogError("Reached", targetPoint);
                onReached?.Invoke(targetPoint);
                delta = dist;
                ind = (ind + 1) % waypoints.Count;
            }

            dir = (dir + (1.5f - hit.distance) * correction / 1.5f).normalized;
            transform.position += delta * dir;

            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(dir), 3 * Time.deltaTime);
        }
        else if (jobType == 1 && hungryTimer <= 0)
        {
            if (dish == null || dish.food == null)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, waitingPoint.rotation, 3 * Time.deltaTime);
            }
            else
            {
                transform.position = Vector3.Lerp(transform.position, feedingPoint.position, 3 * Time.deltaTime);
                transform.rotation = Quaternion.Lerp(transform.rotation, feedingPoint.rotation, 3 * Time.deltaTime);
            }
        }
        else if (jobType == 2 && sleepTimer <= 0)
        {
            if (fur != null)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, waitForSleepPoint.rotation, 3 * Time.deltaTime);
            }
            else
            {
                if (Vector3.Distance(transform.position, sleepingPoint.position) > 0.5f) Debug.LogError("!!!");
                transform.position = Vector3.Lerp(transform.position, sleepingPoint.position, 3 * Time.deltaTime);
                transform.rotation = Quaternion.Lerp(transform.rotation, sleepingPoint.rotation, 3 * Time.deltaTime);
            }
        }

        if (hungryTimer > 0)
        {
            hungryTimer -= Time.deltaTime;
        }

        if (shitTimer > 0)
        {
            shitTimer -= Time.deltaTime;
            if (shitTimer <= 0) wannaShit = true;
        }

        if (sleepTimer > 0)
        {
            sleepTimer -= Time.deltaTime;
        }
    }

    public void Hungry()
    {
        hungryTimer = 0;
    }

    public void Sleep()
    {
        sleepTimer = 0;
    }

    public void Play(AnimationClip clip)
    {
        Debug.LogError("Play " + clip.name);

        if (anim.GetClip(clip.name) == null) anim.AddClip(clip, clip.name);

        anim.CrossFade(clip.name, 0.5f, PlayMode.StopAll);
    }

    private void OnDrawGizmos()
    {
        if (correction.magnitude > 0)
        {
            Gizmos.DrawLine(nose.transform.position, hit.point);
            Gizmos.DrawLine(hit.point, hit.point + correction);
        }
    }

    private RaycastHit hit;
    private void FixedUpdate()
    {
        Vector3 perp = nose.right;
        float r = Random.Range(-0.1f, 0.1f);
        if (correction.magnitude > 0) r = 0.1f * Vector3.Dot(correction, perp);
        Ray ray = new Ray(nose.position, nose.forward + r * perp);

        bool collided = Physics.SphereCast(ray, 0.25f, out hit, 1.5f);

        if (!collided)
        {
            ray = new Ray(nose.position - 0.75f * Vector3.up, nose.forward + 0.75f * perp);
            collided = Physics.SphereCast(ray, 0.25f, out hit, 1.5f);
        }
        if (!collided)
        {
            ray = new Ray(nose.position - 0.75f * Vector3.up, nose.forward - 0.75f * perp);
            collided = Physics.SphereCast(ray, 0.25f, out hit, 1.5f);
        }
        if (!collided)
        {
            ray = new Ray(nose.position - 0.75f * Vector3.up, 0.5f * nose.forward + perp);
            collided = Physics.SphereCast(ray, 0.25f, out hit, 1.5f);
        }
        if (!collided)
        {
            ray = new Ray(nose.position - 0.75f * Vector3.up, 0.5f * nose.forward - perp);
            collided = Physics.SphereCast(ray, 0.25f, out hit, 1.5f);
        }

        if (!collided)
        {
            ray = new Ray(nose.position, nose.forward + 0.75f * perp);
            collided = Physics.SphereCast(ray, 0.25f, out hit, 1.5f);
        }
        if (!collided)
        {
            ray = new Ray(nose.position, nose.forward - 0.75f * perp);
            collided = Physics.SphereCast(ray, 0.25f, out hit, 1.5f);
        }
        if (!collided)
        {
            ray = new Ray(nose.position, 0.5f * nose.forward + perp);
            collided = Physics.SphereCast(ray, 0.25f, out hit, 1.5f);
        }
        if (!collided)
        {
            ray = new Ray(nose.position, 0.5f * nose.forward - perp);
            collided = Physics.SphereCast(ray, 0.25f, out hit, 1.5f);
        }

        if (collided)
        {
            Vector3 v = hit.point - nose.position;
            v.y = 0;
            float dot = Vector3.Dot(v, perp);

            Vector3 newCorrection = -Mathf.Sign(dot) * perp;

            if (Vector3.Dot(newCorrection, correction) < 0) correction = 0.9f * correction + 0.1f * newCorrection;
            else correction = newCorrection;
        }
        else correction = Vector3.zero;
    }

    private MyAudioSource sadSound;

    private IEnumerator Walking()
    {
        jobType = 3;

        Play (walk);

        ind = 0;
        float min = float.MaxValue;
        for (int i = 0; i < waypoints.Count; i++)
        {
            float dist = Vector3.Distance(transform.position, waypoints[i].position);
            if (dist < min)
            {
                min = dist;
                ind = i;
            }
        }

        while (true)
        {
            Play(walk);

            if (ind >= 0 && ind < waypoints.Count) targetPoint = waypoints[ind];
            else targetPoint = transform;

            reached = false;

            while (!reached || isShitting) yield return null;
        }
    }

    private bool isShitting;
    private IEnumerator Shitting ()
    {
        isShitting = true;

        targetPoint = transform;

        Play(shitting);

        yield return new WaitForSeconds(2f);

        if (Random.Range(0f, 1f) < 0.999f)
            Instantiate(pooPrefab, transform.position, transform.rotation).SetActive(true);
        else
            Instantiate(peePrefab, transform.position, transform.rotation).SetActive(true);

        SFXPlayer.Instance().Play(SFXLibrary.Instance().dogFart, false, true);

        Play(walk);

        if (ind >= 0 && ind < waypoints.Count) targetPoint = waypoints[ind];
        else targetPoint = transform;

        //reached = false;
        wannaShit = false;

        isShitting = false;
    }

    private IEnumerator Job1 ()
    {
        jobType = 1;

        if (eat != null) imgWant.sprite = eat;

        hungryTimer = 15f;

        dish = FindObjectOfType<DogDish>();
        if (dish != null) feedingPoint = dish.feedingPoint;

        Play(walk);

        ind = 0;
        float min = float.MaxValue;
        for (int i = 0; i < waypoints.Count; i++)
        {
            float dist = Vector3.Distance(transform.position, waypoints[i].position);
            if (dist < min )
            {
                min = dist;
                ind = i;
            }
        }

        while (true)
        {
            if (hungryTimer > 0)
            {
                Play(walk);

                if (ind >= 0 && ind < waypoints.Count) targetPoint = waypoints[ind];
                else targetPoint = transform;

                reached = false;

                while (!reached || isShitting) yield return null;

                if (hungryTimer > 0 && Random.Range(0f, 1f) < 0.5f)
                {
                    Play (idle);

                    yield return new WaitForSeconds(Mathf.Min(hungryTimer, 2f));
                }
            }
            else
            {
                Play(walk);

                if (dish != null && dish.food != null) targetPoint = feedingPoint;
                else targetPoint = waitingPoint;

                reached = false;

                Debug.LogError ("Here");

                while (!reached) yield return null;

                if (need != null) need.active = true;

                if (dish == null || dish.food == null)
                {
                    if (need != null) need.allright = false;

                    canvasWants.SetActive(true);

                    if(!GameController.Instance().completed)
                        sadSound = SFXPlayer.Instance().Play(SFXLibrary.Instance().dogSad, true, true);

                    EventManager.Instance().TriggerEvent("NewWish");

                    Play(lieDown);

                    yield return new WaitForSeconds(0.5f);

                    Play(lieing);

                    while (dish == null || dish.food == null) yield return null;

                    sadSound.Stop();

                    Play(standUp);

                    yield return new WaitForSeconds(0.5f);
                }

                canvasWants.SetActive(false);

                if (sadSound != null && sadSound.clip == SFXLibrary.Instance().dogSad) sadSound.Stop();

                if (need != null) need.allright = true;

                Play(eating);

                yield return new WaitForSeconds(2f);

                SFXPlayer.Instance().Play(SFXLibrary.Instance().dogEat);

                yield return new WaitForSeconds(3f);

                if (dish != null && dish.food != null)
                {
                    Destroy(dish.food);
                }

                Play(idle);

                onWishSatisfied?.Invoke();

                shitTimer = Random.Range(5f, 15f);

                hungryTimer = 20f;

                if (need != null) need.active = false;
            }

            yield return null;
        }

        yield return null;
    }

    private IEnumerator Job2 ()
    {
        jobType = 2;

        if (sleep != null) imgWant.sprite = sleep;

        sleepTimer = 15f;

        var carpet = FindObjectOfType<DogCarpet>();
        if (carpet != null && carpet.furs != null) fur = carpet.furs.gameObject;

        Play(walk);

        ind = 0;
        float min = float.MaxValue;
        for (int i = 0; i < waypoints.Count; i++)
        {
            float dist = Vector3.Distance(transform.position, waypoints[i].position);
            if (dist < min)
            {
                min = dist;
                ind = i;
            }
        }

        while (true)
        {
            if (sleepTimer > 0)
            {
                Play(walk);

                if (ind >= 0 && ind < waypoints.Count) targetPoint = waypoints[ind];
                else targetPoint = transform;

                reached = false;

                while (!reached || isShitting) yield return null;

                if (sleepTimer > 0 && Random.Range(0f, 1f) < 0.5f)
                {
                    Play(idle);

                    yield return new WaitForSeconds(Mathf.Min(sleepTimer, 2f));
                }
            }
            else
            {
                Play(walk);

                if (fur == null) targetPoint = sleepingPoint;
                else targetPoint = waitForSleepPoint;

                reached = false;

                Debug.LogError("Here");

                while (!reached) yield return null;

                if (need != null) need.active = true;

                if (fur != null)
                {
                    canvasWants.SetActive(true);

                    if (!GameController.Instance().completed)
                        sadSound = SFXPlayer.Instance().Play(SFXLibrary.Instance().dogSad, true, true);

                    if (need != null) need.allright = false;

                    Play(lieDown);

                    EventManager.Instance().TriggerEvent("NewWish");

                    yield return new WaitForSeconds(0.5f);

                    Play(lieing);

                    while (fur != null) yield return null;

                    if (need != null) need.allright = true;

                    sadSound.Stop();

                    jobType = -1;

                    yield return new WaitForSeconds(0.5f);

                    Play(standUp);

                    yield return new WaitForSeconds(1.5f);
                }

                canvasWants.SetActive(false);

                if (sadSound != null && sadSound.clip == SFXLibrary.Instance().dogSad) sadSound.Stop();

                jobType = 2;

                Play(walk);

                targetPoint = sleepingPoint;
                reached = false;
                while (!reached) yield return null;

                Play(lieDown);

                yield return new WaitForSeconds(0.5f);

                Play(sleeping);

                yield return new WaitForSeconds(10f);

                fur = Instantiate(furPrefab, transform.position, transform.rotation);
                fur.SetActive(true);

                if (need != null) need.active = false;

                Play(standUp);

                yield return new WaitForSeconds(0.5f);

                onWishSatisfied?.Invoke();

                sleepTimer = 15f;
            }

            yield return null;
        }

        yield return null;
    }

    public void Fine (float time)
    {
        var need = TaskManager.Instance().GetNeed<DogMood>();
        if(need != null && !need.allright) need.level -= 0.2f;
        else
        {
            hungryTimer = 0;
            shitTimer = 0;
            sleepTimer = 0;
        }
    }

    private void OnDisable()
    {
        if (sadSound != null) sadSound.Stop();
    }
}
