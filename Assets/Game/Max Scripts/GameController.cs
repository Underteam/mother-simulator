﻿using Facebook.Unity;
using GMATools.Common;
using MoreMountains.NiceVibrations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoSingleton<GameController>
{
    public PlayerController player;

    public GameObject startGameMenu;
    public GameObject finishGameMenu;
    public GameObject btnX5;
    public GameObject btnNo;
    public GameObject pnlSucceess;

    public GameObject gameOverMenu;
    //public GameObject btnSkip;
    //public GameObject btnFinish;
    public GameObject skipPanel;

    public GameObject continueMenu;
    public GameObject btnContinue;
    public GameObject btnNoThanks;
    public Image continueTimer;
    public Text lblContinueTimer;
    private float continue_timer;

    public GameObject openworldPanel;
    public GameObject openworldPanel2;
    public GameObject gameplayPanel;

    public GameObject gameStarter;
    public GameObject baby;

    public LoadingMenu loadingMenu;

    public ParticleSystem foodParticles;
    public float particlesDist = 1f;

    public List<Task> tasks;

    public List<BoosterUI> boosters;
    public GameObject lblGreeting;

    public Tutorial firstLaunchTutorial;
    public Tutorial secondLaunchTutorial;
    public bool firstLaunch;
    public AgeSettings ageSettings;

    public List<Text> lblsDay;

    public List<Text> lblsDayTitle;

    [System.Serializable]
    public class DoorSettings
    {
        public string name;
        public List<Switcher> doors;
        public List<SpriteRenderer> stateIcons;
        public int openLevel;
        public Sprite doorIcon;
        public Sprite doorIcon2;
        public Sprite closeIcon;
        public UnityEvent onClicked;
    }

    public List<DoorSettings> doorsSettings;
    public Sprite openIcon;
    public Sprite closeIcon;
    public DoorKey doorKey;

    //public Image nextDoorProgress;
    //public Text lblNextDoorProgress;

    public NewDoorMenu newDoorMenu;

    public List<BabySettings> babySettings;
    public List<MotherSettings> motherSettings;
    public Shop shop;

    public List<Image> boostersIcons;

    public List<DoorSettings> specialDoors;

    public bool developerMode;
    public bool skipFirstTutor;

    public PauseMenu pauseMenu;

    public AudioClip music;

    public Image shutter;

    private BabyMood babyMood;

    protected override void Init()
    {
        if (MainTheme.instance != null) MainTheme.instance.Play(music);

        GameState.Instance().started = false;

        PlayerPrefs.SetInt("NewDoorShowed0", 1);

        string language = Application.systemLanguage.ToString();
        language = PlayerPrefs.GetString("SelectedLanguage", language);

        Localizer.Initialize(language);

        inited = true;

        Debug.Log("GameController");
    }

    // Start is called before the first frame update
    void Start()
    {
        TrackRetantion();

        t0 = tasks[0];
        t1 = tasks[1];

        Debug.Log("Start,", this);
        OnStart();

        SceneManager.sceneLoaded += (s, m) =>
        {
            //Debug.LogError("Loaded");
            loaded = true;
            //OnStart();
        };
    }

    public void PlusDay()
    {
        GameState.Instance().currentTask++;// = (GameState.Instance().currentTask + 1) % tasks.Count;
        PlayerPrefs.SetInt("CurentTask", GameState.Instance().currentTask);

        OnStart();
    }

    public void MinuDay ()
    {
        GameState.Instance().currentTask--;// = (GameState.Instance().currentTask - 1 + tasks.Count) % tasks.Count;
        if (GameState.Instance().currentTask < 0) GameState.Instance().currentTask = 0;
        PlayerPrefs.SetInt("CurentTask", GameState.Instance().currentTask);

        OnStart();
    }

    private IEnumerator SendDaySeuenceTypeEvent ()
    {
        if (PlayerPrefs.GetInt("DSTEventSent", 0) == 1) yield break;

        while (!FB.IsInitialized) yield return null;

        if (Analytics.Instance().SendEvent("af_sequence_type", 0, "type", PlayerPrefs.GetInt("DaysSequenceType", 0)))
        {
            PlayerPrefs.SetInt("DSTEventSent", 1);
        }
    }

    private int currentTask = 0;
    private int prevDoor;
    private int nextDoor;
    private Task t0;
    private Task t1;
    private void OnStart ()
    {
        if (!inited) Init();

        //if (!PlayerPrefs.HasKey("DaysSequenceType"))
        //{
        //    PlayerPrefs.SetInt("DaysSequenceType", Random.Range(0, 2));
        //}

        //StartCoroutine(SendDaySeuenceTypeEvent());

        //int dst = PlayerPrefs.GetInt("DaysSequenceType", 0);

        //if (dst > 0)
        //{
        //    //Debug.LogError(tasks[0] + " " + (tasks[0] != t0) + " " + (tasks[0] != t1), tasks[0]);

        //    if (tasks[0] != null && tasks[0] != t0 && tasks[0] != t1) Destroy(tasks[0].gameObject);
        //    if (tasks[1] != null && tasks[1] != t0 && tasks[1] != t1) Destroy(tasks[1].gameObject);

        //    tasks[0] = Instantiate(t1);
        //    tasks[1] = Instantiate(t0);
        //    tasks[1].boosters.Clear();
        //    tasks[1].boosters.AddRange(tasks[0].boosters);
        //    tasks[0].boosters.Clear();
        //    tasks[0].wishes.RemoveAt(1);
        //}

        //Debug.Log("Scene loaded " + GameState.Instance().state);
        AdsController.Instance().debugMode = developerMode;

        //shop.Init();

        startGameMenu.SetActive(false);

        if (baby == null) baby = FindObjectOfType<Baby>().gameObject;

        GameState.Instance().currentTask = PlayerPrefs.GetInt("CurentTask", 0);

        int repeatFrom = 6;
        currentTask = GameState.Instance().currentTask;
        if (currentTask >= tasks.Count)
        {
            currentTask = repeatFrom + (currentTask - repeatFrom) % (tasks.Count - repeatFrom);
        }

        for (int i = 0; i < lblsDay.Count; i++)
        {
            string dayStr = Localizer.GetString("day", "Day %%");
            dayStr = dayStr.Replace("%%", (GameState.Instance().currentTask + 1).ToString());
            lblsDay[i].text = dayStr;
        }

        prevDoor = 0;
        nextDoor = 0;
        int setInd = 0;//указывает на настройки текущей двери
        for (int i = 0; i < doorsSettings.Count; i++)
        {
            int l = doorsSettings[i].openLevel;
            if (l > nextDoor) nextDoor = l;
        }
        
        for (int i = 0; i < doorsSettings.Count; i++)
        {
            int l = doorsSettings[i].openLevel;
            if (l <= GameState.Instance().currentTask && l > prevDoor)
            {
                prevDoor = l;
                setInd = i;
            }
            if (l > prevDoor && l < nextDoor) nextDoor = l;

            bool locked = doorsSettings[i].openLevel > GameState.Instance().currentTask;
            for (int j = 0; j < doorsSettings[i].doors.Count; j++)
            {
                if (doorsSettings[i].doors[j] == null) continue;
                doorsSettings[i].doors[j].locked = locked;
            }

            for (int j = 0; !locked && j < doorsSettings[i].stateIcons.Count; j++)
            {
                if (doorsSettings[i].stateIcons[j] != null) doorsSettings[i].stateIcons[j].sprite = openIcon;// SetActive(doorsSettings[i].door.locked);
            }
        }

        for (int i = 0; i < specialDoors.Count; i++)
        {
            for (int j = 0; j < specialDoors[i].stateIcons.Count; j++)
            {
                if (currentTask < specialDoors[i].openLevel)
                {
                    specialDoors[i].stateIcons[j].sprite = specialDoors[i].closeIcon;
                }
                else
                {
                    int ind = i;
                    bool alreadyShowed = PlayerPrefs.GetInt("SpecialDoorShowed" + ind, 0) == 1;
                    if (!alreadyShowed)
                    {
                        Debug.LogError("Show " + specialDoors[ind].name + " " + i + " " + j);

                        string doorName = specialDoors[ind].name;
                        doorName = Localizer.GetString("door" + doorName, doorName);
                        newDoorMenu.Show(doorName, specialDoors[ind].doorIcon);
                        var localizer = newDoorMenu.title.GetComponent<LocalizeText>();
                        localizer.key = "closeddoorYard";
                        localizer.Localize();
                        newDoorMenu.onClose = () =>
                        {
                            TutorialManager.Instance().FocusOn(specialDoors[ind].doors[0].transform);
                            PlayerPrefs.SetInt("SpecialDoorShowed" + ind, 1);
                            confetti2.transform.position = specialDoors[ind].stateIcons[0].transform.position - 0.6f * specialDoors[ind].stateIcons[0].transform.forward;
                            confetti2.transform.rotation = Quaternion.LookRotation(specialDoors[ind].stateIcons[0].transform.forward);
                            confetti2.SetActive(true);
                            localizer.key = "newroom";
                            localizer.Localize();
                        };
                    }
                    specialDoors[i].stateIcons[j].sprite = specialDoors[i].doorIcon;
                }
            }   
        }

        //Debug.LogError(prevDoor + " " + GameState.Instance().currentTask + " " + nextDoor + " | " + setInd);

        confetti.SetActive(false);

        //if (nextDoor > prevDoor)
        {
            if (prevDoor < 0) prevDoor = 0;
            float progress = (float)(GameState.Instance().currentTask - prevDoor) / (nextDoor - prevDoor);
            if (progress > 1) progress = 1;

            for (int i = 0; i < nextRoomProgress.Count; i++)
            {
                nextRoomProgress[i].gameObject.SetActive(true);
                nextRoomProgress[i].Apply();
            }
        }
        //else
        //{
        //    for (int i = 0; i < nextRoomProgress.Count; i++)
        //    {
        //        nextRoomProgress[i].gameObject.SetActive(false);
        //    }
        //}

        for (int i = 0; i < boosters.Count; i++) boosters[i].gameObject.SetActive(false);

        if (currentTask >= 0 && currentTask < tasks.Count)
        {
            var task = tasks[currentTask];
            for (int i = 0; i < lblsDayTitle.Count; i++)
                lblsDayTitle[i].text = Localizer.GetString("taskname" + currentTask, task.taskName);

            var mood = task.GetComponentInChildren<BabyMood>();
            float rt = mood.remainingTime;
            addSeconds = 120;// (int)(0.3f * rt);

            //Debug.LogError("Task " + task + " " + mood.remainingTime + " " + addSeconds + " | " + mood.level + " " + mood.speed, mood);

            for (int i = 0; i < boosters.Count && i < task.boosters.Count; i++)
            {
                boosters[i].gameObject.SetActive(true);
                boosters[i].SetBooster(task.boosters[i]);
                if (task.boosters[i].name.Equals("BoosterBaby"))
                {
                    boosters[i].lblDescription.text = boosters[i].lblDescription.text.Replace("%%", addSeconds.ToString());
                    boosters[i].lblShortDescription.text = boosters[i].lblShortDescription.text.Replace("%%", addSeconds.ToString());
                }
            }

            if (task.boosters.Count == 0) lblGreeting.SetActive(true);
            else lblGreeting.SetActive(false);
        }

        ageSettings.Init();

        if (GameState.Instance().state == GameState.State.Menu)
        {
            //EventManager.Instance().Clear();

            var listener = EventManager.Instance().AddListener((n, d) =>
            {
                if (this == null) return;

                if (adTimer >= 120)
                {
                    adTimer = 0;
                    if (GameState.Instance().state == GameState.State.Menu && !AdsController.Instance().noAds)
                    {
                        waitingForAdEnd = true;
                        Time.timeScale = 0f;
                        AudioListener.volume = 0f;
                        AdsController.Instance().ShowAd(AdsProvider.AdType.skipablevideo, (b) =>
                        {
                            Time.timeScale = 1f;
                            AudioListener.volume = 1f;

                            if (!waitingForAdEnd) return;
                            waitingForAdEnd = false;

                            if (!b) return;

                            Analytics.Instance().SendEvent("OpenWorldAd", 0, AppEventParameterName.Level, GameState.Instance().currentTask);

                            Analytics.Instance().SendEvent("af_ads_success");
                            Analytics.Instance().SendEvent("af_ads_success_int");

                            TrackAdsWatched();
                        });
                    }
                }
            });
            listeners.Add(listener);

            //gameStarter.SetActive(true);
            //baby.SetActive(false);

            openworldPanel.SetActive(true);
            openworldPanel2.SetActive(true);
            gameplayPanel.SetActive(false);

            Debug.LogWarning("Check slt " + PlayerPrefs.GetInt("FirstLaunch", 1));

            if (firstLaunch || PlayerPrefs.GetInt("FirstLaunch", 1) == 1)
            {
                if (!skipFirstTutor) TutorialManager.Instance().SetTutorial(firstLaunchTutorial);
            }
            else if (currentTask == 1 && PlayerPrefs.GetInt("FirstLaunch", 1) == 2)
            {
                TutorialManager.Instance().SetTutorial(secondLaunchTutorial);
                //PlayerPrefs.SetInt("FirstLaunch", 0);
            }

            GameState.Instance().appliedBoosters.Clear();

            bool alreadyShowed = PlayerPrefs.GetInt("NewDoorShowed" + setInd, 0) == 1;
            if (firstLaunch) alreadyShowed = false;
            if (prevDoor == GameState.Instance().currentTask && !alreadyShowed)
            {
                Debug.LogError("Show " + prevDoor + " " + nextDoor);
                for (int j = 0; j < doorsSettings[setInd].stateIcons.Count; j++)
                {
                    if (doorsSettings[setInd].stateIcons[j] != null) doorsSettings[setInd].stateIcons[j].sprite = closeIcon;

                    var key = Instantiate(doorKey);
                    keys.Add(key.gameObject);
                    key.gameObject.SetActive(true);
                    key.transform.position = doorsSettings[setInd].stateIcons[j].transform.position - 0.6f * doorsSettings[setInd].stateIcons[j].transform.forward;
                    key.onCollect += OpenNewDoor;
                    key.door = doorsSettings[setInd].stateIcons[j].transform;
                }

                for (int j = 0; j < doorsSettings[setInd].doors.Count; j++)
                {
                    if (doorsSettings[setInd].doors[j] == null) continue;
                    doorsSettings[setInd].doors[j].locked = true;
                }

                string doorName = doorsSettings[setInd].name;
                doorName = Localizer.GetString("door" + doorName, doorName);
                newDoorMenu.Show(doorName, doorsSettings[setInd].doorIcon);
                newDoorMenu.onClose = null;
                doorInd = setInd;
            }
            else if (GetPrevDoorInd() == GameState.Instance().currentTask && prevDoor >= nextDoor && GameState.Instance().currentTask > prevDoor)
            {
                Debug.LogError("show " + prevDoor + " " + nextDoor + " " + GetPrevDoorInd() + " " + GameState.Instance().currentTask + " " + alreadyShowed);
                if (PlayerPrefs.GetInt("TasksRewardTaked" + GameState.Instance().currentTask, 0) == 0)
                {
                    TasksRewards.Instance().Show(1000);
                    PlayerPrefs.SetInt("TasksRewardTaked" + GameState.Instance().currentTask, 1);
                }
            }

            PlayerPrefs.SetInt("CanContinue", 1);
        }

        if (GameState.Instance().state == GameState.State.Game)
        {
            Analytics.Instance().SendEvent("LevelStarted", 0, AppEventParameterName.Level, GameState.Instance().currentTask);

            openworldPanel.SetActive(false);
            openworldPanel2.SetActive(false);
            gameplayPanel.SetActive(true);

            adShowed = false;
            var listener = EventManager.Instance().AddListener((n, o) =>
            {
                var list = (object[])o;

                if (list == null) return;

                int ind = (int)list[0];
                int num = (int)list[1];

                bool adReady = AdsController.Instance().IsAdReady(AdsProvider.AdType.interstitial);
                //Debug.LogError("WishDone " + ind + " " + num + " " + AdsController.Instance().noAds + " " + adReady + " " + GameState.Instance().currentTask);

                if (GameState.Instance().currentTask == 0) return;
                if (num <= 3 && ind != 1) return;
                if (num > 3 && ind != 2) return;

                if (!adReady || AdsController.Instance().noAds) return;

                Debug.Log("Show Ad");

                Time.timeScale = 0f;
                AudioListener.volume = 0f;

                AdsController.Instance().ShowAd(AdsProvider.AdType.interstitial, (b) =>
                {
                    Time.timeScale = 1f;
                    AudioListener.volume = 1f;

                    Debug.LogError("Ad showed");
                    adShowed = true;

                    Analytics.Instance().SendEvent("af_ads_success");
                    Analytics.Instance().SendEvent("af_ads_success_int");
                });
            },
            "WishDone");
            listeners.Add(listener);

            if (currentTask < tasks.Count)
            {
                var task = Instantiate(tasks[currentTask]);
                TaskManager.Instance().StartTask(task, currentTask != GameState.Instance().currentTask);
            }

            for (int i = 0; i < boostersIcons.Count; i++) boostersIcons[i].transform.parent.gameObject.SetActive(false);

            for (int i = 0; i < GameState.Instance().appliedBoosters.Count; i++)
            {
                DoApplyBooster(GameState.Instance().appliedBoosters[i]);
                if (i < boostersIcons.Count)
                {
                    boostersIcons[i].sprite = GameState.Instance().appliedBoosters[i].smallIcon;
                    boostersIcons[i].transform.parent.gameObject.SetActive(true);
                }
            }

            {
                var list = continueMenu.GetComponentsInChildren<LocalizeText>(true);
                for (int i = 0; i < list.Length; i++)
                {
                    if (list[i].key.Equals("continue"))
                    {
                        list[i].Localize();
                        var text = list[i].GetComponent<Text>();
                        text.text = text.text.Replace("%%", addSeconds.ToString());
                        Destroy(list[i]);
                    }
                }
            }

            if (PlayerPrefs.GetInt("FirstLaunch", 1) == 1)
                PlayerPrefs.SetInt("FirstLaunch", 2);
        }

        babyMood = TaskManager.Instance().GetNeed<BabyMood>();

        GameState.Instance().started = true;

        if (PlayerPrefs.GetInt("ShowInterstitial", 0) == 1)
        {
            PlayerPrefs.SetInt("ShowInterstitial", 0);
            if (AdsController.Instance().TryToShowLargeBanner())
            {
                Analytics.Instance().SendEvent("af_ads_success");
                Analytics.Instance().SendEvent("af_ads_success_int");
            }
        }

        EventManager.Instance().TriggerEvent("GameStarted");
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
        //EventManager.Instance().Clear();
    }

    private bool adShowed;

    private int doorInd;
    public GameObject confetti;
    public GameObject confetti2;
    private List<GameObject> keys = new List<GameObject>();
    public void OpenNewDoor (DoorKey key)
    {
        PlayerPrefs.SetInt("NewDoorShowed" + doorInd, 1);
        newDoorMenu.gameObject.SetActive(false);

        for (int j = 0; j < doorsSettings[doorInd].stateIcons.Count; j++)
        {
            if (doorsSettings[doorInd].stateIcons[j] != null) doorsSettings[doorInd].stateIcons[j].sprite = openIcon;
        }

        for (int j = 0; j < doorsSettings[doorInd].doors.Count; j++)
        {
            if (doorsSettings[doorInd].doors[j] == null) continue;
            doorsSettings[doorInd].doors[j].locked = false;
        }

        for (int i = 0; i < keys.Count; i++) Destroy(keys[i]);
        keys.Clear();

        List<Switcher> posibleDoors = new List<Switcher>(doorsSettings[doorInd].doors);
        posibleDoors.Sort((x, y) =>
        {
            Vector3 v1 = x.transform.position - player.transform.position;
            Vector3 v2 = y.transform.position - player.transform.position;
            v1.y = 0;
            v2.y = 0;
            return v1.magnitude.CompareTo(v2.magnitude);
        });
        if (posibleDoors.Count > 0) posibleDoors[0].Switch();

        if (key != null)
        {
            confetti2.transform.position = key.transform.position;
            confetti2.transform.rotation = Quaternion.LookRotation(key.door.forward);
        }

        confetti2.SetActive(true);

        Haptic.Instance().Play(HapticTypes.Success);
    }

    public void ApplyBooster(Booster booster)
    {
        //Debug.LogError("Apply booster " + booster.name);

        if (!GameState.Instance().appliedBoosters.Contains(booster)) GameState.Instance().appliedBoosters.Add(booster);

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);

        //for (int i = 0; i < boosters.Count; i++)
        //{
        //    boosters[i].OnlyForCrystals();
        //}
    }

    public bool dirtyBooster;

    private void DoApplyBooster (Booster booster)
    {
        //Debug.LogError("Apply booster " + booster.name);

        if (booster.name.Equals("BoosterBaby"))
        {
            var need = TaskManager.Instance().GetNeed<BabyMood>();
            if (need != null)
            {
                need.speed = need.level * need.speed / (need.level + addSeconds * need.speed);
            }

            var bar = TaskManager.Instance().GetBar<BabyMood>();
            if (bar != null)
            {
                var anim = bar.transform.parent.GetComponentInChildren<Animator>(true);
                if (anim != null) anim.gameObject.SetActive(true);
            }
        }
        else if (booster.name.Equals("BoosterClean"))
        {
            dirtyBooster = true;
        }
        else if (booster.name.Equals("BoosterDog"))
        {
            var need = TaskManager.Instance().GetNeed<DogMood>();
            if (need != null)
            {
                need.speed *= 0.7f;

                var bar = TaskManager.Instance().GetBar<DogMood>();
                if (bar != null)
                {
                    var anim = bar.GetComponentInChildren<Animator>(true);
                    if (anim != null) anim.gameObject.SetActive(true);
                }
            }
        }
        else if (booster.name.Equals("BoosterGuest"))
        {
            var need = TaskManager.Instance().GetNeed<GuestMood>();
            if (need != null) need.speed *= 0.7f;

            var bar = TaskManager.Instance().GetBar<GuestMood>();
            if (bar != null)
            {
                var anim = bar.GetComponentInChildren<Animator>(true);
                if (anim != null) anim.gameObject.SetActive(true);
            }
        }
        else if (booster.name.Equals("BoosterHungry"))
        {
            var need = TaskManager.Instance().GetNeed<MomHungry>();
            if (need != null) need.speed *= 0.7f;

            var bar = TaskManager.Instance().GetBar<MomHungry>();
            if (bar != null)
            {
                var anim = bar.GetComponentInChildren<Animator>(true);
                if (anim != null) anim.gameObject.SetActive(true);
            }
        }
        else if (booster.name.Equals("BoosterHusband"))
        {
            var need = TaskManager.Instance().GetNeed<HusbandMood>();
            if (need != null) need.speed *= 0.7f;

            var bar = TaskManager.Instance().GetBar<HusbandMood>();
            if (bar != null)
            {
                var anim = bar.GetComponentInChildren<Animator>(true);
                if (anim != null) anim.gameObject.SetActive(true);
            }

            var need2 = TaskManager.Instance().GetNeed<NeedForFire>();
            if (need2 != null) need2.speed *= 0.7f;

            var bar2 = TaskManager.Instance().GetBar<NeedForFire>();
            if (bar2 != null)
            {
                var anim = bar2.GetComponentInChildren<Animator>(true);
                if (anim != null) anim.gameObject.SetActive(true);
            }
        }
        else if (booster.name.Equals("BoosterHygien"))
        {
            var need = TaskManager.Instance().GetNeed<NeedForHygien>();
            if (need != null) need.speed *= 0.7f;

            var bar = TaskManager.Instance().GetBar<NeedForHygien>();
            if (bar != null)
            {
                var anim = bar.GetComponentInChildren<Animator>(true);
                if (anim != null) anim.gameObject.SetActive(true);
            }
        }
        else if (booster.name.Equals("BoosterPep"))
        {
            var need = TaskManager.Instance().GetNeed<MomPep>();
            if (need != null) need.speed *= 0.7f;

            var bar = TaskManager.Instance().GetBar<MomPep>();
            if (bar != null)
            {
                var anim = bar.GetComponentInChildren<Animator>(true);
                if (anim != null) anim.gameObject.SetActive(true);
            }
        }
        else if (booster.name.Equals("BoosterStress"))
        {
            var need = TaskManager.Instance().GetNeed<MomStress>();
            if (need != null) need.speed *= 0.7f;

            var bar = TaskManager.Instance().GetBar<MomStress>();
            if (bar != null)
            {
                var anim = bar.GetComponentInChildren<Animator>(true);
                if (anim != null) anim.gameObject.SetActive(true);
            }
        }
        else if (booster.name.Equals("BoosterToilet"))
        {
            var need = TaskManager.Instance().GetNeed<NeedForPee>();
            if (need != null) need.speed *= 0.7f;

            var bar = TaskManager.Instance().GetBar<NeedForPee>();
            if (bar != null)
            {
                var anim = bar.GetComponentInChildren<Animator>(true);
                if (anim != null) anim.gameObject.SetActive(true);
            }
        }

        EventManager.Instance().TriggerEvent("taskBooster");
    }

    private bool loaded;

    private float adTimer;

    private MyAudioSource clock;

    // Update is called once per frame
    void Update()
    {
        if (babyMood != null)
        {
            if (babyMood.remainingTime < 10 && clock == null)
            {
                clock = SFXPlayer.Instance().Play(SFXLibrary.Instance().clock, false, true);
            }
            else if (babyMood.remainingTime > 10 && clock != null)
            {
                clock.Stop();
                clock = null;
            }
        }

        if (continue_timer > 0)
        {
            continue_timer -= Time.unscaledDeltaTime;

            lblContinueTimer.text = "" + (int)(1 + continue_timer);

            continueTimer.fillAmount = continue_timer / 9f;

            if (continue_timer <= 0)
            {
                lblContinueTimer.text = "0";

                btnContinue.SetActive(false);
                btnNoThanks.SetActive(true);
            }
        }


        if (loaded)
        {
            loaded = false;

            OnStart();
        }

        adTimer += Time.deltaTime;
    }

    public void ShowStartGameMenu()
    {
        if (currentTask < tasks.Count)
        {
            SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
            Haptic.Instance().Play(HapticTypes.MediumImpact);

            startGameMenu.SetActive(true);
        }
    }

    public void StartGame()
    {
        if (GameState.Instance().state != GameState.State.Menu) return;

        GameState.Instance().state = GameState.State.Game;

        if (keys.Count > 0)
        {
            PlayerPrefs.SetInt("NewDoorShowed" + doorInd, 1);
        }

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);

        loadingMenu.Load(SceneManager.GetActiveScene().buildIndex);
    }

    public void LoadScene (string sceneName)
    {
        loadingMenu.Load(sceneName);
    }

    public bool completed;
    public List<NextRoomProgress> nextRoomProgress;
    private int baseReward = 50;
    public void TaskCompleted ()
    {
        PlayerPrefs.SetInt("ShowGift", 1);

        if (PlayerPrefs.GetInt("CurrentTaskCompleted", 0) == 1)
        {
            //var eventParams = new Dictionary<string, object>();
            //eventParams[AppEventParameterName.Level] = GameState.Instance().currentTask;
            //eventParams["fb_session_id"] = PlayerPrefs.GetInt("SessionID");
            //FB.LogAppEvent("LevelCompleted", 0, eventParams);

            Analytics.Instance().SendEvent("LevelCompleted", 0, AppEventParameterName.Level, GameState.Instance().currentTask, "fb_session_id", PlayerPrefs.GetInt("SessionID"));

            GameState.Instance().currentTask++;
            PlayerPrefs.SetInt("CurrentTaskCompleted", 0);
            PlayerPrefs.SetInt("CurentTask", GameState.Instance().currentTask);

            TrackLevelUp();
        }

        completed = true;

        Groups.Instance().GetGroup("MainNeedBar")[0].transform.SetParent(finishGameMenu.transform.parent);
        Groups.Instance().SetGroupState("Canvases", false);

        StartCoroutine(TimeToCrystals());

        var mood = TaskManager.Instance().GetNeed<BabyMood>();
        float rt = mood.remainingTime;
        if (rt >= 60) EventManager.Instance().TriggerEvent("taskSpeedRun");

        if(!Baby.instance.cried) EventManager.Instance().TriggerEvent("taskNoTears");

        finishGameMenu.SetActive(true);
        pnlSucceess.SetActive(true);
        btnX5.SetActive(true);
        btnNo.SetActive(false);
        confetti.SetActive(true);
        var dp = FindObjectOfType<DayProgress>();
        if (dp != null) dp.gameObject.SetActive(false);

        lblReward.text = "" + (baseReward + reward);

        //Debug.LogError(prevDoor + " " + GameState.Instance().currentTask + " " + nextDoor);

        //if (nextDoor > prevDoor)
        {
            if (prevDoor < 0) prevDoor = 0;
            float from = (float)(GameState.Instance().currentTask - 1 - prevDoor) / (nextDoor - prevDoor);
            float to = (float)(GameState.Instance().currentTask - prevDoor) / (nextDoor - prevDoor);
            for (int i = 0; i < nextRoomProgress.Count; i++)
            {
                nextRoomProgress[i].gameObject.SetActive(true);
                nextRoomProgress[i].Set(GameState.Instance().currentTask - 1, GameState.Instance().currentTask);
            }
            //Debug.LogError(from + " " + to);
        }
        //else
        //{
        //    for (int i = 0; i < nextRoomProgress.Count; i++)
        //    {
        //        nextRoomProgress[i].gameObject.SetActive(false);
        //    }
        //}

        SFXPlayer.Instance().StopAll();
        SFXPlayer.Instance().Play(SFXLibrary.Instance().dayPassed);

        if (!AdsController.Instance().noAds && GameState.Instance().currentTask > 1 && !adShowed) PlayerPrefs.SetInt("ShowInterstitial", 1);

        Invoke("ShowButton", 3f);
    }

    private string TimeToStr (float time)
    {
        int sec = (int)(0.5f + time);
        int min = sec / 60;
        sec = sec - min * 60;

        string strMin = "" + min;
        if (min < 10) strMin = "0" + min;

        string strSec = "" + sec;
        if (sec < 10) strSec = "0" + sec;

        return strMin + ":" + strSec;
    }

    public Text lblReward;
    public FlyingCoin fcPrefab;
    public void FinishLevel()
    {
        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);

        StartCoroutine(RewardCoroutine(10, baseReward + reward + timeBonus, () => 
        {
            finishGameMenu.SetActive(false);
            GiftController.Instance().Show();
        }));
    }

    public void Exit ()
    {
        GameState.Instance().state = GameState.State.Menu;

        if (GameState.Instance().currentTask == 0) PlayerPrefs.SetInt("ShowInterstitial", 0);

        loadingMenu.Load(SceneManager.GetActiveScene().buildIndex);
    }

    public void Restart ()
    {
        GameState.Instance().state = GameState.State.Game;

        if (GameState.Instance().currentTask == 0) PlayerPrefs.SetInt("ShowInterstitial", 0);
        
        loadingMenu.Load(SceneManager.GetActiveScene().buildIndex);
    }

    bool waitingForAdEnd;
    public void GetX5 ()
    {
        waitingForAdEnd = true;
        Time.timeScale = 0f;
        AudioListener.volume = 0f;
        AdsController.Instance().ShowAd(AdsProvider.AdType.unskipablevideo, (b) =>
        {
            Time.timeScale = 1f;
            AudioListener.volume = 1f;

            if (!waitingForAdEnd) return;
            waitingForAdEnd = false;

            if (!b) return;

            Analytics.Instance().SendEvent("GetX5", 0, AppEventParameterName.Level, GameState.Instance().currentTask);

            Analytics.Instance().SendEvent("af_ads_success");
            Analytics.Instance().SendEvent("af_ads_success_rv");

            TrackAdsWatched();

            mult = 3;

            int r = (baseReward + reward + timeBonus) * 3;

            if (timeToCrystalsCoroutine == null)
                lblReward.text = "" + r;

            PlayerPrefs.SetInt("ShowInterstitial", 0);

            CancelInvoke("ShowButton");

            StartCoroutine(RewardCoroutine(20, r, () =>
            {
                finishGameMenu.SetActive(false);
                GiftController.Instance().Show();
            }));
        });

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    private int timeBonus;
    public FlyingCoin timeToCrystalsFC;
    public Transform timeToCrystalTarget;
    private Updater timeToCrystalsCoroutine;
    private int mult = 1;
    private IEnumerator TimeToCrystals()
    {
        var babyMood = TaskManager.Instance().GetNeed<BabyMood>();
        var bar = TaskManager.Instance().GetBar<BabyMood>();

        float time = babyMood.remainingTime;

        int num = (int)(1 + time / (120 * 0.25f));
        float startTime = Time.time;
        //Debug.LogError("Remain: " + time + " " + num);
        timeToCrystalsCoroutine = FlyingCoin.Run(timeToCrystalsFC, num, timeToCrystalTarget, 0.25f, () => { Debug.Log("Time: " + (Time.time - startTime)); });

        timeBonus = (int)(time / 10);
        int bonus = timeBonus;

        float accum = 0;

        while (time > 0)
        {
            time -= 120 * Time.deltaTime;
            accum += 120 * Time.deltaTime / 10;

            int a = (int)accum;
            if (a > bonus) a = bonus;
            if (a > 0)
            {
                lblReward.text = "" + mult * (baseReward + reward + a);
            }

            if (time < 0) time = 0;

            bar.lblProgress.text = TimeToStr(time);

            yield return null;
        }

        timeToCrystalsCoroutine = null;
    }

    private bool rewarding;
    private IEnumerator RewardCoroutine (int numCoins, int reward, System.Action onFinish)
    {
        if (rewarding) yield break;
        rewarding = true;

        btnX5.SetActive(false);
        btnNo.SetActive(false);

        yield return null;

        int sum = reward;
        float coinCost = (float)sum / numCoins;
        float accum = 0;

        FlyingCoin lastFC = null;

        for (int i = 0; i < numCoins; i++)
        {
            var coin = Instantiate(fcPrefab);
            coin.transform.SetParent(fcPrefab.transform.parent);
            coin.transform.position = fcPrefab.transform.position;
            coin.transform.localScale = fcPrefab.transform.localScale;
            coin.transform.SetParent(fcPrefab.transform.parent.parent);
            coin.gameObject.SetActive(true);
            lastFC = coin;

            accum += coinCost;
            int add = (int)accum;
            accum -= add;
            if (i == numCoins - 1) add = sum;
            sum -= add;

            Wallet.GetWallet(Wallet.CurrencyType.crystals).Add(add);

            yield return new WaitForSeconds(0.025f);
        }

        while (lastFC != null && lastFC.gameObject.activeSelf) yield return null;

        rewarding = false;

        if (onFinish != null) onFinish();
    }

    public void GameOver ()
    {
        if (PlayerPrefs.GetInt("CanContinue", 0) == 1)
        {
            Analytics.Instance().SendEvent("LevelFailed", 0, AppEventParameterName.Level, GameState.Instance().currentTask, "can_continue", true);

            btnContinue.SetActive(true);
            btnNoThanks.SetActive(false);

            continueMenu.SetActive(true);
            continue_timer = 9f;
        }
        else
        {
            Analytics.Instance().SendEvent("LevelFailed", 0, AppEventParameterName.Level, GameState.Instance().currentTask, "can_continue", false);

            gameOverMenu.SetActive(true);
        }

        SFXPlayer.Instance().StopAll();
        SFXPlayer.Instance().Play(SFXLibrary.Instance().dayFailed);

        if (!AdsController.Instance().noAds && !adShowed) PlayerPrefs.SetInt("ShowInterstitial", 1);
    }

    private void ShowButton()
    {
        btnNo.SetActive(true);
    }

    private int addSeconds;
    public void Continue ()
    {
        waitingForAdEnd = true;
        Time.timeScale = 0f;
        AudioListener.volume = 0f;
        AdsController.Instance().ShowAd(AdsProvider.AdType.unskipablevideo, (b) =>
        {
            Time.timeScale = 1f;
            AudioListener.volume = 1f;
            if (!waitingForAdEnd) return;
            waitingForAdEnd = false;

            if (!b) return;

            Analytics.Instance().SendEvent("LevelContinued", 0, AppEventParameterName.Level, GameState.Instance().currentTask);

            Analytics.Instance().SendEvent("af_ads_success");
            Analytics.Instance().SendEvent("af_ads_success_rv");

            TrackAdsWatched();

            PlayerPrefs.SetInt("CanContinue", 0);

            gameOverMenu.SetActive(false);
            continueMenu.SetActive(false);

            TaskManager.Instance().Continue(addSeconds);
        });

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    public void Skip()
    {
        waitingForAdEnd = true;
        Time.timeScale = 0f;
        AudioListener.volume = 0f;
        AdsController.Instance().ShowAd(AdsProvider.AdType.unskipablevideo, (b) =>
        {
            Time.timeScale = 1f;
            AudioListener.volume = 1f;

            if (!waitingForAdEnd) return;
            waitingForAdEnd = false;

            if (!b) return;

            Analytics.Instance().SendEvent("LevelSkiped", 0, AppEventParameterName.Level, GameState.Instance().currentTask);

            Analytics.Instance().SendEvent("af_ads_success");
            Analytics.Instance().SendEvent("af_ads_success_rv");

            TrackAdsWatched();

            skipPanel.SetActive(false);
            gameOverMenu.SetActive(false);

            TaskManager.Instance().Finish();

            PlayerPrefs.SetInt("ShowInterstitial", 0);
        });

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    public void TrackAdsWatched ()
    {
        int aw = PlayerPrefs.GetInt("AdsWatched", 0);
        aw++;
        PlayerPrefs.SetInt("AdsWatched", aw);
        if (aw == 10 || aw == 20 || aw == 30 || aw == 40 || aw == 50 || aw == 75 || aw == 100)
        {
            Analytics.Instance().SendEvent("ads_watched_" + aw + "_times");
        }
    }

    public void TrackLevelUp ()
    {
        int l = GameState.Instance().currentTask;
        if (l <= 10 || l == 15 || l == 20 || l == 25 || l == 30 || l == 40 || l == 50 || l == 100)
        {
            Analytics.Instance().SendEvent("af_level_up_" + l);
        }
    }

    public void TrackRetantion ()
    {
        if (PlayerPrefs.HasKey("RetantionTracker"))
        {
            string str = PlayerPrefs.GetString("RetantionTracker");
            System.DateTime time;
            if (System.DateTime.TryParse(str, out time))
            {
                //Debug.Log("Parsed " + str + " -> " + time);

                var span = System.DateTime.UtcNow - time;
                int hours = (int)span.TotalHours;
                float rest = (float)(span.TotalHours - hours);

                //Debug.Log("Span " + span.TotalHours + " " + hours + " " + rest);

                if (hours >= 24)
                {
                    int n = hours / 24;
                    int d = PlayerPrefs.GetInt("TrackedDay", 0);
                    d += n;
                    //Debug.Log("Add " + n);
                    PlayerPrefs.SetInt("TrackedDay", d);
                    time = time.AddHours(n * 24);
                    PlayerPrefs.SetString("RetantionTracker", time.ToString());
                    Analytics.Instance().SendEvent("af_retention_d" + d);
                }
            }
            else
            {
                //Debug.Log("Parse error");
                PlayerPrefs.SetString("RetantionTracker", System.DateTime.UtcNow.ToString());
            }
        }
        else
        {
            PlayerPrefs.SetString("RetantionTracker", System.DateTime.UtcNow.ToString());
            PlayerPrefs.SetInt("TrackedDay", 0);
        }
    }

    public int reward;

    public RoomClosed roomClosedMenu;
    public void RoomClosed (Switcher sw)
    {
        for (int i = 0; i < specialDoors.Count; i++)
        {
            for (int j = 0; j < specialDoors[i].doors.Count; j++)
            {
                if (specialDoors[i].doors[j] == sw)
                {
                    Debug.LogError("Found " + GameState.Instance().state);

                    if (currentTask >= specialDoors[i].openLevel)
                    {
                        Debug.LogError("Here");
                        if (GameState.Instance().state == GameState.State.Game)
                        {
                            string message = Localizer.GetString("challengesDaydoormessage", "Для открытия требуется Ключ от Холла");
                            roomClosedMenu.Show(message);
                        }
                        else if (specialDoors[i].onClicked != null && specialDoors[i].onClicked.GetPersistentEventCount() > 0)
                        {
                            Debug.LogError("Invoke " + GameState.Instance().state);
                            specialDoors[i].onClicked.Invoke();
                        }
                    }
                    else
                    {
                        string message = Localizer.GetString("specialdoormessage" + sw.name, "Для открытия требуется Ключ от Холла");
                        roomClosedMenu.Show(message);
                    }

                    return;
                }
            }
        }

        //Debug.LogError("Not Found", sw);

        for (int i = 0; i < doorsSettings.Count; i++)
        {
            for(int j = 0; j < doorsSettings[i].doors.Count; j++)
            {
                if (doorsSettings[i].doors[j] == sw)
                {
                    //Debug.LogError("Found");

                    string message = Localizer.GetString("roomclosedmessage", "Для того чтобы открыть\n\r%% пройдите");
                    //string m = message;
                    string roomname = Localizer.GetString("closedroom" + sw.name, sw.name);
                    //string level = Localizer.GetString("level", "level");
                    message = message.Replace("%%1", roomname);
                    message = message.Replace("%%2", "" + (doorsSettings[i].openLevel + 1));
                    //Debug.LogError(m + "->" + message);
                    roomClosedMenu.Show(message);
                    return;
                }
            }
        }

        //Debug.LogError("Not Found", sw);
    }

    //сколько осталось до следующей двери
    public int GetLevelsToNextDoor ()
    {
        return nextDoor - GameState.Instance().currentTask;
    }

    //количество уровней в группе
    public int GetLevelsInGroup()
    {
        //Debug.LogError("GLIG " + nextDoor + " " + prevDoor);

        if (nextDoor > prevDoor)
            return nextDoor - prevDoor;

        return 10;
    }

    public int GetNextDoorInd()
    {
        //Debug.LogError("GNDI " + nextDoor + " " + prevDoor + " " + GameState.Instance().currentTask);

        if (nextDoor > prevDoor)
            return nextDoor;

        int ind = prevDoor;
        while (ind <= GameState.Instance().currentTask) ind += 10;

        return ind;
    }

    public int GetPrevDoorInd()
    {
        //Debug.LogError("GPDI " + nextDoor + " " + prevDoor + " " + GameState.Instance().currentTask);

        if (nextDoor > prevDoor)
            return prevDoor;

        int ind = prevDoor;
        while (ind <= GameState.Instance().currentTask) ind += 10;

        return ind - 10;
    }

    public string GetNextDoorName()
    {
        for(int i = 0; i < doorsSettings.Count; i++)
            if(doorsSettings[i].openLevel == nextDoor)
                return doorsSettings[i].name;

        return "";
    }

    public Sprite crystals;
    public Sprite GetNextDoorIcon ()
    {
        if (nextDoor <= prevDoor) return crystals;

        for (int i = 0; i < doorsSettings.Count; i++)
            if (doorsSettings[i].openLevel == nextDoor)
                return doorsSettings[i].doorIcon2;

        return crystals;
    }

    public bool IsDayWTutor (int day)
    {
        if (day < 0 || day >= tasks.Count) return false;

        return tasks[day].tutor != null && tasks[day].tutor.somethingNew;
    }

    //[EditorButton]
    //public void RemoveTaskAt(int ind)
    //{
    //    if (ind >= 0 && ind < tasks.Count) tasks.RemoveAt(ind);
    //}
}
