﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LateCloser : MonoBehaviour
{
    public UnityEvent onEvent;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (run && onEvent != null) onEvent.Invoke();

        run = false;
    }

    private bool run;
    public void Do ()
    {
        run = true;
    }
}
