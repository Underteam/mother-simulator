﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VIPAccess : MonoSingleton<VIPAccess>
{
    public GameObject panel;

    public IAP iap;

    public ShopItemsPack pack;

    public bool ready { get; private set; }

    public Text largeText;
    public Text smallText;
    public Text buttonText;

    public List<GameObject> toHide;
    public List<GameObject> toShow;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return CheckCoroutine();

        if (PlayerPrefs.GetInt("ShowVIP", 0) == 1)
        {
            Show();
            PlayerPrefs.SetInt("ShowVIP", 0);
        }
    }

    private IEnumerator CheckCoroutine ()
    {
        //Debug.LogError("Check " + IAPController.Instance().receiptsReady);

        while (!IAPController.Instance().receiptsReady) yield return null;

        //Debug.LogError("Continue");

        string price = IAPController.Instance().GetPrice(iap.iapid);

        smallText.text = Localizer.GetString("vipPrice", "price").Replace("%", price);
        largeText.text = Localizer.GetString("vipDesc", "description").Replace("%", price);
        buttonText.text = Localizer.GetString("vipPrice", "price").Replace("%", price);

        if (PlayerPrefs.GetInt("WasSubscribed", 0) == 1)
        {
            smallText.gameObject.SetActive(false);
            buttonText.gameObject.SetActive(false);
        }
        else
        {

        }

        ready = true;

        if (IsSubscribeActive())
        {
            Debug.LogError("Active");

            Buyed();

            System.DateTime time = System.DateTime.UtcNow;
            time = time.AddHours(-23.99);
            string lastShowTimeStr = PlayerPrefs.GetString("VipAccessLastTime", time.ToString());
            System.DateTime.TryParse(lastShowTimeStr, out time);

            var span = System.DateTime.UtcNow - time;
            int th = (int)span.TotalHours;
            if (th >= 24)
            {
                int n = th / 24;
                th = th - (th % 24);
                time = time.AddHours(th);
                PlayerPrefs.SetString("VipAccessLastTime", time.ToString());

                int numCrystals = 1000 * n;
                int numKeys = 5 * n;
            }
        }
        else
        {
            Debug.LogError("Cancel " + PlayerPrefs.GetInt("ShowVIP", 0));

            Cancel();
        }

        IAPController.Instance().CheckSubscription(iap.iapName);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Show ()
    {
        StartCoroutine(CheckCoroutine());

        Debug.LogError(ready + " " + (active == null ? "null" : active.Value.ToString()));

        if (ready && active != null && active.Value) return;

        MenuQueue.Instance().Show(panel, 0);
    }

    public void Close ()
    {
        panel.SetActive(false);
    }

    private bool? active;
    public bool IsSubscribeActive ()
    {
#if UNITY_EDITOR
        return PlayerPrefs.GetInt("VipAccessActive", 0) == 1; 
#else
        if (!ready) return false;

        if (active == null) active = IAPController.Instance().CheckIAP(iap.iapName);

        return active.Value;
#endif
    }

    public void Subscribe ()
    {
        iap.Buy();
    }

    public void Buyed ()
    {
        PlayerPrefs.SetInt("WasSubscribed", 1);

        bool alreadyBuyed = PlayerPrefs.GetInt("VIPBuyed", 0) == 1;

        Debug.LogError("Buyed " + alreadyBuyed);

        for (int i = 0; pack != null && i < pack.items.Count; i++)
        {
            Shop.Instance().OpenItem(pack.items[i]);
            if (!alreadyBuyed) Shop.Instance().ApplyItem(pack.items[i]);
        }

        PlayerPrefs.SetInt("VIPBuyed", 1);
        PlayerPrefs.SetInt("VipAccessActive", 1);

        for (int i = 0; i < toHide.Count; i++) toHide[i].SetActive(false);
        for (int i = 0; i < toShow.Count; i++) toShow[i].SetActive(true);

        active = true;

        Close();

        Shop.Instance().UpdateVisual();
    }

    public void Cancel ()
    {
        PlayerPrefs.SetInt("VipAccessActive", 0);

        for (int i = 0; pack != null && i < pack.items.Count; i++)
        {
            Shop.Instance().CloseItem(pack.items[i]);
        }

        Shop.Instance().UpdateVisual();
    }
}
