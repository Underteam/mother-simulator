﻿using Facebook.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Analytics : MonoSingleton<Analytics>
{
    // Start is called before the first frame update
    void Start()
    {
        GameAnalyticsSDK.GameAnalytics.Initialize();
    }

    public bool SendEvent(string evtName, float toSum = 0, params object[] parameters)
    {

        bool ready = FB.IsInitialized && MaxSdk.IsInitialized();

        if (!ready)
        {
            StartCoroutine(Resend(evtName, toSum, parameters));
            return false;
        }

        var eventParams = new Dictionary<string, object>();
        var eventParamsStr = new Dictionary<string, string>();
        List<Firebase.Analytics.Parameter> firebaseParameters = new List<Firebase.Analytics.Parameter>();

        for (int i = 0; i < parameters.Length - 1; i += 2)
        {
            if (!(parameters[i] is string pname)) continue;

            eventParams[pname] = parameters[i+1];
            eventParamsStr[pname] = parameters[i + 1].ToString();
            Firebase.Analytics.Parameter p = new Firebase.Analytics.Parameter(pname, parameters[i + 1].ToString());
            firebaseParameters.Add(p);
        }

        try
        {
            //Debug.LogError("Send event: " + evtName);
            //foreach (var kvp in eventParams) Debug.LogError(kvp.Key + ":" + kvp.Value);

            FB.LogAppEvent(evtName, toSum, eventParams);
            AppsFlyer.trackRichEvent(evtName, eventParamsStr);
            MaxSdk.TrackEvent(evtName, eventParamsStr);
            GameAnalyticsSDK.GameAnalytics.NewDesignEvent(evtName, toSum);
            AppMetrica.Instance.ReportEvent(evtName, eventParams);

            Firebase.Analytics.FirebaseAnalytics.LogEvent(evtName, firebaseParameters.ToArray());
        }
        catch
        {
            return false;
        }

        return true;
    }

    public IEnumerator Resend (string evtName, float toSum = 0, params object[] parameters)
    {
        bool ready = false;

        while (!ready)
        {
            ready = FB.IsInitialized && MaxSdk.IsInitialized();
            yield return new WaitForSeconds(0.5f);
        }

        SendEvent(evtName, toSum, parameters);
    }
}
