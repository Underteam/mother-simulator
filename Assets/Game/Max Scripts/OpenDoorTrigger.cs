﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoorTrigger : MonoBehaviour
{
    public InteractableDoor door;

    private List<Dog> dogs = new List<Dog>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.LogError("Enter " + other, other);

        var dog = other.GetComponent<Dog>();
        if (dog != null)
        {
            dogs.Add(dog);

            var switchers = door.GetComponentsInChildren<Switcher>();

            if (door != null && door.closed)
            {
                door.targetPerson = dog.transform;
                //Debug.Log("Try to open " + switchers.Length);
                if (switchers.Length > 0) switchers[0].Switch();
            }

            for (int i = 0; i < switchers.Length; i++) switchers[i].enabled = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var dog = other.GetComponent<Dog>();
        if (dog != null)
        {
            if (dogs.Contains(dog)) dogs.Remove(dog);

            if (dogs.Count == 0)
            {
                var switchers = door.GetComponentsInChildren<Switcher>();

                for (int i = 0; i < switchers.Length; i++) switchers[i].enabled = true;

                door.targetPerson = PlayerController.instance.transform;
            }
        }
    }
}
