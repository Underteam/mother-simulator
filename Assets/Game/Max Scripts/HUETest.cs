﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUETest : MonoBehaviour
{
    public Texture2D tex1;
    public Texture2D tex2;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [Sirenix.OdinInspector.Button]
    public void Test ()
    {
        //int w1 = tex1.width;
        //int h1 = tex1.height;

        //int w2 = tex2.width;
        //int h2 = tex2.height;

        var pixels1 = tex1.GetPixels();
        var pixels2 = tex2.GetPixels();

        if (pixels1.Length != pixels2.Length)
        {
            Debug.LogError("Incorrect");
            return;
        }

        int n = pixels1.Length / 100;

        for (int i = 0; i < pixels1.Length; i++)
        {
            float h1, s1, v1, h2, s2, v2;

            float a1, a2;

            Color.RGBToHSV(pixels1[i], out h1, out s1, out v1);
            Color.RGBToHSV(pixels2[i], out h2, out s2, out v2);
            
            a1 = pixels1[i].grayscale;
            a2 = pixels2[i].grayscale;

            if (n == 0 || i % n == 0)
                Debug.LogError((h1 - h2) + " " + (s1 - s2) + " " + (v1 - v2) + " | " + (a1-a2));
        }
    }

    Matrix4x4 m = new Matrix4x4();
}
