﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskManager : MonoSingleton<TaskManager>
{
    public Task currentTask;

    public List<ProgressBar> bars;

    private bool completed;

    public System.Action<Wish> onNewWish;

    public DayProgress dayProgress;

    public bool displayWishes { get; set; } = true;

    public WishesProgress wishesProgress;

    public GameObject plus20sec;
    public GameObject minus10sec;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void Init()
    {
        if (inited) return;

        if (GameState.Instance().state == GameState.State.Menu) return;

        for (int i = 0; dayProgress != null && i < dayProgress.lblWishes.Count; i++) dayProgress.lblWishes[i].gameObject.SetActive(false);

        for (int i = 0; i < bars.Count; i++) bars[i].gameObject.SetActive(false);

        //Debug.LogError("I am here", this);
        Apply();

        inited = true;
    }

    [EditorButton]
    public void Apply ()
    {
        if (dayProgress != null) dayProgress.Apply(true);
    }

    private WishBox lastWish;

    public void StartTask (Task task, bool skipTutor)
    {
        currentTask = task;

        for (int i = 0; i < task.wishes.Count; i++)
        {
            task.wishes[i] = Instantiate(task.wishes[i]);
            task.wishes[i].transform.SetParent(task.transform);
        }

        Debug.LogError("Start task " + currentTask + " " + skipTutor, currentTask);

        EventManager.Instance().AddListener(OnTaskComplete, "TaskCompleted");
        EventManager.Instance().AddListener(OnTaskFailed, "TaskFailed");

        for (int i = 0; dayProgress != null && i < dayProgress.lblWishes.Count; i++) dayProgress.lblWishes[i].gameObject.SetActive(false);

        for (int i = 0; i < bars.Count; i++) bars[i].gameObject.SetActive(false);

        if (currentTask != null)
        {
            currentTask.onNextWish += (w) =>
            {
                //Debug.LogError ("New wish " + w.description + " " + displayWishes);

                EventManager.Instance().TriggerEvent("NewWish");
                EventManager.Instance().TriggerEvent("NewBabyWish", w);

                if (lastWish != null) CompleteWish(lastWish);

                if (displayWishes)
                {
                    lastWish = AddWish(Localizer.GetString("wish" + w.eventName, w.description));
                    if(lastWish != null) lastWish.Apply(w);
                }

                for (int i = 0; wishesProgress != null && i < currentTask.wishes.Count; i++)
                {
                    if (currentTask.wishes[i].done)
                        wishesProgress.SetState(i, 2);
                    else if(currentTask.wishes[i] == w)
                        wishesProgress.SetState(i, 1);
                }

                Apply();
            };

            currentTask.onWishDone += (w) =>
            {
                for (int i = 0; i < currentTask.wishes.Count; i++)
                {
                    if (currentTask.wishes[i] == w)
                    {
                        if (wishesProgress != null) wishesProgress.SetState(i, 2);
                        if (i < currentTask.wishes.Count - 1 && plus20sec != null)
                        {
                            plus20sec.SetActive(true);
                        }

                        EventManager.Instance().TriggerEvent("WishDone", i, currentTask.wishes.Count);
                    }
                }
            };

            for (int i = 0; i < currentTask.needs.Count && i < bars.Count; i++)
            {
                currentTask.needs[i].Init();
                bars[i].gameObject.SetActive(true);
                bars[i].SetName(currentTask.needs[i].description, currentTask.needs[i].icon);
                bars[i].inverted = currentTask.needs[i].inverted;
            }

            if (wishesProgress != null)
            {
                wishesProgress.SetNumVisible(currentTask.wishes.Count);
                wishesProgress.SetState(1, 1);
                for (int i = 1; i < currentTask.wishes.Count; i++) wishesProgress.SetState(i, 0);
            }

            currentTask.StartTask(skipTutor);
            EventManager.Instance().TriggerEvent("TaskStarted");

            completed = false;
        }
    }

    private void OnDestroy()
    {
        EventManager.Instance().DetachListener(OnTaskComplete, "TaskCompleted");
        EventManager.Instance().DetachListener(OnTaskFailed, "TaskFailed");
    }

    public void CompleteCurrentWish ()
    {
        if (lastWish != null) CompleteWish(lastWish);
    }

    public WishBox AddWish(string wish, bool removeLast = false, params string[] sublines)
    {
        if (dayProgress == null) return null;//костыль из-за Challengeй

        WishBox wishBox = null;

        int lastActive = -1;
        for (int i = dayProgress.lblWishes.Count - 1; i >= 0 ; i--)
        {
            if (dayProgress.lblWishes[i].gameObject.activeSelf)
            {
                lastActive = i;
                break;
            }
        }

        if (removeLast && lastActive >= 0)
        {
            wishBox = dayProgress.lblWishes[lastActive];
        }
        else
        {
            int firstInactive = -1;
            for (int i = 0; i < dayProgress.lblWishes.Count; i++)
            {
                if (!dayProgress.lblWishes[i].gameObject.activeSelf)
                {
                    firstInactive = i;
                    break;
                }
            }

            if (firstInactive >= 0) wishBox = dayProgress.lblWishes[firstInactive];
            else
            {
                wishBox = Instantiate(dayProgress.lblWishes[0]);
                wishBox.transform.SetParent(dayProgress.lblWishes[0].transform.parent);
                wishBox.transform.localScale = dayProgress.lblWishes[0].transform.localScale;
                dayProgress.lblWishes.Add(wishBox);
            }
        }

        wishBox.UnComplete();
        wishBox.lblWish.text = wish;
        wishBox.gameObject.SetActive(true);
        wishBox.transform.SetSiblingIndex(dayProgress.lblWishes.Count);

        for (int i = 0; i < sublines.Length; i++) wishBox.SetSubline(i, sublines[i]);

        wishBox.SetNumVisibleSublines(sublines.Length);

        Apply();

        return wishBox;
    }

    public void CompleteWish (WishBox wish)
    {
        wish.Complete();
    }

    public void CompleteWish(int ind)
    {
        if (dayProgress == null) return;

        if (ind < 0 || ind >= dayProgress.lblWishes.Count) return;

        dayProgress.lblWishes[ind].Complete();
    }

    public void RemoveWish (WishBox wish)
    {
        wish.gameObject.SetActive(false);

        Apply();
    }

    private void OnTaskComplete (string name, object data)
    {
        if (this == null) return;

        //Debug.LogError("task completed");

        if (lastWish != null) CompleteWish(lastWish);

        completed = true;

        PlayerPrefs.SetInt("CurrentTaskCompleted", 1);

        Invoke("Finish", 1f);
    }

    public void Finish ()
    {
        completed = true;
        PlayerPrefs.SetInt("CurrentTaskCompleted", 1);
        GameController.Instance().TaskCompleted();
    }

    private void OnTaskFailed (string name, object data)
    {
        if (this == null) return;

        Debug.LogError("task failed");

        completed = true;

        GameController.Instance().GameOver();
    }

    public void Continue (int addSeconds)
    {
        completed = false;
        currentTask.failed = false;

        for(int i = 0; i < currentTask.needs.Count; i++)
        {
            if (currentTask.needs[i] is BabyMood) currentTask.needs[i].AddSeconds(addSeconds);
            else currentTask.needs[i].level += currentTask.needs[i].scaleFrom;
            if (currentTask.needs[i].level > 1) currentTask.needs[i].level = 1f;
        }
    }

    private bool setLevels = true;
    // Update is called once per frame
    void Update()
    {
        if (completed) return;

        if (currentTask != null)
        {
            for(int i = 0; i < currentTask.needs.Count && i < bars.Count; i++)
            {
                currentTask.needs[i].OnUpdate();
                if(currentTask.currentWish != null) currentTask.currentWish.OnUpdate();
                bool inv = currentTask.needs[i].inverted;
                float val = currentTask.needs[i].level;
                if (currentTask.needs[i].instantSet)
                {
                    bars[i].ApplyValue(inv ? (1 - val) : val, true);
                    currentTask.needs[i].instantSet = false;
                }
                else
                    bars[i].val = (inv ? (1 - val) : val);

                int sec = (int)(0.5f + currentTask.needs[i].remainingTime);
                int min = sec / 60;
                sec = sec - min * 60;

                string strMin = "" + min;
                if (min < 10) strMin = "0" + min;

                string strSec = "" + sec;
                if (sec < 10) strSec = "0" + sec;

                if (bars[i].lblProgress != null)
                {
                    if (currentTask.needs[i].animate)
                        bars[i].lblProgress.color = Color.Lerp(bars[i].lblProgress.color, Color.green, 10 * Time.deltaTime);
                    else
                        bars[i].lblProgress.color = Color.Lerp(bars[i].lblProgress.color, Color.white, 10 * Time.deltaTime);

                    bars[i].lblProgress.text = strMin + ":" + strSec;
                }

                if (currentTask.needs[i].level < currentTask.needs[i].scaleFrom && (currentTask.needs[i].anim == null || !currentTask.needs[i].anim.enabled))
                {
                    if (currentTask.needs[i].anim == null)
                    {
                        currentTask.needs[i].anim = bars[i].GetComponent<ScaleAnimation>();
                        if (currentTask.needs[i].anim == null) currentTask.needs[i].anim = bars[i].gameObject.AddComponent<ScaleAnimation>();
                    }

                    currentTask.needs[i].anim.enabled = true;
                }

                if (currentTask.needs[i].level > Mathf.Min(currentTask.needs[i].scaleFrom + 0.1f, 0.99f) && currentTask.needs[i].anim != null && currentTask.needs[i].anim.enabled)
                {
                    currentTask.needs[i].anim.enabled = false;
                }
            }

            setLevels = true;
        }
    }

    public T GetNeed<T> () where T : Need
    {
        if (currentTask != null)
        {
            for (int i = 0; i < currentTask.needs.Count; i++)
            {
                if (currentTask.needs[i] is T) return currentTask.needs[i] as T;
            }
        }

        return null;
    }

    public ProgressBar GetBar<T> () where T : Need
    {
        if (currentTask != null)
        {
            for (int i = 0; i < currentTask.needs.Count; i++)
            {
                if (currentTask.needs[i] is T && i < bars.Count) return bars[i];
            }
        }

        return null;
    }

    public List<T> GetWish<T> (string eventName = "") where T : Wish
    {
        List<T> result = new List<T>();

        if (currentTask != null)
        {
            for (int i = 0; i < currentTask.wishes.Count; i++)
            {
                if (currentTask.wishes[i] is T)
                {
                    if (string.IsNullOrEmpty(eventName)) result.Add(currentTask.wishes[i] as T);
                    else if(eventName.Equals(currentTask.wishes[i].eventName)) result.Add(currentTask.wishes[i] as T);
                }
            }
        }

        return result;
    }
}
