﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splinters : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<PlayerController>() != null)
        {
            MomStress stress = FindObjectOfType<MomStress>();
            if (stress != null) stress.level -= 0.2f;
        }
    }
}
