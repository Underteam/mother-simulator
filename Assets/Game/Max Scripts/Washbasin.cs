﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Washbasin : MonoBehaviour, IAppropriatanceProvider
{
    public Transform targetPos;

    public float camRot;
    private Quaternion targetRot;

    private FPSCharacterController player;

    private bool playing;

    public AnimationClip washHandLeft;
    public AnimationClip washHandRight;
    public AnimationClip washItem;

    public GameObject water;

    // Start is called before the first frame update
    void Start()
    {
        targetRot = Quaternion.Euler(camRot, 0, 0);
    }

    private float timer;
    void Update()
    {
        if (player != null)
        {
            float horizontal = InputManager.Instance().GetAxis("Horizontal");
            float vertical = InputManager.Instance().GetAxis("Vertical");
            if (Mathf.Abs(horizontal) + Mathf.Abs(vertical) > 0) timer += Time.deltaTime;
            else timer = 0;

            if (timer > 0.2f)
            {
                AllowMovement();
                timer = 0;
                return;
            }

            player.transform.position = Vector3.Lerp(player.transform.position, targetPos.position, 5 * Time.deltaTime);
            player.transform.rotation = Quaternion.Lerp(player.transform.rotation, targetPos.rotation, 5 * Time.deltaTime);
            player.pivot.transform.localRotation = Quaternion.Lerp(player.pivot.transform.localRotation, targetRot, 5 * Time.deltaTime);

            float d1 = Vector3.Distance(player.transform.position, targetPos.position);
            float d2 = Quaternion.Angle(player.transform.rotation, targetPos.rotation) / 10f;

            if (!playing && Mathf.Max(d1, d2) < 0.1f)//готовы чтобы помыть
            {
                playing = true;
                if (item == null)
                {
                    PlayerController.instance.leftHand.Play(washHandLeft);
                    PlayerController.instance.rightHand.Play(washHandRight);
                    PlayerController.instance.handReachedOut = true;
                    Invoke("AllowMovement", 1.5f);
                }
                else
                {
                    PlayerController.instance.rightHand.Play(washItem);
                    PlayerController.instance.handReachedOut = true;
                    Invoke("AllowMovement", 2f);
                }
            }
        }
    }

    private void AllowMovement()
    {
        playing = false;
        player = null;
        item = null;
        if (PlayerController.instance == null) return;
        PlayerController.instance.handReachedOut = false;
        PlayerController.instance.leftHand.Stop();
        PlayerController.instance.rightHand.Stop();
        PlayerController.instance.GetComponent<FPSCharacterController>().disableMovement = false;
        PlayerController.instance.GetComponent<FPSCharacterController>().disableRotation = false;

        water.SetActive(false);
    }

    private Item item;

    public void ApplyItem (Item item)
    {
        //Debug.Log("Apply " + item, item);

        if (PlayerController.instance != null && PlayerController.instance.currentItem != null && PlayerController.instance.currentItem.GetComponent<Baby>() != null) return;

        if (item != null && item.GetComponent<Baby>() != null) return;

        if (player != null) return;

        this.item = item;

        water.gameObject.SetActive(true);
        if (!water.gameObject.activeInHierarchy) return;

        if (PlayerController.instance == null) return;

        player = PlayerController.instance.GetComponent<FPSCharacterController>();
        player.disableMovement = true;
        player.disableRotation = true;
    }

    public bool IsAppropriate(Item item)
    {
        if (item != null && item.GetComponent<Baby>() != null) return false;

        if (player != null) return false;

        //if (!water.gameObject.activeInHierarchy) return false;

        if (PlayerController.instance == null) return false;

        return true;
    }
}
