﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class ProgressBar : MonoBehaviour {

    public float minVal = 0f;

    public float maxVal = 1f;

	public Image fill;

    public float val;

    public Text lblName;
    public Text lblProgress;

    public Image icon;

    public bool inverted;
    public bool colorize;

    public float greenLevel = 1f;
    public float yellowLevel = 0.5f;
    public float redLevel = 0.0f;

    private float _val;

    private bool started;
    void Start ()
    {
        if (!started)
        {
            _val = val;
            //Debug.Log("Start " + _val, this);
            ApplyValue(val, true);
        }
    }

	public void ApplyValue(float val, bool setVal = false)
    {
        started = true;

        //if (setVal) Debug.LogError("Set val " + val, this);
        //if (!setVal && Mathf.Abs(val - _val) > 0.1f) Debug.LogError("??? " + val + " " + _val, this);

        if (setVal) _val = val;

		val = Mathf.Clamp (val, 0, 1);
		fill.fillAmount = minVal + val * (maxVal-minVal);
        if (colorize)
        {
            float v = val;

            if (inverted) v = 1 - val;

            Color c = Color.green;
            if (v <= redLevel)
            {
                c = Color.red;
            }
            else if (v <= yellowLevel)
            {
                v = (v - redLevel) / (yellowLevel - redLevel);
                c = Color.Lerp(Color.red, Color.yellow, v);
            }
            else if (v <= greenLevel)
            {
                v = (v - yellowLevel) / (greenLevel - yellowLevel);
                c = Color.Lerp(Color.yellow, Color.green, v);
            }

            fill.color = c;
        }
    }

    private void Update()
    {
        float speed = 3f;
        if (val > _val) speed = 1.5f;

        _val = Mathf.Lerp(_val, val, speed * Time.deltaTime);
        ApplyValue(_val);
    }

    public void SetName (string name, Sprite icon = null)
    {
        name = Localizer.GetString("scale" + name, name);

        if (lblName != null) lblName.text = name;

        if (this.icon != null && icon != null) this.icon.sprite = icon;
    }
}
