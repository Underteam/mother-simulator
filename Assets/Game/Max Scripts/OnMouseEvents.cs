﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnMouseEvents : MonoBehaviour
{
    public UnityEvent onMouseDown;

    public UnityEvent onMouseUp;

    public Vector4 region;

    private float screenWidth;
    private float screenHeight;

    // Start is called before the first frame update
    void Start()
    {
        screenWidth = Screen.width;
        screenHeight = Screen.height;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Input.mousePosition.x >= region.x * screenWidth &&
                Input.mousePosition.x <= region.z * screenWidth &&
                Input.mousePosition.y >= region.y * screenHeight &&
                Input.mousePosition.y <= region.w * screenHeight &&
                onMouseDown != null) onMouseDown.Invoke();
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (onMouseUp != null) onMouseUp.Invoke();
        }
    }
}
