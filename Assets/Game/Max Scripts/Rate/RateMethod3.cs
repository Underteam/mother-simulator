﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RateMethod3 : RateMethod
{
    private void Awake()
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public override void Run()
    {
        if (PlayerPrefs.GetInt("Rated", 0) == 1)
        {
            return;
        }

        StartCoroutine(Job());
    }

    private GameObject newDoorMenu;

    private IEnumerator Job ()
    {
        while (true)
        {
            newDoorMenu = GameController.Instance().newDoorMenu.gameObject;

            while (newDoorMenu != null && !newDoorMenu.activeSelf) yield return null;

            if (newDoorMenu == null) continue;

            while (newDoorMenu.activeSelf) yield return null;

            {
                RateUs.instance.ShowRatePanel(index);
                PlayerPrefs.SetInt("Rated", 1);
                break;
            }

            yield return null;
        }
    }
}
