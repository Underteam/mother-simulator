﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RateUs : MonoBehaviour
{
    public static RateUs instance { get; private set; }

    [SerializeField]
    private Rate ratePanel;

    private int method;

    public List<RateMethod> methods;

    public bool test;

    private void Awake()
    {
        if (instance != null)
        {
            if (instance.ratePanel == null) instance.ratePanel = ratePanel;
            Destroy(gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        Init();
    }

    private void Init ()
    {
        if (methods.Count == 0) return;

        //PlayerPrefs.SetInt("Rated", 0);

        if (PlayerPrefs.GetInt("Rated", 0) == 1) return;
        
        {
            method = -1;
            if (test)
            {
                if (PlayerPrefs.HasKey("RateMethod"))
                {
                    method = PlayerPrefs.GetInt("RateMethod");
                    //Debug.LogError("Restore method " + method);
                }
                else
                    method = Random.Range(0, methods.Count);
            }
            else
            {
                for (int i = 0; i < methods.Count; i++)
                {
                    if (PlayerPrefs.GetInt("RateMethodSkiped" + methods[i].index) == 1)
                    {
                        //Debug.LogError("Skip " + i + " " + methods[i].index);
                        continue;
                    }
                    method = i;
                    break;
                }
            }

            //Debug.LogError ("Chose method " + method + " " + PlayerPrefs.GetInt("Rated"));

            PlayerPrefs.SetInt("RateMethod", method);
        }

        //Debug.LogError("Method is " + method);

        //if (GameController.Instance().developerMode)
        //{
        //    method = 0;
        //    PlayerPrefs.SetInt("Rated", 0);
        //}

        if (method >= 0 && method < methods.Count)
        {
            //Debug.LogError("Run " + method  + " " + methods[method], methods[method]);
            //Debug.LogError("RateUs", this);
            methods[method].Run();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowRatePanel (int method)
    {
        Debug.LogError("Show " + method);

        ratePanel.Show((r) =>
        {
            if (r < 0)
            {
                Debug.LogError("Skiped " + r);

                PlayerPrefs.SetInt("RateMethodSkiped" + method, 1);
                PlayerPrefs.SetInt("Rated", 0);
                Init();
            }
            else
            {
                Debug.LogError("Rated " + r);

                PlayerPrefs.SetInt("Rated", 1);
            }
        });
    }
}
