﻿using Facebook.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rate : MonoSingleton<Rate>
{
    public List<Image> stars;
    public List<Sprite> babies;

    public Image imgBaby;

    public GameObject btnSubmit;
    public GameObject btnLater;
    public GameObject btnClose;

    private int rating;

    public Text lblInfo;

    public string storeURL;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnEnable()
    {
        for (int i = 0; i < stars.Count; i++)
        {
            stars[i].gameObject.SetActive(false);
        }

        lblInfo.text = Localizer.GetString("ratingDescription", "Понравилась игра?");

        btnSubmit.SetActive(false);
        btnLater.SetActive(true);
        btnClose.SetActive(false);

        finished = false;
    }

    public void SetRating (int r)
    {
        if (finished) return;

        for (int i = 0; i < r && i < stars.Count; i++)
        {
            stars[i].gameObject.SetActive(true);
        }

        for (int i = r; i < stars.Count; i++)
        {
            stars[i].gameObject.SetActive(false);
        }

        btnSubmit.SetActive(true);

        rating = r;

        imgBaby.sprite = babies[r-1];
    }

    public void Submit ()
    {
        Analytics.Instance().SendEvent("Rate", 0, "rate_type", PlayerPrefs.GetInt("RateMethod"), "rate_val", rating);

        if (rating >= 4)
        {
#if UNITY_IOS
            if (!UnityEngine.iOS.Device.RequestStoreReview())
            {
                Close();
            }
#else
            {
                Application.OpenURL(storeURL);
                Close();
            }
#endif
        }

        lblInfo.text = Localizer.GetString("ratinglowText", "Спасибо за отзыв");
        
        btnSubmit.SetActive(false);
        btnLater.SetActive(false);
        btnClose.SetActive(true);
        finished = true;
    }

    private bool finished;
    public void Close ()
    {
        onRated?.Invoke(rating);
        gameObject.SetActive(false);
    }

    private System.Action<int> onRated;
    public void Show (System.Action<int> onRated)
    {
        this.onRated = onRated;
        MenuQueue.Instance().Show(gameObject, 4);
    }

    public void Later()
    {
        Analytics.Instance().SendEvent("SkipRate", 0, "rate_type", PlayerPrefs.GetInt("RateMethod"));

        gameObject.SetActive(false);

        onRated?.Invoke(-1);
    }
}
