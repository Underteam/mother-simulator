﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RateMethod4 : RateMethod
{
    private bool running;

    private void Awake()
    {
        //PlayerPrefs.SetInt("Rated", 0);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    public override void Run()
    {
        if (PlayerPrefs.GetInt("Rated", 0) == 1)
        {
            //Debug.LogError("Rated method 4");
            return;
        }

        //Debug.LogError ("Run method 1");
        SceneManager.sceneLoaded -= Check;
        SceneManager.sceneLoaded += Check;

        running = true;
    }

    private void Check (Scene scene, LoadSceneMode mode)
    {
        if (!running) return;

        StartCoroutine(CheckCoroutine());
    }

    private IEnumerator CheckCoroutine()
    {
        //Debug.LogError("Run coroutine");

        while (!GameState.Instance().started) yield return null;

        yield return null;

        if (GameState.Instance().currentTask == 7 && GameState.Instance().state == GameState.State.Menu)
        {
            RateUs.instance.ShowRatePanel(index);
            PlayerPrefs.SetInt("Rated", 1);
            running = false;
        }
    }
}
