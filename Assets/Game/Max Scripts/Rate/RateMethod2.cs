﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RateMethod2 : RateMethod
{
    private void Awake()
    {
        //PlayerPrefs.SetInt("Rated", 0);
    }

    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey("FirstLaunchDate"))
        {
            PlayerPrefs.SetString("FirstLaunchDate", System.DateTime.UtcNow.ToString());
        }
    }

    public override void Run()
    {
        if (PlayerPrefs.GetInt("Rated", 0) == 1)
        {
            Debug.LogError("Rated method 2");
            return;
        }

        if (!PlayerPrefs.HasKey("FirstLaunchDate"))
        {
            PlayerPrefs.SetString("FirstLaunchDate", System.DateTime.UtcNow.ToString());
        }

        System.DateTime t = System.DateTime.UtcNow;

        if (System.DateTime.TryParse(PlayerPrefs.GetString("FirstLaunchDate"), out t))
        {
            var span = System.DateTime.UtcNow - t;

            if (span.TotalHours > 12)
            {
                RateUs.instance.ShowRatePanel(index);
                PlayerPrefs.SetInt("Rated", 1);
            }
        }
        else
        {
            PlayerPrefs.SetString("FirstLaunchDate", System.DateTime.UtcNow.ToString());
        }
    }
}
