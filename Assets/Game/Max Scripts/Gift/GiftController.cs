﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GiftController : MonoSingleton<GiftController>
{
    public GameObject panel;

    public Mask mask;

    public GameObject btnContinue1;
    public GameObject btnContinue2;
    public GameObject lblNoThanks;
    public Image btnTake;
    public GameObject group1;
    public GameObject group2;
    public Sprite withAds;
    public Sprite woAds;

    public GameObject boxLight;
    public GameObject backLight;

    public Image progressBar;
    public Text lblProgress;

    private float progress;
    private float newProgress;

    public Transform cap;
    public Transform startPos;

    public Animator anim;

    public Image giftImage;
    public List<Gift> gifts;
    private int currGift;

    protected override void Init()
    {
        progress = PlayerPrefs.GetFloat("GiftProgress", 0);

        base.Init();
    }

    private void Start()
    {
        if (!inited) Init();

        //if (GameState.Instance().state == GameState.State.Menu) Show();
    }

    [Sirenix.OdinInspector.Button]
    public void ShowTest ()
    {
        Show(100);
    }

    public void Show ()
    {
        for (int i = 0; i < gifts.Count; i++)
        {
            if (PlayerPrefs.GetInt("IsGiftReady" + i, 0) == 1)
            {
                PlayerPrefs.SetInt("GiftReady" + gifts[i].item.packID + "_" + gifts[i].item.itemID, 1);
            }
        }

        bool show = PlayerPrefs.GetInt("ShowGift", 0) == 1;

        currGift = PlayerPrefs.GetInt("CurrentGift", 0);

        if (currGift >= gifts.Count) show = false;
        else if (currGift >= 0)
        {
            var gift = gifts[currGift];
            giftImage.sprite = gift.icon;
        }

        {
            PlayerPrefs.SetInt("ShowGift", 0);
            if (show)
            {
                if (GameState.Instance().currentTask < 3)
                    Show(50);
                else
                    Show(25);
            }
            else
            {
                GameController.Instance().Exit();
            }
        }
    }

    public void ShowButtons ()
    {
        btnContinue2.SetActive(true);
        lblNoThanks.SetActive(true);
    }

    private bool applyProgress;
    private bool animate;
    private float delay;

    // Update is called once per frame
    void Update()
    {
        if (!panel.activeSelf) return;

        if (delay > 0)
        {
            delay -= Time.deltaTime;
            return;
        }

        if (applyProgress)
        {
            progress += 20 * Time.unscaledDeltaTime;

            if (progress >= newProgress)
            {
                progress = newProgress;
                applyProgress = false;
                if(progress < 100)
                {
                    anim.enabled = false;
                    btnContinue1.SetActive(true);
                }
            }

            if (progress >= 100)
            {
                progress = 100;
                applyProgress = false;
                animate = true;
            }

            lblProgress.text = (int)progress + "%";
            progressBar.fillAmount = progress / 100;
        }
        else if (animate)
        {
            anim.Play("GiftShow");
            Debug.LogError("Animate");
            animate = false;
            Invoke("ShowButtons", 3.1f);
            Debug.LogError("Invoke");
        }
    }

    public void Show (float add)
    {
        MenuQueue.Instance().Show(panel, 0);

        Debug.LogError("show");

        btnContinue1.SetActive(false);
        btnContinue2.SetActive(false);
        lblNoThanks.SetActive(false);
        btnTake.gameObject.SetActive(false);
        btnTake.sprite = withAds;
        group1.SetActive(true);
        group2.SetActive(false);

        bool vip = PlayerPrefs.GetInt("VipAccessActive", 1) == 1;
        if (vip)
        {
            btnTake.sprite = woAds;
            group1.SetActive(false);
            group2.SetActive(true);
        }

        boxLight.SetActive(false);
        backLight.SetActive(false);

        cap.position = startPos.position;

        mask.enabled = true;

        lblProgress.text = (int)progress + "%";
        progressBar.fillAmount = progress / 100;

        progress = PlayerPrefs.GetFloat("GiftProgress", 0);
        newProgress = progress + add;
        PlayerPrefs.SetFloat("GiftProgress", newProgress);

        if (newProgress >= 100)
        {
            PlayerPrefs.SetInt("IsGiftReady" + currGift, 1);
            PlayerPrefs.SetInt("CurrentGift", currGift+1);
            PlayerPrefs.SetFloat("GiftProgress", 0);

            PlayerPrefs.SetInt("GiftReady" + gifts[currGift].item.packID + "_" + gifts[currGift].item.itemID, 1);

            //gifts[currGift].item.priceType = ShopItem.PriceType.Ads;
            //gifts[currGift].item.price = 1;
        }

        Debug.Log("Show " + progress + " " + newProgress);

        anim.Play("GiftStart");
        
        applyProgress = true;

        delay = 0.25f;
    }

    private void CheckProgress ()
    {
        if (progress >= 100)
        {

        }
    }

    public void Exit ()
    {
        Debug.Log("Exit");
        panel.SetActive(false);

        GameController.Instance().Exit();
    }

    bool waitingForAdEnd;
    public void Take ()
    {
        bool vip = VIPAccess.Instance().IsSubscribeActive();

        if (!vip)
        {
            waitingForAdEnd = true;
            Time.timeScale = 0f;
            AudioListener.volume = 0f;
            AdsController.Instance().ShowAd(AdsProvider.AdType.unskipablevideo, (b) =>
            {
                Time.timeScale = 1f;
                AudioListener.volume = 1f;

                if (!waitingForAdEnd) return;
                waitingForAdEnd = false;

                if (!b) return;


                Analytics.Instance().SendEvent("GiftForAd", 0);

                Analytics.Instance().SendEvent("af_ads_success");
                Analytics.Instance().SendEvent("af_ads_success_rv");

                GameController.Instance().TrackAdsWatched();

                StartCoroutine(DoTake());
            });
        }
        else
        {
            StartCoroutine(DoTake());
        }
    }

    private IEnumerator DoTake ()
    {
        yield return null;

        if (currGift >= 0 && currGift < gifts.Count)
        {
            var gift = gifts[currGift];
            Shop.Instance().OpenItem(gift.item);
            Shop.Instance().ApplyItem(gift.item);
        }

        anim.Play("GiftTake");

        //Exit();
    }
}
