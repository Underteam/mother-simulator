﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableTracker : MonoBehaviour {

    public System.Action onEnable;

    public System.Action onDisable;

    public System.Action onDestroy;

    // Use this for initialization
    void Start () {
		
	}

    private void OnEnable()
    {
        Debug.Log("Enabled " + this, this);
        onEnable?.Invoke();
    }

    private void OnDisable()
    {
        Debug.Log("Disabled " + this, this);
        onDisable?.Invoke();
    }

    private void OnDestroy()
    {
        Debug.Log("Destroy " + this, this);
        onDestroy?.Invoke();
    }
}
