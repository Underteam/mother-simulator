﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToyShuffler : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (GameState.Instance().state == GameState.State.Game) Shuffle();   
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [Sirenix.OdinInspector.Button]
    public void Shuffle ()
    {
        List<ToyPlace> list = new List<ToyPlace>(FindObjectsOfType<ToyPlace>());

        List<Vector3> positions = new List<Vector3>();
        for (int i = 0; i < list.Count; i++)
        {
            positions.Add(list[i].transform.position);
        }

        while (list.Count > 0)
        {
            int ind1 = Random.Range(0, list.Count);
            int ind2 = Random.Range(0, positions.Count);

            list[ind1].transform.position = positions[ind2] + 0.05f * Vector3.up;

            list.RemoveAt(ind1);
            positions.RemoveAt(ind2);
        }
    }
}
