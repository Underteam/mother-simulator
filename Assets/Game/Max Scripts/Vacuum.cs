﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vacuum : MonoBehaviour
{
    public AudioSource sound;

    public AudioClip start;
    public AudioClip loop;
    public AudioClip end;
    public AudioClip broken;

    public bool disabled { get { return brokenTimer > 0; } }

    public GameObject paricles;

    private bool isOn;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (brokenTimer > 0)
        {
            brokenTimer -= Time.deltaTime;
            if (brokenTimer <= 0)
            {
                paricles.SetActive(false);
                if (isOn) StartSound();
            }
        }
    }

    public void Taked ()
    {
        Groups.Instance().SetGroupState("Button Throw", false);

        isOn = true;

        StartSound();
    }

    private void StartSound ()
    {
        sound.clip = start;
        sound.loop = true;
        sound.Play();

        Invoke("Loop", Mathf.Max(0, start.length - 0.05f));
    }

    private void StopSound ()
    {
        CancelInvoke("Loop");

        sound.clip = end;
        sound.loop = false;
        sound.Play();
    }

    public void Released ()
    {
        isOn = false;

        StopSound();

        Groups.Instance().SetGroupState("Button Throw", true);
    }

    private void Loop ()
    {
        sound.clip = loop;
        sound.loop = true;
        sound.Play();
    }

    private float brokenTimer;
    public void Broke (float time)
    {
        brokenTimer = time;
        paricles.SetActive(true);

        StopSound();

        sound.clip = broken;
        sound.loop = true;
        sound.Play();
    }
}
