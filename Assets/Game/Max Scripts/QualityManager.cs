﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QualityManager : MonoBehaviour
{
    public bool apply;

    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 60;

        if (apply)
        {
            if (SystemInfo.systemMemorySize < 1000 || SystemInfo.graphicsMemorySize < 500)
                QualitySettings.SetQualityLevel(0, true);
            else
                QualitySettings.SetQualityLevel(1, true);

        }

        int level = QualitySettings.GetQualityLevel();

        //Debug.LogError("Current Level " + level);
    }

    public void Change ()
    {
        int level = QualitySettings.GetQualityLevel();
        int num = QualitySettings.names.Length;

        level = (level + 1) % num;

        QualitySettings.SetQualityLevel(level, true);

        Debug.LogError("Current Level " + level);
    }

    private LightmapData[] lightmaps;
    private bool useLMs = true;
    public void SwitchLightmaps()
    {
        if (lightmaps == null) lightmaps = LightmapSettings.lightmaps;

        useLMs = !useLMs;

        LightmapSettings.lightmaps = useLMs ? lightmaps : null;
    }

}
