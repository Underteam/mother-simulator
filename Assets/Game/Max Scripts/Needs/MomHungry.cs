﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MomHungry : Need
{
    private float hungryTimer = 5f;

    public System.Action onOverflow;

    void Start ()
    {
        Debug.LogError("Start " + level, this);
    }

    private GameObject red;

    public bool setMarkers { get; set; } = true;

    public List<Sprite> sprites;

    public override void OnUpdate()
    {
        if (hungryTimer > 0)
        {
            hungryTimer -= Time.deltaTime;
        }
        else if (level > 0)
        {
            level -= speed * Time.deltaTime;
            if (level <= 0 && onOverflow != null) onOverflow();
        }

        if (setMarkers) SetMarkers();

        if (level <= 0 && (red == null || !red.activeSelf))
        {
            if (red == null)
            {
                var bar = TaskManager.Instance().GetBar<MomHungry>();
                if (bar != null) red = bar.transform.Find("Red").gameObject;
                if (red != null) red.SetActive(true);
            }
        }
        else if (level > 0 && red != null && red.activeSelf)
        {
            red.SetActive(false);
        }
    }

    private List<GameObject> markers = new List<GameObject>();
    public void SetMarkers ()
    {
        if (level <= scaleFrom && markers.Count == 0)
        {
            float maxDist = 2.5f;

            List<Food> allFood = new List<Food>(FindObjectsOfType<Food>());

            List<List<Food>> groups = new List<List<Food>>();

            int cnt = 0;

            while (allFood.Count > 0 && cnt++ < 1000)
            {
                Transform o = allFood[0].transform;

                allFood.Sort((a, b) =>
                {
                    float d1 = Vector3.Distance(o.position, a.transform.position);
                    float d2 = Vector3.Distance(o.position, b.transform.position);
                    return d1.CompareTo(d2);
                });

                int i = 0;
                for (i = 0; i < allFood.Count; i++)
                {
                    float d = Vector3.Distance(o.position, allFood[i].transform.position);
                    if (d > maxDist) break;
                }

                if (i == 0) i = 1;

                List<Food> group = new List<Food>();
                group.AddRange(allFood.GetRange(0, i));
                allFood.RemoveRange(0, i);
                groups.Add(group);
            }

            for (int i = 0; i < groups.Count; i++)
            {
                for (int j = i + 1; j < groups.Count; j++)
                {

                    var group1 = groups[i];
                    var group2 = groups[j];

                    Vector3 p1 = Vector3.zero;
                    Vector3 p2 = Vector3.zero;

                    for (int k = 0; k < group1.Count; k++) p1 += group1[k].transform.position;
                    for (int k = 0; k < group2.Count; k++) p2 += group2[k].transform.position;

                    p1 /= group1.Count;
                    p2 /= group2.Count;

                    if (Vector3.Distance(p1, p2) <= maxDist)//Merge groups
                    {
                        group1.AddRange(group2);
                        groups.Remove(group2);
                        i--;
                        break;
                    }
                }
            }

            for (int i = 0; i < groups.Count; i++)
            {
                var group = groups[i];
                Vector3 p = Vector3.zero;
                for (int j = 0; j < group.Count; j++) p += group[j].transform.position;
                p /= group.Count;

                GameObject go = new GameObject("TargetPoint");
                go.transform.position = p;

                var m = NeedMarkers.Instance().HightlightObject(go, 0.75f, sprites);
                m.stickToEdge = false;

                markers.Add(go);
            }
        }
        else if (level > scaleFrom && markers.Count > 0)
        {
            for (int i = 0; i < markers.Count; i++)
            {
                NeedMarkers.Instance().UnHightlightObject(markers[i]);
                Destroy(markers[i]);
            }

            markers.Clear();
        }
    }

    public void Eat (float amount)
    {
        level += amount;
        if (level > 1) level = 1;

        hungryTimer = 5f;
    }
}
