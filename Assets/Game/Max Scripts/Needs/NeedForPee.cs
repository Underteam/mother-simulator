﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeedForPee : Need
{
    public bool onToilet;

    public System.Action onOverflow;

    public GameObject peePrefab;

    //private float prevLevel;

    // Use this for initialization
    void Start ()
    {
        //prevLevel = level;
    }

    public override void OnUpdate()
    {
        //if (level < prevLevel) prevLevel = level;

        if (onToilet)
        {
            if (level < 1) level += 10 * speed * Time.deltaTime;
            if (level > 1) level = 1;

            //if (level - prevLevel > 0.1f)
            //{
            //    FlyingSmiles.Instance().Run(1, null);
            //    prevLevel = level;
            //}
        }
        else
        {
            if (level > 0)
            {
                if (animate)
                {
                    float s = Mathf.Sign(targetLevel - level);
                    level += 8 * s * speed * Time.deltaTime;
                    if (s > 0 && level >= targetLevel)
                    {
                        level = targetLevel;
                        animate = false;
                    }
                    else if (s < 0 && level <= targetLevel)
                    {
                        level = targetLevel;
                        animate = false;
                    }
                }
                else
                    level -= speed * Time.deltaTime;

                if (level <= 0)
                {
                    animate = false;
                    level = 0.6f;
                    if (onOverflow != null) onOverflow();
                    var player = PlayerController.instance;
                    Instantiate(peePrefab, player.transform.position, player.transform.rotation).SetActive(true);
                    SFXPlayer.Instance().Play(SFXLibrary.Instance().motherFart, false, true);
                }
            }
        }

        if (setMarkers) SetMarkers();
    }

    public bool setMarkers { get; set; } = true;
    public List<Sprite> sprites;
    private GameObject marker;
    public void SetMarkers()
    {
        if (level <= scaleFrom && marker == null)
        {
            var toilet = FindObjectOfType<Toilet>();
            if (toilet != null)
            {
                marker = toilet.gameObject;
                NeedMarkers.Instance().HightlightObject(marker, 1.5f, sprites);
            }
        }
        else if(level > scaleFrom && marker != null)
        {
            NeedMarkers.Instance().UnHightlightObject(marker);
            marker = null;
        }
    }
}
