﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GMATools.Common;

public class MomStress : Need
{
    // Start is called before the first frame update
    void Start()
    {
        var listener = EventManager.Instance().AddListener((n, d) => 
        {
            if (this == null) return;

            level -= speed;
            if (level < 0.001f) level = 0;
        },
        "NewWish");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    private GameObject red;

    public override void OnUpdate()
    {
        if (level <= 0 && (red == null || !red.activeSelf))
        {
            if (red == null)
            {
                var bar = TaskManager.Instance().GetBar<MomStress>();
                if (bar != null) red = bar.transform.Find("Red").gameObject;
                if (red != null) red.SetActive(true);
            }
        }
        else if (level > 0 && red != null && red.activeSelf)
        {
            red.SetActive(false);
        }
    }
}
