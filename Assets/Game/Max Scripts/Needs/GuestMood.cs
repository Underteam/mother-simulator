﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuestMood : Need
{
    private Guest guest;
    private MainDish dish;
    private DiningChair chair;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public override void OnUpdate()
    {
        if (setMarkers) SetMarkers();
    }

    public bool setMarkers { get; set; } = true;
    public List<Sprite> sprites0;
    public List<Sprite> sprites1;
    public List<Sprite> sprites2;
    private GameObject marker;
    public void SetMarkers()
    {
        if (level <= scaleFrom && marker == null)
        {
            if (guest == null) guest = FindObjectOfType<Guest>();
            if (guest != null && guest.behaviourType == 1)
            {
                if (dish == null) dish = FindObjectOfType<MainDish>();
                if (dish != null)
                {
                    marker = dish.gameObject;
                    if(guest.foodType == GuestFood.FoodType.Cake)
                        NeedMarkers.Instance().HightlightObject(marker, 0.5f, sprites1);
                    else
                        NeedMarkers.Instance().HightlightObject(marker, 0.5f, sprites0);
                }
            }
            else if (guest != null && guest.behaviourType == 2)
            {
                if (chair == null) chair = FindObjectOfType<DiningChair>();
                if (chair != null)
                {
                    marker = chair.gameObject;
                    NeedMarkers.Instance().HightlightObject(marker, 1f, sprites2);
                }
            }
        }
        else if (level > scaleFrom && marker != null)
        {
            NeedMarkers.Instance().UnHightlightObject(marker);
            marker = null;
        }
    }
}
