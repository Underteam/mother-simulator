﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MomPep : Need
{
    public System.Action onOverflow;

    private float timer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public override void OnUpdate()
    {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        else if (level > 0)
        {
            level -= speed * Time.deltaTime;
            if (level <= 0 && onOverflow != null) onOverflow();
        }

        if (setMarkers) SetMarkers();
    }

    public bool setMarkers { get; set; } = true;
    public List<Sprite> sprites;
    private GameObject marker;
    public void SetMarkers()
    {
        if (level <= scaleFrom && marker == null)
        {
            var teapot = FindObjectOfType<Teapot>();
            if (teapot != null)
            {
                marker = teapot.gameObject;
                NeedMarkers.Instance().HightlightObject(marker, 1f, sprites);
            }
        }
        else if (level > scaleFrom && marker != null)
        {
            NeedMarkers.Instance().UnHightlightObject(marker);
            marker = null;
        }
    }

    public void AddEnergy(float e)
    {
        level += e;
        if (level > 1) level = 1;

        timer = 0;
    }
}
