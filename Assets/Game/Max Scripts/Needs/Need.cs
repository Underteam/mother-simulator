﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Need : MonoBehaviour {

    public string description;

    public float speed = 0.015f;

    public float level;

    public bool inverted;

    public bool critical;

    public Sprite icon;

    [HideInInspector]
    public bool instantSet;

    public ScaleAnimation anim;
    public float scaleFrom = 0.99f;

    public float remainingTime { get { return level / speed; } }

    // Use this for initialization
    void Start ()
    {
        
	}
	
    public virtual void Init ()
    {
        //level = 0.8f;
        instantSet = true;
    }

	// Update is called once per frame
	void Update ()
    {

    }

    public void AddSeconds (float sec)
    {
        level += sec * speed;

        if (level > 1) level = 1;
    }

    public float targetLevel;
    public bool animate { get; set; }
    public void AddSeconds (float sec, bool animate)
    {
        targetLevel = level + sec * speed;

        if (targetLevel > 1) targetLevel = 1;
        if (targetLevel < 0) targetLevel = 0;

        this.animate = true;
    }

    public virtual void OnUpdate ()
    {
        if (level > 0)
        {
            if (animate)
            {
                float s = Mathf.Sign(targetLevel - level);
                level += 8 * s * speed * Time.deltaTime;
                if (s > 0 && level >= targetLevel)
                {
                    level = targetLevel;
                    animate = false;
                }
                else if(s < 0 && level <= targetLevel)
                {
                    level = targetLevel;
                    animate = false;
                }
            }
            else
                level -= speed * Time.deltaTime;
        }
    }
}
