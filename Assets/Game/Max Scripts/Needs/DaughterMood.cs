﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DaughterMood : Need
{
    public bool daughterAngry { get; set; }

    public bool setMarker { get; set; } = true;

    private GameObject markerTarget;

    public List<Sprite> sprites;

    private Daughter daughter;

    // Start is called before the first frame update
    void Start()
    {
        daughter = FindObjectOfType<Daughter>();
    }

    public override void OnUpdate()
    {
        if (!daughterAngry && level < 1) level = 1f;
        else if (daughterAngry && level > 0 && !daughter.changing) level -= speed * Time.deltaTime;

        if (daughterAngry && level <= scaleFrom && markerTarget == null && setMarker)
        {
            markerTarget = daughter.gameObject;
            NeedMarkers.Instance().HightlightObject(markerTarget, 2f, sprites);
        }
        else if (!daughterAngry && markerTarget != null)
        {
            NeedMarkers.Instance().UnHightlightObject(markerTarget);
            markerTarget = null;
        }
    }
}
