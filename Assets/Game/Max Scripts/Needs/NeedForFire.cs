﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeedForFire : Need
{
    private Fire fire;

    public bool allRight;

    //private FPSCharacterController player;
    private FrozenFloor floor;

    public bool setMarker { get; set; } = true;

    public List<Sprite> sprites;

    private void Awake()
    {
        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            fire = FindObjectOfType<Fire>();

            //player = PlayerController.instance.GetComponent<FPSCharacterController>();

            //floor = FindObjectOfType<FrozenFloor>();
        },
        "TaskStarted");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    // Start is called before the first frame update
    void Start()
    {
        Debug.LogError("Start");
    }

    private GameObject firePlaceMarker;

    public override void OnUpdate()
    {
        if (fire != null && fire.scale <= 0.1f)
        {
            level -= speed * Time.deltaTime;
            if (level < 0) level = 0;

            if (level <= 0 && allRight)
            {
                allRight = false;

                EventManager.Instance().TriggerEvent("NewWish");
                if (setMarker)
                {
                    firePlaceMarker = fire.gameObject;
                    NeedMarkers.Instance().HightlightObject(firePlaceMarker, 0.5f, sprites);
                    Debug.LogError("Mark");
                }
            }
        }
        else
        {
            allRight = true;

            if (level < 1) level += 10 * speed * Time.deltaTime;
            if (level > 1) level = 1;
        }

        if (allRight && firePlaceMarker != null)
        {
            NeedMarkers.Instance().UnHightlightObject(firePlaceMarker);
            firePlaceMarker = null;
            Debug.LogError("Unmark");
        }
    }
}
