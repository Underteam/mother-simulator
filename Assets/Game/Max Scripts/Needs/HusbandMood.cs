﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HusbandMood : Need
{
    public bool husbandAngry { get; set; }

    public bool setMarker { get; set; } = true;

    private GameObject markerTarget;

    public List<Sprite> sprites;

    //private float prevLevel = 1f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public override void OnUpdate()
    {
        if (!husbandAngry && level < 1) level = 1f;
        else if (husbandAngry && level > 0) level -= speed * Time.deltaTime;

        if (husbandAngry && markerTarget == null && setMarker)
        {
            var teapot = FindObjectOfType<Teapot>().gameObject;
            if(teapot != null) markerTarget = teapot.gameObject;
            NeedMarkers.Instance().HightlightObject(markerTarget, 1f, sprites);
        }
        else if (!husbandAngry && markerTarget != null)
        {
            NeedMarkers.Instance().UnHightlightObject(markerTarget);
            markerTarget = null;
        }

        //if (level < prevLevel) prevLevel = level;

        //if (level - prevLevel > 0.1f)
        //{
        //    FlyingSmiles.Instance().Run(1, null);
        //    prevLevel = level;
        //}
    }
}
