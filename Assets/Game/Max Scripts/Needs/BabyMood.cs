﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabyMood : Need
{
    // Start is called before the first frame update
    public override void Init()
    {
        level = 1f;
        instantSet = true;

        var listener = EventManager.Instance().AddListener((n, d) => 
        {
            if (this == null) return;

            AddSeconds(20, true);
        },
        "WishSatisfied");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            AddSeconds(-20);
            Debug.LogError("-20");
            TaskManager.Instance().minus10sec.SetActive(true);
        },
        "WishUnSatisfied");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }
}
