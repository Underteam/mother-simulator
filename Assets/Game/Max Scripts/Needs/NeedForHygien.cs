﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeedForHygien : Need
{
    public System.Action onOverflow;

    private GameObject red;

	// Use this for initialization
	void Start () {
		
	}

    public override void OnUpdate()
    {
        if (level <= 0 && (red == null || !red.activeSelf))
        {
            if (red == null)
            {
                var bar = TaskManager.Instance().GetBar<NeedForHygien>();
                if (bar != null) red = bar.transform.Find("Red").gameObject;
                if (red != null) red.SetActive(true);
            }
        }
        else if (level > 0 && red != null && red.activeSelf)
        {
            red.SetActive(false);
        }
    }

    public void AddDirty (float amount)
    {
        level -= amount * speed;
        if (level < 0)
        {
            level = 0;
            if (onOverflow != null) onOverflow();
        }
    }
}
