﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Dog
public class DogMood : Need {

    private Dog dog;

    public bool active;

    public bool allright = true;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    public override void OnUpdate()
    {
        if (setMarkers) SetMarkers();

        if (level <= 0 || !active) return;

        if (!allright) level -= speed * Time.deltaTime;
        else level += 10 * speed * Time.deltaTime;

        if (level > 1) level = 1f;
        if (level < 0) level = 0;
    }

    public bool setMarkers { get; set; } = true;
    public List<Sprite> sprites1;
    public List<Sprite> sprites2;
    private GameObject marker;
    public void SetMarkers()
    {
        if (!allright && level <= scaleFrom && marker == null)
        {
            if (dog == null) dog = FindObjectOfType<Dog>();

            if (dog != null && dog.behaviourType == 1)
            {
                var dish = FindObjectOfType<DogDish>();
                if (dish != null)
                {
                    marker = dish.gameObject;
                    NeedMarkers.Instance().HightlightObject(marker, 1f, sprites1);
                }
            }
            else if (dog != null && dog.behaviourType == 2)
            {
                var carpet = FindObjectOfType<DogCarpet>();
                if (carpet != null)
                {
                    marker = carpet.gameObject;
                    NeedMarkers.Instance().HightlightObject(marker, 1f, sprites2);
                }
            }
        }
        else if ((allright || level > scaleFrom) && marker != null)
        {
            NeedMarkers.Instance().UnHightlightObject(marker);
            marker = null;
        }
    }
}
