﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotherIdle : MonoBehaviour
{
    private float timer;

    public AnimationClip leftHand;
    public AnimationClip rightHand;

    private bool playing;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    
        if (Input.GetMouseButton(0))
        {
            timer = 0;
            if (playing)
            {
                Finish();
            }
        }
        else if (!playing)
        {
            timer += Time.deltaTime;
            if (timer > 5f)
            {
                playing = true;
                PlayerController.instance.leftHand.Play(leftHand);
                PlayerController.instance.rightHand.Play(rightHand);
                Invoke("Finish", 3f);
            }
        }
    }

    private void Finish ()
    {
        playing = false;
        PlayerController.instance.leftHand.Stop();
        PlayerController.instance.rightHand.Stop();
        timer = 0;
    }
}
