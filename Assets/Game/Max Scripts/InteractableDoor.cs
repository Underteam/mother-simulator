﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableDoor : MonoBehaviour
{
    public Vector3 closedPos;
    public Vector3 openedPos;
    public Vector3 openedPos2;

    private Vector3 targetPos;
    private Vector3 currPos;

    public bool closed;

    public bool log;

    public bool playSound = true;

    public bool rememberState = true;

    public Transform targetPerson { get; set; }

    private void Awake()
    {
        int s = closed ? 0 : 1;
        if (rememberState) s = PlayerPrefs.GetInt("DoorState" + name, s);

        targetPerson = GameController.Instance().player.transform;

        closed = s == 0;

        if (closed)
            currPos = closedPos;
        else
        {
            Vector3 dir = targetPerson.position - transform.position;
            dir.y = 0;
            vector2 = dir;
            float dot = Vector3.Dot(dir, Quaternion.Euler(0, -transform.localRotation.eulerAngles.y + closedPos.y, 0) * transform.forward);
            if (dot > 0)
                currPos = openedPos2;
            else
                currPos = openedPos;
        }

        transform.localRotation = Quaternion.Euler(currPos);
        targetPos = currPos;

        var switchers = GetComponentsInChildren<Switcher>();
        for (int i = 0; i < switchers.Length; i++)
        {
            switchers[i].isOn = !closed;
            switchers[i].description = Localizer.GetString("door" + switchers[i].name, switchers[i].description);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        currPos = Vector3.Lerp(currPos, targetPos, 5 * Time.deltaTime);

        transform.localRotation = Quaternion.Euler(currPos);
    }

    public void Open ()
    {
        Vector3 dir = targetPerson.position - transform.position;
        dir.y = 0;

        vector = dir;

        float dot = Vector3.Dot(dir, Quaternion.Euler(0, -transform.localRotation.eulerAngles.y + closedPos.y, 0) * transform.forward);
        //Debug.LogError ("Open " + transform.localRotation.eulerAngles + " " + dir + " " + dot, this);
        
        if (dot > 0)
            targetPos = openedPos2;
        else
            targetPos = openedPos;

        if (rememberState)
        {
            if (log) Debug.LogError("Set state 1", this);
            PlayerPrefs.SetInt("DoorState" + name, 1);
        }

        if (closed && playSound)
        {
            var sound = SFXPlayer.Instance().Play(SFXLibrary.Instance().doorClip);
            sound.SetTime(0.3f);
        }

        closed = false;
    }

    public void Close ()
    {
        targetPos = closedPos;

        if (log) Debug.LogError("Close", this);

        if (rememberState) PlayerPrefs.SetInt("DoorState" + name, 0);

        if (!closed && playSound)
        {
            var sound = SFXPlayer.Instance().Play(SFXLibrary.Instance().doorClip);
            sound.SetTime(0.3f);
        }

        closed = true;
    }

    private Vector3 vector;
    private Vector3 vector2;
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, transform.position + vector.normalized);

        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position + 0.1f * Vector3.up, transform.position + vector2.normalized);
    }
}
