﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuestTasksController : MonoBehaviour
{
    public Guest guest;

    public int shuffleFromDay = 45;

    // Start is called before the first frame update
    void Start()
    {
        if (GameState.Instance().currentTask + 1 < shuffleFromDay) return;

        guest.onWishSatisfied += () =>
        {
            if (guest.behaviourType == 1)
                guest.SetBehaviourType(2);
            else if (guest.behaviourType == 2)
                guest.SetBehaviourType(1);
        };
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
