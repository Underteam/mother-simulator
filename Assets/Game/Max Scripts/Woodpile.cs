﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Woodpile : MonoBehaviour
{
    public Item woodPrefab;

    public InterfaceProvider interfaceProvider;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetFirewood ()
    {
        var newPrefab = Instantiate(woodPrefab);
        newPrefab.transform.SetParent(transform);
        newPrefab.transform.localScale = woodPrefab.transform.localScale;
        newPrefab.name = woodPrefab.name;

        interfaceProvider.providedInterface = newPrefab.GetComponent<InterfaceCollect>();

        woodPrefab.gameObject.SetActive(true);
        woodPrefab.onBeforeTake.RemoveAllListeners();
        woodPrefab.onBeforeTake.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.Off);

        woodPrefab = newPrefab;
    }
}
