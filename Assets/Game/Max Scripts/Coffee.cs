﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class Coffee : MonoBehaviour
{
    public float minPos = 0;
    public float maxPos = 0.26f;
    //public float maxAng = 16f;

    public AnimationCurve radiusOverLevel;
    public AnimationCurve maxAngCurve;

    [Range(0f, 1f)]
    public float level;

    public Vector3 scale;

    public Transform cup;

    public float ang;

    public ParticleSystem particles;

    private float timer;

    private GameObject target;

    public Color coffeeColor;
    public Color waterColor;

    private Renderer rend;

    public bool isItCoffee { get; private set; }

    private void Awake()
    {
        rend = GetComponent<Renderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        target = new GameObject();
        target.transform.SetParent(particles.transform.parent);
        particles.transform.SetParent(null);
    }

    public void SetState(bool isItCoffee)
    {
        this.isItCoffee = isItCoffee;

        if (isItCoffee)
        {
            rend.material.color = coffeeColor;
            var main = particles.main;
            var col = coffeeColor;
            col.a = 0.7f;
            main.startColor = col;
        }
        else
        {
            rend.material.color = waterColor;
            var main = particles.main;
            var col = waterColor;
            col.a = 0.7f;
            main.startColor = col;
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = new Vector3(0, minPos + level * (maxPos - minPos), 0);
        float s = radiusOverLevel.Evaluate(level);
        transform.localScale = s * scale;

        Vector3 up = transform.up;
        ang = Vector3.Angle(cup.up, Vector3.up);

        if (Mathf.Abs(ang) > 0.1f)
        {
            Vector3 forward = cup.up;
            forward.y = 0;
            forward.Normalize();
            Vector3 right = Vector3.Cross(Vector3.up, cup.up);

            Vector3 f = forward;
            Vector3 u = Vector3.up;

            float maxAng = maxAngCurve.Evaluate(level);
            if (Mathf.Abs(ang) > maxAng)
            {
                if (level > 0 && !particles.gameObject.activeSelf) particles.gameObject.SetActive(true);
                if (level > 0) timer = 0.5f;
                forward = cup.InverseTransformVector(forward);
                forward.y = 0;
                forward.Normalize();
                Vector3 localPos = new Vector3(0, maxPos, 0) + 0.15f * forward;
                target.transform.localPosition = localPos;
                //particles.transform.position = transform.TransformPoint(localPos);
                forward = cup.TransformVector(forward);
                particles.transform.rotation = Quaternion.LookRotation(Vector3.down, forward);
                Quaternion q = Quaternion.AngleAxis(Mathf.Abs(ang) - maxAng, right);
                f = q * f;
                u = q * u;
                if (level > 0) level -= 0.33f * Time.deltaTime;
            }
            else if (particles.gameObject.activeSelf && timer <= 0) particles.gameObject.SetActive(false);

            //transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(f, u), 15 * Time.deltaTime);
        }
        else
        {
            if (particles.gameObject.activeSelf && timer <= 0) particles.gameObject.SetActive(false);

            //transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(cup.forward, Vector3.up), 15 * Time.deltaTime);
        }

        if (timer > 0) timer -= Time.deltaTime;

        if (level <= 0 && particles.gameObject.activeSelf && timer <= 0) particles.gameObject.SetActive(false);

        particles.transform.position = target.transform.position;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        Gizmos.DrawLine(particles.transform.position, particles.transform.position + 3 * particles.transform.forward);
    }
}
