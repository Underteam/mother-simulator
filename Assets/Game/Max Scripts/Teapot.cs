﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Teapot : MonoBehaviour
{
    public bool isOn;

    public UnityEvent onSwitchOn;

    public UnityEvent onSwitchOff;

    public AudioClip sound;

    public Transform targetPos;

    public float camRot;
    private Quaternion targetRot;

    private FPSCharacterController player;

    public AnimationClip clip;
    private bool playing;

    // Start is called before the first frame update
    void Start()
    {
        targetRot = Quaternion.Euler(camRot, 0, 0);

        if (isOn)
        {
            if (onSwitchOn != null) onSwitchOn.Invoke();
        }
        else
        {
            if (onSwitchOff != null) onSwitchOff.Invoke();
        }
    }

    private float timer;

    void Update()
    {
        if (player != null)
        {
            float horizontal = InputManager.Instance().GetAxis("Horizontal");
            float vertical = InputManager.Instance().GetAxis("Vertical");
            if (Mathf.Abs(horizontal) + Mathf.Abs(vertical) > 0) timer += Time.deltaTime;
            else timer = 0;

            if (timer > 0.2f)
            {
                AllowMovement();
                timer = 0;
                return;
            }

            player.transform.position = Vector3.Lerp(player.transform.position, targetPos.position, 5 * Time.deltaTime);
            player.transform.rotation = Quaternion.Lerp(player.transform.rotation, targetPos.rotation, 5 * Time.deltaTime);
            player.pivot.transform.localRotation = Quaternion.Lerp(player.pivot.transform.localRotation, targetRot, 5 * Time.deltaTime);

            float d1 = Vector3.Distance(player.transform.position, targetPos.position);
            float d2 = Quaternion.Angle(player.transform.rotation, targetPos.rotation) / 10f;

            if (!playing && Mathf.Max(d1, d2) < 0.1f)
            {
                playing = true;
                PlayerController.instance.rightHand.Play(clip);
                PlayerController.instance.handReachedOut = true;
                Invoke("AllowMovement", 2f);
            }
        }
    }

    private void AllowMovement()
    {
        playing = false;
        player = null;
        if (PlayerController.instance == null) return;
        PlayerController.instance.handReachedOut = false;
        PlayerController.instance.leftHand.Stop();
        PlayerController.instance.rightHand.Stop();
        PlayerController.instance.GetComponent<FPSCharacterController>().disableMovement = false;
        PlayerController.instance.GetComponent<FPSCharacterController>().disableRotation = false;

        Switch();
    }

    public void SwitchOn ()
    {
        if (isOn) return;
        SFXPlayer.Instance().Play(SFXLibrary.Instance().teapot);
        isOn = true;
    }

    public void SwitchOff()
    {
        if (!isOn) return;
        SFXPlayer.Instance().Play(SFXLibrary.Instance().teapot);
        isOn = false;
    }

    public void Switch()
    {
        isOn = !isOn;

        SFXPlayer.Instance().Play(SFXLibrary.Instance().teapot);

        if (isOn)
        {
            if (onSwitchOn != null) onSwitchOn.Invoke();
        }
        else
        {
            if (onSwitchOff != null) onSwitchOff.Invoke();
        }
    }

    public void ApplyItem(Item item)
    {
        if (player != null) return;

        if (item == null)
        {
            Switch();
            return;
        }

        var cup = item.GetComponent<CoffeeCup>();

        if (cup == null || !isOn)
        {
            Switch();
            //return;
        }

        if (cup == null) return;

        if (PlayerController.instance == null) return;

        player = PlayerController.instance.GetComponent<FPSCharacterController>();
        player.disableMovement = true;
        player.disableRotation = true;
    }
}
