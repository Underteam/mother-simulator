﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test1 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [Button]
    void Test ()
    {
        var list = Resources.FindObjectsOfTypeAll(typeof(Rate));
        for (int i = 0; i < list.Length; i++) Debug.Log(list[i], list[i]);
    }
}
