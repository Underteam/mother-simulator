﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingMenu : MonoBehaviour
{
    public GameObject panel;

    private AsyncOperation loadingProgress;

    public Image loadingBar;
    private float loadingTimer;

    public Animator anim;

    private string sceneName;
    private int sceneInd;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (loadingProgress != null)
        {
            loadingBar.fillAmount = Mathf.Min(loadingTimer, loadingProgress.progress);
            if (loadingTimer < 1f) loadingTimer += Time.unscaledDeltaTime;
            else loadingProgress.allowSceneActivation = true;
        }
    }

    public void Load (int ind)
    {
        panel.SetActive(true);
        sceneName = null;
        sceneInd = ind;
        anim.Play("LoadingAppear");
        loadingBar.fillAmount = 0;
    }

    public void Load (string name)
    {
        panel.SetActive(true);
        sceneName = name;
        sceneInd = -1;
        anim.Play("LoadingAppear");
        loadingBar.fillAmount = 0;
    }

    public void StartLoading ()
    {
        if (!string.IsNullOrEmpty(sceneName))
        {
            loadingProgress = SceneManager.LoadSceneAsync(sceneName);
            loadingProgress.allowSceneActivation = false;
            loadingTimer = 0f;
        }
        else
        {
            loadingProgress = SceneManager.LoadSceneAsync(sceneInd);
            loadingProgress.allowSceneActivation = false;
            loadingTimer = 0f;
        }
    }
}
