﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedPenalty : MonoSingleton<SpeedPenalty>
{
    public FPSCharacterController controller;

    private List<Dirty> dirty = new List<Dirty>();

    private List<System.Func<bool>> penalties = new List<System.Func<bool>>();

    private float timer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < dirty.Count; i++)
        {
            if(dirty[i] == null)
            {
                dirty.RemoveAt(i);
                i--;
            }
        }

        bool penalty = false;
        for (int i = 0; i < penalties.Count; i++)
        {
            if (penalties[i] == null)
            {
                penalties.RemoveAt(i);
                i--;
            }

            penalty = penalty || penalties[i]();
        }

        if (dirty.Count > 0)
            timer = 0.1f;
        else if (timer > 0)
            timer -= Time.deltaTime;

        if (timer > 0 || penalty)
        {
            controller.speedMult = 0.2f;
        }
        else
        {
            controller.speedMult = 1f;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Dirty dirty = other.GetComponent<Dirty>();

        if (dirty != null && !this.dirty.Contains(dirty)) this.dirty.Add(dirty);
    }

    private void OnTriggerExit (Collider other)
    {
        Dirty dirty = other.GetComponent<Dirty>();

        if (dirty != null && this.dirty.Contains(dirty)) this.dirty.Remove(dirty);
    }

    public void AddPenaltyProvider (System.Func<bool> provider)
    {
        if (!penalties.Contains(provider)) penalties.Add(provider);
    }
}
