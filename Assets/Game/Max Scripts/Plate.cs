﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plate : MonoBehaviour
{
    public Rigidbody rb;

    public GameObject splinters;

    public GameObject dust;

    public List<Renderer> renderers;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (broke && splinters != null)
        {
            for (int i = 0; i < renderers.Count; i++) renderers[i].enabled = false;

            float dot = Vector3.Dot(point - transform.position, Vector3.down);
            float dist = Vector3.Distance(point, transform.position);

            Debug.Log("Broke " + dot + " " + dist);

            if (dot > 0 && dist > 0.5f)
            {
                dust.transform.SetParent(null);
                dust.transform.position = transform.position + 0.01f * Vector3.down;
                dust.transform.rotation = Quaternion.Euler(90, 0, 0);
                dust.SetActive(true);

                float time = Mathf.Sqrt(2 * dist / 9.81f);
                Invoke("ShowSplinters", time);

                
                //MomStress stress = FindObjectOfType<MomStress>();
                //if (stress != null) stress.level += 0.5f;
                //Debug.LogError("Plate broken " + (stress != null));
            }
            else
            {
                ShowSplinters();
                gameObject.SetActive(false);
            }

            broke = false;
            GetComponent<Rigidbody>().isKinematic = true;
        }
    }

    private void ShowSplinters ()
    {
        splinters.transform.SetParent(null);
        splinters.SetActive(true);
        splinters.transform.position = point;
        splinters.transform.rotation = Quaternion.Euler(0, splinters.transform.eulerAngles.y, 0);
    }

    private Vector3 point;
    private bool broke;

    private void OnCollisionEnter(Collision collision)
    {
        //bug.Log("Collision " + collision.relativeVelocity, collision.transform);

        if (collision.relativeVelocity.magnitude > 6.5f)
        {
            var hits = Physics.RaycastAll(transform.position, Vector3.down);
            //bug.Log("Found " + hits.Length + " hits");
            for (int i = 0; i < hits.Length; i++)
            {
                //bug.Log("Hit " + hits[i].transform.tag, hits[i].transform);
                if (hits[i].transform.tag.Equals("Floor"))
                {
                    Debug.LogError("Broke");
                    broke = true;
                    point = hits[i].point;
                    break;
                }
            }
        }
    }
}
