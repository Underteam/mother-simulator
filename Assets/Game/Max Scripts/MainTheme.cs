﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainTheme : MonoBehaviour
{
    public static MainTheme instance;

    public AudioSource player;

    private void Awake()
    {
        if (instance != null) Destroy(gameObject);
        else instance = this;

        DontDestroyOnLoad(this);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Play (AudioClip clip)
    {
        if (player.clip == clip) return;

        player.clip = clip;
        player.Play();
    }
}
