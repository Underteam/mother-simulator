﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Toilet : MonoBehaviour
{
    private NeedForPee need;

    public Collider col;

    public Switcher cap;

    public SitPoint potty;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (need != null)
        {
            if (need.level >= 1f)
            {
                var ab = PlayerController.instance.GetComponent<AbilitySit>();
                if (ab != null) ab.Stand();
            }
        }
        else if (timer > 0)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                var ab = PlayerController.instance.GetComponent<AbilitySit>();
                if (ab != null) ab.Stand();
            }
        }

        if (cap.isOn && potty.buisy) potty.buisy = false;
        else if (!cap.isOn && !potty.buisy) potty.buisy = true;
    }

    private float timer = 0f;
    private MyAudioSource peeSound;
    public void OnSit ()
    {
        need = FindObjectOfType<NeedForPee>();
        if (need != null) need.onToilet = true;
        col.enabled = false;
        timer = 3.5f;

        Invoke("PlaySound", 1f);
    }

    public void OnStand ()
    {
        //Debug.LogError("Stand");

        if (need != null) need.onToilet = false;
        need = null;
        timer = 0;
        col.enabled = true;

        CancelInvoke("PlaySound");

        if (peeSound != null)
        {
            peeSound.Stop();
            peeSound = null;
        }
    }

    private void PlaySound ()
    {
        peeSound = SFXPlayer.Instance().Play(SFXLibrary.Instance().pee);
    }
}
