﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Test : MonoBehaviour
{
    public List<string> buttons;

    public List<float> delays;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Execute ()
    {
        if (running) return;

        StartCoroutine(Job());
    }

    bool running;
    private IEnumerator Job ()
    {
        running = true;

        for (int i = 0; i < buttons.Count; i++)
        {
            InputManager.Instance().SetButton(buttons[i], true);

            yield return new WaitForSeconds(0.1f);

            InputManager.Instance().SetButton(buttons[i], false);

            yield return new WaitForSeconds(delays[i]);
        }

        running = false;
    }
}
