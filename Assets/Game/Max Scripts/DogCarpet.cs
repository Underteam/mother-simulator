﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogCarpet : MonoBehaviour
{
    public Furs furs;

    // Start is called before the first frame update
    void Awake ()
    {
        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;
            var need = TaskManager.Instance().GetNeed<DogMood>();
            if (need == null && furs != null)
            {
                Destroy(furs.gameObject);
            }
        },
        "TaskStarted");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
