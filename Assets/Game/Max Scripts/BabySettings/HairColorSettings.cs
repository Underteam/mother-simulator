﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HairColorSettings : MonoBehaviour, IBabySettingsApplier
{
    public List<Material> styles;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Apply()
    {
        for (int i = 0; i < GameController.Instance().babySettings.Count; i++)
        {
            for (int j = 0; j < styles.Count; j++)
            {
                GameController.Instance().babySettings[i].SetHair(styles[j], j);
            }
        }
    }

    public void Apply(BabySettings settings)
    {
        for (int j = 0; j < styles.Count; j++)
        {
            settings.SetHair(styles[j], j);
        }
    }
}
