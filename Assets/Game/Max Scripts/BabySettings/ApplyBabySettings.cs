﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyBabySettings : MonoBehaviour
{
    public BabySettings settings;

    // Start is called before the first frame update
    void Start()
    {
        var allPacks = Shop.Instance().allPacks;

        for (int i = 0; i < allPacks.Count; i++)
        {
            if (allPacks[i].target != Shop.ItemsPack.Target.Baby) continue;
            int packID = i;
            int itemID = PlayerPrefs.GetInt("BabySettings" + packID, 0);
            if (PlayerPrefs.GetInt("IsSlotActive" + packID + "_" + itemID, 0) != 1) itemID = 0;
            IBabySettingsApplier applier = null;
            if(itemID >= 0 && itemID < allPacks[i].items.Count) applier = allPacks[i].items[itemID].GetComponent<IBabySettingsApplier>();
            if (applier != null) applier.Apply(settings);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
