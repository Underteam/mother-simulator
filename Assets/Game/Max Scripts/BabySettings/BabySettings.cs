﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBabySettingsApplier
{
    void Apply(BabySettings settings);
}

public class BabySettings : MonoBehaviour
{
    public Renderer body;
    public Renderer face;
    public Renderer eyesLeft;
    public Renderer eyesRight;
    public Renderer diaper;
    public List<Renderer> flatDiapers;
    public List<Renderer> roundDiapers;
    public List<Renderer> hair;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetSkin(Material mat, Color color)
    {
        mat.color = color;

        body.sharedMaterial = mat;
        face.sharedMaterial = mat;
    }

    public void SetEyes(Material mat, Color color)
    {
        mat.color = color;
        eyesLeft.sharedMaterial = mat;
        eyesRight.sharedMaterial = mat;
    }

    public void SetHair(Material mat, int style)
    {
        if (style < 0 || style >= hair.Count) return;

        hair[style].sharedMaterial = mat;
    }

    public void SetHairStyle(int s)
    {
        for (int i = 0; i < hair.Count; i++)
        {
            hair[i].gameObject.SetActive(false);
        }

        hair[s].gameObject.SetActive(true);
    }

    private SkinnedMeshRenderer currentHairFull;
    private SkinnedMeshRenderer currentHairCrop;
    private Dictionary<SkinnedMeshRenderer, SkinnedMeshRenderer> addedMeshes = new Dictionary<SkinnedMeshRenderer, SkinnedMeshRenderer>();
    public void SetHair (SkinnedMeshRenderer full, SkinnedMeshRenderer crop)
    {
        if (currentHairFull != null) currentHairFull.gameObject.SetActive(false);
        if (currentHairCrop != null) currentHairCrop.gameObject.SetActive(false);

        if (full != null)
        {
            if (!addedMeshes.ContainsKey(full))
            {
                var inst = Instantiate(full);
                inst.transform.SetParent(body.transform.parent);
                inst.transform.localScale = body.transform.localScale;
                inst.transform.localPosition = body.transform.localPosition;

                Remap(inst, body as SkinnedMeshRenderer);

                addedMeshes.Add(full, inst);

                inst.gameObject.layer = body.gameObject.layer;
            }

            full = addedMeshes[full];

            currentHairFull = full;
        }

        if (crop != null)
        {
            if (!addedMeshes.ContainsKey(crop))
            {
                var inst = Instantiate(crop);
                inst.transform.SetParent(body.transform.parent);
                inst.transform.localScale = body.transform.localScale;
                inst.transform.localPosition = body.transform.localPosition;

                Remap(inst, body as SkinnedMeshRenderer);

                addedMeshes.Add(crop, inst);

                inst.gameObject.layer = body.gameObject.layer;
            }

            crop = addedMeshes[crop];

            currentHairCrop = crop;
        }

        if (currentHairFull != null) currentHairFull.gameObject.SetActive(hairType == HairType.Full);
        if (currentHairCrop != null) currentHairCrop.gameObject.SetActive(hairType == HairType.Crop);
    }

    public enum HairType
    {
        Full,
        Crop,
        None,
    }

    private HairType hairType;
    private SkinnedMeshRenderer currenClothes;
    public void SetClothes(SkinnedMeshRenderer mesh, Material diaperMat, HairType hairType, Material flatDiaper, Material roundDiaper)
    {
        this.hairType = hairType;

        diaper.sharedMaterial = diaperMat;

        for (int i = 0; flatDiaper != null && i < flatDiapers.Count; i++) flatDiapers[i].sharedMaterial = flatDiaper;
        for (int i = 0; roundDiaper!= null && i < roundDiapers.Count; i++) roundDiapers[i].sharedMaterial = roundDiaper;

        if (currenClothes != null) currenClothes.gameObject.SetActive(false);

        if (currentHairFull != null) currentHairFull.gameObject.SetActive(hairType == HairType.Full);
        if (currentHairCrop != null) currentHairCrop.gameObject.SetActive(hairType == HairType.Crop);

        if (mesh == null) return;

        if (!addedMeshes.ContainsKey(mesh))
        {
            var inst = Instantiate(mesh);
            inst.transform.SetParent(body.transform.parent);
            inst.transform.localScale = body.transform.localScale;
            inst.transform.localPosition = body.transform.localPosition;

            Remap(inst, body as SkinnedMeshRenderer);

            addedMeshes.Add(mesh, inst);

            inst.gameObject.layer = body.gameObject.layer;
        }

        mesh = addedMeshes[mesh];

        currenClothes = mesh;

        mesh.gameObject.SetActive(true);
    }

    private SkinnedMeshRenderer currenGlasses;
    public void SetGlasses(SkinnedMeshRenderer mesh)
    {
        if (currenGlasses != null) currenGlasses.gameObject.SetActive(false);

        if (mesh == null) return;

        if (!addedMeshes.ContainsKey(mesh))
        {
            var inst = Instantiate(mesh);
            inst.transform.SetParent(body.transform.parent);
            inst.transform.localScale = body.transform.localScale;
            inst.transform.localPosition = body.transform.localPosition;

            Remap(inst, body as SkinnedMeshRenderer);

            addedMeshes.Add(mesh, inst);

            inst.gameObject.layer = body.gameObject.layer;
        }

        mesh = addedMeshes[mesh];

        currenGlasses = mesh;

        mesh.gameObject.SetActive(true);
    }

    private void Remap(SkinnedMeshRenderer copyTo, SkinnedMeshRenderer copyFrom)
    {
        copyTo.rootBone = copyFrom.rootBone;

        List<Transform> list = new List<Transform>(copyFrom.bones);
        copyTo.bones = list.ToArray();
    }

    public void ClothesOff ()
    {
        if (currenGlasses != null) currenGlasses.enabled = false;
        if (currenClothes != null) currenClothes.enabled = false;

        if (currentHairFull != null) currentHairFull.gameObject.SetActive(true);
        if (currentHairCrop != null) currentHairCrop.gameObject.SetActive(false);
    }

    public void ClothesOn ()
    {
        if (currenGlasses != null) currenGlasses.enabled = true;
        if (currenClothes != null) currenClothes.enabled = true;

        if (currentHairFull != null) currentHairFull.gameObject.SetActive(hairType == HairType.Full);
        if (currentHairCrop != null) currentHairCrop.gameObject.SetActive(hairType == HairType.Crop);
    }
}
