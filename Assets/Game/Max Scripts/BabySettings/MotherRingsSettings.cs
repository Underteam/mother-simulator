﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotherRingsSettings : MonoBehaviour
{
    public GameObject ring1;
    public GameObject ring2;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Apply()
    {
        for (int i = 0; i < GameController.Instance().motherSettings.Count; i++)
        {
            GameController.Instance().motherSettings[i].SetRings(ring1, ring2);
        }
    }
}
