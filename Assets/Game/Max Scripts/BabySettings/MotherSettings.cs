﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotherSettings : MonoBehaviour
{
    public Renderer body;

    public Transform leftFinger;
    public Transform rightFinger;

    public Transform leftArm;
    public Transform rightArm;

    private Dictionary<GameObject, GameObject> addedMeshes = new Dictionary<GameObject, GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void SetSkin (Material mat, Color color)
    {
        mat.color = color;

        body.sharedMaterial = mat;
    }

    private GameObject currenRing1;
    private GameObject currenRing2;
    public void SetRings(GameObject ring1, GameObject ring2)
    {
        //Debug.LogError("Set rings", this);

        if (currenRing1 != null) currenRing1.gameObject.SetActive(false);
        if (currenRing2 != null) currenRing2.gameObject.SetActive(false);

        if (ring1 != null)
        {
            if (!addedMeshes.ContainsKey(ring1))
            {
                var inst = Instantiate(ring1);
                inst.transform.SetParent(leftFinger);
                inst.transform.localScale = ring1.transform.localScale;
                inst.transform.localPosition = ring1.transform.localPosition;
                inst.transform.localRotation = ring1.transform.localRotation;

                addedMeshes.Add(ring1, inst);

                inst.gameObject.layer = body.gameObject.layer;
            }

            ring1 = addedMeshes[ring1];

            currenRing1 = ring1;

            ring1.SetActive(true);
        }

        if (ring2 != null)
        {
            if (!addedMeshes.ContainsKey(ring2))
            {
                var inst = Instantiate(ring2);
                inst.transform.SetParent(rightFinger);
                inst.transform.localScale = ring2.transform.localScale;
                inst.transform.localPosition = ring2.transform.localPosition;
                inst.transform.localRotation = ring2.transform.localRotation;

                addedMeshes.Add(ring2, inst);

                inst.gameObject.layer = body.gameObject.layer;
            }

            ring2 = addedMeshes[ring2];

            currenRing2 = ring2;

            ring2.SetActive(true);
        }
    }

    private GameObject currenBracelet;
    public void SetBracelet(GameObject bracelet)
    {
        if (currenBracelet != null) currenBracelet.gameObject.SetActive(false);

        if (bracelet != null)
        {
            if (!addedMeshes.ContainsKey(bracelet))
            {
                var inst = Instantiate(bracelet);
                inst.transform.SetParent(rightArm);
                inst.transform.localScale = bracelet.transform.localScale;
                inst.transform.localPosition = bracelet.transform.localPosition;
                inst.transform.localRotation = bracelet.transform.localRotation;

                addedMeshes.Add(bracelet, inst);

                inst.gameObject.layer = body.gameObject.layer;
            }

            bracelet = addedMeshes[bracelet];

            currenBracelet = bracelet;

            bracelet.SetActive(true);
        }
    }

    private GameObject currenWatch;
    public void SetWatch(GameObject watch)
    {
        if (currenWatch != null) currenWatch.gameObject.SetActive(false);

        if (watch != null)
        {
            if (!addedMeshes.ContainsKey(watch))
            {
                var inst = Instantiate(watch);
                inst.transform.SetParent(leftArm);
                inst.transform.localScale = watch.transform.localScale;
                inst.transform.localPosition = watch.transform.localPosition;
                inst.transform.localRotation = watch.transform.localRotation;

                addedMeshes.Add(watch, inst);

                inst.gameObject.layer = body.gameObject.layer;
            }

            watch = addedMeshes[watch];

            currenWatch = watch;

            watch.SetActive(true);
        }
    }
}
