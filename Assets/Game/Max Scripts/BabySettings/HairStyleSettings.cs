﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HairStyleSettings : MonoBehaviour, IBabySettingsApplier
{
    public int style;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Apply ()
    {
        for(int i = 0; i < GameController.Instance().babySettings.Count; i++)
        {
            GameController.Instance().babySettings[i].SetHairStyle(style);
        }
    }

    public void Apply(BabySettings settings)
    {
        settings.SetHairStyle(style);
    }
}
