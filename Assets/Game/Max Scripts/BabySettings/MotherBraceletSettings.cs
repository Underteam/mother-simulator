﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotherBraceletSettings : MonoBehaviour
{
    public GameObject bracelet;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Apply()
    {
        for (int i = 0; i < GameController.Instance().motherSettings.Count; i++)
        {
            GameController.Instance().motherSettings[i].SetBracelet(bracelet);
        }
    }
}
