﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotherWatchSettings : MonoBehaviour
{
    public GameObject watch;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void Apply()
    {
        for (int i = 0; i < GameController.Instance().motherSettings.Count; i++)
        {
            GameController.Instance().motherSettings[i].SetWatch(watch);
        }
    }
}
