﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinSettings : MonoBehaviour, IBabySettingsApplier
{
    public Material mat;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Apply ()
    {
        for (int i = 0; i < GameController.Instance().babySettings.Count; i++)
        {
            GameController.Instance().babySettings[i].SetSkin(mat, Color.white);
        }
    }

    public void Apply(BabySettings settings)
    {
        settings.SetSkin(mat, Color.white);
    }
}
