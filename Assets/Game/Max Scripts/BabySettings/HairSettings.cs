﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HairSettings : MonoBehaviour, IBabySettingsApplier
{
    public SkinnedMeshRenderer full;
    public SkinnedMeshRenderer crop;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Apply()
    {
        for (int i = 0; i < GameController.Instance().babySettings.Count; i++)
        {
            GameController.Instance().babySettings[i].SetHair(full, crop);
        }
    }

    public void Apply(BabySettings settings)
    {
        settings.SetHair(full, crop);
    }
}
