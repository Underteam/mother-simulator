﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlassesSettings : MonoBehaviour, IBabySettingsApplier
{
    public SkinnedMeshRenderer mesh;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Apply()
    {
        for (int i = 0; i < GameController.Instance().babySettings.Count; i++)
        {
            GameController.Instance().babySettings[i].SetGlasses(mesh);
        }
    }

    public void Apply(BabySettings settings)
    {
        settings.SetGlasses(mesh);
    }
}
