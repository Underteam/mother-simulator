﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClothesSetings : MonoBehaviour, IBabySettingsApplier
{
    public SkinnedMeshRenderer mesh;
    public Material pampersMaterial;
    public Material flatDiaperMaterial;
    public Material roundDiaperMaterial;

    public BabySettings.HairType hairType = BabySettings.HairType.Full;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Apply()
    {
        //if (meshInstance == null)
        //{
        //    meshInstance = Instantiate(mesh);
        //    meshInstance.gameObject.SetActive(false);
        //}

        for (int i = 0; i < GameController.Instance().babySettings.Count; i++)
        {
            GameController.Instance().babySettings[i].SetClothes(mesh, pampersMaterial, hairType, flatDiaperMaterial, roundDiaperMaterial);
        }
    }

    public void Apply(BabySettings settings)
    {
        settings.SetClothes(mesh, pampersMaterial, hairType, flatDiaperMaterial, roundDiaperMaterial);
    }
}
