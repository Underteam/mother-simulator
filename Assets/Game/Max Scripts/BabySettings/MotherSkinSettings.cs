﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotherSkinSettings : MonoBehaviour
{
    public Material mat;

    // Start is called before the first frame update
    void Start()
    {

    }

    public void Apply()
    {
        for (int i = 0; i < GameController.Instance().motherSettings.Count; i++)
        {
            GameController.Instance().motherSettings[i].SetSkin(mat, Color.white);
        }
    }
}
