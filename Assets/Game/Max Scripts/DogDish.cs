﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogDish : MonoBehaviour, IAppropriatanceProvider
{
    public Transform feedingPoint;

    public Transform foodPoint;

    public GameObject food;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool log;

    public void OnApply(Item item)
    {
        if (item == null) return;

        if (this.food != null) return;

        DogFood food = item.GetComponent<DogFood>();

        if (food == null) return;

        this.food = Instantiate(food.foodPrefab);
        SFXPlayer.Instance().Play(SFXLibrary.Instance().dogFood);

        this.food.SetActive(true);
        this.food.transform.position = foodPoint.position;
        this.food.transform.rotation = foodPoint.rotation;
        this.food.transform.localScale = Vector3.one;
    }

    public bool IsAppropriate(Item item)
    {
        if (item == null) return false;
        return item.GetComponent<DogFood>() != null;
    }
}
