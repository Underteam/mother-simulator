﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnTriggerEnterDelegate : MonoBehaviour
{
    [System.Serializable]
    public class OnTriggerEvent : UnityEvent<Collider> { }

    public OnTriggerEvent onEnter;
    public OnTriggerEvent onExit;
    public OnTriggerEvent onStay;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        onEnter?.Invoke(other);
    }

    private void OnTriggerExit(Collider other)
    {
        onExit?.Invoke(other);
    }

    private void OnTriggerStay(Collider other)
    {
        onStay?.Invoke(other);
    }
}
