﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class CheckForAd : MonoBehaviour
{
    public FlyingText lblInfo;

    public Button button;

    private Button.ButtonClickedEvent events;

    private bool skipInvoke;

    public UnityEvent onAdReady;

    public UnityEvent onAdNotReady;

    // Start is called before the first frame update
    void Start()
    {
        if (button != null)
        {
            events = button.onClick;

            for (int i = 0; i < events.GetPersistentEventCount(); i++)
            {
                events.SetPersistentListenerState(i, UnityEngine.Events.UnityEventCallState.Off);
            }

            events.AddListener(new UnityEngine.Events.UnityAction(() =>
            {
                Debug.Log("Invoke " + !skipInvoke, this);

                if (!skipInvoke)
                {
                    skipInvoke = true;

                    bool ready = AdsController.Instance().IsAdReady(AdsProvider.AdType.unskipablevideo);

                    if (ready)
                    {
                        for (int i = 0; i < events.GetPersistentEventCount(); i++)
                        {
                            events.SetPersistentListenerState(i, UnityEngine.Events.UnityEventCallState.RuntimeOnly);
                        }

                        events.Invoke();

                        for (int i = 0; i < events.GetPersistentEventCount(); i++)
                        {
                            events.SetPersistentListenerState(i, UnityEngine.Events.UnityEventCallState.Off);
                        }
                    }
                    else
                    {
                        var canvas = GetComponentInParent<Canvas>();
                        if (canvas != null)
                        {
                            var info = Instantiate(lblInfo);

                            if (Application.internetReachability == NetworkReachability.NotReachable)
                            {
                                info.text.text = Localizer.GetString("internetLost", "No connection");
                            }
                            else
                            {
                                info.text.text = Localizer.GetString("adLoad", "Ad is loading");
                            }

                            info.onDisappear = () => Destroy(info.gameObject);

                            info.transform.SetParent(canvas.transform);
                            info.transform.SetAsLastSibling();
                            var rt = info.transform as RectTransform;
                            info.transform.localScale = Vector3.one;
                            rt.sizeDelta = (transform as RectTransform).rect.size;
                            rt.position = Input.mousePosition + Screen.height * canvas.transform.localScale.y * 0.05f * Vector3.up;// transform.position;
                            info.gameObject.SetActive(true);
                        }
                    }
                }
            //else skipInvoke = false;
            }));
        }
    }

    private void OnEnable()
    {
        StopCoroutine("CheckStatus");
        StartCoroutine(CheckStatus());
    }

    // Update is called once per frame
    void Update()
    {
        skipInvoke = false;
    }

    public IEnumerator CheckStatus ()
    {
        bool haveAd = true;

        onAdReady?.Invoke();

        while (true)
        {
            yield return new WaitForSeconds(0.5f);

            bool ready = AdsController.Instance().IsAdReady(AdsProvider.AdType.unskipablevideo);

            if (haveAd && !ready)
            {
                haveAd = false;
                onAdNotReady?.Invoke();
            }
            else if(!haveAd && ready)
            {
                haveAd = true;
                onAdReady?.Invoke();
            }

            yield return null;
        }
    }
}
