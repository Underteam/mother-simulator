﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blinker : MonoBehaviour
{
    public RectTransform upperLid;
    public RectTransform lowerLid;

    public Vector2 openPos;
    public Vector2 closePos;

    private Vector2 targetPos;
    private Vector2 currPos;

    public bool blinking;

    private bool open;

    private float timer;

    private MomPep pep;

    private bool fine;

    public System.Action onBlinked;

    private void Awake()
    {
        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            upperLid.anchoredPosition = openPos;
            lowerLid.anchoredPosition = -openPos;
            timer = t1;

            pep = FindObjectOfType<MomPep>();
        },
        "TaskStarted");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    // Start is called before the first frame update
    void Start()
    {
        timer = t1;
        blinking = false;
        upperLid.anchoredPosition = openPos;
        lowerLid.anchoredPosition = -openPos;

        targetPos = openPos;
        currPos = openPos;
    }

    public float t1 = 1f;
    public float t2 = 1f;

    // Update is called once per frame
    void Update()
    {
        if (pep != null)
        {
            if (!blinking && pep.level <= 0)
            {
                blinking = true;
                fine = true;
            }
        }

        if (open) targetPos = openPos;
        else targetPos = closePos;

        if (blinking)
        {
            timer -= Time.unscaledDeltaTime;
            if (timer <= 0)
            {
                if (open) timer = t1;
                else timer = t2;

                open = !open;

                if (open) blinking = false;

                Debug.Log(open);

                if (open && onBlinked != null) onBlinked();
            }
        }

        if (!blinking) targetPos = openPos;

        currPos = Vector2.Lerp(currPos, targetPos, 5 * Time.unscaledDeltaTime);
        upperLid.anchoredPosition = currPos;
        lowerLid.anchoredPosition = -currPos;

        if (fine && !open && (currPos - targetPos).magnitude < 5f)
        {
            Debug.Log("Fine " + (currPos - targetPos).magnitude);
            fine = false;

            if (pep != null) pep.level = 0.6f;

            var dog = FindObjectOfType<Dog>();
            var husband = FindObjectOfType<Husband>();
            var husband1 = FindObjectOfType<Husband1>();
            var guest = FindObjectOfType<Guest>();

            if (dog != null) dog.Fine(20);
            if (husband != null) husband.Fine(20);
            if (husband1 != null) husband1.Fine(20);
            if (guest != null) guest.Fine(20);

            var babyMood = TaskManager.Instance().GetNeed<BabyMood>();
            babyMood.AddSeconds(-20);

            var pee = TaskManager.Instance().GetNeed<NeedForPee>();
            if (pee != null) pee.level -= 0.2f;

            var stress = TaskManager.Instance().GetNeed<MomStress>();
            if (stress != null) stress.level -= 0.2f;

            var hungry = TaskManager.Instance().GetNeed<MomHungry>();
            if (hungry != null) hungry.level -= 0.2f;

            var hygien = TaskManager.Instance().GetNeed<NeedForHygien>();
            if (hygien != null) hygien.level -= 0.2f;
        }
    }
}
