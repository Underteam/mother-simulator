﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Guest : MonoBehaviour
{
    public float timer;

    private bool allRight;

    private GuestMood need;

    public int behaviourType = 0;

    public GameObject canvasWants;

    public Animator anim;
    public IKControl ikControl;

    public System.Action onWishSatisfied;

    public Image wishImage;

    public Sprite cakeWish;
    public Sprite pizzaWish;
    public Sprite talkWish;

    public GuestFood.FoodType foodType { get; private set; }

    void Awake ()
    {
        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            need = TaskManager.Instance().GetNeed<GuestMood>();

            Debug.LogError("Task started " + need, need);

            if (behaviourType == 1)
                job = Job1();
            else if (behaviourType == 2)
                job = Job2();

            StartCoroutine(job);
        },
        "TaskStarted");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    // Start is called before the first frame update
    void Start()
    {
        var pos = transform.position;
        pos.y = 0.17f;
        transform.position = pos;
    }

    private IEnumerator job;
    public void SetBehaviourType(int type)
    {
        if (type == behaviourType) return;

        behaviourType = type;

        StopAllCoroutines();

        if (behaviourType == 1)
            job = Job1();
        else if (behaviourType == 2)
            job = Job2();

        StartCoroutine(job);
    }

    public bool log;

    // Update is called once per frame
    void Update()
    {
        if (need == null) return;

        if (log) Debug.LogError("Here " + allRight + " " + timer + " " + need.level);

        if (allRight)
        {
            if (need.level < 1) need.level += 10f * need.speed * Time.deltaTime;
            if (timer > 0) timer -= Time.deltaTime;
            if (need.level > 1) need.level = 1;
        }
        else if (need.level > 0) need.level -= need.speed * Time.deltaTime;

        if (allRight && canvasWants.activeSelf) canvasWants.SetActive(false);
        else if (!allRight && !canvasWants.activeSelf) canvasWants.SetActive(true);
    }

    private MyAudioSource borredSound;

    //Food
    private IEnumerator Job1 ()
    {
        yield return null;

        MainDish dish = FindObjectOfType<MainDish>();

        if (need == null) yield break;

        allRight = true;

        timer = 15f;

        while (true)
        {
            var allFood = FindObjectsOfType<GuestFood>();
            List<GuestFood.FoodType> foodTypes = new List<GuestFood.FoodType>();
            for (int i = 0; i < allFood.Length; i++) if (!foodTypes.Contains(allFood[i].type)) foodTypes.Add(allFood[i].type);
            if (foodTypes.Count == 0) yield break;

            foodType = foodTypes[Random.Range(0, foodTypes.Count)];

            if (foodType == GuestFood.FoodType.Cake)
                wishImage.sprite = cakeWish;
            else
                wishImage.sprite = pizzaWish;

            while (timer > 0)
            {
                if (fineTime > 0)
                {
                    if (fineTime < timer)
                    {
                        timer -= fineTime;
                        fineTime = 0;
                    }
                    else
                    {
                        fineTime -= timer;
                        timer = 0;
                    }
                }

                yield return null;
            }

            allRight = false;

            if (dish.food == null)
            {
                Debug.LogError("New wish");
                EventManager.Instance().TriggerEvent("NewWish");

                if (!GameController.Instance().completed)
                    borredSound = SFXPlayer.Instance().Play(SFXLibrary.Instance().guestSad, true, true);
            }
            else
                Debug.LogError("No wish");

            bool wait = dish.food == null || (foodType == GuestFood.FoodType.Cake ? dish.food.type != GuestFood.FoodType.Cake : dish.food.type != GuestFood.FoodType.Pizza);
            Debug.LogError("Wait " + wait);
            while (wait)
            {
                if (fineTime > 0 && need != null)
                {
                    need.level -= fineTime / 100f;
                    fineTime = 0;
                }

                yield return null;

                wait = dish.food == null || (foodType == GuestFood.FoodType.Cake ? dish.food.type != GuestFood.FoodType.Cake : dish.food.type != GuestFood.FoodType.Pizza);
            }

            allRight = true;

            if (borredSound != null && borredSound.clip == SFXLibrary.Instance().guestSad) borredSound.Stop();

            anim.SetBool("Eating", true);
            ikControl.ikActive = false;

            takeFood = false;
            eated = false;

            while (!takeFood) yield return null;

            dish.food.transform.SetParent(ikControl.ikPrefs[1].boneTransform);
            dish.food.transform.localPosition = new Vector3(-0.274f, 0.19f, -0.122f);
            dish.food.transform.localRotation = Quaternion.Euler(-47, -231, 54);

            while (!eated) yield return null;

            Destroy(dish.food.gameObject);
            anim.SetBool("Eating", false);
            ikControl.ikActive = true;

            onWishSatisfied?.Invoke();

            timer = 15f;

            while (timer > 0)
            {
                if (fineTime > 0)
                {
                    if (fineTime < timer)
                    {
                        timer -= fineTime;
                        fineTime = 0;
                    }
                    else
                    {
                        fineTime -= timer;
                        timer = 0;
                    }
                }

                yield return null;
            }

            if (dish.food != null) Destroy(dish.food);

            timer = 15f;

            yield return null;
        }
    }

    private bool takeFood;
    private bool eated;
    public void OnAnimationEvent(AnimationEvent ev)
    {
        if (ev.stringParameter.Equals("TakeFood")) takeFood = true;
        if (ev.stringParameter.Equals("Eated")) eated = true;
    }

    //Conversation
    private IEnumerator Job2()
    {
        Debug.LogError("Start job 2 " + need, need);

        wishImage.sprite = talkWish;

        yield return null;

        DiningChair chair = FindObjectOfType<DiningChair>();

        Debug.LogError("Start job 2 " + need + " " + chair, this);

        if (need == null) yield break;

        allRight = true;

        timer = 15f;

        while (true)
        {
            while (timer > 0)
            {
                if (fineTime > 0)
                {
                    if (fineTime < timer)
                    {
                        timer -= fineTime;
                        fineTime = 0;
                    }
                    else
                    {
                        fineTime -= timer;
                        timer = 0;
                    }
                }

                yield return null;
            }

            allRight = false;

            if (!chair.sitting)
            {
                EventManager.Instance().TriggerEvent("NewWish");
                if (!GameController.Instance().completed)
                    borredSound = SFXPlayer.Instance().Play(SFXLibrary.Instance().guestSad);
            }

            while (!chair.sitting)
            {
                if (fineTime > 0 && need != null)
                {
                    need.level -= fineTime / 100f;
                    fineTime = 0;
                }

                yield return null;
            }

            SFXPlayer.Instance().Play(SFXLibrary.Instance().chatter);

            allRight = true;

            if (borredSound != null && borredSound.clip == SFXLibrary.Instance().guestSad) borredSound.Stop();

            onWishSatisfied?.Invoke();

            timer = 15f;

            while (timer > 0)
            {
                if (fineTime > 0)
                {
                    if (fineTime < timer)
                    {
                        timer -= fineTime;
                        fineTime = 0;
                    }
                    else
                    {
                        fineTime -= timer;
                        timer = 0;
                    }
                }

                yield return null;
            }

            timer = 15f;

            yield return null;
        }
    }

    private float fineTime;
    public void Fine(float time)
    {
        fineTime = time;
    }

    private void OnDisable()
    {
        if (borredSound != null) borredSound.Stop();
    }
}
