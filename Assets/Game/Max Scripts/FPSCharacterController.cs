﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;
using GMATools.Common;

[RequireComponent(typeof(CharacterController))]
public class FPSCharacterController : MonoBehaviour
{
    public Animator anim;

    public Transform pivot;
    public Transform cam;

    public float camMinRot;
    public float camMaxRot;

    public float speed = 3f;
    public float speedMult = 1f;

    private CharacterController controller;

    private Quaternion m_CharacterTargetRot;
    private Quaternion m_CameraTargetRot;

    public float f;

    public bool disableMovement;
    public bool disableRotation;

    public bool smooth;

    private Transform targetLook;
    public Vector3 targetShift;
    private Vector3 targetPrevPos;

    private Vector3 pivotPos;

    private Vector3 restMove;
    public float frozenFactor;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();

        m_CharacterTargetRot = transform.localRotation;
        m_CameraTargetRot = pivot.transform.localRotation;

        pivotPos = pivot.localPosition;

        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            var b = i.GetComponent<Baby>();
            if (b != null)
            {
                maxDist = 1f;
                return;
            }
        },
        "ItemTaked");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            var b = i.GetComponent<Baby>();
            if (b != null)
            {
                maxDist = 0.2f;
                return;
            }
        },
        "ItemDroped");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    private float prevH;
    private float prevV;

    public string info;

    public System.Action<Transform> onLookAtTarget;

    public void SetTarget (Transform target)
    {
        targetLook = target;

        if (target != null)
        {
            targetPrevPos = target.position;
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (Mathf.Approximately(Time.timeScale, 0)) return;

        float horizontal = InputManager.Instance().GetAxis("Horizontal");
        float vertical = InputManager.Instance().GetAxis("Vertical");

        float mult = Time.deltaTime / 0.015f;
        if (smooth) mult = 1;
        info = "mult " + mult + " " + Time.deltaTime;
        float yRot = InputManager.Instance().GetAxis("Mouse X") * mult;
        float xRot = InputManager.Instance().GetAxis("Mouse Y") * mult;

        if (targetLook != null)
        {
            if (Vector3.Distance(targetLook.position, targetPrevPos) > 0.1f)
                targetLook = null;
            else
                targetPrevPos = targetLook.position;
        }

        if (targetLook != null)
        {
            xRot = 0;
            yRot = 0;
        }

        //if (Mathf.Abs(xRot) > 0 || Mathf.Abs(yRot) > 0) Debug.LogError(xRot + " " + yRot);

        if (!disableMovement)
            m_CharacterTargetRot *= Quaternion.Euler(0f, yRot, 0f);
        else
            m_CharacterTargetRot = transform.localRotation;

        if (!disableRotation)
            m_CameraTargetRot *= Quaternion.Euler(-xRot, 0f, 0f);
        else
            m_CameraTargetRot = pivot.transform.localRotation;// Quaternion.Slerp(m_CameraTargetRot, pivot.transform.localRotation, 10 * Time.deltaTime);

        if (targetLook != null)
        {
            Vector3 dir = targetLook.position + targetShift - transform.position;
            dir.y = 0;
            Quaternion q1 = m_CharacterTargetRot;
            Quaternion q2 = Quaternion.LookRotation(dir.normalized, Vector3.up);
            if (dir.magnitude > 0) m_CharacterTargetRot = Quaternion.Lerp(q1, q2, 10 * Time.deltaTime);

            float delta = Quaternion.Angle(m_CharacterTargetRot, q2);

            dir = targetLook.position + targetShift - (cam.transform.position - pivot.transform.position) - pivot.transform.position;
            dir -= pivot.right * Vector3.Dot(dir, pivot.right);
            if (dir.magnitude > 0)
            {
                q1 = m_CameraTargetRot;
                pivot.transform.rotation = Quaternion.LookRotation(dir.normalized, Vector3.up);
                q2 = pivot.transform.localRotation;
                m_CameraTargetRot = Quaternion.Lerp(q1, q2, 10 * Time.deltaTime);

                delta += Quaternion.Angle(m_CameraTargetRot, q2);
            }

            info += " | " + delta;

            if (delta < 0.2f && onLookAtTarget != null) onLookAtTarget(targetLook);
        }
        else
        {
            pivot.localPosition = pivotPos;
            pivot.localRotation = Quaternion.Euler(pivot.localRotation.eulerAngles.x, 0, 0);
        }

        m_CameraTargetRot = ClampRotationAroundXAxis(m_CameraTargetRot);

        float r = Quaternion.Angle(transform.localRotation, m_CharacterTargetRot);
        r = 1f / Mathf.Exp(Mathf.Abs(r));//r [0, inf] -> [1, 0]
        r = 12f / (1.1f  - r);
        if (smooth)
            transform.localRotation = Quaternion.Slerp(transform.localRotation, m_CharacterTargetRot, r * Time.deltaTime);
        else
            transform.localRotation = m_CharacterTargetRot;

        r = Quaternion.Angle(pivot.transform.localRotation, m_CameraTargetRot);
        r = 1f / Mathf.Exp(Mathf.Abs(r));//r [0, inf] -> [1, 0]
        r = 12f / (1.1f - r);
        if (smooth)
            pivot.transform.localRotation = Quaternion.Slerp(pivot.transform.localRotation, m_CameraTargetRot, r * Time.deltaTime);
        else
            pivot.transform.localRotation = m_CameraTargetRot;

        Vector3 move = Vector3.zero;
        float s = 0;
        if (!disableMovement)
        {
            move = vertical* transform.forward + horizontal * transform.right;
            s = move.magnitude;
            if (s > 0) move.Normalize();
        }

        float m = Mathf.Max(move.magnitude, f);
        anim.SetFloat("Forward", m, 0.035f, Time.deltaTime);

        if (m > 0)
        {
            anim.SetFloat("HandsSpeed", Mathf.Lerp(0.1f, 1f, s));
        }
        else
        {
            anim.SetFloat("HandsSpeed", 1);
        }

        if (dist < 1f)
        {
            Vector3 dir = cam.transform.forward;
            dir.y = 0;
            dir.Normalize();

            m = Vector3.Dot(move, dir);
            float delta = speedMult * speed * Time.deltaTime * m;

            if (dist - delta < maxDist)
            {
                move -= dir * m;

                delta = dist - maxDist;
                m = delta / (speedMult * speed * Time.deltaTime);

                move += dir * m;
            }
        }

        move *= s;
        float ff = 0.99f * frozenFactor;
        move = ff * restMove + (1-ff) * move;
        restMove = move;

        if(float.IsNaN(move.x) || float.IsNaN(move.y) || float.IsNaN(move.z))
        {
            move = Vector3.zero;
            restMove = Vector3.zero;
        }

        moveStr = speedMult + " " + speed + " " + move;
        controller.Move(speedMult * speed * Time.deltaTime * move);

        if (!controller.isGrounded)
        {
            controller.Move(5 * Time.deltaTime * Vector3.down);
        }
    }

    public string moveStr;

    public float dist;//Dist to obstacle
    public float maxDist = 0.2f;
    private List<RaycastHit> list = new List<RaycastHit>();
    void FixedUpdate ()
    {
        this.dist = 1f;

        Vector3 dir = cam.transform.forward;
        dir.y = 0;
        dir.Normalize();

        Ray ray = new Ray(cam.transform.position - 0.2f * dir, dir);

        var hits = Physics.RaycastAll(ray);
        list.Clear();
        list.AddRange(hits);
        list.Sort((x, y) => {
            return x.distance.CompareTo(y.distance);
        });

        for(int i = 0; i < list.Count; i++)
        {
            if (IsChildOf(transform, list[i].transform)) continue;

            float dist = list[i].distance - 0.2f;
            if (dist > 1f) return;

            this.dist = dist;
            return;
        }
    }

    private bool IsChildOf(Transform parent, Transform child)
    {
        Transform p = child;

        while (p != null)
        {
            if (p == parent) return true;
            p = p.parent;
        }

        return false;
    }

    Quaternion ClampRotationAroundXAxis(Quaternion q)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

        angleX = Mathf.Clamp(angleX, camMinRot, camMaxRot);

        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

        return q;
    }
}
