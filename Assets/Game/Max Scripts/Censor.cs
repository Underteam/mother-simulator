﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Censor : MonoBehaviour
{
    public Transform target;

    public GameObject activator;

    private Camera cam;

    public Renderer rend;

    public float dist;

    private Baby baby;

    private Color col = Color.white;
    private Material mat;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;

        baby = FindObjectOfType<Baby>();

        mat = rend.material;
    }

    // Update is called once per frame
    void Update()
    {
        if (activator.activeInHierarchy && rend.enabled) rend.enabled = false;
        else if(!activator.activeInHierarchy && !rend.enabled) rend.enabled = true;

        Vector3 dir = cam.transform.position - target.position;
        dir.Normalize();

        transform.position = target.position + dist * dir;
        transform.rotation = Quaternion.LookRotation(-dir);

        col.a = Mathf.Lerp(col.a, baby.wet ? 0f : 0.5f, Time.deltaTime);
        mat.color = col;

    }
}
