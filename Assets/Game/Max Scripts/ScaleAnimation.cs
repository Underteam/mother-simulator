﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleAnimation : MonoBehaviour
{
    public float scale = 1.1f;
    private float s = 1f;
    private float targetScale;

    private Vector3 originalScale;

    // Start is called before the first frame update
    void Start()
    {
        originalScale = transform.localScale;
        targetScale = scale;
    }

    // Update is called once per frame
    void Update()
    {
        s = Mathf.Lerp(s, targetScale, 5 * Time.unscaledDeltaTime);

        transform.localScale = originalScale * s;

        if (Mathf.Abs(s - targetScale) < 0.01f)
        {
            if (targetScale == 1f) targetScale = scale;
            else targetScale = 1f;
        }
    }

    public void Delete ()
    {
        transform.localScale = originalScale;
        Destroy(this);
    }

    void OnDisable ()
    {
        transform.localScale = originalScale;
    }
}
