﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Husband1 : MonoBehaviour
{
    public Animator anim;

    private float timer = 3f;

    public bool angry;

    public GameObject canvasWants;

    private MyAudioSource angrySound;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            if (!angry)
            {
                //anim.SetTrigger("Idle1");
                timer = Random.Range(5f, 8f);
            }
            else
            {
                //anim.SetTrigger("Angry");
                timer = Random.Range(3f, 5f);
            }
        }

        if (!angry && angrySound != null && angrySound.clip == SFXLibrary.Instance().husbandAngry)
        {
            angrySound.Stop();
            angrySound = null;
        }

        if (angry && angrySound == null && !GameController.Instance().completed) angrySound = SFXPlayer.Instance().Play(SFXLibrary.Instance().husbandAngry, true, true);

        if (angry && !canvasWants.activeSelf) canvasWants.SetActive(true);
        else if (!angry && canvasWants.activeSelf) canvasWants.SetActive(false);
    }

    public void Fine (float time)
    {
        if (angry)
        {
            var need = TaskManager.Instance().GetNeed<NeedForFire>();
            if (need != null) need.level -= 0.2f;
        }
        else
        {
            var fire = FindObjectOfType<Fire>();
        }
    }

    private void OnDisable()
    {
        if (angrySound != null) angrySound.Stop();
    }
}
