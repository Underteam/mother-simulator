﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WishDrawer : MonoBehaviour
{
    public Text lblWish;

    public Image imgWish;

    public GameObject alert;

    private void Awake()
    {
        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            lblWish.transform.parent.gameObject.SetActive(true);

            Wish w = d as Wish;
            SetWish(w);

            //Debug.LogError("New wish " + w, w);
        },
        "NewBabyWish");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            lblWish.transform.parent.gameObject.SetActive(false);
        },
        "TaskCompleted");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            lblWish.transform.parent.gameObject.SetActive(false);
        },
        "WishSatisfied");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    public void SetWish (Wish wish)
    {
        //Debug.Log("Set wish " + wish, this);

        if (wish != null)
        {
            lblWish.text = wish.description;
            imgWish.sprite = wish.image;
            alert.SetActive(true);
        }
        else
        {
            imgWish.sprite = null;
            lblWish.text = "?";
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnBecameVisible()
    {
        Debug.LogError("Visible");
    }
}
