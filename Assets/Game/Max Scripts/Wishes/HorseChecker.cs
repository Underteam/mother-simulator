﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorseChecker : WishChecker
{
    private Baby baby;

    private BabySitPoint horse;

    // Start is called before the first frame update
    void Start()
    {

    }

    public override void Init()
    {
        base.Init();

        baby = FindObjectOfType<Baby>();
        horse = FindObjectOfType<ToyHorse>().GetComponent<BabySitPoint>();

        //stepsCheckers.Add(CheckStep1);
        stepsCheckers.Add(CheckStep2);
    }

    private bool CheckStep1()
    {
        return baby.onHands || horse.baby != null;
    }

    private bool CheckStep2()
    {
        return horse.baby != null;
    }

    protected override bool IsFinished()
    {
        return baby.happy;
    }
}
