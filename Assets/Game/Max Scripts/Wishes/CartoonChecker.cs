﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CartoonChecker : WishChecker
{
    private Baby baby;
    private PlayerController player;
    private CartoonWish cartoonWish;
    private BabySitPoint sitPoint;

    // Start is called before the first frame update
    void Start()
    {

    }

    public override void Init()
    {
        base.Init();

        baby = FindObjectOfType<Baby>();
        player = PlayerController.instance;
        cartoonWish = wish as CartoonWish;
        sitPoint = FindObjectOfType<DVDPlayer>().sitPoint;

        stepsCheckers.Add(CheckStep0);
        stepsCheckers.Add(CheckStep1);
        stepsCheckers.Add(CheckStep2);
    }

    private bool CheckStep0()
    {
        if (sitPoint.baby == null) return false;

        return true;

    }
    private bool CheckStep1()
    {
        if (player.currentItem == null) return false;

        var disc = player.currentItem.GetComponent<DVDDisc>();

        if (disc == null) return false;

        if (disc != cartoonWish.disc) return false;

        return true;
    }

    private bool CheckStep2()
    {
        return baby.happy;
    }

    protected override bool IsFinished()
    {
        return baby.happy;
    }
}
