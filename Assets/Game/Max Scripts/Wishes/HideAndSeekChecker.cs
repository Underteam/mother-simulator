﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideAndSeekChecker : WishChecker
{
    private Baby baby;

    private HideAndSeekGame game;

    // Start is called before the first frame update
    void Start()
    {

    }

    public override void Init()
    {
        base.Init();

        baby = FindObjectOfType<Baby>();
        game = FindObjectOfType<HideAndSeekGame>();

        //stepsCheckers.Add(CheckStep1);
        stepsCheckers.Add(CheckStep2);
        stepsCheckers.Add(CheckStep3);
    }

    private bool CheckStep1()
    {
        return baby.onHands || game.playing;
    }

    private bool CheckStep2()
    {
        return game.playing;
    }

    private bool CheckStep3()
    {
        return baby.onHands && game.playing;
    }

    protected override bool IsFinished()
    {
        return baby.happy;
    }
}
