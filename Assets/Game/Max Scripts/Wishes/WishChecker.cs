﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WishChecker : MonoBehaviour
{
    public List<System.Func<bool>> stepsCheckers = new List<System.Func<bool>>();

    protected Wish wish;

    private bool finished;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public virtual void Init ()
    {
        wish = GetComponent<Wish>();
        stepsCheckers.Clear();
        prevValues.Clear();
        finished = false;
    }

    protected virtual bool IsFinished ()
    {
        return false;
    }

    private List<bool> prevValues = new List<bool>();
    public virtual void Check ()
    {
        if (finished) return;

        finished = IsFinished ();
        if (finished && wish.box != null)
        {
            for(int i = 0; i < wish.box.sublines.Count; i++) wish.box.CompleteSubline(i);
            return;
        }

        for (int i = 0; i < stepsCheckers.Count; i++)
        {
            if (prevValues.Count <= i) prevValues.Add(false);
            bool res = stepsCheckers[i]();

            if (!prevValues[i] && res)
            {
                prevValues[i] = true;
                if (wish.box != null) wish.box.CompleteSubline(i);
                continue;
            }
            else if (prevValues[i] && !res)
            {
                prevValues[i] = false;
                if (wish.box != null) wish.box.ErrorSubline(i);
                finished = false;
            }

            if (!prevValues[i])
            {
                finished = false;
                //break;
            }
        }
    }
}
