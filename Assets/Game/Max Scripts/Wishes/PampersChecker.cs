﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PampersChecker : WishChecker
{
    private Baby baby;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public override void Init()
    {
        base.Init();

        baby = FindObjectOfType<Baby>();

        stepsCheckers.Add(CheckStep1);
        stepsCheckers.Add(CheckStep2);
        stepsCheckers.Add(CheckStep3);
    }

    private bool CheckStep1 ()
    {
        return !baby.diaper.activeSelf;
    }

    private bool CheckStep2()
    {
        return !baby.wet;
    }

    private bool CheckStep3()
    {
        return baby.diaper.activeSelf && !baby.wet;
    }

    protected override bool IsFinished()
    {
        return baby.diaper.activeSelf && !baby.wet;
    }
}
