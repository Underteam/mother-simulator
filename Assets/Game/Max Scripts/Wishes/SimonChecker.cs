﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimonChecker : WishChecker
{
    private Baby baby;

    private SimonGame game;

    // Start is called before the first frame update
    void Start()
    {

    }

    public override void Init()
    {
        base.Init();

        baby = FindObjectOfType<Baby>();
        game = FindObjectOfType<SimonGame>();

        stepsCheckers.Add(CheckStep1);
        stepsCheckers.Add(CheckStep2);
        stepsCheckers.Add(CheckStep3);
    }

    private bool CheckStep1()
    {
        return baby.onHands;
    }

    private bool CheckStep2()
    {
        return game.playing;
    }

    private bool CheckStep3()
    {
        return baby.happy;
    }

    protected override bool IsFinished()
    {
        return baby.happy;
    }
}
