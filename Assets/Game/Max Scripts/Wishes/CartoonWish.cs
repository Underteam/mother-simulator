﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CartoonWish : Wish
{
    public DVDDisc disc;

    private Coroutine coroutine;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void Init()
    {
        base.Init();

        PickNewCartoon();

        //if (coroutine == null) coroutine = StartCoroutine(Job());
    }

    public override bool Check(string evName, object data)
    {
        if (!(data is object[])) return false;

        var list = data as object[];

        if (list.Length < 2) return false;

        if (!(list[0] is string str)) return false;

        if (!eventName.Equals(str)) return false;

        if (!(list[1] is DVDDisc d)) return false;

        return disc != null && d != null && d.cartonID == disc.cartonID;
    }

    private void PickNewCartoon()
    {
        List<DVDDisc> discs = new List<DVDDisc>(FindObjectsOfType<DVDDisc>());

        if (discs.Count < 1) return;

        disc = discs[Random.Range(0, discs.Count)];

        SetDisc(disc);
    }

    public void SetDisc(DVDDisc disc)
    {
        this.disc = disc;

        if (disc != null)
        {
            //description = disc.discName;// "Disc " + disc.discName;
            image = disc.icon;
        }
        //else description = "Cartoon";
    }

    private IEnumerator Job()
    {
        while (true)
        {
            if (disc == null) PickNewCartoon();

            yield return null;
        }
    }
}
