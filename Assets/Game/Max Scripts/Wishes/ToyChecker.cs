﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToyChecker : WishChecker
{
    private Baby baby;
    private PlayerController player;
    private ToyWish toyWish;

    // Start is called before the first frame update
    void Start()
    {

    }

    public override void Init()
    {
        base.Init();

        baby = FindObjectOfType<Baby>();
        player = PlayerController.instance;
        toyWish = wish as ToyWish;

        stepsCheckers.Add(CheckStep1);
        stepsCheckers.Add(CheckStep2);
    }

    private bool CheckStep1 ()
    {
        if (player.currentItem == null) return false;

        var toy = player.currentItem.GetComponent<Toy>();

        if (toy == null) return false;

        if (toy != toyWish.toy) return false;

        return true;
    }

    private bool CheckStep2 ()
    {
        return baby.happy;
    }

    protected override bool IsFinished ()
    {
        return baby.happy;
    }
}
