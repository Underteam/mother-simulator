﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WashChecker : WishChecker
{
    private Baby baby;

    private Bath bath;

    // Start is called before the first frame update
    void Start()
    {

    }

    public override void Init()
    {
        base.Init();

        baby = FindObjectOfType<Baby>();
        bath = FindObjectOfType<Bath>();

        stepsCheckers.Add(CheckStep1);
        stepsCheckers.Add(CheckStep2);
        stepsCheckers.Add(CheckStep3);
    }

    private bool CheckStep1()
    {
        return bath.baby != null;
    }

    private bool CheckStep2()
    {
        return baby.soaped;
    }

    private bool CheckStep3()
    {
        return !baby.dirty;
    }

    protected override bool IsFinished()
    {
        return !baby.dirty;
    }
}
