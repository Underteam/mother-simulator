﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wish : MonoBehaviour
{
    public string description;

    public string eventName;

    public Sprite image;

    public List<string> subTasks;

    protected WishChecker checker;

    public WishBox box;

    public bool done { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public virtual void Init ()
    {
        checker = GetComponent<WishChecker>();
        if (checker != null) checker.Init();

        //Debug.LogError("Init wish " + this, this);
    }

    public virtual bool Check (string evName, object data)
    {
        if (data is string) return eventName.Equals((string)data);

        if (!(data is object[])) return false;

        var list = data as object[];

        if (list.Length < 2) return false;

        if (!(list[0] is string str)) return false;

        if (!eventName.Equals(str)) return false;

        if (!(list[1] is bool b)) return false;

        return b;
    }

    public virtual void OnUpdate ()
    {
        if (checker != null) checker.Check();
    }
}
