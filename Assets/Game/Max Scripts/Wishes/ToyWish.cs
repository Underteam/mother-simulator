﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToyWish : Wish
{
    public Toy toy;

    private Coroutine coroutine;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void Init()
    {
        base.Init();

        PickNewToy();

        //if (coroutine == null) coroutine = StartCoroutine(Job());
    }

    public override bool Check(string evName, object data)
    {
        if (!(data is object[])) return false;

        var list = data as object[];

        if (list.Length < 2) return false;

        if (!(list[0] is string str)) return false;

        if (!eventName.Equals(str)) return false;

        if (!(list[1] is Toy t)) return false;

        return t == toy && toy != null;
    }

    private void PickNewToy ()
    {
        List<Toy> toys = new List<Toy>(FindObjectsOfType<Toy>());

        if (toys.Count < 1) return;

        toy = toys[Random.Range(0, toys.Count)];

        SetToy(toy);
    }

    public void SetToy(Toy toy)
    {
        this.toy = toy;

        if (toy != null)
        {
            description = "Toy " + toy.toyName;
            image = toy.icon;
        }
        else description = "Toy";
    }

    private IEnumerator Job ()
    {
        while (true)
        {
            if (toy == null) PickNewToy();

            yield return null;
        }
    }
}
