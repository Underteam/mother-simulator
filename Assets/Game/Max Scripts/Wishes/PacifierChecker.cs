﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacifierChecker : WishChecker
{
    private Baby baby;
    private PlayerController player;
    private DirtyScript pacifier;

    // Start is called before the first frame update
    void Start()
    {

    }

    public override void Init()
    {
        base.Init();

        baby = FindObjectOfType<Baby>();
        player = PlayerController.instance;
        pacifier = null;

        stepsCheckers.Add(CheckStep1);
        stepsCheckers.Add(CheckStep2);
        stepsCheckers.Add(CheckStep3);
    }

    private bool CheckStep1 ()
    {
        if (player.currentItem == null) return false;

        if (player.currentItem.GetComponent<Pacifier>() == null) return false;

        return true;
    }

    private bool CheckStep2 ()
    {
        if (pacifier == null && player.currentItem != null)
        {
            if (player.currentItem.GetComponent<Pacifier>() != null)
                pacifier = player.currentItem.GetComponent<DirtyScript>();
        }

        if (pacifier != null && player.currentItem != null && pacifier.gameObject != player.currentItem.gameObject)
        {
            if (player.currentItem.GetComponent<Pacifier>() != null)
                pacifier = player.currentItem.GetComponent<DirtyScript>();
            else
                pacifier = null;
        }

        if (pacifier == null) return false;

        if (!pacifier.isClean) return false;

        return true;
    }

    private bool CheckStep3()
    {
        if (!baby.myPacifier.activeSelf) return false;

        return true;
    }

    protected override bool IsFinished ()
    {
        return baby.myPacifier.activeSelf && baby.happy;
    }
}
