﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottleChecker : WishChecker
{
    private Baby baby;
    private PlayerController player;
    private BabyBottle bottle;
    private Item itemBottle;
    private DirtyScript bottleDirty;
    private BottlePacifier pacifier;
    private Item itemPacifier;

    // Start is called before the first frame update
    void Start()
    {

    }

    public override void Init()
    {
        base.Init();

        baby = Baby.instance;
        player = PlayerController.instance;

        stepsCheckers.Add(CheckStep1);
        stepsCheckers.Add(CheckStep2);
        stepsCheckers.Add(CheckStep4);
        //stepsCheckers.Add(CheckStep3);
        stepsCheckers.Add(CheckStep5);

        //Debug.LogError("Init " + bottle, bottle);
    }

    private bool CheckStep1()
    {
        if (player.currentItem != null)
        {
            if (itemBottle == null)
            {
                bottle = player.currentItem.GetComponent<BabyBottle>();
                if (bottle != null) itemBottle = player.currentItem;
            }

            return player.currentItem == itemBottle;

        }
        else
        {
            itemBottle = null;
            bottle = null;

            return false;
        }
    }

    private bool CheckStep2()
    {
        if (bottle == null) return false;

        if (bottleDirty == null) bottleDirty = bottle.GetComponent<DirtyScript>();

        if (pacifier == null) pacifier = bottle.pacifier;

        if (pacifier == null) return false;

        return bottleDirty.isClean;
    }

    //private bool CheckStep3()
    //{
    //    if (bottle == null) return false;

    //    return bottle.withFood && bottle.withWater && bottle.withPacifier;
    //}

    private bool CheckStep4()
    {
        if (bottle == null) return false;

        return bottle.withFood && bottle.withWater;
    }

    private bool CheckStep5()
    {
        //Debug.LogError("Step5 " + baby.happy);

        return baby.happy;
    }

    protected override bool IsFinished()
    {
        //Debug.LogError("IsFinished " + baby.happy);

        return baby.happy;
    }
}
