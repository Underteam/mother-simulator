﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DailyReward : MonoBehaviour
{
    public Sprite icon;

    public ShopItemsPack pack;

    public int amount;

    public enum RewardType
    {
        Crystals,
        Present,
        Item,
    }

    public RewardType rewardType;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
