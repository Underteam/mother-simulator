﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DailyRewardUI : MonoBehaviour
{
    public Image body;

    public Text lblDay;

    public Image rewardIcon;

    public Text lblAmount;

    public GameObject ok;

    public Sprite done;
    public Sprite current;
    public Sprite next;
    public Sprite crystals;
    public Sprite keys;

    public UnityEvent onTaked;

    private DailyReward myReward;

    public FlyingCoin fcPrefab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Set (DailyReward reward, int state)
    {
        myReward = reward;

        rewardIcon.sprite = reward.icon;

        if (state < 0)
        {
            body.sprite = done;
            ok.SetActive(true);
        }
        else if (state == 0)
        {
            body.sprite = current;
            ok.SetActive(false);
        }
        else if (state > 0)
        {
            body.sprite = next;
            ok.SetActive(false);
        }

        if (myReward.rewardType == DailyReward.RewardType.Present)
        {
            lblAmount.gameObject.SetActive(false);
        }
        else if (reward.amount > 1)
        {
            lblAmount.gameObject.SetActive(true);
            lblAmount.text = "" + reward.amount;
        }
        else
        {
            lblAmount.gameObject.SetActive(false);
        }
    }

    public void Taked ()
    {
        if (myReward.rewardType == DailyReward.RewardType.Crystals)
        {
            rewardIcon.GetComponent<Animator>().Play("DailyRewardAnim2");
            StartCoroutine(FlyingCrystals(10));
        }
        else if (myReward.rewardType == DailyReward.RewardType.Present)
        {
            var clone = Instantiate(rewardIcon);
            clone.transform.SetParent(rewardIcon.transform.parent);
            clone.transform.position = rewardIcon.transform.position;
            clone.transform.localScale = rewardIcon.transform.localScale;

            clone.GetComponent<Animator>().Play("DailyRewardAnim1");

            if (GameState.Instance().currentTask > 4)
            {
                rewardIcon.sprite = keys;
                lblAmount.text = "" + 3;
            }
            else
            {
                rewardIcon.sprite = crystals;
                lblAmount.text = "" + myReward.amount;
            }
            rewardIcon.gameObject.SetActive(false);
            lblAmount.gameObject.SetActive(false);
        }
        else if (myReward.rewardType == DailyReward.RewardType.Item)
        {
            var anim = rewardIcon.GetComponent<Animator>();
            if (anim != null) anim.Play("DailyRewardAnim2");
            Shop.Instance().OpenPack(myReward.pack);
        }

        Invoke("SetOK", 0.75f);

        onTaked?.Invoke();
    }

    private void SetOK ()
    {
        ok.SetActive(true);
        body.sprite = done;
        if (myReward.rewardType == DailyReward.RewardType.Crystals)
        {
            Wallet w = Wallet.GetWallet(Wallet.CurrencyType.crystals);
            w.Add(myReward.amount);
        }

        if (myReward.rewardType == DailyReward.RewardType.Present)
        {
            rewardIcon.gameObject.SetActive(true);
            lblAmount.gameObject.SetActive(true);

            if (GameState.Instance().currentTask <= 4)
            {
                Wallet w = Wallet.GetWallet(Wallet.CurrencyType.crystals);
                w.Add(myReward.amount);
                StartCoroutine(FlyingCrystals(10));
            }
            else
            {
                Wallet w = Wallet.GetWallet(Wallet.CurrencyType.keys);
                w.Add(3);
            }
        }

        PlayerPrefs.SetInt("NumReadyToTakeGifts", 0);
    }

    private List<FlyingCoin> instantiated = new List<FlyingCoin>();
    private IEnumerator FlyingCrystals (int num)
    {
        yield return new WaitForSeconds(0.5f);

        for (int i = 0; i < num; i++)
        {
            var coin = Instantiate(fcPrefab);

            coin.transform.SetParent(fcPrefab.transform.parent);
            coin.transform.position = rewardIcon.transform.position;
            coin.transform.localScale = fcPrefab.transform.localScale;
            coin.gameObject.SetActive(true);

            instantiated.Add(coin);

            yield return new WaitForSeconds(0.05f);
        }
    }

    private void OnDisable()
    {
        for (int i = 0; i < instantiated.Count; i++)
        {
            if (instantiated[i] != null) Destroy(instantiated[i].gameObject);
        }
    }
}
