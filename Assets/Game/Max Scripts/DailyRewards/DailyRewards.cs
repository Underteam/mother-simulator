﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DailyRewards : MonoBehaviour
{
    public GameObject panel;

    public List<DailyReward> rewards;

    public List<DailyRewardUI> rewardUIs;

    private int currentReward;

    private List<int> readyToTake = new List<int>();

    // Start is called before the first frame update
    IEnumerator Start()
    {
        int numReadyToTake = PlayerPrefs.GetInt("NumReadyToTakeGifts", 0);
        for (int i = 0; i < numReadyToTake; i++)
        {
            int ind = PlayerPrefs.GetInt("ReadyToTakeGift" + i, i);
            readyToTake.Add(ind);
        }

        bool forceOpen = readyToTake.Count > 0;
        
        currentReward = PlayerPrefs.GetInt("CurrentDailyReward", -1);

        for (int i = 0; i < currentReward && i < rewards.Count; i++)//открыть скин если по какой-то причине мы его не взяли, хотя должны были
        {
            if (rewards[i].pack != null) Shop.Instance().OpenPack(rewards[i].pack);
        }

        if (currentReward >= 7 && !forceOpen)
        {
            panel.SetActive(false);
            yield break;
        }

        while (!GameState.Instance().started) yield return null;

        bool canShow = PlayerPrefs.GetInt("CanShowDailyRewards", 0) == 1;//чтобы не показывать при первом запуске

        if (!canShow && GameState.Instance().state == GameState.State.Game)
        {
            PlayerPrefs.SetInt("CanShowDailyRewards", 1);
            yield break;
        }

        if (!canShow || GameState.Instance().state == GameState.State.Game) yield break;

        System.DateTime time = System.DateTime.UtcNow;
        time = time.AddHours(-24);
        string lastShowTimeStr = PlayerPrefs.GetString("DailyRewardsLastTime", time.ToString());
        System.DateTime.TryParse(lastShowTimeStr, out time);

        var span = System.DateTime.UtcNow - time;
        int th = (int)span.TotalHours;
        if (th >= 24)
        {
            th = th - (th % 24);

            time = time.AddHours(th);
            PlayerPrefs.SetString("DailyRewardsLastTime", time.ToString());

            currentReward++;
            PlayerPrefs.SetInt("CurrentDailyReward", currentReward);

            if (!readyToTake.Contains(currentReward))
            {
                readyToTake.Add(currentReward);
                PlayerPrefs.SetInt("NumReadyToTakeGifts", readyToTake.Count);
                for (int i = 0; i < readyToTake.Count; i++)
                {
                    PlayerPrefs.SetInt("ReadyToTakeGift" + i, readyToTake[i]);
                }
            }
        }
        else if (!forceOpen) yield break;

        MenuQueue.Instance().Show(panel, 2);

        for (int i = 0; i < rewards.Count && i < rewardUIs.Count; i++)
        {
            bool readyToTake = this.readyToTake.Contains(i);

            int state = -1;
            if (i > currentReward) state = 1;
            else if (readyToTake) state = 0;

            //Debug.LogError("Check " + i + " " + readyToTake + " " + currentReward + " " + state);

            rewardUIs[i].Set(rewards[i], state);
            rewardUIs[i].lblDay.text = Localizer.GetString("day", "Day " + (i + 1)).Replace("%%", "" + (i+1));
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private bool taking;
    public void Take ()
    {
        if (taking) return;
        taking = true;

        for (int i = 0; i < readyToTake.Count; i++)
        {
            int ind = readyToTake[i];
            if (ind >= 0 && ind < rewardUIs.Count) rewardUIs[ind].Taked();
        }

        readyToTake.Clear();
        //PlayerPrefs.SetInt("NumReadyToTakeGifts", readyToTake.Count);

        Invoke ("Close", 2.5f);
    }

    private void Close ()
    {
        panel.SetActive(false);
    }
}
