﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiaperUsed : MonoBehaviour
{
    public bool counted;

    public bool inBin;

    public GameObject dirtyPrefab;

    private static List<GameObject> dirties = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionStay (Collision col)
    {
        if (inBin) return;

        if (col.collider.CompareTag("Floor"))
        {
            Vector3 p = col.contacts[0].point;

            float closest = float.MaxValue;
            for (int i = 0; i < dirties.Count; i++)
            {
                if (dirties[i] == null)
                {
                    dirties.RemoveAt(i);
                    i--;
                    continue;
                }
                float d = Vector3.Distance(p, dirties[i].transform.position);
                if (d < closest) closest = d;
            }

            if (dirtyPrefab != null && closest > 1.0f)
            {
                var dirty = Instantiate(dirtyPrefab);
                dirty.SetActive(true);
                dirty.transform.position = p;
                dirty.transform.rotation = Quaternion.Euler(0, Random.Range(0f, 360f), 0);
                dirty.transform.localScale = Vector3.one;
                dirties.Add(dirty);
            }
        }
    }
}
