﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BabySitPoint : MonoBehaviour
{
    public string description;

    public Transform sitPoint;

    public Baby baby { get; set; }

    public Transform parent;

    public UnityEvent onSit;
    public UnityEvent onStand;

    // Start is called before the first frame update
    void Start()
    {
        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            if (i.GetComponent<Baby>() == baby)
            {
                baby = null;
                if (onStand != null) onStand.Invoke();
            }
        },
        "ItemTaked");
        listeners.Add(listener);

    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    public void SitBaby(Item baby)
    {
        if (baby == null) return;

        this.baby = baby.GetComponent<Baby>();
        Debug.Log("Sit " + this.baby, this.baby);

        if (this.baby != null)
        {
            this.baby.onReleased = () =>
            {
                this.baby.transform.SetParent (parent);
                this.baby.transform.position = sitPoint.position;
                this.baby.transform.rotation = sitPoint.rotation;
                this.baby.Sit();
                this.baby.onReleased = null;
                if (onSit != null) onSit.Invoke();
            };

            PlayerController.instance.ReleaseItem();
        }
    }
}
