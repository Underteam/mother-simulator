﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

delegate void MyAction();

delegate void MyAction<T>(T p);
