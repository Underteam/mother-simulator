﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GMATools.Common;

public class BabyChair : MonoBehaviour, IAppropriatanceProvider
{
    public Transform sitPoint;

    public Baby baby { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            if (i.GetComponent<Baby>() == baby) baby = null;
        },
        "ItemTaked");
        listeners.Add(listener);

    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SitBaby (Item baby)
    {
        if (baby == null) return;

        this.baby = baby.GetComponent<Baby>();
        Debug.Log("Sit " + this.baby, this.baby);

        if (this.baby != null)
        {
            this.baby.onReleased = () =>
            {
                if (this.baby == null) return;
                this.baby.transform.position = sitPoint.position;
                this.baby.transform.rotation = sitPoint.rotation;
                this.baby.Sit();
                this.baby.onReleased = null;
                EventManager.Instance().TriggerEvent("BabyChair");
            };

            PlayerController.instance.ReleaseItem();
        }
    }

    public bool IsAppropriate(Item item)
    {
        if (item == null) return false;
        return item.GetComponent<Baby>() != null;
    }
}
