﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrashBin : MonoBehaviour
{
    public Text lblReward;
    public Image imgReward;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnEnter (Collider other)
    {
        var diaper = other.GetComponent<DiaperUsed>();

        if (diaper != null) diaper.inBin = true;

        if (diaper != null && !diaper.counted)
        {
            if (GameState.Instance().state == GameState.State.Game)
            {
                float rnd = Random.Range(0f, 1f);
                if (rnd < 0.25f)
                {
                    lblReward.text = Localizer.GetString("trashbinTimer", "+10sec");//"+10" + Localizer.GetString("sec", "сек");
                    imgReward.gameObject.SetActive(false);
                    var mood = TaskManager.Instance().GetNeed<BabyMood>();
                    if (mood != null) mood.AddSeconds(10, true);
                }
                else if (rnd < 0.5f)
                {
                    lblReward.text = "+1";
                    imgReward.gameObject.SetActive(true);
                    GameController.Instance().reward += 1;
                }
                else if (rnd < 0.75f)
                {
                    lblReward.text = "+2";
                    imgReward.gameObject.SetActive(true);
                    GameController.Instance().reward += 2;
                }
                else
                {
                    lblReward.text = "+3";
                    imgReward.gameObject.SetActive(true);
                    GameController.Instance().reward += 3;
                }

                lblReward.gameObject.SetActive(true);
            }
            else
            {
                EventManager.Instance().TriggerEvent("PampersInTrashbin", diaper, this);
            }

            diaper.counted = true;
        }
    }

    public void ShowText (string text, bool iconState)
    {
        lblReward.text = text;
        lblReward.gameObject.SetActive(true);
        imgReward.gameObject.SetActive(iconState);
    }

    public void OnExit (Collider other)
    {
        var diaper = other.GetComponent<DiaperUsed>();

        if (diaper != null) diaper.inBin = false;
    }
}
