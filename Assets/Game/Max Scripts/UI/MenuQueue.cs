﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuQueue : MonoSingleton<MenuQueue>
{
    private List<GameObject> queue = new List<GameObject>();
    private List<int> priorities = new List<int>();

    private bool showing;

    protected override void Init()
    {
        base.Init();
    }

    public void Show(GameObject menu, int priority)
    {
        //Debug.Log("Add " + menu + " " + priority + " " + showing, menu);

        if (queue.Contains(menu)) return;

        queue.Add(menu);
        priorities.Add(priority);

        for (int i = priorities.Count - 1; i >= 1; i--)
        {
            if (priorities[i] < priorities[i - 1])
            {
                var temp1 = queue[i];
                queue[i] = queue[i - 1];
                queue[i - 1] = temp1;

                var temp2 = priorities[i];
                priorities[i] = priorities[i - 1];
                priorities[i - 1] = temp2;
            }
        }

        for(int i = 0; i < queue.Count; i++)
        {
            //Debug.Log(i + ") " + queue[i] + " " + priorities[i], queue[i]);
        }
        //if (queue.Count == 1) menu.SetActive(true);
    }

    private void Update()
    {
        if (queue.Count > 0)
        { 
            if (!queue[0].activeSelf)
            {
                if (!showing)
                {
                    queue[0].SetActive(true);
                    showing = true;
                }
                else
                {
                    queue.RemoveAt(0);
                    priorities.RemoveAt(0);

                    if (queue.Count > 0) queue[0].SetActive(true);
                    else showing = false;
                }
            }
        }
        else
        {
            showing = false;
        }
    }
}
