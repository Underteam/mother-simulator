﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Setting : MonoBehaviour
{
    public string settingName;

    public bool state { get; set; }

    public bool defaultState;

    public GameObject on;
    public GameObject off;

    [System.Serializable]
    public class OnStateChanged : UnityEvent<bool> { }

    public OnStateChanged onStateChanged;

    // Start is called before the first frame update
    void Start()
    {
        if (!loaded) Load (true);
    }

    private bool loaded;
    public void Load (bool apply)
    {
        state = PlayerPrefs.GetInt(settingName + "State", defaultState ? 1 : 0) == 1;

        Apply(apply);

        loaded = true;
    }

    public void Switch ()
    {
        state = !state;

        Apply(true);
    }

    public void Apply (bool apply)
    {
        PlayerPrefs.SetInt(settingName + "State", state ? 1 : 0);

        on.SetActive(state);
        off.SetActive(!state);

        if (apply) onStateChanged?.Invoke(state);
    }
}
