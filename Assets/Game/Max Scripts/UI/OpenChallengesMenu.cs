﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenChallengesMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Open()
    {
        Shop.Instance().Close();

        var specialDoors = GameController.Instance().specialDoors;

        for (int i = 0; i < specialDoors.Count; i++)
        {
            if (specialDoors[i].name.Equals("Yard"))
            {
                TutorialManager.Instance().FocusOn(specialDoors[i].doors[0].transform);
                break;
            }
        }
    }
}
