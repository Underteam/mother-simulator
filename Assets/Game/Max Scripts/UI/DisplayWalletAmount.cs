﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayWalletAmount : MonoBehaviour
{
    public Wallet.CurrencyType type;

    public Text lblAmount;

    private Wallet wallet;

    private void Awake()
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
        wallet = Wallet.GetWallet(type);
        wallet.onValueChanged += OnValueChanged;
        lblAmount.text = "" + wallet.amount;

        //Debug.LogError("Display " + wallet.amount);
    }

    private void OnValueChanged (Wallet w)
    {
        if (lblAmount == null)
        {
            Debug.LogError(">>>", this);
            Transform tr = transform;
            while (tr != null)
            {
                Debug.Log(tr.name);
                tr = tr.parent;
            }
        }
        else lblAmount.text = "" + w.amount;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDestroy()
    {
        if (wallet == null) return;

        wallet.onValueChanged -= OnValueChanged;
    }
}
