﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulsation : MonoBehaviour
{
    public AnimationCurve pulsation;

    public float speed = 1f;

    private float timer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float s = pulsation.Evaluate(timer);

        transform.localScale = s * Vector3.one;

        timer += speed * Time.deltaTime;
        while (timer > 1) timer -= 1f;
    }
}
