﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIFollowTarget : MonoBehaviour
{
    public Transform player;

    public Transform target;

    private Camera mainCamera;
    
    private RectTransform rt;

    private Transform canvas;

    private float screeWidth;

    private float screenHeight;

    private float limit = 30;

    public bool log;

    void Awake()
    {
        rt = GetComponent<RectTransform>();        
        CalculateLimits();
        mainCamera = Camera.main;
    }

    private void CalculateLimits()
    {
        canvas = GetComponentInParent<Canvas>().transform;

        var rt = canvas as RectTransform;

        screeWidth = rt.sizeDelta.x;
        screenHeight = rt.sizeDelta.y;

        Debug.Log(screeWidth + " " + screenHeight);
    }

    Vector3 direction;

    void Update()
    {
        if (target == null || player == null) return;

        Vector3 dir = target.position - player.position;
        dir.y = 0;
        dir.Normalize();

        direction = dir;

        dir = Quaternion.AngleAxis(-mainCamera.transform.rotation.eulerAngles.y, Vector3.up) * dir;
        dir.y = dir.z;

        float aspect = screeWidth / screenHeight;
        float k = 1;
        if (Mathf.Abs(dir.y) > 0) k = dir.x / dir.y;

        float x = 0;
        float y = 0;

        if (log) Debug.Log("Aspects " + aspect + " " + k);

        if(Mathf.Abs(k) < aspect)
        {
            float t = Mathf.Abs(screenHeight / 2 / dir.y);
            x = t * dir.x;
            y = t * dir.y;
            if (log) Debug.Log("Here1 " + t + " " + x + " " + y);
        }
        else
        {
            float t = Mathf.Abs(screeWidth / 2 / dir.x);
            x = t * dir.x;
            y = t * dir.y;
            if (log) Debug.Log("Here2 " + t + " " + x + " " + y);
        }

        if (log) Debug.Log("Dir " + 100 * dir + " " + x + " " + y);

        var pos = mainCamera.WorldToViewportPoint(target.position) - 0.5f * Vector3.one;

        if (log) Debug.Log(pos + " " + mainCamera.WorldToScreenPoint(target.position));

        pos.x *= screeWidth;
        pos.y *= screenHeight;
        pos.z = 0;

        if (log) Debug.Log(pos + " " + (mainCamera.WorldToScreenPoint(target.position) - new Vector3(screeWidth / 2, screenHeight / 2, 0)));

        float diagonal = Mathf.Sqrt(screeWidth * screeWidth + screenHeight * screenHeight);

        float m = pos.magnitude;
        if (m > diagonal)
        {
            if (log) Debug.Log("Here " + m + " " + (diagonal / 2f));
            pos.x = x;
            pos.y = y;
        }
        else if (m > diagonal / 2)
        {
            if(log) Debug.Log("Here " + m + " " + (diagonal / 2f));

            pos = pos.normalized * diagonal / 2;

            Vector3 pos2 = new Vector3(x, y, 0);

            float a = 2f * (m / diagonal - 0.5f);

            pos = pos * (1 - a) + pos2 * a;
        }
        else
        {
            if (log) Debug.Log("Here " + m + " " + (diagonal / 2f));
        }

        float w = -50 + screeWidth / 2;
        float h = -50 + screenHeight / 2;

        if (pos.x > w) pos.x = w;
        if (pos.x < -w) pos.x = -w;
        if (pos.y > h) pos.y = h;
        if (pos.y < -h) pos.y = -h;

        if (pos.y == h)
        {
            transform.localScale = new Vector3(1, 1, 1);
            transform.localRotation = Quaternion.Euler(0, 0, 90);
        }
        else if (pos.y == -h)
        {
            transform.localScale = new Vector3(1, 1, 1);
            transform.localRotation = Quaternion.Euler(0, 0, -90);
        }
        else if (pos.x < w / 2)
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
            transform.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
            transform.localScale = new Vector3(1, 1, 1);
        }

        if (log) Debug.Log("Pos " + pos);

        rt.anchoredPosition = pos;

        //Vector3 dir = rt.localPosition;
        //rt.localRotation = Quaternion.LookRotation(Vector3.forward, dir);
    }

    private void OnDrawGizmos()
    {
        if (player == null) return;

        Gizmos.color = Color.red;

        Gizmos.DrawRay(player.position, direction);
    }
}