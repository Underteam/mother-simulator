﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NextRoomProgress : MonoBehaviour
{
    public Image roomIcon;

    public DayBar barPrefab;

    public Sprite done;
    public Sprite doneTutor;
    public Sprite current;
    public Sprite currTutor;
    public Sprite undone;
    public Sprite undoneTutor;

    public Animator key;

    private List<DayBar> bars = new List<DayBar>();

    public HorizontalLayoutGroup group;

    //TODO костыль
    public Sprite challengesDoorKey;

    public Transform info;
    public Transform infoPos;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        while (!GameState.Instance().started) yield return null;
    }

    public void Apply (int currTask = -1)
    {
        Sprite icon = GameController.Instance().GetNextDoorIcon();
        if (icon != null) roomIcon.sprite = icon;

        int cnt = GameController.Instance().GetLevelsInGroup();
        int nd = GameController.Instance().GetNextDoorInd();
        int pd = nd - cnt;// GameController.Instance().GetPrevDoorInd();
        int curr = GameState.Instance().currentTask;
        if (currTask >= 0) curr = currTask;

        //Debug.LogError("Apply " + currTask + " | " + pd + " " + nd + " " + cnt + " " + curr);

        for (int i = bars.Count; i < cnt; i++)
        {
            var bar = Instantiate(barPrefab);
            bar.transform.SetParent(barPrefab.transform.parent);
            bar.transform.localScale = barPrefab.transform.localScale;
            bars.Add(bar);
        }

        for (int i = 0; i < cnt; i++)
        {
            bool isTutor = GameController.Instance().IsDayWTutor(pd + i);

            if (pd + i < curr)
            {
                if (isTutor)
                    bars[i].body.sprite = doneTutor;
                else
                    bars[i].body.sprite = done;
            }
            else if (pd + i == curr)
            {
                if (isTutor)
                    bars[i].body.sprite = currTutor;
                else
                    bars[i].body.sprite = current;
            }
            else
            {
                if (isTutor)
                    bars[i].body.sprite = undoneTutor;
                else
                    bars[i].body.sprite = undone;
            }

            if (pd + i == 4 && curr < 5) bars[i].SetIcon(challengesDoorKey);
            else bars[i].SetIcon(null);

            bars[i].gameObject.SetActive(true);
        }

        if (curr >= nd)
        {
            //Debug.LogError("Scale");
            key.Play("Scale");
        }

        for (int i = cnt; i < bars.Count; i++)
        {
            bars[i].gameObject.SetActive(false);
        }

        group.CalculateLayoutInputHorizontal();

        if (gameObject.activeInHierarchy) StartCoroutine(Correct());

        //Debug.LogError(pd + " " + nd + " " + cnt);
    }

    private void Update()
    {
        if (info != null) info.position = infoPos.position;    
    }

    public void Set (int from, int to)
    {
        if (gameObject.activeSelf) StartCoroutine(ProgressCoroutine(from, to));
    }

    private IEnumerator Correct ()
    {
        group.spacing += 0.01f;

        yield return null;

        group.spacing -= 0.01f;
    }

    private IEnumerator ProgressCoroutine (int from, int to)
    {
        Apply(from);

        group.spacing += 0.01f;

        yield return null;

        group.spacing -= 0.01f;

        yield return new WaitForSeconds(1f);

        Apply(to);
    }
}
