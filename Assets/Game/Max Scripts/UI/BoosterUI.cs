﻿using Facebook.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoosterUI : MonoBehaviour
{
    public Image icon;

    public GameObject btnBuyWAds;
    public GameObject btnBuyWCrystals;
    public GameObject btnActive;

    public Text price;

    private Booster booster;

    public GameObject pnlDescription;
    public Text lblDescription;
    public Text lblShortDescription;

    private bool hideInfo;

    private bool buyed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnEnable ()
    {
        pnlDescription.SetActive(false);
    }

    bool skip = false;

    // Update is called once per frame
    void Update()
    {
        if(skip)
        {
            skip = false;
            return;
        }

        if (pnlDescription.activeSelf && Input.GetMouseButtonDown(0))
        {
            hideInfo = true;
        }
        else if (hideInfo)
        {
            pnlDescription.SetActive(false);
            hideInfo = false;
        }
    }

    public void ShowInfo ()
    {
        if (!pnlDescription.activeSelf)
        {
            hideInfo = false;
            pnlDescription.SetActive(true);
            skip = true;
        }
        else
        {
            pnlDescription.SetActive(false);
        }
    }

    public void SetBooster (Booster booster)
    {
        if (booster == null)
        {
            gameObject.SetActive(false);
            return;
        }

        //if (booster.currency == Wallet.CurrencyType.ads)
        {
            btnBuyWAds.SetActive(true);
            btnBuyWCrystals.SetActive(false);
            btnActive.SetActive(false);
        }
        //else
        //{
        //    btnBuyWAds.SetActive(false);
        //    btnBuyWCrystals.SetActive(true);
        //}

        icon.sprite = booster.icon;
        price.text = "" + booster.price;

        this.booster = booster;

        lblDescription.text = Localizer.GetString("boosterdesc" + booster.name, booster.longDescription);
        lblShortDescription.text = Localizer.GetString("boostershortdesc" + booster.name, booster.shortDescription);

        onlyForCrystals = false;
        buyed = false;
    }

    private bool onlyForCrystals;
    public void OnlyForCrystals ()
    {
        if (buyed) return;

        btnBuyWAds.SetActive(false);
        btnBuyWCrystals.SetActive(true);
        btnActive.SetActive(false);
        onlyForCrystals = true;
    }

    private bool waitingForAdEnd;
    public void Buy ()
    {
        //if (booster.currency == Wallet.CurrencyType.ads)
        if (!onlyForCrystals)
        {
            waitingForAdEnd = true;
            Time.timeScale = 0f;
            AudioListener.volume = 0f;
            AdsController.Instance().ShowAd(AdsProvider.AdType.unskipablevideo, (b) => 
            {
                Time.timeScale = 1f;
                AudioListener.volume = 1f;

                if (!waitingForAdEnd) return;
                waitingForAdEnd = false;

                if (!b) return;

                booster.Buyed();
                //gameObject.SetActive(false);
                btnBuyWAds.SetActive(false);
                btnBuyWCrystals.SetActive(false);
                btnActive.SetActive(true);
                buyed = true;
                GameController.Instance().ApplyBooster(booster);

                //var eventParams = new Dictionary<string, object>();
                //eventParams[AppEventParameterName.Level] = GameState.Instance().currentTask;
                //eventParams[AppEventParameterName.Description] = booster.name;
                //FB.LogAppEvent("BoosterForAd", 0, eventParams);

                Analytics.Instance().SendEvent("BoosterForAd", 0, AppEventParameterName.Level, GameState.Instance().currentTask, AppEventParameterName.Description, booster.name);

                //AppsFlyer.trackRichEvent("af_ads_success", new Dictionary<string, string>());
                //AppsFlyer.trackRichEvent("af_ads_success_rv", new Dictionary<string, string>());

                Analytics.Instance().SendEvent("af_ads_success");
                Analytics.Instance().SendEvent("af_ads_success_rv");

                GameController.Instance().TrackAdsWatched();
            });
        }
        else
        {
            Wallet w = Wallet.GetWallet(Wallet.CurrencyType.crystals);
            if (w.amount >= booster.price)
            {
                w.Spend(booster.price);
                booster.Buyed();
                //gameObject.SetActive(false);
                btnBuyWAds.SetActive(false);
                btnBuyWCrystals.SetActive(false);
                btnActive.SetActive(true);
                buyed = true;
                GameController.Instance().ApplyBooster(booster);
            }
        }
    }
}
