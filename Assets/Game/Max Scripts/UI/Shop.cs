﻿using Facebook.Unity;
using MoreMountains.NiceVibrations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoSingleton<Shop>
{
    public GameObject shopPanel;

    [System.Serializable]
    public class PanelSettings
    {
        public string title;
        public Image bookmark;
        public Sprite activeBookmark;
        public Sprite inactiveBookmark;
        public GameObject activeIcon;
        public GameObject inactiveIcon;
        public List<GameObject> elements;
        public List<ItemsPack> packs;
        public GameObject exclamation;
    }

    public Text lblRarity;
    public Text lblInfo;
    public List<PanelSettings> panels;
    public List<Image> buttons;
    public List<GameObject> exclamations;
    public GameObject mainExclamation;
    public List<GameObject> knobs;
    public List<GameObject> arrows;

    [System.Serializable]
    public class ItemsPack
    {
        public string name;
        public List<ShopItem> items;
        public int price = 200;
        public int packID;
        public enum Target
        {
            Baby,
            Mother,
        }
        public Target target;
    }

    public List<ShopItemUI> itemsSlots;

    public List<ItemsPack> allPacks { get; private set; } = new List<ItemsPack>();

    public Text category;
    public Text price;

    private bool randomizing;

    public GameObject btnRandomSelect;
    public GameObject btnGetCrystals;

    public Sprite unknown;
    public Sprite activeButton;
    public Sprite inactiveButton;
    public Sprite dices;
    public Sprite crystal;
    public Sprite tv;

    private int currentPanel;
    private int panelPage;
    private Dictionary<int, int> storedPages = new Dictionary<int, int>();
    private Dictionary<KeyValuePair<int, int>, ShopItem.Rarity> storedRarities = new Dictionary<KeyValuePair<int, int>, ShopItem.Rarity>();

    private ShopItem.Rarity rarity = ShopItem.Rarity.usual;

    public bool openAll;

    public Transform fcTarget;

    public bool applySettings = true;

    // Start is called before the first frame update
    void Start()
    {
        if (!inited) Init();
    }

    protected override void Init()
    {
        if (inited) return;

        if (PlayerPrefs.GetInt("ShopVersion", 0) < 1)
        { 
            int packID = 2;

            for (int j = 0; j < 8; j++)
            {
                int itemID = j;
                PlayerPrefs.SetInt("IsSlotActive" + packID + "_" + itemID, 0);
            }

            packID = 3;

            for (int j = 0; j < 8; j++)
            {
                int itemID = j;
                bool active = PlayerPrefs.GetInt("IsSlotActive" + packID + "_" + itemID, 0) == 1;

                if (active) PlayerPrefs.SetInt("IsSlotActive" + (packID - 1) + "_" + itemID, 1);
            }

            PlayerPrefs.SetInt("ShopVersion", 1);
        }

        for (int i = 0; i < panels.Count; i++)
        {
            for (int j = 0; j < panels[i].packs.Count; j++)
            {
                var pack = panels[i].packs[j];
                pack.packID = allPacks.Count;
                allPacks.Add(pack);

                for (int k = 0; k < pack.items.Count; k++)
                {
                    pack.items[k].packID = pack.packID;
                    pack.items[k].itemID = k;
                    if (pack.items[k].active)
                    {
                        PlayerPrefs.SetInt("IsSlotActive" + pack.packID + "_" + k, 1);
                        //Debug.LogError("Set " + pack.packID + " " + k + " " + pack.items[k].name, pack.items[k]);
                    }
                }
            }
        }

        if (applySettings)
        {
            for (int i = 0; i < allPacks.Count; i++)
            {
                //if (allPacks[i].target != ItemsPack.Target.Baby) continue;
                int packID = i;
                int itemID = PlayerPrefs.GetInt("BabySettings" + packID, 0);
                //Debug.LogError("BabySettings " + packID + " " + itemID);
                if (!openAll && PlayerPrefs.GetInt("IsSlotActive" + packID + "_" + itemID, 0) != 1) itemID = 0;
                ApplyItem(packID, itemID);
            }
        }

        ShowPanel(0);

        var wallet = Wallet.GetWallet(Wallet.CurrencyType.crystals);

        wallet.onValueChanged += CheckBuingPosibility;

        inited = true;
    }

    private float shopY = 0;
    private float shopX = 0;
    private float timer;
    void Update()
    {
        shopY = 0.5f * shopY;
        shopY += InputManager.Instance().GetAxis("ShopY");
        shopX = 0.5f * shopX;
        shopX += InputManager.Instance().GetAxis("ShopX");
        if (Input.GetMouseButtonUp(0))
        {
            float y = shopY;
            float x = shopX;
            if (Mathf.Abs(x) > Mathf.Abs(y)) y = 0;
            else x = 0;

            if (y > 1) Next();
            else if (y < -1) Prev();
            else if (x > 1) NextPage();
            else if (x < -1) PrevPage();

        }

        if (numCoins > 0)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                numCoins--;
                timer = 0.025f;
                var coin = Instantiate(currPrefab);
                //Debug.LogError("Instantiate", currPrefab);
                coin.transform.SetParent(currPrefab.transform.parent);
                //Debug.LogError("Set parent", currPrefab.transform.parent);
                coin.transform.position = currPrefab.transform.position;
                coin.transform.localScale = currPrefab.transform.localScale;
                coin.transform.SetParent(transform);
                coin.gameObject.SetActive(true);

                accum += coinCost;
                int add = (int)accum;
                accum -= add;
                if (numCoins == 0) add = sum;
                sum -= add;
                
                Wallet.GetWallet(Wallet.CurrencyType.crystals).Add(add);
            }
        }
    }

    public void Open ()
    {
        storedPages.Clear();
        storedRarities.Clear();

        shopPanel.SetActive(true);

        ShowPanel(0);

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    public void Close ()
    {
        if (randomizing) return;

        storedPages.Clear();
        storedRarities.Clear();

        shopPanel.SetActive(false);

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    public void ShowPanel (int p)
    {
        if (randomizing) return;

        if (currentPanel != p)
        {
            SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
            Haptic.Instance().Play(HapticTypes.MediumImpact);
        }

        currentPanel = p;

        for (int i = 0; i < panels.Count; i++)
        {
            panels[i].bookmark.sprite = panels[i].inactiveBookmark;
            panels[i].activeIcon.SetActive(false);
            panels[i].inactiveIcon.SetActive(true);
            for (int j = 0; j < panels[i].elements.Count; j++) panels[i].elements[j].SetActive(false);
        }

        for (int i = 0; i < buttons.Count; i++)
        {
            buttons[i].gameObject.SetActive(false);
            if (i == 0) buttons[i].sprite = activeButton;
            else buttons[i].sprite = inactiveButton;

            var button = buttons[i].GetComponent<Button>();
            button.onClick.RemoveAllListeners();
            int pageInd = i;
            button.onClick.AddListener(new UnityEngine.Events.UnityAction(() =>
            {
                var key = new KeyValuePair<int, int>(currentPanel, pageInd);
                if (storedRarities.ContainsKey(key))
                    rarity = storedRarities[key];
                else
                    rarity = ShopItem.Rarity.usual;
                ShowPanelPage(pageInd);
            }));
        }

        if (p >= 0 && p < panels.Count)
        {
            var panel = panels[p];
            panel.bookmark.sprite = panel.activeBookmark;
            panel.activeIcon.SetActive(true);
            panel.inactiveIcon.SetActive(false);
            
            for (int i = 0; i < panel.elements.Count; i++) panel.elements[i].SetActive(true);

            for (int i = 0; i < panel.packs.Count; i++)
            {
                if (i >= buttons.Count) break;
                var text = buttons[i].GetComponentInChildren<Text>();
                text.text = Localizer.GetString(panel.packs[i].name, panel.packs[i].name);
                buttons[i].gameObject.SetActive(true);
            }

            {
                int pageInd = 0;
                if (storedPages.ContainsKey(p))
                    pageInd = storedPages[p];

                var key = new KeyValuePair<int, int>(currentPanel, pageInd);
                if (storedRarities.ContainsKey(key))
                    rarity = storedRarities[key];
                else
                    rarity = ShopItem.Rarity.usual;

                ShowPanelPage(pageInd);
            }
        }

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    private string GetRarityName (ShopItem.Rarity rarity)
    {
        if(rarity == ShopItem.Rarity.usual)
        {
            return Localizer.GetString("shoprarityRegular", "Regular");
        }
        else if(rarity == ShopItem.Rarity.rare)
        {
            return Localizer.GetString("shoprarityRare", "Rare");
        }
        else if (rarity == ShopItem.Rarity.unique)
        {
            return Localizer.GetString("shoprarityUnique", "Unique");
        }
        else if(rarity == ShopItem.Rarity.gift)
        {
            return Localizer.GetString("shoppageGifts", "Gifts");
        }

        return rarity.ToString();
    }
    public void ShowPanelPage (int p)
    {
        if (randomizing) return;

        if (p != panelPage)
        {
            SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
            Haptic.Instance().Play(HapticTypes.MediumImpact);
        }

        panelPage = p;
        if (!storedPages.ContainsKey(currentPanel)) storedPages.Add(currentPanel, p);
        else storedPages[currentPanel] = p;

        for (int i = 0; i < buttons.Count; i++)
        {
            if (i == p) buttons[i].sprite = activeButton;
            else buttons[i].sprite = inactiveButton;
        }

        var panel = panels[currentPanel];

        if (p >= 0 && p < panel.packs.Count)
        {
            var pack = panel.packs[p];

            List<ShopItem.Rarity> allTypesOfRarity = new List<ShopItem.Rarity>();
            for (int i = 0; i < pack.items.Count; i++)
            {
                if (!allTypesOfRarity.Contains(pack.items[i].rarity)) allTypesOfRarity.Add(pack.items[i].rarity);
            }

            for (int i = 0; i < knobs.Count; i++) knobs[i].transform.parent.gameObject.SetActive(false);

            //Debug.LogError("Found " + allTypesOfRarity.Count);

            if (allTypesOfRarity.Count == 1)
            {
                rarity = allTypesOfRarity[0];
                for (int i = 0; i < arrows.Count; i++) arrows[i].SetActive(false);
            }
            else
            {
                if (!allTypesOfRarity.Contains(rarity) && allTypesOfRarity.Count > 0) rarity = allTypesOfRarity[0];
                for (int i = 0; i < knobs.Count; i++) knobs[i].transform.parent.gameObject.SetActive(true);
                for (int i = 0; i < arrows.Count; i++) arrows[i].SetActive(true);
            }
        }

        lblRarity.text = GetRarityName(rarity);

        if (rarity == ShopItem.Rarity.unique)
        {
            lblInfo.gameObject.SetActive(true);
            lblInfo.text = Localizer.GetString("shopuniqueDesc", "Unique");
        }
        else if (rarity == ShopItem.Rarity.gift)
        {
            lblInfo.gameObject.SetActive(true);
            lblInfo.text = Localizer.GetString("storeMothergiftbox", "Gifts");
        }
        else
        {
            lblInfo.gameObject.SetActive(false);
        }

        int r = (int)rarity;
        for (int i = 0; i < knobs.Count; i++) knobs[i].SetActive(false);
        if (r >= 0 && r < knobs.Count) knobs[r].SetActive(true);

        for (int i = 0; i < itemsSlots.Count; i++) itemsSlots[i].parent.gameObject.SetActive(false);

        if (p >= 0 && p < panel.packs.Count)
        {
            var pack = panel.packs[p];

            int numdeactive = 0;

            List<ShopItem> items = new List<ShopItem>();
            for (int i = 0; i < pack.items.Count; i++)
            {
                if (pack.items[i].rarity == rarity) items.Add(pack.items[i]);
            }

            for (int i = 0; i < items.Count && i < itemsSlots.Count; i++)
            {
                var item = items[i];
                itemsSlots[i].parent.gameObject.SetActive(true);
                itemsSlots[i].image.sprite = item.image;
                itemsSlots[i].currItem = item;

                bool active = openAll || PlayerPrefs.GetInt("IsSlotActive" + pack.packID + "_" + item.itemID, 0) == 1;
                bool weared = PlayerPrefs.GetInt("BabySettings" + pack.packID, 0) == item.itemID;

                //Debug.LogError("Check " + pack.packID + " (" + item.packID + ") " + item.itemID + " " + weared, item);

                itemsSlots[i].dices.SetActive(false);
                itemsSlots[i].btnBuyWAds.SetActive(false);
                itemsSlots[i].btnBuyWCrystals.SetActive(false);
                itemsSlots[i].btnWear.gameObject.SetActive(false);
                itemsSlots[i].frame.gameObject.SetActive(false);
                itemsSlots[i].btnGet.transform.parent.gameObject.SetActive(false);
                itemsSlots[i].lblInfo.gameObject.SetActive(false);
                itemsSlots[i].imInfo.gameObject.SetActive(false);
                itemsSlots[i].lblName.gameObject.SetActive(false);

                if (item.empty)
                {
                    itemsSlots[i].lblName.gameObject.SetActive(true);
                    itemsSlots[i].lblName.text = Localizer.GetString("ShopEmpty", "EMPTY");
                }

                var priceType = item.priceType;
                if (pack.target == ItemsPack.Target.Mother && priceType == ShopItem.PriceType.Reward)
                {
                    if (PlayerPrefs.GetInt("GiftReady" + item.packID + "_" + item.itemID, 0) == 1)
                    {
                        priceType = ShopItem.PriceType.Ads;
                        //Debug.LogError("---------------------------------> Rready " + item.packID + " " + item.itemID);
                    }
                }

                if (active)
                {
                    itemsSlots[i].btnWear.gameObject.SetActive(true);
                    itemsSlots[i].button.targetGraphic = itemsSlots[i].btnWear.GetComponent<Graphic>();
                    if (weared)
                    {
                        itemsSlots[i].btnWear.enabled = false;
                        itemsSlots[i].lblWear.text = Localizer.GetString("shopWearing", "надето");
                        itemsSlots[i].frame.gameObject.SetActive(true);
                    }
                    else
                    {
                        itemsSlots[i].btnWear.enabled = true;
                        itemsSlots[i].lblWear.text = Localizer.GetString("shopSelection", "надеть");
                    }
                }
                else if (priceType == ShopItem.PriceType.Reward)
                {
                    if (item.priceIcon != null)
                    {
                        itemsSlots[i].btnGet.transform.parent.gameObject.SetActive(true);
                        itemsSlots[i].btnGet.sprite = item.priceIcon;
                        itemsSlots[i].button.targetGraphic = itemsSlots[i].btnGet.GetComponent<Graphic>();
                    }
                    else
                    {
                        itemsSlots[i].imInfo.gameObject.SetActive(true);
                        itemsSlots[i].lblInfo.gameObject.SetActive(true);
                        itemsSlots[i].lblInfo.text = Localizer.GetString(item.info, item.info);
                        itemsSlots[i].imInfo.sprite = item.infoIcon;
                    }
                }
                else if (priceType == ShopItem.PriceType.Dice)
                {
                    itemsSlots[i].dices.SetActive(true);
                }
                else if (priceType == ShopItem.PriceType.Crystals)
                {
                    itemsSlots[i].btnBuyWCrystals.SetActive(true);
                    itemsSlots[i].crystalsPrice.text = "" + item.price;
                    itemsSlots[i].button.targetGraphic = itemsSlots[i].btnBuyWCrystals.GetComponent<Graphic>();
                }
                else
                {
                    itemsSlots[i].btnBuyWAds.SetActive(true);
                    int nwa = PlayerPrefs.GetInt("NumWatchedAds" + item.packID + "_" + item.itemID);
                    itemsSlots[i].adsPrice.text = nwa + "/" + item.price;
                    itemsSlots[i].crystalsPrice.alignment = TextAnchor.MiddleLeft;
                    itemsSlots[i].button.targetGraphic = itemsSlots[i].btnBuyWAds.GetComponent<Graphic>();
                }

                bool hide = false;

                if (active) hide = false;
                else if (priceType == ShopItem.PriceType.Dice) hide = true; 
                else if (pack.target == ItemsPack.Target.Mother && priceType == ShopItem.PriceType.Reward)
                {
                    hide = PlayerPrefs.GetInt("GiftReady" + item.packID + "_" + item.itemID, 0) == 0;
                }

                //Debug.LogError("Check " + pack.packID + "_" + item.itemID + " = " + active + " " + item.name + " " + hide + " " + pack.target + " " + priceType + " " + item.priceType, item);

                if (!hide)//показываем
                {
                    //Debug.LogError("Show");
                    itemsSlots[i].parent.color = Color.white;
                }
                else//прячем
                {
                    //Debug.LogError("Hide");
                    itemsSlots[i].parent.color = Color.white;
                    itemsSlots[i].image.sprite = unknown;
                    numdeactive++;
                }
            }

            if (numdeactive > 0 && rarity == ShopItem.Rarity.usual)
            {
                btnRandomSelect.SetActive(true);
                price.text = "" + GetRNDPrice(currentPanel, panelPage);
            }
            else 
                btnRandomSelect.SetActive(false);

            btnGetCrystals.SetActive((rarity != ShopItem.Rarity.unique && rarity != ShopItem.Rarity.gift));
        }

        CheckBuingPosibility();
    }

    private int GetRNDPrice (int panelID, int pageID)
    {
        var panel = panels[panelID];
        var pack = panel.packs[pageID];
        int basePrice = pack.price;
        int priceStep = pack.price;
        int numSteps = PlayerPrefs.GetInt("ShopRandomsNum" + panelID + "_" + pageID, 0);

        return (basePrice + priceStep * numSteps);
    }

    private void NextRNDPrice (int panelID, int pageID)
    {
        int numSteps = PlayerPrefs.GetInt("ShopRandomsNum" + panelID + "_" + pageID, 0);
        numSteps++;
        PlayerPrefs.SetInt("ShopRandomsNum" + panelID + "_" + pageID, numSteps);
    }

    public void SetRarity (int r)
    {
        int n = System.Enum.GetNames(typeof(ShopItem.Rarity)).Length;

        r = r % n;
        
        rarity = (ShopItem.Rarity)r;

        var key = new KeyValuePair<int, int>(currentPanel, panelPage);
        if (!storedRarities.ContainsKey(key)) storedRarities.Add(key, rarity);
        else storedRarities[key] = rarity;

        ShowPanelPage(panelPage);

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    public void NextPage ()
    {
        var panel = panels[currentPanel];
        int cnt = panel.packs.Count;

        if (panelPage < cnt - 1)
        {
            ShowPanelPage(panelPage + 1);
        }
    }

    public void PrevPage()
    {
        var panel = panels[currentPanel];
        int cnt = panel.packs.Count;

        if (panelPage > 0)
        {
            ShowPanelPage(panelPage - 1);
        }
    }

    public void Next ()
    {
        if (randomizing) return;

        int n = System.Enum.GetNames(typeof(ShopItem.Rarity)).Length;

        int r = (int)rarity;

        r = (r + 1) % n;

        rarity = (ShopItem.Rarity)r;

        var key = new KeyValuePair<int, int>(currentPanel, panelPage);
        if (!storedRarities.ContainsKey(key)) storedRarities.Add(key, rarity);
        else storedRarities[key] = rarity;

        ShowPanelPage(panelPage);

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    public void Prev ()
    {
        if (randomizing) return;

        int n = System.Enum.GetNames(typeof(ShopItem.Rarity)).Length;

        int r = (int)rarity;

        r = (r - 1 + n) % n;

        rarity = (ShopItem.Rarity)r;

        var key = new KeyValuePair<int, int>(currentPanel, panelPage);
        if (!storedRarities.ContainsKey(key)) storedRarities.Add(key, rarity);
        else storedRarities[key] = rarity;

        ShowPanelPage(panelPage);

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    public void CheckBuingPosibility (Wallet w = null)
    {
        if (w == null) w = Wallet.GetWallet(Wallet.CurrencyType.crystals);
        int am = w.amount;

        if (mainExclamation != null) mainExclamation.SetActive(false);
        for (int i = 0; i < panels.Count; i++) panels[i].exclamation.SetActive(false);
        for (int i = 0; i < exclamations.Count; i++) exclamations[i].SetActive(false);

        List<KeyValuePair<int, int>> skip = new List<KeyValuePair<int, int>>();

        for (int i = 0; i < panels.Count; i++)
        {
            var panel = panels[i];
            for (int j = 0; j < panel.packs.Count; j++)
            {
                var kvp = new KeyValuePair<int, int>(i, j);
                if (skip.Contains(kvp))
                {
                    //Debug.LogError("Skip " + i + " " + j);
                    continue;
                }

                var pack = panel.packs[j];
                for (int k = 0; k < pack.items.Count; k++)
                {
                    var item = pack.items[k];

                    if (pack.target == ItemsPack.Target.Mother && item.priceType == ShopItem.PriceType.Reward)
                    {
                        if (PlayerPrefs.GetInt("GiftReady" + item.packID + "_" + item.itemID, 0) == 0)
                        {
                            //Debug.LogError("Not ready " + item.packID + " " + item.itemID);
                            continue;
                        }
                        //Debug.LogError("Guft ready " + (PlayerPrefs.GetInt("IsSlotActive" + item.packID + "_" + item.itemID, 0) != 1));
                    }
                    else if (item.priceType == ShopItem.PriceType.Dice)
                    {
                        int price = GetRNDPrice(i, j);
                        if (price > am) continue;
                    }
                    else if (item.priceType == ShopItem.PriceType.Crystals)
                    {
                        int price = item.price;
                        if (price > am) continue;
                    }
                    else continue;

                    if (PlayerPrefs.GetInt("IsSlotActive" + item.packID + "_" + item.itemID, 0) != 1)
                    {
                        skip.Add(kvp);
                        if (currentPanel == i) exclamations[j].SetActive(true);
                        panel.exclamation.SetActive(true);
                        if (mainExclamation != null) mainExclamation.SetActive(true);
                        //Debug.LogError("Exclamation " + i + " " + j + " " + k + " " + item.name, item);
                        break;
                    }
                }
            }
        }
    }

    public void UpdateVisual ()
    {
        ShowPanelPage(panelPage);
    }

    public void OpenPack (ShopItemsPack pack)
    {
        for(int i = 0; i < pack.items.Count; i++)
        {
            OpenItem(pack.items[i]);
        }
    }

    public void OpenItem (ShopItem item)
    {
        PlayerPrefs.SetInt("IsSlotActive" + item.packID + "_" + item.itemID, 1);
    }

    public bool IsItemOpened (ShopItem item)
    {
        return openAll || PlayerPrefs.GetInt("IsSlotActive" + item.packID + "_" + item.itemID, 0) == 1;
    }

    public void CloseItem (ShopItem item)
    {
        PlayerPrefs.SetInt("IsSlotActive" + item.packID + "_" + item.itemID, 0);

        if (PlayerPrefs.GetInt("BabySettings" + item.packID, 0) == item.itemID)
        {
            ApplyItem(item.packID, 0);
        }
    }

    private bool waitingForAd;
    public void ApplyItem(ShopItem item)
    {
        bool avaliable = openAll || PlayerPrefs.GetInt("IsSlotActive" + item.packID + "_" + item.itemID, 0) == 1;

        var priceType = item.priceType;
        if (allPacks[item.packID].target == ItemsPack.Target.Mother && priceType == ShopItem.PriceType.Reward)
        {
            if (PlayerPrefs.GetInt("GiftReady" + item.packID + "_" + item.itemID, 0) == 1) priceType = ShopItem.PriceType.Ads;
        }

        //Debug.LogError("Apply item " + item + " " + avaliable + " " + item.packID + " " + item.itemID + " " + priceType, item);

        if (!avaliable)
        {
            if (priceType == ShopItem.PriceType.Crystals)
            {
                Wallet w = Wallet.GetWallet(Wallet.CurrencyType.crystals);
                if (w.amount >= item.price)
                {
                    PlayerPrefs.SetInt("IsSlotActive" + item.packID + "_" + item.itemID, 1);
                    w.Spend(item.price);
                    ShowPanelPage(panelPage);
                }
            }
            else if (priceType == ShopItem.PriceType.Ads)
            {
                waitingForAdEnd = true;
                Time.timeScale = 0f;
                AudioListener.volume = 0f;
                Debug.LogError("Watch ad");
                AdsController.Instance().ShowAd(AdsProvider.AdType.unskipablevideo, (b) =>
                {
                    Time.timeScale = 1f;
                    AudioListener.volume = 1f;

                    Debug.LogError("Ad watched");

                    if (!waitingForAdEnd) return;
                    waitingForAdEnd = false;

                    if (!b) return;

                    Analytics.Instance().SendEvent("ItemForAd", 0, "pack_id", item.packID, "item_id", item.itemID);

                    Analytics.Instance().SendEvent("af_ads_success");
                    Analytics.Instance().SendEvent("af_ads_success_rv");

                    GameController.Instance().TrackAdsWatched();

                    int nwa = PlayerPrefs.GetInt("NumWatchedAds" + item.packID + "_" + item.itemID);
                    nwa++;
                    PlayerPrefs.SetInt("NumWatchedAds" + item.packID + "_" + item.itemID, nwa);

                    //Debug.LogError(nwa + " " + item.price);

                    if (nwa >= item.price)
                    {
                        //Debug.LogError("Open item");
                        PlayerPrefs.SetInt("IsSlotActive" + item.packID + "_" + item.itemID, 1);
                    }

                    ShowPanelPage(panelPage);
                });
            }
            else if (priceType == ShopItem.PriceType.Reward)
            {
                item.OnTryToBuy();
            }
        }
        else
        {
            ApplyItem(item.packID, item.itemID);
        }
    }

    public void ApplyPack(ShopItemsPack pack)
    {
        if (pack == null) return;

        for (int i = 0; i < pack.items.Count; i++)
        {
            ApplyItem(pack.items[i]);
        }
    }

    //надеть предмет
    public void ApplyItem(int packID, int itemID)
    {
        //Debug.LogError("Apply item " + packID + " " + itemID);

        ItemsPack pack = null;
        for(int i = 0; i < allPacks.Count; i++)
        {
            if(allPacks[i].packID == packID)
            {
                pack = allPacks[i];
                break;
            }
        }

        if (pack == null) return;

        if (itemID < 0 || itemID >= pack.items.Count) return;

        var item = pack.items[itemID];

        bool active = openAll || PlayerPrefs.GetInt("IsSlotActive" + packID + "_" + itemID, 0) == 1;
        if (!active) return;

        if (inited) SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        if (inited) Haptic.Instance().Play(HapticTypes.MediumImpact);

        PlayerPrefs.SetInt("BabySettings" + packID, itemID);

        item.Apply();

        for (int i = 0; i < itemsSlots.Count; i++)
        {
            if (!itemsSlots[i].btnWear.gameObject.activeSelf) continue;

            bool weared = itemsSlots[i].currItem == item;
            if (weared)
            {
                itemsSlots[i].btnWear.enabled = false;
                itemsSlots[i].lblWear.text = Localizer.GetString("shopWearing", "надето");
                itemsSlots[i].frame.gameObject.SetActive(true);
            }
            else
            {
                itemsSlots[i].btnWear.enabled = true;
                itemsSlots[i].lblWear.text = Localizer.GetString("shopSelection", "надеть");
                itemsSlots[i].frame.gameObject.SetActive(false);
            }
        }
    }

    public void RandomSelect ()
    {
        if (randomizing) return;

        var pack = panels[currentPanel].packs[panelPage];

        Analytics.Instance().SendEvent("BuyShopItem", 0, "Item", pack.name);

        int price  = GetRNDPrice(currentPanel, panelPage);

        var w = Wallet.GetWallet(Wallet.CurrencyType.crystals);

        if (w.amount < price) return;

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);

        w.Spend(price);

        NextRNDPrice(currentPanel, panelPage);
        this.price.text = "" + GetRNDPrice(currentPanel, panelPage);

        StartCoroutine(RandomSelectCoroutine());
    }

    List<Image> images = new List<Image>();
    List<int> indices = new List<int>();

    private IEnumerator RandomSelectCoroutine ()
    {
        randomizing = true;

        yield return null;

        images.Clear();
        indices.Clear();

        for (int i = 0; i < itemsSlots.Count; i++)
        {
            Image img = itemsSlots[i].parent;
            if (!img.gameObject.activeSelf) continue;

            var pack = panels[currentPanel].packs[panelPage];
            bool active = openAll || PlayerPrefs.GetInt("IsSlotActive" + pack.packID + "_" + itemsSlots[i].currItem.itemID, 0) == 1;
            if (!active)
            {
                images.Add(img);
                indices.Add(i);
            }
        }

        int n = Random.Range(38, 42);
        int prev = -1;
        for(int i = 0; images.Count > 1 && i < n; i++)
        {
            int ind = Random.Range(0, images.Count);
            while(ind == prev) ind = Random.Range(0, images.Count);
            prev = ind;

            images[ind].color = Color.gray;

            float delay = 1f / (42 - i);
            if (delay < 0.05f) delay = 0.05f;

            yield return new WaitForSeconds(delay);

            images[ind].color = Color.white;
        }

        FinishRandomize();
    }

    private void FinishRandomize ()
    {
        randomizing = false;

        if (images.Count == 0) return;

        int ind = Random.Range(0, images.Count);

        images[ind].color = Color.white;

        var pack = panels[currentPanel].packs[panelPage];

        PlayerPrefs.SetInt("IsSlotActive" + pack.packID + "_" + itemsSlots[indices[ind]].currItem.itemID, 1);

        ShowPanelPage(panelPage);
    }

    private void OnEnable()
    {
        numCoins = 0;
    }

    private float storedSens;
    private void OnDisable()
    {
        if (randomizing) FinishRandomize();

        Wallet.GetWallet(Wallet.CurrencyType.crystals).Add(sum);
        numCoins = 0;
    }

    public FlyingCoin flyingCoinPrefab;
    private FlyingCoin currPrefab;

    private int numCoins;
    private float coinCost;
    private int sum;
    private float accum = 0;
    bool waitingForAdEnd;
    public void AddCrystals (int num)
    {
        if (randomizing) return;

        waitingForAdEnd = true;
        Time.timeScale = 0f;
        AudioListener.volume = 0f;
        AdsController.Instance().ShowAd(AdsProvider.AdType.unskipablevideo, (b) =>
        {
            Time.timeScale = 1f;
            AudioListener.volume = 1f;

            if (!waitingForAdEnd) return;
            waitingForAdEnd = false;

            if (!b) return;

            numCoins = (int)Mathf.Lerp(1, 20, num / 100f);
            sum = num;
            coinCost = 1f;
            accum = 0;

            currPrefab = flyingCoinPrefab;

            var w = Wallet.GetWallet(Wallet.CurrencyType.crystals);

            Analytics.Instance().SendEvent("CrystalsForAd", 0, AppEventParameterName.Level, GameState.Instance().currentTask, "fb_balance", w.amount);

            Analytics.Instance().SendEvent("af_ads_success");
            Analytics.Instance().SendEvent("af_ads_success_rv");

            GameController.Instance().TrackAdsWatched();
        });

        SFXPlayer.Instance().Play(SFXLibrary.Instance().buttonClip, false, true);
        Haptic.Instance().Play(HapticTypes.MediumImpact);
    }

    public void BuyIAP (IAP iap)
    {
        //Debug.LogError("Buy " + iap.iapName, iap);

        if (iap.buyed)
        {
            var pack = iap.GetComponent<ShopItemsPack>();
            ApplyPack(pack);
        }
        else
        {
            iap.Buy();
        }
    }

    public List<ShopItem> GetUnbuyedItems (ShopItem.PriceType priceType)
    {
        if (!inited) Init();

        List<ShopItem> result = new List<ShopItem>();

        //Debug.LogError("Check " + allPacks.Count + " packs");

        for (int i = 0; i < allPacks.Count; i++)
        {
            var pack = allPacks[i];
            for (int j = 0; j < pack.items.Count; j++)
            {
                var item = pack.items[j];
                if (item.priceType != priceType) continue;
                bool buyed = PlayerPrefs.GetInt("IsSlotActive" + item.packID + "_" + item.itemID, 0) == 1;
                //Debug.Log("Check " + item.itemName + " " + buyed, item);
                if (!buyed) result.Add(item);
            }
        }

        return result;
    }

    public void PackBuyed (ShopItemsPack pack)
    {
        bool alreadyBought = PlayerPrefs.GetInt("PackBought" + pack.packName, 0) == 1;

        OpenPack(pack);

        if (!alreadyBought)
        {
            PlayerPrefs.SetInt("PackBought" + pack.packName, 1);
            ApplyPack(pack);
        }
    }

    public void CrystalsBuoght (CrystalsPack pack)
    {
        //Debug.LogError("Bought " + pack.packID);

        bool alreadyBought = PlayerPrefs.GetInt("CrystalsPackBought" + pack.packID, 0) == 1;
        if (alreadyBought && pack.onlyOnce) return;

        Wallet w = Wallet.GetWallet(Wallet.CurrencyType.crystals);
        w.Add(pack.amount);

        PlayerPrefs.SetInt("CrystalsPackBought" + pack.packID, 1);

        FlyingCoin.Run(pack.fcPrefab, 20, fcTarget, 0.1f, null);
    }

    public void OpenVIP ()
    {
        VIPAccess.Instance().Show();
    }
}
