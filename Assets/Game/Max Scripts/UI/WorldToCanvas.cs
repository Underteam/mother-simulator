﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class WorldToCanvas : MonoBehaviour
{
    public Transform target;

    public Vector3 shift;

    [SerializeField]
    private Image image;
    private List<Sprite> sprites = new List<Sprite>();

    public Camera cam;

    private RectTransform rt;

    private Vector2 size;

    public bool hideWhenVisible;

    public bool stickToEdge = true;

    public Text lblInfo;

    public Animator anim;

    private float timer;
    private int spriteInd = 0;

    public float addRotation = 90;

    public Transform player;

    // Start is called before the first frame update
    void Start()
    {
        if (cam == null) cam = Camera.main;

        rt = transform as RectTransform;

        rt.anchorMin = Vector2.zero;
        rt.anchorMax = Vector2.zero;

        size = new Vector2(rt.localScale.x * Screen.width / rt.lossyScale.x, rt.localScale.y * Screen.height / rt.lossyScale.y);
    }

    private bool side;
    private bool behind;
    private float m;
    private float dot;
    private Vector2 distToEdge;
    private Vector3 p1Test;
    private Vector3 p2Test;

    private void Update()
    {
        if (sprites.Count == 0)
        {
            if (image.gameObject.activeSelf) image.gameObject.SetActive(false);
            return;
        }
        else
        {
            if (!image.gameObject.activeSelf) image.gameObject.SetActive(true);
        }

        timer += Time.deltaTime;
        if (timer > 0.25f)
        {
            spriteInd = (spriteInd + 1) % sprites.Count;
            image.sprite = sprites[spriteInd];
            timer = 0;
        }
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        if (target == null)
        {
            if (!hiding) Hide();
            return;
        }

        if (player != null)
        {
            Vector3 direction = player.position - target.position;
            direction.y = 0;
            float dist = direction.magnitude;
            dist = Mathf.Clamp(dist, 3, 8);
            float scale = Mathf.Lerp(1.0f, 0.7f, (dist - 3) / 5);
            transform.localScale = scale * Vector3.one;
        }

        Vector3 s = rt.lossyScale;
        s.x /= rt.localScale.x;
        s.y /= rt.localScale.y;
        s.z /= rt.localScale.z;

        float gap = 10;

        var p1 = cam.WorldToScreenPoint(target.position + shift);
        var p2 = cam.WorldToViewportPoint(target.position + shift);

        //p1.x = gap + (size.x - 2 * gap) * p2.x;
        //p1.y = gap + (size.y - 2 * gap) * p2.y;
        //p1.z = 0;

        p1.x /= s.x;
        p1.y /= s.y;
        p1.z = 0;

        p1Test = p1;
        p2Test = p2;

        var dir = (target.position + shift - cam.transform.position).normalized;
        dot = Vector3.Dot(dir, cam.transform.forward);

        Vector2 targetPos = p1;//обычная позиция
        Quaternion targetRot = Quaternion.Euler(0, 0, addRotation);

        bool hide = false;// hideWhenVisible;

        if (lblInfo != null) lblInfo.text = p1 + " | " + p2 + " | " + dot + " | ";

        behind = false;

        {
            Vector2 d1 = new Vector2((p2.x - 0.5f) * size.x, (p2.y - 0.5f) * size.y);
            Vector2 d2 = size / 2;

            float x = d1.x / (d2.x);
            float y = d1.y / (d2.y);

            float dist = d2.x - Mathf.Abs(d1.x);//расстояние от края экрана
            if (Mathf.Abs(y) > Mathf.Abs(x))
            {
                dist = d2.y - Mathf.Abs(d1.y);
            }

            distToEdge = new Vector2(d2.x - Mathf.Abs(d1.x), d2.y - Mathf.Abs(d1.y));

            x = d1.x / (d2.x - gap);
            y = d1.y / (d2.y - gap);
            m = Mathf.Max(Mathf.Abs(x), Mathf.Abs(y));

            behind = m > 1.2f || dot < 0.01f;

            if (stickToEdge && dist <= gap)//за краем экрана
            {
                side = true;

                d1 = new Vector2((p2.x - 0.5f) * size.x, (p2.y - 0.5f) * size.y);
                d1.x *= size.x / (size.x - 0.5f * gap);
                d1.y *= size.y / (size.y - 0.5f * gap);

                if (m > 1) d1 /= m;

                p1.x = d1.x + size.x / 2;
                p1.y = d1.y + size.y / 2;

                hide = false;
            }
            else
                side = false;

            targetPos = p1;

            float k = Mathf.Lerp(1f, 0f, (1 - m) / 0.1f);

            var q1 = Quaternion.Euler(0, 0, addRotation);
            var q2 = Quaternion.LookRotation(Vector3.forward, -d1) * Quaternion.Euler(0, 0, 90);

            if (stickToEdge) targetRot = Quaternion.Lerp(q1, q2, k);

            {
                float sign = 1;
                if (Vector3.Dot(dir, cam.transform.right) < 0) sign = -1;

                p1 = new Vector3(-sign * gap + (1 + sign) * size.x / 2, size.y / 2);

                var q = Quaternion.Euler(0, 0, 90 + sign * 90);

                k = (dot - 0.03f) / 0.13f;
                targetPos = Vector2.Lerp(p1, targetPos, k);
                targetRot = Quaternion.Lerp(q, targetRot, k);

                if (k <= 0) hide = false;
            }
        }

        if (false/*hideWhenVisible*/)
        {
            if (hide)
            {
                if (hited) hide = false;

                if (hide)
                {
                    targetPos = 2 * size;
                }
            }
        }

        if (behind && !stickToEdge) targetPos = 2 * size;
        //if (stickToEdge) anim.SetBool("Side", side);

        rt.anchoredPosition = targetPos;
        rt.localRotation = targetRot;
    }

    private bool hited;
    private void FixedUpdate()
    {
        hited = false;

        if (false/*hideWhenVisible*/)
        {
            Vector3 dir = (target.position + shift - cam.transform.position);
            float dist = dir.magnitude;
            dir.Normalize();
            ray = new Ray (cam.transform.position, dir);
            colides.Clear();
            var list = Physics.RaycastAll(ray);
            for (int i = 0; i < list.Length; i++)
            {
                colides.Add(list[i].distance);
                if (list[i].transform.IsChildOf(PlayerController.instance.transform)) continue;
                if (list[i].distance < dist - 0.1f)
                {
                    hited = true;
                }
            }
        }
    }

    public List<float> colides;
    private Ray ray;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(ray.origin, cam.transform.position + ray.direction * 5);
    }

    //private void OnEnable()
    //{
    //    Debug.Log("Enabled", this);
    //}

    //private void OnDisable()
    //{

    //}

    public bool hiding { get; private set; }
    public void Hide ()
    {
        //Debug.Log("Hide " + this, this);

        target = null;
        GetComponent<Animator>().SetTrigger("Hide");
        hiding = true;
    }

    public void Hided ()
    {
        hiding = false;
        gameObject.SetActive(false);
        stickToEdge = true;
    }

    public void SetSprites (List<Sprite> sprites)
    {
        if (sprites == this.sprites) return;

        if (sprites == null) sprites = new List<Sprite>();

        this.sprites = sprites;
        
        if (sprites.Count == 0)
        {
            if (image.gameObject.activeSelf) image.gameObject.SetActive(false);
        }
        else
        {
            if (!image.gameObject.activeSelf) image.gameObject.SetActive(true);
            image.sprite = sprites[0];
        }

        timer = 0;
    }
}
