﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomName : MonoBehaviour
{
    public Text lblName;

    public Switcher switcher;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        while (switcher != null && !switcher.inited) yield return null;

        if (switcher != null) lblName.text = switcher.description;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
