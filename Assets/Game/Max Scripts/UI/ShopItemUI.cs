﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopItemUI : MonoBehaviour
{
    public Image parent;
    public Image image;
    public Image frame;
    
    public GameObject dices;

    public Image btnWear;
    public Text lblWear;

    public Image btnGet;

    public GameObject btnBuyWCrystals;
    public Text crystalsPrice;
    public GameObject crystalIcon;

    public GameObject btnBuyWAds;
    public Text adsPrice;

    public ShopItem currItem { get; set; }

    //сплошные костыли
    public Text lblInfo;
    public Image imInfo;

    public Text lblName;

    public Button button;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Apply ()
    {
        Shop.Instance().ApplyItem(currItem);
    }
}
