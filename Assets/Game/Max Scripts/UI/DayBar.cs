﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DayBar : MonoBehaviour
{
    public Image body;
    public Image eventIcon;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetIcon (Sprite icon)
    {
        if (eventIcon == null) return;

        eventIcon.sprite = icon;
        if (icon != null)
            eventIcon.transform.parent.gameObject.SetActive(true);
        else
            eventIcon.transform.parent.gameObject.SetActive(false);
    }
}
