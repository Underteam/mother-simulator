﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ShopItem : MonoBehaviour
{
    public string itemName;

    public Sprite image;

    public UnityEvent onApply;
    public UnityEvent onTryToBuy;

    public enum Rarity
    {
        usual,
        rare,
        unique,
        gift,
    }

    public Rarity rarity;

    public enum PriceType
    {
        Dice,
        Crystals,
        Ads,
        Reward,
    }

    public PriceType priceType;
    public Sprite priceIcon;
    public int price;
    public bool hide;
    public bool active;
    public bool empty;

    public string info;
    public Sprite infoIcon;

    public int itemID { get; set; }
    public int packID { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Apply ()
    {
        if (onApply != null) onApply.Invoke();
    }

    public void OnTryToBuy ()
    {
        onTryToBuy?.Invoke();
    }
}
