﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopExclamation : MonoBehaviour
{
    public GameObject mark;

    public Shop shop;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        while (!shop.inited) yield return null;

        var wallet = Wallet.GetWallet(Wallet.CurrencyType.crystals);

        wallet.onValueChanged += Check;

        Check(wallet);
    }

    public void Check (Wallet w)
    {
        //Debug.LogError("Check " + w.amount + " " + shop);

        int am = w.amount;

        bool canBuy = false;

        for (int i = 0; !canBuy && i < shop.allPacks.Count; i++)
        {
            if (am >= shop.allPacks[i].price)
            {
                for (int j = 0; j < shop.allPacks[i].items.Count; j++)
                {
                    if (PlayerPrefs.GetInt("IsSlotActive" + i + "_" + j, 0) != 1)
                    {
                        //Debug.Log("Can buy " + i + " " + j);
                        canBuy = true;
                        break;
                    }
                }
            }
        }

        mark.SetActive(canBuy);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
