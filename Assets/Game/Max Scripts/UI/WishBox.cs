﻿using MoreMountains.NiceVibrations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class WishBox : MonoBehaviour
{
    public Text lblWish;

    public Image imgDone;

    public Image imgBG;

    public Sprite uncompleted;
    public Sprite completed;
    public Sprite error;

    public RectTransform subListRT;

    private RectTransform rt;

    public List<Subline> sublines;

    public float height { get { return 10 + Mathf.Max(36, subListRT.gameObject.activeSelf ? subListRT.rect.height : 0); } }
    
    public bool changed { get; set; }

    public Wish myWish { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Complete ()
    {
        imgBG.sprite = completed;
        imgDone.gameObject.SetActive(true);

        for (int i = 0; i < sublines.Count; i++) CompleteSubline(i);

        Invoke("HideSublines", 1f);
    }

    public void CompleteSubline(int i)
    {
        if (i >= 0 || i < sublines.Count)
        {
            sublines[i].image.sprite = completed;
            SFXPlayer.Instance().Play(SFXLibrary.Instance().lineCompleted);
            Haptic.Instance().Play(HapticTypes.Success);
        }
    }

    public void ErrorSubline(int i)
    {
        if (i >= 0 || i < sublines.Count) sublines[i].image.sprite = error;
    }

    public void UncompliteSubline(int i)
    {
        if (i >= 0 || i < sublines.Count) sublines[i].image.sprite = uncompleted;
    }

    public void UnComplete()
    {
        imgBG.sprite = uncompleted;
        imgDone.gameObject.SetActive(false);
    }

    public string info;

    private float prevH = -1;

    void Update ()
    {
        if (rt == null) rt = transform as RectTransform;

        if (subListRT == null || rt == null) return;

        info = "" + subListRT.sizeDelta + " " + subListRT.rect.height;

        float w = rt.sizeDelta.x;
        float h = Mathf.Max(36, subListRT.rect.height);
        if (!subListRT.gameObject.activeSelf) h = 36;

        if (prevH < 0 || !Mathf.Approximately(h, prevH))
        {
            rt.sizeDelta = new Vector2(w, h);
            prevH = h;
            changed = true;
        }
        else changed = false;
    }

    public void Apply (Wish wish)
    {
        //Debug.LogError("Apply " + wish.description + " " + wish.name, wish);
        //for (int i = 0; i < wish.subTasks.Count; i++) Debug.Log(i + " ) " + wish.subTasks[i]);

        myWish = wish;
        wish.box = this;

        lblWish.text = Localizer.GetString("wish" + wish.eventName, wish.description);

        for (int i = 0; i < sublines.Count; i++) sublines[i].gameObject.SetActive(false);

        for (int i = sublines.Count; i < wish.subTasks.Count; i++)
        {
            var box = Instantiate(sublines[0]);
            box.transform.SetParent(sublines[0].transform.parent);
            box.transform.localScale = sublines[0].transform.localScale;
            sublines.Add(box);
        }

        for(int i = 0; i < wish.subTasks.Count && i < sublines.Count; i++)
        {
            sublines[i].gameObject.SetActive(true);
            sublines[i].image.sprite = uncompleted;
            sublines[i].text.text = Localizer.GetString("wish" + wish.eventName + "Sub" + (i+1), wish.subTasks[i]);
        }

        Update();
    }

    public void SetSubline (int ind , string line)
    {
        for (int i = sublines.Count; i <= ind; i++)
        {
            var box = Instantiate(sublines[0]);
            box.transform.SetParent(sublines[0].transform.parent);
            box.transform.localScale = sublines[0].transform.localScale;
            sublines.Add(box);
        }

        sublines[ind].gameObject.SetActive(true);
        sublines[ind].image.sprite = uncompleted;
        sublines[ind].text.text = line;
    }

    public void InsertSubline(int ind, string line)
    {
        {
            var box = Instantiate(sublines[0]);
            box.transform.SetParent(sublines[0].transform.parent);
            box.transform.localScale = sublines[0].transform.localScale;
            sublines.Add(box);
        }

        for (int i = sublines.Count - 1; i > ind; i--)
        {
            sublines[i].image.sprite = sublines[i - 1].image.sprite;
            sublines[i].text.text = sublines[i - 1].text.text;
        }

        for (int i = sublines.Count; i <= ind; i++)
        {
            var box = Instantiate(sublines[0]);
            box.transform.SetParent(sublines[0].transform.parent);
            box.transform.localScale = sublines[0].transform.localScale;
            sublines.Add(box);
        }

        sublines[ind].gameObject.SetActive(true);
        sublines[ind].image.sprite = uncompleted;
        sublines[ind].text.text = line;
    }

    public void SetNumVisibleSublines (int n)
    {
        for (int i = 0; i < n && i < sublines.Count; i++) sublines[i].gameObject.SetActive(true);
        for (int i = n; i < sublines.Count; i++) sublines[i].gameObject.SetActive(false);

        Update();
    }

    public void HideSublines ()
    {
        for (int i = 0; i < sublines.Count; i++) sublines[i].gameObject.SetActive(false);

        Update();
    }
}
