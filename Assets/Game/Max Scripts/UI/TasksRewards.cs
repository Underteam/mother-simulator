﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TasksRewards : MonoSingleton<TasksRewards>
{
    public GameObject panel;

    public FlyingCoin fcPrefab;
    public Transform fcTarget;

    public Image imgReward;
    public Text lblReward;

    // Start is called before the first frame update
    void Start()
    {
        int unpaid = PlayerPrefs.GetInt("TasksReward", 0);
        if (unpaid > 0)
        {
            Wallet w = Wallet.GetWallet(Wallet.CurrencyType.crystals);
            w.Add(unpaid);
            PlayerPrefs.SetInt("TasksReward", 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private int reward;
    public void Show(int reward)
    {
        MenuQueue.Instance().Show(panel, 10);

        lblReward.text = "" + reward;
        this.reward = reward;

        int unpaid = PlayerPrefs.GetInt("TasksReward", 0);
        PlayerPrefs.SetInt("TasksReward", unpaid + reward);
    }

    public void Claim ()
    {
        PlayerPrefs.SetInt("TasksReward", 0);
        Wallet w = Wallet.GetWallet(Wallet.CurrencyType.crystals);
        w.Add(reward);

        FlyingCoin.Run(fcPrefab, 20, fcTarget, 0.05f, () =>
        {
            Close();
        });   
    }

    public void Close ()
    {
        panel.SetActive(false);
    }
}
