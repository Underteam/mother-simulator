﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public interface IBlocker
{
    bool blocked { get; }
}

public class UIBlocker : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IBlocker
{
    public bool blocked { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
#if !UNITY_EDITOR
        if (Input.touchCount == 0) blocked = false;
#endif
    }

    private void LateUpdate()
    {
        if (removeBlock)
        {
            blocked = false;
            removeBlock = false;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        blocked = true;
        removeBlock = false;
    }

    private bool removeBlock;
    public void OnPointerUp(PointerEventData eventData)
    {
        removeBlock = true;
    }

    private void OnDisable()
    {
        blocked = false;
    }
}
