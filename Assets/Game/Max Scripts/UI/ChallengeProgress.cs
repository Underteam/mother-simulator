﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class ChallengeProgress : MonoBehaviour
{
    public RectTransform container;

    public RectTransform bar;

    private RectTransform rt;

    public float progress;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (rt == null) rt = transform as RectTransform;

        var rect = container.rect;

        rt.sizeDelta = new Vector2(rect.width, rt.sizeDelta.y);

        if (bar != null) bar.sizeDelta = new Vector2(Mathf.Clamp(progress, 0f, 1f) * rect.width, bar.sizeDelta.y);

        Vector3 pos = rt.localPosition;
        pos.x = container.localPosition.x;
        rt.localPosition = pos;
    }
}
