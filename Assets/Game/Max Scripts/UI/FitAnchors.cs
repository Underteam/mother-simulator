﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class FitAnchors : MonoBehaviour {

    public AspectRatioFitter fitter;

    private RectTransform rt;

    public float addWidth;

    public bool log;

	// Use this for initialization
	void Start ()
    {
        DoTheJob();
    }

    private void OnEnable()
    {
        DoTheJob();
    }

    // Update is called once per frame
    void Update ()
    {
        DoTheJob();
	}

    public void DoTheJob()
    {
        if (fitter == null) return;
        if (rt == null) rt = GetComponent<RectTransform>();
        if (rt == null) return;

        if (fitter.aspectMode == AspectRatioFitter.AspectMode.HeightControlsWidth)
        {
            float a1 = fitter.aspectRatio;

            //anchor rect size
            float width = rt.rect.width - rt.sizeDelta.x;
            float height = rt.rect.height - rt.sizeDelta.y;

            float a2 = ((float)width / height);

            float a3 = (rt.rect.width + addWidth) / rt.rect.height;

            if (log) Debug.Log(width + " " + height + " " + a1 + " " + a2 + " " + a3);

            if (a3 > a2)
            {
                //rt.sizeDelta = new Vector2(0, 0);
                float h = (width - addWidth) / a1;

                float sdy = h - height;
                rt.sizeDelta = new Vector2(0, sdy);
            }
            else
            {
                float h = rt.rect.height;
                rt.sizeDelta = new Vector2(0, 0);
            }

            Canvas.ForceUpdateCanvases();
        }
    }
}
