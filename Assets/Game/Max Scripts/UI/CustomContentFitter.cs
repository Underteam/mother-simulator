using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vector3 = System.Numerics.Vector3;

public class CustomContentFitter : MonoBehaviour
{
    [SerializeField] private bool refreshOnStart;
    [SerializeField] private VerticalLayoutGroup verticalLayoutGroup;
    [SerializeField] private LayoutGroup group;
    private Vector2 _baseSize;

    private void OnValidate()
    {
        var rectTransform = (RectTransform) transform;
        _baseSize = rectTransform.sizeDelta;
        verticalLayoutGroup = GetComponent<VerticalLayoutGroup>();
        rectTransform.pivot = new Vector2(0,1);
    }

    private void Start()
    {
        if (refreshOnStart)
        {
            RefreshSize();
        }
    }

    public void RefreshSize()
    {
        var rectTransform = (RectTransform) transform;
        RefreshTransformSize(rectTransform);
    }

    private void RefreshTransformSize(RectTransform subTransform)
    {
        var bounds = new RectBounds();
        var contentFitter = subTransform.GetComponent<CustomContentFitter>();
        bool isContentFitter = contentFitter != null;

        foreach (RectTransform child in subTransform)
        {
            if (!child.gameObject.activeSelf)
            {
                continue;
            }

            RefreshTransformSize(child);
            if (!isContentFitter)
            {
                continue;
            }

            if (contentFitter.verticalLayoutGroup != null)
            {
                // update vertical layout with new child size
                contentFitter.verticalLayoutGroup.SetLayoutVertical();
            }

            var anchoredPosition = child.anchoredPosition;
            bounds.Encapsulate(new Vector2(anchoredPosition.x, -anchoredPosition.y), child.sizeDelta);
        }

        if (!isContentFitter)
        {
            return;
        }

        bounds.Encapsulate(Vector2.zero, contentFitter._baseSize);
        subTransform.sizeDelta = bounds.max - bounds.min;
    }

    private class RectBounds
    {
        public Vector2 min;
        public Vector2 max;

        public RectBounds()
        {
            min = Vector2.positiveInfinity;
            max = Vector2.negativeInfinity;
        }

        public RectBounds(Vector2 position, Vector2 size)
        {
            min = position;
            max = new Vector2(position.x + size.x, position.y + size.y);
        }

        public void Encapsulate(Vector2 position, Vector2 size)
        {
            var min = position;
            var max = new Vector2(position.x + size.x, position.y + size.y);

            if (min.x < this.min.x)
            {
                this.min.x = min.x;
            }

            if (max.x > this.max.x)
            {
                this.max.x = max.x;
            }

            if (min.y < this.min.y)
            {
                this.min.y = min.y;
            }

            if (max.y > this.max.y)
            {
                this.max.y = max.y;
            }
        }
    }
}