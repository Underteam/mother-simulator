﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Special for Ilya
public class SuperPuperProgressBar : MonoBehaviour
{
    public Image pb;

    public ProgressBar copyFrom;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        pb.fillAmount = copyFrom.val;
    }
}
