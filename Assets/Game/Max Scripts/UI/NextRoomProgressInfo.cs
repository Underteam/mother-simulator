﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class NextRoomProgressInfo : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
    public GameObject menu;

    public Text info;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        int n = GameController.Instance().GetLevelsToNextDoor();
        if (n <= 0) return;

        Debug.Log("n " + n);

        string message = Localizer.GetString("havetopass", "Пройдите еще %% уровней");
        message = message.Replace("%%", n.ToString());
        info.text = message;

        message = Localizer.GetString("roomclosedmessage", "Для того чтобы открыть\n\r%% пройдите");
        string roomname = Localizer.GetString("closedroom" + GameController.Instance().GetNextDoorName(), GameController.Instance().GetNextDoorName());
        message = message.Replace("%%1", roomname);
        message = message.Replace("%%2", "" + (GameController.Instance().GetNextDoorInd() + 1));
        info.text = (message);

        menu.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        menu.SetActive(false);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        menu.SetActive(false);
    }
}
