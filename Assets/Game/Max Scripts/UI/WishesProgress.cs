﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class WishesProgress : MonoBehaviour
{
    public HorizontalLayoutGroup group;

    public List<Image> icons;

    public float iconSize = 64;
    public float spacing = 36;
    public float padding = 32;

    public Sprite done;
    public Sprite current;
    public Sprite unknown;

    private RectTransform rt;

    public int mode = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public string info;

    // Update is called once per frame
    void Update()
    {
        if (group != null)
        {
            if (rt == null) rt = group.GetComponent<RectTransform>();

            if (mode == 0)
            {
                group.padding.left = (int)(-iconSize / 2);

                int n = 0;
                for (int i = 0; i < icons.Count; i++)
                {
                    if (icons[i].gameObject.activeInHierarchy) n++;
                }

                if (n > 1)
                {
                    group.spacing = (rt.rect.width - (n - 1) * iconSize) / (n - 1);
                }
                else
                {
                    group.spacing = 0;
                }

                info = n + " " + rt.rect.width;
            }
            else
            {
                int n = 0;
                for (int i = 0; i < icons.Count; i++)
                {
                    if (icons[i].gameObject.activeInHierarchy) n++;
                }

                if (n > 1)
                {
                    float w = rt.rect.width;
                    var size = rt.sizeDelta;
                    float w2 = padding + (n - 1) * (iconSize + spacing);
                    size.x -= (w - w2);
                    rt.sizeDelta = size;

                    group.padding.left = (int)(padding - iconSize / 2);
                    group.spacing = (rt.rect.width - padding - (n - 1) * iconSize) / (n - 1);
                }
                else
                {
                    var size = rt.sizeDelta;
                    size.x = iconSize / 2f;
                    rt.sizeDelta = size;

                    group.padding.left = (int)(- 1 * iconSize / 4);
                    group.spacing = 0;
                }
            }
        }
    }

    public void SetNumVisible (int n)
    {
        for(int i = 0; i < icons.Count; i++)
        {
            icons[i].transform.parent.gameObject.SetActive(false);
        }

        for (int i = 0; i < icons.Count && i < n; i++)
        {
            icons[i].transform.parent.gameObject.SetActive(true);
        }
    }

    public void SetState (int ind, int state)
    {
        if (ind < 0 || ind >= icons.Count) return;

        for (int i = 0; i < ind; i++)
        {
            icons[i].transform.parent.gameObject.SetActive(true);
        }

        if (state == 0)
            icons[ind].sprite = unknown;
        else if(state == 1)
            icons[ind].sprite = current;
        else if (state == 2)
            icons[ind].sprite = done;
    }
}
