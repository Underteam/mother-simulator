﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DayProgress : MonoBehaviour
{
    public Text lblDay;

    public Image imgState;

    public RectTransform wishesPanel;
    public Vector2 showPos;
    public Vector2 hidePos;
    private Vector2 targetPos;

    public List<WishBox> lblWishes;

    public bool showed { get; private set; }

    public ContentSizeFitter fitter;
    public VerticalLayoutGroup layout;

    // Start is called before the first frame update
    void Start()
    {
        wishesPanel.anchoredPosition = showPos;
        showed = false;
        imgState.transform.localScale = new Vector3(1, -1, 1);

        Apply();
    }

    // Update is called once per frame
    void Update()
    {
        wishesPanel.anchoredPosition = Vector2.Lerp(wishesPanel.anchoredPosition, targetPos, 10 * Time.deltaTime);

        bool changed = false;
        for (int i = 0; i < lblWishes.Count; i++)
        {
            changed = changed || lblWishes[i].changed;
            lblWishes[i].changed = false;
        }

        if (changed)
        {
            fitter.SetLayoutVertical();
            layout.CalculateLayoutInputVertical();
            layout.SetLayoutVertical();
            //Debug.LogError("Recalc");
            layout.spacing += 0.01f;

            LayoutRebuilder.ForceRebuildLayoutImmediate(layout.transform as RectTransform);
            Canvas.ForceUpdateCanvases();
            LayoutRebuilder.MarkLayoutForRebuild(layout.transform as RectTransform);

            layout.enabled = false;
            layout.enabled = true;
        }
        else
            layout.spacing = 10f;
    }

    public void Switch ()
    {
        showed = !showed;

        Apply();
    }

    private List<WishBox> active = new List<WishBox>();
    public void Apply (bool instant = false)
    {
        active.Clear();
        for (int i = 0; i < lblWishes.Count; i++)
        {
            if (lblWishes[i].gameObject.activeSelf)
            {
                active.Add(lblWishes[i]);
            }
        }

        float height = 0;
        for (int i = 0; i < active.Count - 1; i++)
        {
            height += lblWishes[i].height;
        }

        hidePos = new Vector2(0, height);

        targetPos = showed ? showPos : hidePos;
        imgState.transform.localScale = new Vector3(1, showed ? -1 : 1, 1);

        if (instant)
        {
            wishesPanel.anchoredPosition = targetPos;
        }
    }
}
