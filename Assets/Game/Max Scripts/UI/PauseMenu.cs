﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //public float storedValue { get; set; }
    public void Switch ()
    {
        if (gameObject.activeSelf)
            Close();
        else
            Show();
    }

    public void Show ()
    {
        gameObject.SetActive(true);
        //storedValue = Time.timeScale;
        Time.timeScale = 0f;

        //Debug.LogError("Show " + storedValue);
    }

    public void Close ()
    {
        gameObject.SetActive(false);
        Time.timeScale = 1;// storedValue;

        //Debug.LogError("Close " + storedValue);
    }

    public void Exit ()
    {
        Time.timeScale = 1;// storedValue;
        if (!AdsController.Instance().noAds) PlayerPrefs.SetInt("ShowInterstitial", 1);
        GameController.Instance().Exit();
    }

    public void Restart ()
    {
        Time.timeScale = 1;// storedValue;
        if (!AdsController.Instance().noAds) PlayerPrefs.SetInt("ShowInterstitial", 1);
        GameController.Instance().Restart();
    }

    public void Finish ()
    {
        gameObject.SetActive(false);
        TaskManager.Instance().Finish();
        Time.timeScale = 1;// storedValue;
    }

    private void OnEnable()
    {
        AudioListener.volume = 0;
    }

    private void OnDisable()
    {
        AudioListener.volume = 1;
    }
}
