﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewDoorMenu : MonoBehaviour
{
    public Text title;

    public Text doorName;
    public Image doorIcon;

    public System.Action onClose;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        GameController.Instance().confetti.SetActive(true);
    }

    public void Show (string name, Sprite icon)
    {
        MenuQueue.Instance().Show(gameObject, 3);

        doorName.text = name;
        doorIcon.sprite = icon;
    }
    
    public void Open ()
    {
        gameObject.SetActive(false);

        if (onClose != null) onClose();

        //GameController.Instance().OpenNewDoor();
    }
}
