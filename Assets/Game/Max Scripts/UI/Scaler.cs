﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Scaler : MonoBehaviour
{
    private RectTransform rt;

    public float maxScale = 1f;
    public float minScale = 0.75f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (rt == null) rt = transform as RectTransform;

        float baseAR = 16f / 9f;
        float minAR = 4f / 3f;

        float ar = (float)Screen.width / Screen.height;

        if (ar < baseAR)
        {
            float k = (baseAR - ar) / (baseAR - minAR);

            transform.localScale = (maxScale - (1-minScale) * k) * Vector3.one;
            //rt.sizeDelta = new Vector2(480 * k, 0);
        }
        else
        {
            transform.localScale = Vector3.one;
            //rt.sizeDelta = Vector3.zero;
        }
    }
}
