﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddCrystals : MonoBehaviour
{
    public int amount = 1000;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Add ()
    {
        Wallet w = Wallet.GetWallet(Wallet.CurrencyType.crystals);
        w.Add(amount);
    }
}
