﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomClosed : MonoBehaviour
{
    public Text lblInfo;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //Debug.LogError("Drop");
            Clicker.Instance().down = false;
            Clicker.Instance().drop = true;
        }
    }

    public void Show (string message)
    {
        lblInfo.text = message;
        gameObject.SetActive(true);
    }

    private void OnDisable()
    {

        //Debug.LogError("Drop");
        Clicker.Instance().down = false;
        Clicker.Instance().drop = true;
    }
}
