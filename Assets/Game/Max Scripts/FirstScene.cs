﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FirstScene : MonoBehaviour
{
    private AsyncOperation ao;

    private float timer = 0;
    private float time = 1f;

    public Image progress;

    private void Awake()
    {
        Localizer.Initialize(Application.systemLanguage.ToString());
    }

    // Start is called before the first frame update
    void Start()
    {
        GameAnalyticsSDK.GameAnalytics.Initialize();

        PlayerPrefs.SetInt("ShowChallengeMenu", 0);

        if(!PlayerPrefs.HasKey("ShowVIP")) PlayerPrefs.SetInt("ShowVIP", 1);
        else if (PlayerPrefs.GetInt("ShowVIP", 0) == 0) PlayerPrefs.SetInt("ShowVIP", -1);
        else if (PlayerPrefs.GetInt("ShowVIP", 0) == -1) PlayerPrefs.SetInt("ShowVIP", 1);

        ao = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
        ao.allowSceneActivation = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (ao == null)
        {
            return;
        }

        timer += Time.deltaTime;

        float max = 0.9f;
        if (timer < time)
        {
            float p = Mathf.Max(max * timer / time, ao.progress);
            progress.fillAmount = Mathf.Min(timer / time, p);
        }
        else
        {
            ao.allowSceneActivation = true;
            progress.fillAmount = Mathf.Max(progress.fillAmount, ao.progress);
        }
    }
}
