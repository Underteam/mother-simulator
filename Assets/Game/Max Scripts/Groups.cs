﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Groups : MonoSingleton<Groups>
{
    public static TutorialManager.ObjectGroup copiedGroup;

    [System.Serializable]
    public class ObjectGroup
    {
        public string name;
        public List<GameObject> objects;

        public CopyPaster copypaster;

        public ObjectGroup ()
        {
            copypaster = new CopyPaster(null, Paste, null, null, null);
        }

        public void Paste ()
        {
            if (copiedGroup != null)
            {
                name = copiedGroup.name;
                objects = new List<GameObject>();
                for (int i = 0; i < copiedGroup.objects.Count; i++)
                {
                    objects.Add(copiedGroup.objects[i]);
                }
            }
        }
    }

    public List<ObjectGroup> groups;

    protected override void Init()
    {
        base.Init();
    }

    public void SetGroupState(string group, bool state)
    {
        for (int i = 0; i < groups.Count; i++)
        {
            if (groups[i].name.Equals(group))
            {
                for (int j = 0; j < groups[i].objects.Count; j++)
                {
                    //Debug.LogError("Set state " + state, groups[i].objects[j]);
                    if (groups[i].objects[j].activeSelf != state) groups[i].objects[j].SetActive(state);
                }
            }
        }
    }

    public List<GameObject> GetGroup(string name)
    {
        for (int i = 0; i < groups.Count; i++)
        {
            if (groups[i].name.Equals(name))
            {
                return groups[i].objects;
            }
        }

        return null;
    }
}
