﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangingTable : MonoBehaviour, IAppropriatanceProvider
{
    public Transform sitPoint;

    public Baby baby { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            if (baby != null && i.GetComponent<Baby>() == baby)
            {
                baby.diaperInterface.gameObject.SetActive(false);
                baby = null;
            }
        },
        "ItemTaked");
        listeners.Add(listener);

        listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            if (baby != null)
            {
                baby.diaperInterface.gameObject.SetActive(false);
                baby = null;
            }
        },
        "BabyAboutToTake");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LieBaby (Item baby)
    {
        if (baby == null) return;

        this.baby = baby.GetComponent<Baby>();

        //Debug.Log("Lie " + this.baby, this.baby);

        if (this.baby != null)
        {
            this.baby.onReleased = () =>
            {
                //Debug.LogError("Released");
                //UnityEditor.EditorApplication.isPaused = true;
                //PlayerController.instance.log = true;
                this.baby.transform.position = sitPoint.position;
                this.baby.transform.rotation = sitPoint.rotation;
                this.baby.Lie();
                this.baby.onReleased = null;
                EventManager.Instance().TriggerEvent("ChangingTable");

                if (this.baby.diaper.activeSelf)
                    this.baby.diaperInterface.gameObject.SetActive(true);
            };

            PlayerController.instance.ReleaseItem();
        }
    }

    public bool IsAppropriate(Item item)
    {
        if (item == null) return false;
        return item.GetComponent<Baby>() != null;
    }
}
