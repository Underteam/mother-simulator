﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorKey : MonoBehaviour
{
    public System.Action<DoorKey> onCollect;

    public Transform door;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponentInParent<PlayerController>();
        Debug.LogError("Enter " + other, other);

        if (player != null)
        {
            if (onCollect != null) onCollect(this);
            gameObject.SetActive(false);
        }
    }
}
