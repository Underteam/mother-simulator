﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Toy : MonoBehaviour
{
    public string toyName;

    public Transform posHandler;
    public Transform leftHandHandler;
    public Transform rightHandHandler;

    public Sprite icon;

    public bool oneHand;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
