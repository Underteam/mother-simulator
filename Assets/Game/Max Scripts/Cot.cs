﻿using GMATools.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cot : MonoBehaviour, IAppropriatanceProvider
{
    public Transform sitPoint;

    public Baby baby { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        var listener = EventManager.Instance().AddListener((n, d) =>
        {
            if (this == null) return;

            Item i = (Item)d;
            if (i == null) return;

            if (i.GetComponent<Baby>() == baby) baby = null;
        },
        "ItemTaked");
        listeners.Add(listener);
    }

    private List<EventManager.EventHandler> listeners = new List<EventManager.EventHandler>();
    private void OnDestroy()
    {
        for (int i = 0; i < listeners.Count; i++) EventManager.Instance().DetachListener(listeners[i]);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LieBaby(Item baby)
    {
        if (baby == null) return;

        if (GameState.Instance().state == GameState.State.Game && !TaskManager.Instance().currentTask.currentWish.eventName.Equals("Sleep")) return;

        this.baby = baby.GetComponent<Baby>();

        //Debug.Log("Lie " + this.baby, this.baby);

        if (this.baby != null)
        {
            this.baby.onReleased = () =>
            {
                //Debug.LogError("Released");
                //UnityEditor.EditorApplication.isPaused = true;
                //PlayerController.instance.log = true;
                this.baby.transform.position = sitPoint.position;
                this.baby.transform.rotation = sitPoint.rotation;
                this.baby.Lie();
                this.baby.onReleased = null;
                if (GameState.Instance().state == GameState.State.Game && TaskManager.Instance().currentTask.currentWish.eventName.Equals("Sleep"))
                {
                    this.baby.Happy();
                    baby.busy = true;
                }
                EventManager.Instance().TriggerEvent("BabyEvent", "Sleep", true);
            };

            PlayerController.instance.ReleaseItem();
        }
    }

    public bool IsAppropriate(Item item)
    {
        if (item == null) return false;
        
        return item.GetComponent<Baby>() != null;
    }
}
