﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteAlways]
public class BoneCopyPaster : MonoBehaviour
{
    private static List<Transform> transforms = new List<Transform>();
    private static List<Vector3> positions = new List<Vector3>();
    private static List<Quaternion> rotations = new List<Quaternion>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [Sirenix.OdinInspector.Button]
    public void Copy ()
    {
        transforms.Clear();
        positions.Clear();
        rotations.Clear();

        transforms.AddRange(GetComponentsInChildren<Transform>());

        for(int i = 0; i < transforms.Count; i++)
        {
            positions.Add(transforms[i].localPosition);
            rotations.Add(transforms[i].localRotation);
        }
    }

    [Sirenix.OdinInspector.Button]
    public void Paste ()
    {
        for (int i = 0; i < transforms.Count; i++)
        {
            transforms[i].localPosition = positions[i];
            transforms[i].localRotation = rotations[i];
        }
    }
}
