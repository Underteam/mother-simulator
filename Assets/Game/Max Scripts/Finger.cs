﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Finger : MonoBehaviour
{
    public RectTransform glow;
    public GameObject hint;

    public Text hintText;

    private RectTransform highlightedButton;
    private string text;

    public RectTransform rt;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Set (RectTransform btn, string text)
    {
        this.text = text;
        highlightedButton = btn;

        rt.position = btn.position;

        if (gameObject.activeSelf)
        {
            OnEnable();
        }
    }

    public float k1 = 0.8f;
    public float k2 = 1f;
    public float s;

    private void OnEnable()
    {
        if (highlightedButton == null)
            glow.gameObject.SetActive(false);
        else
        {
            glow.gameObject.SetActive(true);
            glow.sizeDelta = highlightedButton.sizeDelta + 30 * Vector2.one;
            glow.position = highlightedButton.position;
        }

        if (string.IsNullOrEmpty(text))
            hint.SetActive(false);
        else
        {
            hint.SetActive(true);
            hintText.text = text;
            if (highlightedButton != null)
            {
                s = highlightedButton.sizeDelta.magnitude;
                hint.transform.position = highlightedButton.position;
                (hint.transform as RectTransform ).anchoredPosition += k1 * (new Vector2(-s, k2 * s));
            }
        }
    }

    private void OnDisable()
    {
        if(glow != null)
            glow.gameObject.SetActive(false);

        if(hint != null)
            hint.SetActive(false);
    }
}
