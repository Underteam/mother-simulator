﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using I2.Loc;

public class Localizer : MonoBehaviour {

	public Dictionary<string, string> dictionary = new Dictionary<string, string>();

	private static Localizer instance;

	private static string language;

	private static Localization[] localizations;

    private static Localization currentLocalization;

    public static bool Inited() 
	{
		return instance != null;
	}

	public static string GetString(string sName, string defaultValue, string overrideLang = null) {

        //if (sName.Equals("wash") || defaultValue.Equals("Помыть")) Debug.LogError("Get " + sName + " " + defaultValue);

        if (instance == null)
        {
			Initialize(Application.systemLanguage.ToString());
			//Initialize("English");
			Debug.LogWarning ("Localizer not initialized");
			//return "";
		}

        string res;
        if (LocalizationManager.TryGetTranslation(sName, out res, true, 0, true, false, null, overrideLang))
        {
            res = res.Replace("\\n", "\n");
            return res;
        }
        else
        {
            defaultValue = defaultValue.Replace("\\n", "\n");
            return defaultValue;// LocalizationManager.GetTranslation(sName);
        }

        if (!instance.dictionary.ContainsKey (sName)) {
			//Debug.LogWarning("String " + sName + " doesn't exist in " + language);
			//Debug.LogError(sName + "\t" + defaultValue);
			KeysCatcher.Instance().AddKey(sName, defaultValue);
			return defaultValue;
		}

		return instance.dictionary [sName];
	}

	public static void SetString(string sName, string str) {
		
		if (instance == null) {
			Initialize(Application.systemLanguage.ToString());
			//Initialize("English");
			Debug.LogWarning ("Localizer not initialized");
			//return;
		}
		
		if (instance.dictionary.ContainsKey (sName))
			instance.dictionary [sName] = str;
		else
			instance.dictionary.Add (sName, str);

	}

	public static void Initialize(string lang) {

        Debug.Log ("Initialize localizer " + lang);

		if (instance == null)
        {
			GameObject go = new GameObject("Localizer");
			DontDestroyOnLoad(go);
			instance = go.AddComponent<Localizer>();
		}

        LocalizationManager.InitializeIfNeeded();
        LocalizationManager.CurrentLanguage = lang;

        instance.dictionary.Clear();

		if (lang != null)
			language = lang;
		else
			language = Application.systemLanguage.ToString();

        if (localizations == null) localizations = Resources.LoadAll<Localization> ("Localization");

		bool localized = false;
		for (int i = 0; i < localizations.Length; i++) {
			//Debug.Log("Localization " + localizations[i].name + " " + localizations[i].language + " " + language + " " + (localizations[i].language == language));
			if (localizations[i].language == language) {
				localized = true;
                currentLocalization = localizations[i];
                instance.ReadFromAsset(localizations[i].asset);
			}
		}

		if (!localized) {
			language = "English";
			for (int i = 0; i < localizations.Length; i++) {
				//Debug.Log ("Localization " + localizations [i].name + " " + localizations [i].language + " " + language);
				if (localizations [i].language == language) {
					localized = true;
                    currentLocalization = localizations[i];
                    instance.ReadFromAsset (localizations [i].asset);
				}
			}
		}

		if (!localized) Debug.LogError("Localizations failes not found!");
	}

	public int ReadFromAsset(TextAsset asset) {

		if(asset == null) return 0;

		string[] lines = asset.text.Split ('\n');
		
		if(lines.Length < 1) return 0;
		
		for(int i = 0; i < lines.Length; i++) {
			
			string[] cells = lines[i].Split('\t');
			
			if(cells.Length < 2) continue;

			cells[1] = cells[1].Replace("\\n", "\n");

			SetString(cells[0], cells[1]);

			//Debug.Log ("Set " + cells [0] + " -> " + cells [1]);
		}

		//Debug.Log ("Parse " + lines.Length);

		//for (int i = 0; i < lines.Length; i++) Debug.Log (lines [i]);

		return lines.Length;
	}

	public static string Language() {
	
		return language;
	}

    public static List<string> GetLanguages()
    {
        return LocalizationManager.GetAllLanguages();

        List<string> result = new List<string>();

        var localizations = Resources.LoadAll<Localization>("Localization");

        for (int i = 0; i < localizations.Length; i++)
        {
            result.Add (localizations[i].language);
        }

        return result;
    }

    public static Sprite GetLanguageIcon (string lang = null)
    {
        if (lang != null)
        {
            var translation = LocalizationManager.GetTranslation("LanguageIcon", true, 0, true, false, null, lang);
            Sprite sprite = LocalizationManager.GetTranslatedObject<Sprite>(translation);
            return sprite;
        }
        else
        {
            var translation = LocalizationManager.GetTranslation("LanguageIcon", true, 0, true, false, null, language);
            Sprite sprite = LocalizationManager.GetTranslatedObject<Sprite>(translation);
            return sprite;
        }

        if (currentLocalization != null) return currentLocalization.icon;

        return null;
    }

    private static T FindTranslatedObject<T>(string value) where T : Object
    {
        if (string.IsNullOrEmpty(value))
            return null;

        T obj = LocalizationManager.FindAsset(value) as T;
        if (obj)
            return obj;

        obj = ResourceManager.pInstance.GetAsset<T>(value);
        return obj;
    }
}
