﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Localizator : MonoBehaviour {

	public List<Text> lables;
	public List<string> names;

	// Use this for initialization
	void Start ()
    {
        Localize();
	}
	
    public void Localize ()
    {
        for (int i = 0; i < lables.Count && i < names.Count; i++)
            lables[i].text = Localizer.GetString(names[i], lables[i].text);
    }

	// Update is called once per frame
	void Update () {
	
	}
}
