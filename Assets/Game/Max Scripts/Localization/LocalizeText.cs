﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class LocalizeText : MonoBehaviour
{
    private Text textField;

    public string key;

    // Start is called before the first frame update
    void Start()
    {
        Localize();
    }

    public void Localize ()
    {
        textField = GetComponent<Text>();

        textField.text = Localizer.GetString(key, textField.text);

        if (key == "") Debug.LogError("???", this);
    }
}
