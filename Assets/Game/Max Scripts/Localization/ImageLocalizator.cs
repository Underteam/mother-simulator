﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ImageLocalizator : MonoBehaviour
{
    [System.Serializable]
    public class Setting
    {
        public SystemLanguage language;
        public Sprite sprite;
    }

    public List<Setting> settings;

    public Image img;

    // Start is called before the first frame update
    void Start()
    {
        if (img == null) img = GetComponent<Image>();

        var lang = Localizer.Language();

        for (int i = 0; i < settings.Count; i++)
        {
            if (lang.Equals(settings[i].language.ToString()))
            {
                img.sprite = settings[i].sprite;
                return;
            }
        }

        if (settings.Count > 0)
        {
            img.sprite = settings[0].sprite;
        }
    }
}
