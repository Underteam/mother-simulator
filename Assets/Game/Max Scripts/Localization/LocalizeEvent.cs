﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LocalizeEvent : MonoBehaviour
{
    [System.Serializable]
    public class LocalizedEvent
    {
        public string language;
        public UnityEvent onLocalize;
    }

    public List<LocalizedEvent> events;

    // Start is called before the first frame update
    void Start()
    {
        var lang = Localizer.Language();
        //Debug.LogError("Language: " + lang);

        bool def = true;
        for (int i = 0; i < events.Count; i++)
        {
            if (events[i].language.Equals("Any"))
            {
                if (events[i].onLocalize != null) events[i].onLocalize.Invoke();
            }
            else if (events[i].language.Equals(lang))
            {
                if (events[i].onLocalize != null) events[i].onLocalize.Invoke();
                def = false;
            }
        }

        if (def)
        {
            for (int i = 0; i < events.Count; i++)
            {
                if (events[i].language.Equals("Default"))
                {
                    if (events[i].onLocalize != null) events[i].onLocalize.Invoke();
                }
            }
        }
    }
}
