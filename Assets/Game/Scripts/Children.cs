﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Children : MonoBehaviour {

    private Animator animator;
    private Mooder mooder;
    public RagdollHelper ragdoll;
    Collider colliderT;

    public cakeslice.Outline[] childrenOutline;
    public float outlineDistance = 5;

    public AudioSource audioSource;

    public AudioClip clipSad;
    public AudioClip clipHappy;
    public AudioClip clipCry;
    public AudioClip clipTalk;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        mooder = GetComponentInChildren<Mooder>();
        ragdoll = GetComponent<RagdollHelper>();
        colliderT = GetComponent<Collider>();
    }

    private void FixedUpdate()
    {
        SetChildOutlane(Vector3.Distance(Game.instance.player.transform.position, transform.position) > outlineDistance);

        if (Game.instance.WDYW.emotionDown && audioSource.isPlaying == false)
        {
            if (Game.instance.WDYW.emotionValue < 0.3f && Game.instance.WDYW.emotionValue > 0.01f)
            {
                Cry();
            }
            else
            {
                audioSource.PlayOneShot(clipTalk);
            }
        }
    }

    void SetChildOutlane(bool state)
    {
        for (int i = 0; i < childrenOutline.Length; i++)
        {
            childrenOutline[i].enabled = state;
        }
    }

    public void TriggerActive(bool active)
    {
        colliderT.enabled = active;
    }

    public void Sad()
    {
        animator.SetTrigger("Sad");
        audioSource.PlayOneShot(clipSad);
    }

    public void Happy()
    {
        animator.SetTrigger("Happy");
        audioSource.PlayOneShot(clipHappy);
    }

    public void Cry()
    {
        animator.SetTrigger("Cry");
        audioSource.PlayOneShot(clipCry);
    }

    [EditorButton]
    public void Ragdoll()
    {
        //animator.enabled = false;
        ragdoll.ragdolled = true;
        colliderT.enabled = false;

        Rigidbody[] rbs = GetComponentsInChildren<Rigidbody>(true);
        for (int i = 0; i < rbs.Length; i++)
        {
            rbs[i].isKinematic = false;
            rbs[i].velocity = Vector3.zero;
            rbs[i].angularVelocity = Vector3.zero;
        }
    }

    [EditorButton]
    public void Lie()
    {
        colliderT.enabled = true;
        ragdoll.ragdolled = false;
        gameObject.SetActive(true);
        //animator.enabled = true;
        animator.SetInteger("StateMain", 0);
    }

    [EditorButton]
    public void Sit()
    {
        colliderT.enabled = true;
        ragdoll.ragdolled = false;
        gameObject.SetActive(true);
        //animator.enabled = true;
        animator.SetInteger("StateMain", 1);
        //Debug.Log("Children Sit");
    }
}
