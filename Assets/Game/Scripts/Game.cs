﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game : MonoBehaviour {

    private static Game _instance;
    public static Game instance { get { if (_instance == null) _instance = FindObjectOfType<Game>(); return _instance; } }

    public GameObject children;
    public GameObject player;
    public WhatDoYouWant WDYW;

    public GameObject menuCamera;

    public GameObject panelMenu;
    public GameObject panelGame;
    public GameObject panelPause;
    public GameObject panelFinish;

    public AudioSource hitSoundPrefabs;
    public GameObject BtnContinueAds;

    public Text textFinish;
    public Text textScore;
    public Text textScoreAdd;
    private int _score;
    public int score
    {
        get { return _score; }
        set
        {
            _score = value;
            if (_score < 0) _score = 0;
            textScore.text = score.ToString();
        }
    }

    public void AddScore(int count)
    {
        score += count;
        if (score < 0) score = 0;
        if (score > 0)
        {
            textScoreAdd.text = (count > 0 ? "+" : "-") + Mathf.Abs(count).ToString();
            LeanTween.alphaText(textScoreAdd.rectTransform, 1, 1).setOnComplete(() =>
            {
                LeanTween.alphaText(textScoreAdd.rectTransform, 0, 1);
            });
        }
    }

    private static int state = 0;
    private bool revive = false;
    

    public bool isGame = false;

    private void Awake()
    {
        if(textScoreAdd) LeanTween.alphaText(textScoreAdd.rectTransform, 0, 0.01f);

        if (state == 0) // Меню
        {
            panelMenu.SetActive(true);
            panelGame.SetActive(false);
            menuCamera.SetActive(true);
            children.SetActive(false);
            player.SetActive(false);
            isGame = false;
        }
        else if (state == 1) // Новая игра
        {
            panelMenu.SetActive(false);
            panelGame.SetActive(true);
            menuCamera.SetActive(false);
            children.SetActive(true);
            player.SetActive(true);
            
            WDYW.emotionValue = 1;
            WDYW.GetWantRandom();

            isGame = true;
        }

        UnPause();
    }

    public void Menu()
    {
        state = 0;
        SceneManager.LoadScene(0);
    }

    public void NewGame()
    {
        state = 1;
        SceneManager.LoadScene(0);
    }

    public void Continue()
    {
        Debug.Log("Continue!");
        WDYW.emotionValue = 1;
        isGame = true;
        WDYW.active = true;
        state = 1;
        panelGame.SetActive(true);
        panelPause.SetActive(false);
        panelFinish.SetActive(false);
        revive = true;
        UnPause();
    }

    public void GameOver()
    {
        //Debug.Log("GameOver");
        if (isGame == false || state != 1) return; 
        state = 2;
        textFinish.text = string.Format("Score: {0}", score);
        panelGame.SetActive(false);
        panelPause.SetActive(false);
        panelFinish.SetActive(true);
        isGame = false;
        WDYW.active = false;

        if (score > HighScore.Score)
        {
            HighScore.Score = score;
        }

        if (revive)
        {
            BtnContinueAds.SetActive(false);
        }

        ShowAds();
    }

    public void Pause()
    {
        Time.timeScale = 0;
        panelPause.SetActive(true);

        isGame = false;

        ShowAds();
    }

    public void UnPause()
    {
        Time.timeScale = 1;
        panelPause.SetActive(false);

        isGame = true;
    }

    public void ShowAds()
    {
        if (IAPNoAds.noAds) return;
        AdsController.Instance().ShowAd(AdsProvider.AdType.skipablevideo);
    }

    public void ShowContinueAds()
    {
        AdsController.Instance().onVideoFinished = Continue;
        AdsController.Instance().TryToShowVideoBanner();
    }
}
