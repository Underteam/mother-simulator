﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirtyScript : MonoBehaviour {

    public MeshRenderer[] meshRenderers;

    public UnityEngine.Events.UnityEvent onClear;
    public UnityEngine.Events.UnityEvent onDirty;

    public bool isClean = false; // чистота

    public bool canBeClean = true;

    private float dirty;

    private IEnumerator Start()
    {
        if (GameState.Instance().state == GameState.State.Game)
        {
            while (!GameState.Instance().started) yield return null;

            //Debug.LogError("Start " + GameController.Instance().dirtyBooster, this);

            if (GameController.Instance().dirtyBooster) isClean = true;
        }

        if (isClean) Clear();
        else Dirty();
    }

    private void Update()
    {
        if (!PlayerController.instance.handReachedOut) return;

        if (canBeClean && !isClean && waterCollider != null)
        {
            dirty -= Time.deltaTime;
            SetValue(dirty);
            if (dirty <= 0.1f) Clear();
        }    
    }

    // Чистим
    public void Clear()
    {
        //Debug.LogError("Clean", this);
        isClean = true;
        SetValue(0);
        if (onClear != null) onClear.Invoke();
    }

    // Пачкаем
    public void Dirty()
    {
        //Debug.LogError("Dirty", this);

        isClean = false;
        dirty = 1f;
        SetValue(1);
        if (onDirty != null) onDirty.Invoke();
    }

    void SetValue(float value)
    {
        for (int i = 0; i < meshRenderers.Length; i++)
        {
            MessMaterial(meshRenderers[i], value);
        }
    }

    void MessMaterial(MeshRenderer meshRenderer, float value)
    {
        for(int i = 0; i < meshRenderer.materials.Length; i++)
            meshRenderer.materials[i].SetFloat("_DecalIntensity", value);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Floor"))
        {
            if (GameState.Instance().state != GameState.State.Game || !GameController.Instance().dirtyBooster) Dirty();
        }
    }

    private Collider waterCollider;

    private void OnTriggerEnter(Collider other)
    {
        var water = other.GetComponent<Water>();

        if (water != null) waterCollider = other;
    }

    private void OnTriggerExit(Collider other)
    {
        if (waterCollider != null && other == waterCollider) waterCollider = null;
    }
}
