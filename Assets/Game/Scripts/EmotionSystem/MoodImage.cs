﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoodImage : MonoBehaviour {

    public Sprite[] sprites;

    public Transform target;
    public Image image;
    public Canvas canvas;
    private RectTransform rt;

    [Range(0, 2)]
    public int currentSprite = 0;
    private int oldCurrentSprite = -1;

    public Vector3 offset = new Vector3(0, 2, 0);
    [Range(0, 1)]
    public float size = 0.5f;
    private float oldSize = -1;
    float emoji_size;

    private void OnValidate()
    {
        if (oldCurrentSprite != currentSprite)
        {
            oldCurrentSprite = currentSprite;
            Set(currentSprite);
        }
        if (Mathf.Approximately(oldSize, size) == false)
        {
            oldSize = size;
            ChangeSize();
        }
    }

    private void Awake()
    {
        if (image == null) image = GetComponent<Image>();
        if (canvas == null) canvas = GetComponentInParent<Canvas>();
        if (rt == null) rt = GetComponent<RectTransform>();

        Set(currentSprite);
        //ChangeSize();
    }

    void ChangeSize()
    {
        emoji_size = Screen.width * size;
        RectTransform rt = GetComponent<RectTransform>();
        //rt.sizeDelta = new Vector2(emoji_size, emoji_size) / 100;
    }

    public void Set(int id)
    {
        id = Mathf.Clamp(id, 0, sprites.Length);

        if (image)
        {
            image.sprite = sprites[id];

            if (LeanTween.isTweening(image.gameObject)) return;
            if (id == sprites.Length - 1)
            {
                //if (Mathf.Approximately(image.color.a, 0))
                //{
                //    LeanTween.alpha(image.GetComponent<RectTransform>(), 1, 1);
                //}
                //else
                {
                    LeanTween.alpha(image.GetComponent<RectTransform>(), 0, 1).setDelay(1);
                }
            }
            else
            {
                if (Mathf.Approximately(image.color.a, 0)) LeanTween.alpha(image.GetComponent<RectTransform>(), 1, 1);
            }
        }
    }

    private void Update()
    {
        if (target)
        {
            rt.anchoredPosition = GetPos();
        }
        else
        {
            Destroy(gameObject);
        }
    }


    Vector3 GetPos()
    {
        Vector3 screen_point = canvas.worldCamera.WorldToScreenPoint(target.position + offset) / canvas.scaleFactor;
        //Debug.Log((target.position + offset) + " -> " + screen_point + " (" + this + ")", this);
        return new Vector3(screen_point.x, screen_point.y, screen_point.z);
    }

}
