﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoodSystem : MonoBehaviour {

    private static MoodSystem _instance;
    public static MoodSystem instance { get { if (_instance == null) _instance = FindObjectOfType<MoodSystem>(); return _instance; } }

    public float mood = 0;
    public float moodDownSpeed = 0.001f;

    public MoodSlider moodSlider;
    public GameObject moodTextPrefab;
    public GameObject moodIconPrefab;
    public Transform moodIconsParent;
    public bool showMoodIcons { get { return moodIconsParent.gameObject.activeInHierarchy; } set { moodIconsParent.gameObject.SetActive(value); } }

    public Dictionary<int, MoodImage> moodImages = new Dictionary<int, MoodImage>();


    [System.Serializable]
    public class MoodItem
    {
        public string name;
        public float value;
    }
    public List<MoodItem> moodItems = new List<MoodItem>();
    float GetItemValue(string itemName)
    {
        for (int i = 0; i < moodItems.Count; i++)
        {
            if (moodItems[i].name.Equals(itemName)) return moodItems[i].value;
        }
        return 0;
    }

    public void Init()
    {
        LoadMoods();
        if (autoSave) StartCoroutine(AutoSave());
    }

    #region Save / Load

    float LoadMood(int id) { return PlayerPrefs.GetFloat("Mood" + id, 0); }
    void SaveMood(int id, float value) { PlayerPrefs.SetFloat("Mood" + id, value); }

    [EditorButton]
    void LoadMoods()
    {
        mood = LoadMood(0);
    }

    [EditorButton]
    void SaveMoods()
    {
        SaveMood(0, mood);
        PlayerPrefs.Save();
    }

    public bool autoSave = false;
    public float timeStepToSave = 5;
    IEnumerator AutoSave()
    {
        WaitForSeconds wfs = new WaitForSeconds(timeStepToSave);
        while (true)
        {
            yield return wfs;
            SaveMoods();
        }
    }

    #endregion

    public void Add(string itemName)
    {
        float value = GetItemValue(itemName);

        float oldValue = mood;
        float newValue = mood + value;
        //LeanTween.value(gameObject, (float v) => { moods[animalID] = v; }, oldValue, newValue, 1f);
        //moods[animalID] += value;
        mood = newValue;
        AddAnim(Mathf.RoundToInt(value * 1000));
    }


    [EditorButton]
    public void Add(float count)
    {
        mood += count;
        mood = Mathf.Clamp01(mood);
    }

    public void AddAnim(int count)
    {
        //Debug.Log("Add " + count);
        // Создаем объект
        GameObject newGO = Instantiate(moodTextPrefab, moodIconsParent.parent);
        // Ставим в нужное место
        newGO.transform.position = moodSlider.transform.position + Vector3.down * 3 + Vector3.right * 13;
        // Текст
        UnityEngine.UI.Text text = newGO.GetComponentInChildren<UnityEngine.UI.Text>();
        text.text = string.Format("+{0}", count);
        // Включаем
        newGO.SetActive(true);
        // Двигаем
        LeanTween.moveLocalY(newGO, newGO.transform.localPosition.y + 150, 1);
        // Удаляем в альфу
        LeanTween.alphaText(text.GetComponent<RectTransform>(), 0, 1);
        // Удаляем
        Destroy(newGO, 1);
    }

    private void Update()
    {
        if (mood > 0)
        {
            mood -= Time.deltaTime * moodDownSpeed;
            mood = Mathf.Clamp01(mood);
        }
    }
}
