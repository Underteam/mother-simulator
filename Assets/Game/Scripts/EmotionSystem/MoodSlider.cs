﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoodSlider : MonoBehaviour {

    public Sprite[] sprites;
    public float[] values;

    public Slider slider;
    public Image image;

    [Range(0, 1f)]
    public float value = 0;
    private float oldValue = -1;

    public int id = -1;

    private void OnValidate()
    {
        Check();
    }

    public void ChangeID(int newID)
    {
        id = newID;
        Check();
    }

    void Check()
    {
        if (slider)
        {
            if (Mathf.Approximately(oldValue, value) == false)
            {
                oldValue = value;
                slider.value = value;
                Set();
            }
        }
    }

    private void Awake()
    {
        if (slider == null) slider = GetComponent<Slider>();
        Set();
    }

    public void Set()
    {
        if (image && slider) image.sprite = GetSpriteByValue(value);
    }

    public Sprite GetSpriteByValue(float value)
    {
        return sprites[GetSpriteIDByValue(value)];
    }

    public int GetSpriteIDByValue(float value)
    {
        for (int i = values.Length - 1; i >= 0; i--)
        {
            if (value > values[i]) return i;
        }
        return 0;
    }

    private void FixedUpdate()
    {
        if (id > -1)
        {
            //value = MoodSystem.instance.GetMood(id);
            Check();
        }
    }
}
