﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScore : MonoBehaviour {

    public Text text;

    private void Awake()
    {
        text.text = string.Format("High score: {0}", Score);
    }

    public static int Score { get { return PlayerPrefs.GetInt("HighScore"); } set { PlayerPrefs.SetInt("HighScore", value); PlayerPrefs.Save(); } }
}
