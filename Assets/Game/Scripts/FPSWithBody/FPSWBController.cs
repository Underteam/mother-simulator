using System;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;

namespace FPS_WithBody
{
    [RequireComponent(typeof (CharacterController))]
    [RequireComponent(typeof (AudioSource))]
    public class FPSWBController : MonoBehaviour
    {
        [SerializeField] private bool m_IsWalking;
        [SerializeField] public float m_WalkSpeed;
        [SerializeField] public float m_RunSpeed;
        [SerializeField] [Range(0f, 1f)] private float m_WalkstepLenghten;
        [SerializeField] [Range(0f, 1f)] private float m_RunstepLenghten;
        [SerializeField] private float m_JumpSpeed;
        [SerializeField] private float m_StickToGroundForce;
        [SerializeField] private float m_GravityMultiplier;
        [SerializeField] private MouseLook m_MouseLook;
        [SerializeField] private bool m_UseFovKick;
        [SerializeField] private FOVKick m_FovKick = new FOVKick();
        [SerializeField] private bool m_UseHeadBob;
        [SerializeField] private CurveControlledBob m_HeadBob = new CurveControlledBob();
        [SerializeField] private LerpControlledBob m_JumpBob = new LerpControlledBob();
        [SerializeField] private float m_StepInterval;
        [SerializeField] private AudioClip[] m_FootstepSounds;    // an array of footstep sounds that will be randomly selected from.
        [SerializeField] private AudioClip m_JumpSound;           // the sound played when character leaves the ground.
        [SerializeField] private AudioClip m_LandSound;           // the sound played when character touches back on ground.

        private Camera m_Camera;
        //public Transform headTransform;
        private float cameraYShift;
        private Vector3 cameraShift;
        private bool m_Jump;
        private float m_YRotation;
        private Vector2 m_Input;
        private Vector3 m_MoveDir = Vector3.zero;
        private CharacterController m_CharacterController;
        private CollisionFlags m_CollisionFlags;
        private bool m_PreviouslyGrounded;
        private Vector3 m_OriginalCameraPosition;
        private float m_StepCycle;
        private float m_NextStep;
        private bool m_Jumping;
        private AudioSource m_AudioSource;

        public Animator m_Animator;


        [SerializeField] float m_MovingTurnSpeed = 360;
        [SerializeField] float m_StationaryTurnSpeed = 180;
        [SerializeField] float m_JumpPower = 12f;
        [SerializeField] float m_RunCycleLegOffset = 0.2f; //specific to the character in sample assets, will need to be modified to work with others
        [SerializeField] float m_MoveSpeedMultiplier = 1f;
        [SerializeField] float m_AnimSpeedMultiplier = 1f;
        [SerializeField] float m_GroundCheckDistance = 0.1f;

        float m_OrigGroundCheckDistance;
        const float k_Half = 0.5f;
        float m_TurnAmount;
        float m_ForwardAmount;
        Vector3 m_GroundNormal;
        float xR;

        Vector3 m_CapsuleCenter;
        CapsuleCollider m_Capsule;
        bool m_Crouching;

        private Vector3 m_Move;
        private Vector3 m_CamForward;

        public bool disableMovement;

        // Use this for initialization
        private void Start()
        {
            if (m_Animator == null) m_Animator = GetComponent<Animator>();
            m_CharacterController = GetComponent<CharacterController>();
            m_Camera = Camera.main;
            m_OriginalCameraPosition = m_Camera.transform.localPosition;
            //cameraShift = m_Camera.transform.position - headTransform.position;
            //cameraYShift = cameraShift.y;
            m_FovKick.Setup(m_Camera);
            m_HeadBob.Setup(m_Camera, m_StepInterval);
            m_StepCycle = 0f;
            m_NextStep = m_StepCycle/2f;
            m_Jumping = false;
            m_AudioSource = GetComponent<AudioSource>();
			m_MouseLook.Init(transform , m_Camera.transform);
        }


        // Update is called once per frame
        private void Update()
        {
            RotateView();

            // the jump state needs to read here to make sure it is not missed
            if (!m_Jump)
            {
                m_Jump = InputManager.Instance().GetButtonDown("Jump");
            }

            //if (m_CharacterController.isGrounded)
            {
                //m_Crouching = Input.GetKey(KeyCode.LeftControl);
            }

            //Debug.Log(m_Jump);

            //if (!m_PreviouslyGrounded && m_CharacterController.isGrounded)
            //{
            //    StartCoroutine(m_JumpBob.DoBobCycle());
            //    PlayLandingSound();
            //    m_MoveDir.y = 0f;
            //    m_Jumping = false;
            //}
            //if (!m_CharacterController.isGrounded && !m_Jumping && m_PreviouslyGrounded)
            //{
            //    m_MoveDir.y = 0f;
            //}

            //m_PreviouslyGrounded = m_CharacterController.isGrounded;
        }
        
        private void FixedUpdate()
        {
            if (disableMovement) return;

            float speed;
            GetInputFPS(out speed);
            // always move along the camera forward as it is the direction that it being aimed at
            Vector3 desiredMove = transform.forward * m_Input.y + transform.right * m_Input.x;
            //Debug.Log(desiredMove.magnitude + " " + desiredMove);

            // get a normal for the surface that is being touched to move along it
            RaycastHit hitInfo;
            Physics.SphereCast(transform.position + Vector3.up * m_CharacterController.height / 2f, m_CharacterController.radius, Vector3.down, out hitInfo,
                               m_CharacterController.height / 2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);
            Debug.DrawRay(transform.position, Vector3.down);
            desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal);//.normalized;
            //Debug.Log(desiredMove);
            m_MoveDir.x = desiredMove.x * speed;
            m_MoveDir.z = desiredMove.z * speed;
            //Debug.Log(m_MoveDir);
            if (m_CharacterController.isGrounded)
            {
                m_MoveDir.y = -m_StickToGroundForce;

                if (m_Jump)
                {
                    m_MoveDir.y = m_JumpSpeed;
                    PlayJumpSound();
                    m_Jump = false;
                    m_Jumping = true;
                }
            }
            else
            {
                m_MoveDir += Physics.gravity * m_GravityMultiplier * Time.fixedDeltaTime;
            }
            //m_CollisionFlags = m_CharacterController.Move(m_MoveDir * speed * Time.fixedDeltaTime);

            ProgressStepCycle(speed * 10);
            UpdateCameraPosition(speed);

            m_MouseLook.UpdateCursorLock();
            //=================================================================================

            // pass all parameters to the character control script
            if(!disableMovement) Move(m_MoveDir, m_Jump);
            m_Jump = false;
        }

        #region FootSteps

        private void PlayJumpSound()
        {
            m_AudioSource.clip = m_JumpSound;
            m_AudioSource.Play();
        }

        private void PlayLandingSound()
        {
            m_AudioSource.clip = m_LandSound;
            m_AudioSource.Play();
            m_NextStep = m_StepCycle + .5f;
        }

        private void ProgressStepCycle(float speed)
        {
            if (m_CharacterController.velocity.sqrMagnitude > 0 && (m_Input.x != 0 || m_Input.y != 0))
            {
                m_StepCycle += (m_CharacterController.velocity.magnitude + (speed*(m_IsWalking ? m_WalkstepLenghten : m_RunstepLenghten)))*
                             Time.fixedDeltaTime;
            }

            if (!(m_StepCycle > m_NextStep))
            {
                return;
            }

            m_NextStep = m_StepCycle + m_StepInterval;

            PlayFootStepAudio();
        }

        private void PlayFootStepAudio()
        {
            if (!m_CharacterController.isGrounded)
            {
                return;
            }
            // pick & play a random footstep sound from the array,
            // excluding sound at index 0
            int n = Random.Range(1, m_FootstepSounds.Length);
            m_AudioSource.clip = m_FootstepSounds[n];
            m_AudioSource.PlayOneShot(m_AudioSource.clip);
            // move picked sound to index 0 so it's not picked next time
            m_FootstepSounds[n] = m_FootstepSounds[0];
            m_FootstepSounds[0] = m_AudioSource.clip;
        }

        #endregion

        private void UpdateCameraPosition(float speed)
        {
            //if (m_Crouching)
            //{
            //    m_Camera.transform.position = headTransform.position + cameraShift;
            //    return;
            //}

            Vector3 newCameraPosition;
            //Vector3 camPos = m_Camera.transform.position;
            //camPos.y = headTransform.position.y + cameraYShift;
            if (!m_UseHeadBob)
            {
                return;
            }

            if (m_CharacterController.velocity.magnitude > 0 && m_CharacterController.isGrounded)
            {
                m_Camera.transform.localPosition =
                    m_HeadBob.DoHeadBob(m_CharacterController.velocity.magnitude +
                                      (speed*(m_IsWalking ? 1f : m_RunstepLenghten)));
                newCameraPosition = m_Camera.transform.localPosition;
                newCameraPosition.y = m_Camera.transform.localPosition.y - m_JumpBob.Offset();
            }
            else 
            {
                newCameraPosition = m_Camera.transform.localPosition;
                newCameraPosition.y = m_OriginalCameraPosition.y - m_JumpBob.Offset();
            }

            m_Camera.transform.localPosition = newCameraPosition;
        }

        private void GetInputFPS(out float speed)
        {
            // Read input
            float horizontal = InputManager.Instance().GetAxis("Horizontal");
            float vertical = InputManager.Instance().GetAxis("Vertical");

            if(disableMovement)
            {
                horizontal = 0;
                vertical = 0;
            }

            bool waswalking = m_IsWalking;

#if !MOBILE_INPUT
            // On standalone builds, walk/run speed is modified by a key press.
            // keep track of whether or not the character is walking or running
            m_IsWalking = !Input.GetKey(KeyCode.LeftShift);
#endif
            // set the desired speed to be walking or running
            speed = m_IsWalking ? m_WalkSpeed : m_RunSpeed;
            m_Input = new Vector2(horizontal, vertical);
            //Debug.Log(m_Input.magnitude + " " + m_Input);
            // normalize input if it exceeds 1 in combined length:
            //====================================================================================
            if (m_Input.sqrMagnitude > 1)
            {
                m_Input.Normalize();
            }

            // handle speed change to give an fov kick
            // only if the player is going to a run, is running and the fovkick is to be used
            if (m_IsWalking != waswalking && m_UseFovKick && m_CharacterController.velocity.sqrMagnitude > 0)
            {
                StopAllCoroutines();
                StartCoroutine(!m_IsWalking ? m_FovKick.FOVKickUp() : m_FovKick.FOVKickDown());
            }
        }

        private void RotateView()
        {
            float xsens = m_MouseLook.XSensitivity;
            if (disableMovement) m_MouseLook.XSensitivity = 0;
            m_MouseLook.LookRotation (transform, m_Camera.transform, out xR);
            m_MouseLook.XSensitivity = xsens;
        }

        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            Rigidbody body = hit.collider.attachedRigidbody;
            //dont move the rigidbody if the character is on top of it
            if (m_CollisionFlags == CollisionFlags.Below)
            {
                return;
            }

            if (body == null || body.isKinematic)
            {
                return;
            }
            body.AddForceAtPosition(m_CharacterController.velocity*0.1f, hit.point, ForceMode.Impulse);
        }

        #region ThirdPlayerController

        public void Move(Vector3 move, bool jump)
        {
            //Debug.Log(move);
            // convert the world relative moveInput vector into a local-relative
            // turn amount and forward amount required to head in the desired
            // direction.
            //====================================================================================
            //if (move.magnitude > 1f) move.Normalize();
            //move = transform.InverseTransformDirection(move);
            CheckGroundStatus();
            move = Vector3.ProjectOnPlane(move, m_GroundNormal);
            
            //m_TurnAmount = Mathf.Atan2(move.x, move.z);
            m_ForwardAmount = move.z;
            m_CollisionFlags = m_CharacterController.Move(move * m_MoveSpeedMultiplier * Time.fixedDeltaTime);
            //move = m_CharacterController.velocity.normalized;
            move.z = m_CharacterController.velocity.z;
            //ApplyExtraTurnRotation();
            //move.z = Mathf.Atan2(move.x, move.z);
            // control and velocity handling is different when grounded and airborne:
            if (m_CharacterController.isGrounded)
            {
                HandleGroundedMovement(jump);
            }
            else
            {
                HandleAirborneMovement();
            }

            // send input and other state parameters to the animator
            UpdateAnimator(move);
        }

        void UpdateAnimator(Vector3 move)
        {
            if (xR > 0.5f) xR = 0.5f;
            if (xR < -0.5f) xR = -0.5f;
            // update the animator parameters
            //m_Animator.SetFloat("Strafe", move.x, 0.1f, Time.deltaTime);
            m_Animator.SetFloat("Forward", move.z, 0.1f, Time.deltaTime);
            m_Animator.SetFloat("Turn", xR, 0.1f, Time.deltaTime);
            //if (move.x > 0.5f || move.x < -0.5f) m_Animator.SetFloat("Turn", move.x, 0.1f, Time.deltaTime);
            m_Animator.SetBool("Crouch", m_Crouching);
            m_Animator.SetBool("OnGround", m_CharacterController.isGrounded);
            if (!m_CharacterController.isGrounded)
            {
                m_Animator.SetFloat("Jump", m_CharacterController.velocity.y);
            }


            // the anim speed multiplier allows the overall speed of walking/running to be tweaked in the inspector,
            // which affects the movement speed because of the root motion.
            if (m_CharacterController.isGrounded && move.magnitude > 0)
            {
                m_Animator.speed = m_AnimSpeedMultiplier;// m_RunSpeed;
            }
            else
            {
                // don't use that while airborne
                m_Animator.speed = 1;
            }
        }

        void HandleAirborneMovement()
        {
            // apply extra gravity from multiplier:
            Vector3 extraGravityForce = (Physics.gravity * m_GravityMultiplier) - Physics.gravity;
            m_CharacterController.Move(extraGravityForce);

            m_GroundCheckDistance = m_CharacterController.velocity.y < 0 ? m_OrigGroundCheckDistance : 0.01f;
        }


        void HandleGroundedMovement(bool jump)
        {
            // check whether conditions are right to allow a jump:
            if (jump && m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Grounded"))
            {
                // jump!
                m_CharacterController.SimpleMove(new Vector3(m_CharacterController.velocity.x, m_JumpPower, m_CharacterController.velocity.z));
                //m_CharacterController.isGrounded = false;
                m_Animator.applyRootMotion = false;
                m_GroundCheckDistance = 0.1f;
            }
        }

        void ApplyExtraTurnRotation()
        {
            // help the character turn faster (this is in addition to root rotation in the animation)
            float turnSpeed = Mathf.Lerp(m_StationaryTurnSpeed, m_MovingTurnSpeed, m_ForwardAmount);
            transform.Rotate(0, m_TurnAmount * turnSpeed * Time.deltaTime, 0);
        }

        public void OnAnimatorMove()
        {
            // we implement this function to override the default root motion.
            // this allows us to modify the positional speed before it's applied.
            if (m_CharacterController.isGrounded && Time.deltaTime > 0)
            {
                Vector3 v = (m_Animator.deltaPosition * m_MoveSpeedMultiplier) / Time.deltaTime;

                // we preserve the existing y part of the current velocity.
                v.y = m_CharacterController.velocity.y;
                //m_CharacterController.SimpleMove(v);
            }
        }

        void CheckGroundStatus()
        {
            RaycastHit hitInfo;
#if UNITY_EDITOR
            // helper to visualise the ground check ray in the scene view
            Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * m_GroundCheckDistance));
#endif
            // 0.1f is a small offset to start the ray from inside the character
            // it is also good to note that the transform position in the sample assets is at the base of the character
            if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, m_GroundCheckDistance))
            {
                m_GroundNormal = hitInfo.normal;
                //m_IsGrounded = true;
                m_Animator.applyRootMotion = true;
            }
            else
            {
                //m_IsGrounded = false;
                m_GroundNormal = Vector3.up;
                m_Animator.applyRootMotion = false;
            }
        }

        #endregion
    }
}
