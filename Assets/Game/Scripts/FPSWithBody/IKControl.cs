﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Animator))]

public class IKControl : MonoBehaviour
{
    [System.Serializable]
    public class IKPrefs
    {
        public AvatarIKGoal ikGoal;
        public HumanBodyBones bone;
        public Transform target;
        public Transform boneTransform { get; set; }
        public bool active;

        //[HideInInspector]
        public float weight;
    }

    protected Animator animator;

    public bool ikActive = false;

    public List<IKPrefs> ikPrefs;

    private bool prevState;

    public System.Action<IKPrefs> onCanNotReach;

    void Start()
    {
        animator = GetComponent<Animator>();

        if(animator)
        {
            for (int i = 0; i < ikPrefs.Count; i++)
            {
                ikPrefs[i].boneTransform = animator.GetBoneTransform(ikPrefs[i].bone);
            }
        }
    }

    void OnAnimatorIK()
    {
        if (animator)
        {
            if (ikActive)
            {
                //// Взгляд
                //if (ikLook && lookObj != null)
                //{
                //    animator.SetLookAtWeight(1);
                //    animator.SetLookAtPosition(lookObj.position);
                //}
                for(int i = 0; i < ikPrefs.Count; i++)
                {
                    animator.SetIKPositionWeight(ikPrefs[i].ikGoal, ikPrefs[i].weight);
                    animator.SetIKRotationWeight(ikPrefs[i].ikGoal, ikPrefs[i].weight);
                    animator.SetIKPosition(ikPrefs[i].ikGoal, ikPrefs[i].target.position);
                    animator.SetIKRotation(ikPrefs[i].ikGoal, ikPrefs[i].target.rotation);
                }
            }
            // Сбрасываем влияние
            else //if(prevState)
            {
                for (int i = 0; i < ikPrefs.Count; i++)
                {
                    animator.SetIKPositionWeight(ikPrefs[i].ikGoal, ikPrefs[i].weight);
                    animator.SetIKRotationWeight(ikPrefs[i].ikGoal, ikPrefs[i].weight);
                }
                animator.SetLookAtWeight(0);
            }
        }

        prevState = ikActive;
    }

    public Transform GetTransform(HumanBodyBones type)
    {
        return animator.GetBoneTransform(type);
    }

    public IKPrefs SetIKState (AvatarIKGoal goal, bool state)
    {
        for (int i = 0; i < ikPrefs.Count; i++)
        {
            if (ikPrefs[i].ikGoal == goal)
            {
                ikPrefs[i].active = state;
                //ikPrefs[i].target.position = ikPrefs[i].boneTransform.position;
                return ikPrefs[i];
            }
        }

        return null;
    }

    private void Update()
    {
        for (int i = 0; i < ikPrefs.Count; i++)
        {
            float targetWeight = ikActive && ikPrefs[i].active ? 1 : 0;
            float speed = ikActive && ikPrefs[i].active ? 3 : 10;
            ikPrefs[i].weight = Mathf.Lerp(ikPrefs[i].weight, targetWeight, speed * Time.deltaTime);
        }
    }

    public void LateUpdate()
    {
        if (!ikActive) return;

        for(int i = 0; i < ikPrefs.Count; i++)
        {
            if(Vector3.Distance(ikPrefs[i].target.position, ikPrefs[i].boneTransform.position) > 0.05f)
            {
                if (onCanNotReach != null) onCanNotReach(ikPrefs[i]);
            }
        }
    }
}