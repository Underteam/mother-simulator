﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKMover : MonoBehaviour {

    [System.Serializable]
	public class Action
    {
        public string name;
        public enum Type { None, Take, Drop, Active, Children }
        public Type type;

        public GameObject lastItem;
        public GameObject fakeItem;

        public Transform leftHand;
        public Transform rightHand;

        public Vector3 leftOffset;
        public Vector3 rightOffset;

        public void MoveTo(GameObject targetObj, IKControl ikControl, Transform leftHandT, Transform rightHandT)
        {
            //Debug.Log(type);
            lastItem = targetObj;
            if (leftHand)
            {
                leftHandT.transform.position = leftHand.position + leftOffset;
                leftHandT.rotation = leftHand.rotation;
                ikControl.SetIKState(AvatarIKGoal.LeftHand, true);// ikLeftHand = true;
                if (targetObj && rightHand == null)
                {
                    targetObj.transform.eulerAngles = leftHandT.up * 90;
                }
            }
            if (rightHand)
            {
                rightHandT.transform.position = rightHand.position;
                rightHandT.rotation = rightHand.rotation;
                ikControl.SetIKState(AvatarIKGoal.RightHand, true);// ikRightHand = true;
                //Debug.Log(string.Format("Left {0} Right {1}", leftHand == null, rightHand == null));
                if (targetObj && leftHand == null)
                {
                    targetObj.transform.position += rightOffset;
                    targetObj.transform.eulerAngles = rightHandT.up * 90;
                }
            }
            if (leftHand && rightHand)
            {
                if (targetObj)
                {
                    //targetObj.transform.position = (leftHand.position + rightHand.position) / 2 - new Vector3(-1, 0, 0);
                    //targetObj.transform.eulerAngles = rightHand.up + new Vector3(0, 0, 0);
                }
            }

            //if (fakeItem)
            //{
            //    fakeItem.SetActive(true);
            //    targetObj.SetActive(false);
            //}
        }
    }

    public List<Action> actions;
    public GameObject target;
    public float moveTime = 1;
    public IKControl ikControl;

    public Transform leftTransform;
    public Transform rightTransform;

    public Action this[Action.Type type]
    {
        get
        {
            for (int i = 0; i < actions.Count; i++)
            {
                if (actions[i].type == type) return actions[i];
            }
            return null;
        }
    }

    [EditorButton]
    public void StartAction(int type)
    {
        this[(Action.Type)type].MoveTo(null, ikControl, leftTransform, rightTransform);
        ikControl.ikActive = true;
    }

    public void StartAction(int type, GameObject targetObj)
    {
        this[(Action.Type)type].MoveTo(targetObj, ikControl, leftTransform, rightTransform);
        ikControl.ikActive = true;
    }

    [EditorButton]
    public void Nothing()
    {
        ikControl.ikActive = false;
        //ikControl.ikLeftHand = false;
        //ikControl.ikRightHand = false;
    }
}
