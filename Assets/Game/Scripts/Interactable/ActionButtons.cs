﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionButtons : MonoBehaviour {

    public Actioner actioner;

    [Header("Buttons")]
    public GameObject btnUse;
    public GameObject btnTake;
    public GameObject btnDrop;
    public GameObject btnCast;
    public GameObject btnJump;

    #region Actions

    // Переключение состояния
    public void Use()
    {
        actioner.Use();
    }

    // Взять
    public void Take()
    {
        actioner.Take();
    }

    // Выбросить
    public void Drop()
    {
        actioner.Drop();
    }

    // Кинуть
    public void Cast()
    {
        actioner.Cast();
    }

    // Прыжок
    public void Jump()
    {
        actioner.Jump();
    }

    #endregion

}
