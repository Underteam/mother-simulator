﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterLevel : MonoBehaviour {

    public Transform water;

    public float speed = 1;
    public float highMin = 0;
    public float highMax = 1;

    private float highTarget = 0;
    bool changeHigh = false;

    private Vector3 targetPos;

    private void Update()
    {
        if (changeHigh)
        {
            //if (Mathf.Approximately(water.position.y, highTarget) == false)
            if (Vector3.Distance(water.localPosition, targetPos) > 0.01f)
            {
                water.localPosition = Vector3.Lerp(water.localPosition, targetPos, speed * Time.deltaTime);
            }
            else
            {
                changeHigh = false;
            }
        }
    }

    [EditorButton]
    public void Up()
    {
        Set(highMax);
    }

    [EditorButton]
    public void Down()
    {
        Set(highMin);
    }

    void Set(float value)
    {
        highTarget = value;
        targetPos = new Vector3(water.localPosition.x, highTarget, water.localPosition.z);
        changeHigh = true;
    }
}
