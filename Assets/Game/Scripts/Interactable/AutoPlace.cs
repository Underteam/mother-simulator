﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoPlace : MonoBehaviour {

    public InteractableItem target;
    public InteractableItem place;

	void Start ()
    {
        place.Place(target.gameObject);
    }
}
