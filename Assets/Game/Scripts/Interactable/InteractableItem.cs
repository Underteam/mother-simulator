﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableItem : MonoBehaviour {

    //public enum Type { takeble }

    [System.Serializable]
    public class Interactables
    {
        public bool canTake;
        public bool canUse;
        public bool toBaby;
        public bool isPlace;
        public bool isLie;
    }

    public Interactables interactables = new Interactables();

    public UnityEngine.Events.UnityEvent onActive;
    public UnityEngine.Events.UnityEvent onDeActive;
    public bool active = false;

    public UnityEngine.Events.UnityEvent onPlace;
    public UnityEngine.Events.UnityEvent onPickUp;
    public bool isEmpty = true;

    private new Rigidbody rigidbody;
    private AudioSource hitSource;
    public Collider triggerCollider;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        if (rigidbody == null) rigidbody = gameObject.AddComponent<Rigidbody>();
        if (rigidbody)
        {
            rigidbody.isKinematic = true;
        }
        if (triggerCollider == null)
        {
            triggerCollider = GetComponent<Collider>();
        }
        Action(active);
    }

    #region Actions

    // Переключение состояния
    public void ActionSwitch()
    {
        if (interactables.canUse == false) return;

        active = !active;
        Action(active);
    }

    // Действие
    public void Action(bool active)
    {
        if (interactables.canUse == false) return;

        //Debug.Log(string.Format("{0} active {1}", name, active));

        this.active = active;
        if (active)
        {
            if (onActive != null) onActive.Invoke();
        }
        else
        {
            if (onDeActive != null) onDeActive.Invoke();
        }
    }

    // Взять
    public void Take(Transform target)
    {
        if (interactables.canTake == false) return;

        rigidbody.isKinematic = true;
        transform.parent = target;
        transform.localPosition = Vector3.zero;
        if (triggerCollider) triggerCollider.enabled = false;
        Debug.Log(string.Format("{0} taked from {1}", gameObject.name, target.name));
    }

    // Выбросить
    public void Drop(Vector3? pos = null)
    {
        if (interactables.canTake == false) return;

        if (pos != null)
            transform.position = (Vector3)pos;
        transform.parent = null;
        rigidbody.isKinematic = false;
        if (triggerCollider) triggerCollider.enabled = true;
        Debug.Log(string.Format("{0} dropped!", gameObject.name));
    }

    // Кинуть
    public void Cast()
    {
        if (interactables.canTake == false) return;

        transform.parent = null;
        rigidbody.isKinematic = false;

        rigidbody.interpolation = RigidbodyInterpolation.Interpolate;
        rigidbody.AddForce(Camera.main.transform.forward * 1000);
        if (triggerCollider) triggerCollider.enabled = true;
        Debug.Log(string.Format("{0} Casted!", gameObject.name));
    }

    // Размещаем объект на себе
    public void Place(GameObject obj)
    {
        if (interactables.isPlace == false) return;

        obj.transform.position = transform.position;
        obj.transform.parent = transform;
        obj.transform.localRotation = Quaternion.identity;
        obj.GetComponent<Rigidbody>().isKinematic = true;

        // Если входящий объект ребенок
        Children children = obj.GetComponent<Children>();
        if (children)
        {
            if (interactables.isLie)
            {
                Debug.Log("Прилег");
                children.Lie();
            }
            else
            {
                Debug.Log("Присел");
                children.Sit();
            }
        }

        isEmpty = false;
        if (triggerCollider) triggerCollider.enabled = false;
        if (onPlace != null) onPlace.Invoke();
    }

    // Опустошение
    public void PickUp()
    {
        Debug.Log("Достаем из сидалища");
        isEmpty = true;
        if (triggerCollider) triggerCollider.enabled = true;
        if (onPickUp != null) onPickUp.Invoke();
    }

    #endregion

    void RB_ResetVelocity()
    {
        Rigidbody[] rbs = GetComponentsInChildren<Rigidbody>(true);
        for (int i = 0; i < rbs.Length; i++)
        {
            rbs[i].isKinematic = false;
            rbs[i].velocity = Vector3.zero;
            rbs[i].angularVelocity = Vector3.zero;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (hitSource == null)
        {
            hitSource = Instantiate<AudioSource>(Game.instance.hitSoundPrefabs, transform);
            hitSource.transform.localPosition = Vector3.zero;
        }
        if (hitSource.isPlaying == false) hitSource.Play();
    }
}