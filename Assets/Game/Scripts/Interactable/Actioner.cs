﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Actioner : MonoBehaviour
{
    [Header("Main")]
    public ActionButtons actionButtons;
    public float range = 500f;
    public Camera cam;
    public bool canHover = false;

    [Header("Hand & Items")]
    public Transform leftHand;
    public Transform rightHand;
    public InteractableItem leftItem;
    public InteractableItem rightItem;

    public bool leftIsFree = true;
    public bool rightIsFree = true;
    public IKMover ikMover;

    public InteractableItem interactableItem;
    GameObject raycastedItem;

    public InteractableItem childPlace;

    [Header("Sounds")]
    public AudioSource sndTake;
    public AudioSource sndDrop;
    public AudioSource sndCast;
    public AudioSource sndAction;    

    void Start()
    {
        SetActiveButtons(false, false, false, false);
    }

    #region EditorActions
#if UNITY_EDITOR

    public void Update()
    {
        // События для тестов

        // Взять
        if (Input.GetKeyDown(KeyCode.T))
        {
            Take();
            Debug.Log("Editor Take!");
        }

        // Выбросить
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Drop();
            Debug.Log("Editor Drop!");
        }

        // Кинуть
        if (Input.GetKeyDown(KeyCode.G))
        {
            Cast();
            Debug.Log("Editor Cast!");
        }

        // Переключение состояния
        if (Input.GetKeyDown(KeyCode.E))
        {
            Use();
            Debug.Log("Editor Action!");
        }

        // Прыжок
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
            Debug.Log("Editor Jump!");
        }
    }

#endif
    #endregion

    void FixedUpdate()
    {
        // Нахождение объекта
        RayCast();
        // Определение активных кнопок
        CalcButtons();
    }

    private void LateUpdate()
    {
        interactableItem = null;
    }

    #region Actions

    // Переключение состояния
    public void Use()
    {
        if (interactableItem == null) return;

        if (rightIsFree == false && rightItem != null && rightItem.CompareTag("Children") && interactableItem.interactables.isPlace)
        {
            Debug.Log("Пытаемся посадить");
            interactableItem.Place(rightItem.gameObject);
            rightItem = null;
            rightIsFree = true;
            ikMover.Nothing();
            childPlace.PickUp();
            Game.instance.WDYW.SitTrigger_SetActive(false);
        }
        else if (rightIsFree == false && rightItem != null && interactableItem.CompareTag("Children"))
        {
            if (Game.instance.WDYW.CheckItem(rightItem.gameObject))
            {

            }
            else
            {

            }
            Drop();
        }
        else
        {
            interactableItem.ActionSwitch();
            if (sndAction) sndAction.Play();
        }

    }

    // Взять
    public void Take()
    {
        if (interactableItem == null || interactableItem && interactableItem.interactables.canTake == false) return;

        // Берем ребенка
        if (interactableItem.CompareTag("Children"))
        {
            InteractableItem[] parentIter = interactableItem.GetComponentsInParent<InteractableItem>();
            for (int i = 0; i < parentIter.Length; i++)
            {
                if (parentIter[i] && parentIter[i].interactables.isPlace)
                {
                    Debug.Log("Достаем из сидалища");
                    parentIter[i].PickUp();
                }
            }
            //if (childPlace.isEmpty == false)
            //{
            //    childPlace.PickUp();
            //}

            // Новый способ взятия ребенка
            childPlace.Place(interactableItem.gameObject);
            ikMover.StartAction((int)IKMover.Action.Type.Children, interactableItem.gameObject);

            Children children = interactableItem.GetComponent<Children>();
            children.ragdoll.ragdolled = false;
            children.TriggerActive(false);

            rightItem = interactableItem;
            rightIsFree = false;
            leftIsFree = false;

            Game.instance.WDYW.SitTrigger_SetActive(true);
            //UnityEditor.EditorApplication.isPaused = true;

            return;
            //===========

            //interactableItem.Take(transform);

            //ikMover.StartAction((int)IKMover.Action.Type.Children, interactableItem.gameObject);
            //Children children = interactableItem.GetComponent<Children>();
            //children.ragdoll.ragdolled = false;
            //children.TriggerActive(false);
            //children.transform.Rotate(new Vector3(0, -90, 0), Space.Self);
            //rightItem = interactableItem;
            //rightIsFree = false;
            //leftIsFree = false;
            //return;
        }

        // Берем предмет в правую руку
        if (rightIsFree)
        {
            rightItem = interactableItem;
            //interactableItem.Take(rightHand);
            interactableItem.Take(ikMover.ikControl.GetTransform(HumanBodyBones.RightHand));
            rightHand.position = ikMover[IKMover.Action.Type.Take].rightHand.position;
            ikMover.StartAction((int)IKMover.Action.Type.Take, interactableItem.gameObject);
            rightIsFree = false;
            Debug.Log(string.Format("Take item {1} to {0}", rightHand.name, interactableItem.name));

            if (sndTake) sndTake.Play();
        }
        #region Left Hand
        //else if (leftIsFree)
        //{
        //    leftItem = interactableItem;
        //    //interactableItem.Take(leftHand);
        //    interactableItem.Take(ikMover.ikControl.GetTransform(HumanBodyBones.LeftHand));
        //    leftHand.position = ikMover[IKMover.Action.Type.Take].leftHand.position;
        //    ikMover.StartAction((int)IKMover.Action.Type.Take, interactableItem.gameObject);
        //    leftIsFree = false;
        //    Debug.Log(string.Format("Take item {1} to {0}", leftHand.name, interactableItem.name));

        //    if (sndTake) sndTake.Play();
        //}
        #endregion
    }

    // Выбросить
    public void Drop()
    {
        if (rightIsFree == false && rightItem != null)
        {
            rightItem.Drop(GetDropPos());
            Debug.Log(string.Format("Drop item {1} to {0}", rightHand.name, rightItem.name));

            if (rightIsFree == false && rightItem != null && rightItem.CompareTag("Children"))
            {
                rightItem.transform.position += transform.forward;
                Children children = rightItem.GetComponent<Children>();
                children.ragdoll.ragdolled = true;

                if (childPlace.isEmpty == false) childPlace.PickUp();

                Game.instance.WDYW.SitTrigger_SetActive(false);
            }

            rightItem = null;
            rightIsFree = true;
            ikMover.Nothing();

            if (sndDrop) sndDrop.Play();
        }
        #region Left Hand
        //else if (leftIsFree == false && leftItem != null)
        //{
        //    if (leftIsFree == false && leftItem != null && leftItem.CompareTag("Children"))
        //    {
        //        //Children children = leftItem.GetComponent<Children>();
        //        //children.Ragdoll();
        //        Children children = rightItem.GetComponent<Children>();
        //        children.ragdoll.ragdolled = true;
        //    }

        //    leftItem.Drop(GetDropPos());
        //    Debug.Log(string.Format("Drop item {1} to {0}", leftHand.name, leftItem.name));
        //    leftItem = null;
        //    leftIsFree = true;
        //    ikMover.Nothing();

        //    if (sndDrop) sndDrop.Play();
        //}
        #endregion
    }

    // Кинуть
    public void Cast()
    {
        if (rightIsFree == false && rightItem != null)
        {
            rightItem.Cast();
            Debug.Log(string.Format("Cast item {1} to {0}", rightHand.name, rightItem.name));

            if (rightIsFree == false && rightItem != null && rightItem.CompareTag("Children"))
            {
                rightItem.transform.position += transform.forward;
                Children children = rightItem.GetComponent<Children>();
                children.ragdoll.ragdolled = true;

                if (childPlace.isEmpty == false) childPlace.PickUp();

                Game.instance.WDYW.SitTrigger_SetActive(false);
            }

            rightItem = null;
            rightIsFree = true;
            ikMover.Nothing();

            if (sndCast) sndCast.Play();
        }
        #region Left Hand
        //else if (leftIsFree == false && leftItem != null)
        //{
        //    if (leftIsFree == false && leftItem != null && leftItem.CompareTag("Children"))
        //    {
        //        //Children children = leftItem.GetComponent<Children>();
        //        //children.Ragdoll();
        //        Children children = rightItem.GetComponent<Children>();
        //        children.ragdoll.ragdolled = false;
        //    }

        //    leftItem.Cast();
        //    Debug.Log(string.Format("Cast item {1} to {0}", leftHand.name, leftItem.name));
        //    leftItem = null;
        //    leftIsFree = true;
        //    ikMover.Nothing();

        //    if (sndCast) sndCast.Play();
        //}
        #endregion
    }

    // Прыжок
    public void Jump()
    {

    }

    #endregion

    #region RayCast

    RaycastHit hit;
    Ray ray;
    Vector2 viewport = new Vector2(0.5f, 0.5f);

    Vector3? GetDropPos()
    {
        Physics.Raycast(ray, out hit, range, LayerMask.GetMask("Environment"));
        return hit.collider != null && hit.distance < range / 1.5f ? (Vector3?)hit.point + new Vector3(0, 0.1f, 0) : null;
    }

    void CheckRaycastedItem()
    {
        if (raycastedItem && raycastedItem.layer == LayerMask.NameToLayer("Interactable"))
        {
            if (interactableItem == null && raycastedItem.GetComponent<InteractableItem>() == null)
            {
                interactableItem = raycastedItem.AddComponent<InteractableItem>();
                interactableItem.interactables.canTake = true;
            }
        }
    }

    // Нахождение объекта
    void RayCast()
    {
        ray = cam.ViewportPointToRay(viewport);

        if (Physics.Raycast(ray, out hit, range))
        {
            raycastedItem = hit.collider.gameObject;
            Debug.DrawRay(ray.origin, ray.direction.normalized * range, Color.gray);
            if (canHover == true)
            {
                interactableItem = hit.transform.GetComponent<InteractableItem>();
                if (interactableItem != null)
                {
                    Debug.DrawRay(ray.origin, ray.direction.normalized * range, Color.green);
                }
                else
                {
                    CheckRaycastedItem();
                    Debug.DrawRay(ray.origin, ray.direction.normalized * range, Color.red);
                    SetActiveButtons(false, false, false, false);
                }
            }
        }
        else
        {
            raycastedItem = null;
        }
    }

    #endregion

    // Определение активных кнопок
    void CalcButtons()
    {
        bool canUse = false;
        bool canTake = false;
        bool canDrop = false;
        bool canCast = false;

        if (interactableItem)
        {
            canUse = interactableItem.interactables.canUse;
            canTake = interactableItem.interactables.canTake && rightItem == null;

            if (interactableItem.CompareTag("Children") && rightItem == null)
            {
                if (childPlace.isEmpty == false)
                {
                    canTake = false;
                }
            }
            if (rightIsFree == false && rightItem != null && rightItem.CompareTag("Children"))
            {
                canUse = interactableItem.interactables.isPlace;
            }
            if (rightItem == null && interactableItem.CompareTag("Children"))
            {
                canUse = false;
            }
            //Debug.Log("!!! TRUE");
        }
        else
        {
            canUse = false;
            canTake = false;
            canDrop = false;
            canCast = false;
            //Debug.Log("!!! FALSE");
        }

        if (rightIsFree == false && rightItem != null || (rightIsFree == false && leftIsFree == false))
        {
            canDrop = true;
            canCast = true;
        }

        SetActiveButtons(canUse, canTake, canDrop, canCast);
    }

    // Включение/выключение кнопок действий
    void SetActiveButtons(bool canUse, bool canTake, bool canDrop, bool canCast)
    {
        actionButtons.btnUse.SetActive(canUse);
        actionButtons.btnTake.SetActive(canTake);
        actionButtons.btnDrop.SetActive(canDrop);
        actionButtons.btnCast.SetActive(canCast);
    }
}