﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckWish : MonoBehaviour {

    public GameObject obj;
    public InteractableItem intObj;

    public void Check()
    {
        if (intObj.isEmpty == false)
        {
            Game.instance.WDYW.CheckItem(obj);
        }
    }
}
