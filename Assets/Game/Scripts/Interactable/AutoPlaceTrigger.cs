﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoPlaceTrigger : MonoBehaviour {

    public InteractableItem target;
    public InteractableItem place;
    public Actioner actioner;

	void OnTriggerEnter()
    {
        if (actioner == null) return;

        actioner.interactableItem = target;
        actioner.Take();

        if (place.isEmpty)
        {

        }
        //place.Place(target.gameObject);
    }
}
