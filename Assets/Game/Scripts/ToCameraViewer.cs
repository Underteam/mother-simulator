﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToCameraViewer : MonoBehaviour {

    private Camera cam;

    public float height = 1.5f;

    public bool inverse;

    public bool scaleWithDist;

    private Vector3 originalScale;

    public bool freezeXZ;

    private void Awake()
    {
        originalScale = transform.localScale;
    }

    void Update () {

        if (cam == null) cam = Camera.main;
        if (cam == null) return;

        Vector3 dir = cam.transform.position - transform.position;
        float dist = dir.magnitude;
        dir.y = 0;
        dir.Normalize();

        if (inverse) dir = -dir;

        if (transform.parent != null)
        {
            Vector3 pos = transform.parent.position + height * Vector3.up;
            if (!freezeXZ) pos.x = transform.position.x;
            if (!freezeXZ) pos.z = transform.position.z;
            transform.position = pos;
        }

        transform.rotation = Quaternion.LookRotation(dir, Vector3.up);

        if (scaleWithDist)
        {
            float d = dist - 5;
            if (d < 0) d = 0;
            float s = 1 + d / 5f;
            transform.localScale = originalScale * s;
        }
	}
}
