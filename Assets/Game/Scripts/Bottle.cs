﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bottle : MonoBehaviour {

    public bool isClean { get; set; }
    public float water = 0;
    public string waterType = "Hot";
    public float milk = 0;

    public bool haveWater = false; // заполнена
    public bool haveMilk = false; // 
    public bool havePacifier = false; // соска

    public DirtyScript dirtyScript;

    public void AddWater(float value)
    {

    }

    public void AddMilk(float value)
    {

    }

    public void AddPacifier()
    {

    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Hot"))
        {
            if (haveWater == false) haveWater = true;
        }
        if (other.CompareTag("Water"))
        {
            dirtyScript.Clear();
        }
        if (other.CompareTag("Milk"))
        {
            if (haveMilk == false) haveMilk = true;
        }
    }
}
