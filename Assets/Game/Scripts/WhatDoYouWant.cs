﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WhatDoYouWant : MonoBehaviour {

    [System.Serializable]
	public class Want
    {
        public string desctiption;
        public GameObject cloudObject;
        public GameObject[] realObject;
        
        public int valuePlus;
        public int valueMinus;

        public bool CheckItem(GameObject obj)
        {
            for (int i = 0; i < realObject.Length; i++)
            {
                if (realObject[i].Equals(obj)) return true;
            }
            return false;
        }
    }

    public bool active = false;

    [Header("Main")]
    public GameObject cloud; // облако над ребенком
    public List<Want> wants; // хотелки
    private int lastWant; // последнее желание
    public Children children;

    [Header("Slider")]
    public Slider emotionSlider;
    private float _emotionValue;
    public float emotionValue
    {
        get { return _emotionValue; }
        set
        {
            _emotionValue = value;
            emotionSlider.value = _emotionValue;
        }
    }
    public float emotionSpeed = 10;

    private bool _emotionDown = false;
    public bool emotionDown { get { return _emotionDown; } private set { _emotionDown = value; } }
    public Vector2 delayRanges;
    public float delay = 0f;

    public void SetDelay()
    {
        delay = Random.Range(delayRanges.x, delayRanges.y);
    }

    public UnityEngine.Events.UnityEvent onZero;
    private int count = 0;

    private void Awake()
    {
        OffWant();
    }

    public void Update()
    {
        if (active == false) return;

        if (emotionDown)
        {
            //Debug.Log(emotionValue);
            emotionValue -= emotionSpeed * Time.deltaTime;
            if (emotionValue <= 0)
            {
                emotionDown = false;
                emotionValue = 0;
                if (onZero != null) onZero.Invoke();
            }
        }

        if (emotionDown == false)
        {
            if (delay > 0f)
            {
                delay -= Time.deltaTime;
            }
            else
            {
                delay = 0;
                GetWantRandom();
            }
        }
    }

    public bool CheckItem(GameObject obj)
    {
        if (lastWant == -1) return false;
        bool result = wants[lastWant].CheckItem(obj);
        Debug.Log("!!!" + result);
        if (count > 0)
        {
            if (result)
            {
                Debug.Log("Желание исполнено!");
                Add(wants[lastWant].valuePlus / 10f);
                SetDelay();
                
                children.Happy();
                Game.instance.AddScore(wants[lastWant].valuePlus * 10);
                OffWant();
                Game.instance.ShowAds();
            }
            else
            {
                Debug.Log("Совсем не то!");
                Add(wants[lastWant].valueMinus / 10f);
                children.Sad();
                Game.instance.AddScore(wants[lastWant].valueMinus * 10);
            }
        }
        count++;
        return result;
    }

    // Случайное желание
    [EditorButton]
    public void GetWantRandom()
    {
        cloud.SetActive(true);
        SetWant(GetRandomID());
    }

    [EditorButton]
    public void Add(float count)
    {
        emotionValue += count;
        emotionValue = Mathf.Clamp01(emotionValue);
    }

    // Получение рандомного ID
    int GetRandomID()
    {
        int result = lastWant;
        int count = 1000;
        while (count > 0)
        {
            count--;
            if (result == lastWant) result = Random.Range(0, wants.Count);
            else return result;
        }
        return 0;
    }

    // Выключение хотелки
    [EditorButton]
    public void OffWant()
    {
        lastWant = -1;
        cloud.SetActive(false);
        OutLineEffect(-1);
        emotionDown = false;
    }

    // Установка хотелки
    public void SetWant(int id)
    {
        active = true;
        id = Mathf.Clamp(id, 0, wants.Count);
        lastWant = id;
        OutLineEffect(id);
        emotionDown = true;
    }

    // Активация/деактивация подстветки и объекта
    void OutLineEffect(int id)
    {
        for (int i = 0; i < wants.Count; i++)
        {
            // Объект для камеры
            wants[i].cloudObject.SetActive(id == i);
            for (int j = 0; j < wants[i].realObject.Length; j++)
            {
                // Подсветка
                cakeslice.Outline[] ol = wants[i].realObject[j].GetComponentsInChildren<cakeslice.Outline>(true);
                if (ol != null && ol.Length > 0)
                {
                    for (int k = 0; k < ol.Length; k++)
                    {
                        ol[k].enabled = i == id;
                    }
                }
            }
        }
    }

    BoxCollider[] collidersTriggers;
    public void SitTrigger_SetActive(bool active)
    {
        if (collidersTriggers == null || collidersTriggers.Length == 0)
        {
            GameObject[] sits = GameObject.FindGameObjectsWithTag("Sit");
            List<BoxCollider> result = new List<BoxCollider>();
            for (int i = 0; i < sits.Length; i++)
            {
                result.Add(sits[i].GetComponent<BoxCollider>());
            }
            collidersTriggers = result.ToArray();
        }
        for (int i = 0; i < collidersTriggers.Length; i++)
        {
            collidersTriggers[i].enabled = active;
        }
    }
}
