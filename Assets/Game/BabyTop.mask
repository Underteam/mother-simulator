%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: BabyTop
  m_Mask: 00000000010000000100000000000000000000000100000001000000010000000100000000000000000000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: ROOT
    m_Weight: 0
  - m_Path: ROOT/TB
    m_Weight: 0
  - m_Path: ROOT/TB/TB Pelvis
    m_Weight: 0
  - m_Path: ROOT/TB/TB Pelvis/TB L Thigh
    m_Weight: 0
  - m_Path: ROOT/TB/TB Pelvis/TB L Thigh/TB L Calf
    m_Weight: 0
  - m_Path: ROOT/TB/TB Pelvis/TB L Thigh/TB L Calf/TB L Foot
    m_Weight: 0
  - m_Path: ROOT/TB/TB Pelvis/TB L Thigh/TB L Calf/TB L Foot/TB L Toe0
    m_Weight: 0
  - m_Path: ROOT/TB/TB Pelvis/TB R Thigh
    m_Weight: 0
  - m_Path: ROOT/TB/TB Pelvis/TB R Thigh/TB R Calf
    m_Weight: 0
  - m_Path: ROOT/TB/TB Pelvis/TB R Thigh/TB R Calf/TB R Foot
    m_Weight: 0
  - m_Path: ROOT/TB/TB Pelvis/TB R Thigh/TB R Calf/TB R Foot/TB R Toe0
    m_Weight: 0
  - m_Path: ROOT/TB/TB Pelvis/TB Spine
    m_Weight: 0
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1
    m_Weight: 0
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB L Clavicle
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB L Clavicle/TB L UpperArm
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB L Clavicle/TB L UpperArm/TB L
      Forearm
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB L Clavicle/TB L UpperArm/TB L
      Forearm/TB L Hand
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB L Clavicle/TB L UpperArm/TB L
      Forearm/TB L Hand/TB L Finger0
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB L Clavicle/TB L UpperArm/TB L
      Forearm/TB L Hand/TB L Finger0/TB L Finger01
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB L Clavicle/TB L UpperArm/TB L
      Forearm/TB L Hand/TB L Finger1
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB L Clavicle/TB L UpperArm/TB L
      Forearm/TB L Hand/TB L Finger1/TB L Finger11
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB L Clavicle/TB L UpperArm/TB L
      Forearm/TB L Hand/TB L Finger2
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB L Clavicle/TB L UpperArm/TB L
      Forearm/TB L Hand/TB L Finger2/TB L Finger21
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB L Clavicle/TB L UpperArm/TB L
      Forearm/TB L Hand/TB L Finger3
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB L Clavicle/TB L UpperArm/TB L
      Forearm/TB L Hand/TB L Finger3/TB L Finger31
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB L Clavicle/TB L UpperArm/TB L
      Forearm/TB L Hand/TB L Finger4
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB L Clavicle/TB L UpperArm/TB L
      Forearm/TB L Hand/TB L Finger4/TB L Finger41
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB Neck
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB Neck/TB Head
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB Neck/TB Head/TB Eyebrows
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB Neck/TB Head/TB EyeL
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB Neck/TB Head/TB EyeR
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB Neck/TB Head/TB Jaw
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB R Clavicle
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB R Clavicle/TB R UpperArm
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB R Clavicle/TB R UpperArm/TB R
      Forearm
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB R Clavicle/TB R UpperArm/TB R
      Forearm/TB R Hand
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB R Clavicle/TB R UpperArm/TB R
      Forearm/TB R Hand/TB R Finger0
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB R Clavicle/TB R UpperArm/TB R
      Forearm/TB R Hand/TB R Finger0/TB R Finger01
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB R Clavicle/TB R UpperArm/TB R
      Forearm/TB R Hand/TB R Finger1
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB R Clavicle/TB R UpperArm/TB R
      Forearm/TB R Hand/TB R Finger1/TB R Finger11
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB R Clavicle/TB R UpperArm/TB R
      Forearm/TB R Hand/TB R Finger2
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB R Clavicle/TB R UpperArm/TB R
      Forearm/TB R Hand/TB R Finger2/TB R Finger21
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB R Clavicle/TB R UpperArm/TB R
      Forearm/TB R Hand/TB R Finger3
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB R Clavicle/TB R UpperArm/TB R
      Forearm/TB R Hand/TB R Finger3/TB R Finger31
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB R Clavicle/TB R UpperArm/TB R
      Forearm/TB R Hand/TB R Finger4
    m_Weight: 1
  - m_Path: ROOT/TB/TB Pelvis/TB Spine/TB Spine1/TB R Clavicle/TB R UpperArm/TB R
      Forearm/TB R Hand/TB R Finger4/TB R Finger41
    m_Weight: 1
  - m_Path: TB Body
    m_Weight: 0
  - m_Path: TB Eyes
    m_Weight: 0
  - m_Path: TB Face
    m_Weight: 0
