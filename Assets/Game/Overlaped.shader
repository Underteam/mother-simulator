Shader "Custom/Overlaped" {
Properties {
    _MainTex ("Base (RGB)", 2D) = "white" {}
}
SubShader {
    Tags { "RenderType"="Opaque" "Queue" = "Overlay" "IgnoreProjector"="True"}
    LOD 150

	Pass {
			Cull Off
            ZWrite  Off
            ZTest Off
			Lighting Off
			Blend One One
        }

CGPROGRAM
#pragma surface surf SimpleLambert noforwardadd

		half4 LightingSimpleLambert (SurfaceOutput s, half3 lightDir, half atten) {
              half NdotL = dot (s.Normal, lightDir);
              half4 c;
              c.rgb = s.Albedo;
              c.a = s.Alpha;
              return c;
          }

sampler2D _MainTex;

struct Input {
    float2 uv_MainTex;
};

void surf (Input IN, inout SurfaceOutput o) 
{
    fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
    o.Albedo = c.rgb;
    o.Alpha = c.a;
	
}
ENDCG
}

Fallback "Mobile/VertexLit"
}
