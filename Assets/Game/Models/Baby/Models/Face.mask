%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Face
  m_Mask: 00000000000000000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Baby_model_body
    m_Weight: 0
  - m_Path: Deaper
    m_Weight: 0
  - m_Path: Eyeball.l
    m_Weight: 0
  - m_Path: Eyeball.r
    m_Weight: 0
  - m_Path: Hair_1
    m_Weight: 0
  - m_Path: metarig
    m_Weight: 0
  - m_Path: metarig/Pelvis
    m_Weight: 0
  - m_Path: metarig/Pelvis/spine.001
    m_Weight: 0
  - m_Path: metarig/Pelvis/spine.001/spine.002
    m_Weight: 0
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003
    m_Weight: 0
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck
    m_Weight: 0
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eye_l
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eye_l/TB_Eye_l_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eye_r
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eye_r/TB_Eye_r_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyebrow_l
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyebrow_l/TB_Eyebrow_l_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyebrow_r
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyebrow_r/TB_Eyebrow_r_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_l_1_d
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_l_1_d/TB_Eyelid_l_1_d_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_l_1_u
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_l_1_u/TB_Eyelid_l_1_u_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_l_2_d
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_l_2_d/TB_Eyelid_l_2_d_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_l_2_u
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_l_2_u/TB_Eyelid_l_2_u_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_l_3_d
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_l_3_d/TB_Eyelid_l_3_d_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_l_3_u
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_l_3_u/TB_Eyelid_l_3_u_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_l_4_d
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_l_4_d/TB_Eyelid_l_4_d_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_l_4_u
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_l_4_u/TB_Eyelid_l_4_u_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_l_5_d
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_l_5_d/TB_Eyelid_l_5_d_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_l_5_u
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_l_5_u/TB_Eyelid_l_5_u_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_r_1_d
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_r_1_d/TB_Eyelid_r_1_d_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_r_1_u
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_r_1_u/TB_Eyelid_r_1_u_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_r_2_d
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_r_2_d/TB_Eyelid_r_2_d_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_r_2_u
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_r_2_u/TB_Eyelid_r_2_u_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_r_3_d
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_r_3_d/TB_Eyelid_r_3_d_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_r_3_u
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_r_3_u/TB_Eyelid_r_3_u_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_r_4_d
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_r_4_d/TB_Eyelid_r_4_d_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_r_4_u
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_r_4_u/TB_Eyelid_r_4_u_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_r_5_d
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_r_5_d/TB_Eyelid_r_5_d_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_r_5_u
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Eyelid_r_5_u/TB_Eyelid_r_5_u_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_jaw_c
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_jaw_c/TB_jaw_c_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_jaw_l
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_jaw_l/TB_jaw_l_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_jaw_r
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_jaw_r/TB_jaw_r_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Lip_d_c
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Lip_d_c/TB_Lip_d_c_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Lip_d_l
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Lip_d_l/TB_Lip_d_l_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Lip_d_r
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Lip_d_r/TB_Lip_d_r_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Lip_l
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Lip_l/TB_Lip_l_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Lip_r
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Lip_r/TB_Lip_r_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Lip_u_c
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Lip_u_c/TB_Lip_u_c_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Lip_u_l
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Lip_u_l/TB_Lip_u_l_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Lip_u_r
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_Lip_u_r/TB_Lip_u_r_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_mouth
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/Neck/Head/TB_mouth/TB_mouth_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L
    m_Weight: 0
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.01.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.01.L/f_index.01.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.01.L/f_index.01.L/f_index.02.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.01.L/f_index.01.L/f_index.02.L/f_index.03.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.01.L/f_index.01.L/f_index.02.L/f_index.03.L/f_index.03.L_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.01.L/thumb.01.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.01.L/thumb.01.L/thumb.02.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.01.L/thumb.01.L/thumb.02.L/thumb.03.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.01.L/thumb.01.L/thumb.02.L/thumb.03.L/thumb.03.L_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.02.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.02.L/f_middle.01.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.02.L/f_middle.01.L/f_middle.02.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.02.L/f_middle.01.L/f_middle.02.L/f_middle.03.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.02.L/f_middle.01.L/f_middle.02.L/f_middle.03.L/f_middle.03.L_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.03.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.03.L/f_ring.01.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.03.L/f_ring.01.L/f_ring.02.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.03.L/f_ring.01.L/f_ring.02.L/f_ring.03.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.03.L/f_ring.01.L/f_ring.02.L/f_ring.03.L/f_ring.03.L_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.04.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.04.L/f_pinky.01.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.04.L/f_pinky.01.L/f_pinky.02.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.04.L/f_pinky.01.L/f_pinky.02.L/f_pinky.03.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/palm.04.L/f_pinky.01.L/f_pinky.02.L/f_pinky.03.L/f_pinky.03.L_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R
    m_Weight: 0
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.01.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.01.R/f_index.01.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.01.R/f_index.01.R/f_index.02.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.01.R/f_index.01.R/f_index.02.R/f_index.03.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.01.R/f_index.01.R/f_index.02.R/f_index.03.R/f_index.03.R_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.01.R/thumb.01.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.01.R/thumb.01.R/thumb.02.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.01.R/thumb.01.R/thumb.02.R/thumb.03.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.01.R/thumb.01.R/thumb.02.R/thumb.03.R/thumb.03.R_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.02.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.02.R/f_middle.01.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.02.R/f_middle.01.R/f_middle.02.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.02.R/f_middle.01.R/f_middle.02.R/f_middle.03.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.02.R/f_middle.01.R/f_middle.02.R/f_middle.03.R/f_middle.03.R_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.03.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.03.R/f_ring.01.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.03.R/f_ring.01.R/f_ring.02.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.03.R/f_ring.01.R/f_ring.02.R/f_ring.03.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.03.R/f_ring.01.R/f_ring.02.R/f_ring.03.R/f_ring.03.R_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.04.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.04.R/f_pinky.01.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.04.R/f_pinky.01.R/f_pinky.02.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.04.R/f_pinky.01.R/f_pinky.02.R/f_pinky.03.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/palm.04.R/f_pinky.01.R/f_pinky.02.R/f_pinky.03.R/f_pinky.03.R_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/thigh.L
    m_Weight: 0
  - m_Path: metarig/Pelvis/thigh.L/shin.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/thigh.L/shin.L/foot.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/thigh.L/shin.L/foot.L/toe.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/thigh.L/shin.L/foot.L/toe.L/toe.L_end
    m_Weight: 1
  - m_Path: metarig/Pelvis/thigh.R
    m_Weight: 0
  - m_Path: metarig/Pelvis/thigh.R/shin.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/thigh.R/shin.R/foot.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/thigh.R/shin.R/foot.R/toe.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/thigh.R/shin.R/foot.R/toe.R/toe.R_end
    m_Weight: 1
  - m_Path: Mouth
    m_Weight: 0
