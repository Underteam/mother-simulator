﻿Shader "Custom/Decal2" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Base (RGB)", 2D) = "white" {}
		_DecalTex("Decal", 2D) = "white" {}
		_DecalIntensity("DecalIntensity", Range(0,1)) = 0.0
	}
		SubShader{
			Tags{ "RenderType" = "Opaque" "Queue" = "Geometry+1" "ForceNoShadowCasting" = "True" }
		LOD 200
		Offset -1, -1

		CGPROGRAM
			#pragma surface surf Lambert decal:blend

				sampler2D _MainTex;
				sampler2D _DecalTex;

				struct Input {
					float2 uv_MainTex;
					float2 uv_DecalTex;
				};

				half _DecalIntensity;
				fixed4 _Color;

				void surf(Input IN, inout SurfaceOutput o) {
					// Albedo comes from a texture tinted by color
					fixed4 c = tex2D(_MainTex, IN.uv_MainTex);

					half4 decal_intens = tex2D(_DecalTex, IN.uv_DecalTex) * _DecalIntensity;
					o.Albedo = c.rgb * lerp(half4(1, 1, 1, 1), half4(0, 0, 0, 0), decal_intens) * _Color;
					o.Alpha = c.a;
				}
			ENDCG
		}
}