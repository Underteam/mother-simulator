﻿Shader "Custom/Decal2" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_ColorDecal("ColorDecal", Color) = (1,1,1,1)
		_MainTex("Base (RGB)", 2D) = "white" {}
		_DecalTex("Decal", 2D) = "white" {}
		_DecalIntensity("DecalIntensity", Range(0,1)) = 0.0
	}
		SubShader{
			Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		LOD 200
		Offset -1, -1

		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		CGPROGRAM
			//#pragma surface surf Lambert decal:blend
			#pragma surface surf Lambert decal:blend

				sampler2D _MainTex;
				sampler2D _DecalTex;

				struct Input {
					float2 uv_MainTex;
					float2 uv_DecalTex;
				};

				half _DecalIntensity;
				fixed4 _Color;
				fixed4 _ColorDecal;

				void surf(Input IN, inout SurfaceOutput o) {
					// Albedo comes from a texture tinted by color
					fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
					fixed4 decal = tex2D(_DecalTex, IN.uv_DecalTex);

					decal = lerp(decal, half4(1,1,1,1), 1-decal.a);
					half4 result = lerp(c, c*decal, _DecalIntensity) * _Color;
					//half4 decal_intens = tex2D(_DecalTex, IN.uv_DecalTex) * _DecalIntensity;
					//o.Albedo = c.rgb * lerp(half4(1, 1, 1, 1), half4(0, 0, 0, 0), decal_intens);


					//half4 c = tex2D(_MainTex, IN.uv_MainTex);
					o.Albedo = result.rgb;
					o.Alpha = 1.0f;
				}
			ENDCG
		}
}