﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent (typeof(AudioSource))]
public class StegobubblesLogoScript : MonoBehaviour {

    public GameObject policyPanel, stegoPanel;

    public Sprite[] sprites;
    public Image image;
    public Image explosion;
    public float delay = 0.13f;

    int spriteIndex = 0;

    public AudioClip bubbleSound;
    public Animator anim;
    
    public void Explosion()
    {
        audioSource.PlayOneShot(bubbleSound);
        RectTransform explosionRectTransform = explosion.rectTransform;
        CopyTransform(ref explosionRectTransform, (image.transform.GetChild(0).transform as RectTransform));
        image.gameObject.SetActive(false);
        if(coruetine!=null)
            StopCoroutine(coruetine);
        anim.SetTrigger("Explosion");
    }

    public void CopyTransform(ref RectTransform copyTo, RectTransform rectTransform)
    {
        //copyTo.anchorMin = rectTransform.anchorMin;
        //copyTo.anchorMax = rectTransform.anchorMax;
        copyTo.anchoredPosition = rectTransform.anchoredPosition;
        //copyTo.sizeDelta = rectTransform.sizeDelta;
    }

    IEnumerator ChangeSpriteCoruetine()
    {
        while(true)
        {
            yield return new WaitForSeconds(delay);

            if (sprites.Length > 0 && image != null)
            {
                if (++spriteIndex >= sprites.Length)
                    spriteIndex = 0;

                image.sprite = sprites[spriteIndex];
            }
        }
    }

    IEnumerator LoadAsyncLoading()
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex+1);
        async.allowSceneActivation = false;
        yield return new WaitForSeconds(preLoadDelayMinutes);
        async.allowSceneActivation = true;
    }

    AudioSource audioSource;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void SkipPolicy()
    {
        PlayerPrefs.SetInt("isPolicyAgree", 1);
        stegoPanel.gameObject.SetActive(true);
        policyPanel.gameObject.SetActive(false);
        coruetine = StartCoroutine(ChangeSpriteCoruetine());
        StartCoroutine(LoadAsyncLoading());
    }

    Coroutine coruetine;
    [Header("Время перед загрузкой, с")]
    public float preLoadDelayMinutes = 3;
    public bool usePolicy;
    private void Start()
    {
        if (!usePolicy || PlayerPrefs.GetInt("isPolicyAgree",0)==1)
        {
            SkipPolicy();
        }
        else
        {
            stegoPanel.gameObject.SetActive(false);
            policyPanel.gameObject.SetActive(true);
        }
    }
}
