﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonOpenURL : MonoBehaviour {
    
    public string onlyForLanguage = "";
    private void Start()
    {
        if (onlyForLanguage != "" && Application.systemLanguage.ToString() != onlyForLanguage)
            {
                gameObject.SetActive(false);
            }
    }

    public string url;
    public void OpenURL()
    {
        Application.OpenURL(url);
    }
}
