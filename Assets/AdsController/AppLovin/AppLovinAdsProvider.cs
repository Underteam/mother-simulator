﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppLovinAdsProvider : AdsProvider
{
    private bool showing;
    private bool waitingForAdEnd;
    private Action<bool> onFinish;

    private bool bannerLoaded;

    private string bannerAdUnitId;
    private string interstitialAdUnitId;
    private string rewardedAdUnitId;

    public override void Init()
    {
        Debug.LogError("Init applovin");

        if (inited) return;

        #if UNITY_ANDROID
        bannerAdUnitId = bannerAdUnitIdAndroid;
        #else
        bannerAdUnitId = bannerAdUnitIdIOS;
        #endif

        #if UNITY_ANDROID
        interstitialAdUnitId = interstitialAdUnitIdAndroid;
        #else
        interstitialAdUnitId = interstitialAdUnitIdIOS;
        #endif

        #if UNITY_ANDROID
        rewardedAdUnitId = rewardedAdUnitIdAndroid;
        #else
        rewardedAdUnitId = rewardedAdUnitIdIOS;
        #endif

        MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguration) => 
        {
            Debug.LogError("applovin initialized");

            int age = PlayerPrefs.GetInt("AgeSettings", 12);
            MaxSdk.SetIsAgeRestrictedUser(age < 13);

            //MaxSdk.SetDoNotSell(true);
            //MaxSdk.SetHasUserConsent(false);

            if (!string.IsNullOrEmpty(interstitialAdUnitId)) InitializeInterstitialAds();
            if (!string.IsNullOrEmpty(rewardedAdUnitId)) InitializeRewardedAds();

            Debug.LogError("applovin initialized 2");

            // AppLovin SDK is initialized, start loading ads
            inited = true;
        };

        //MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguration) => {
        //    // Show Mediation Debugger
        //    MaxSdk.ShowMediationDebugger();
        //};

        //MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguration) => 
        //{
        //    if (sdkConfiguration.ConsentDialogState == MaxSdkBase.ConsentDialogState.Applies)
        //    {
        //        // Show user consent dialog

        //        // If the user has consented, please set the following flag to true
        //        MaxSdk.SetHasUserConsent(true);
        //    }
        //    else if (sdkConfiguration.ConsentDialogState == MaxSdkBase.ConsentDialogState.DoesNotApply)
        //    {
        //        // No need to show consent dialog, proceed with initialization

        //        // If the user has not consented, please set the following flag to false.
        //        MaxSdk.SetHasUserConsent(false);
        //    }
        //    else
        //    {
        //        // Consent dialog state is unknown. Proceed with initialization, but check if the consent
        //        // dialog should be shown on the next application initialization
        //    }

        //    //MaxSdk.SetIsAgeRestrictedUser(false);
        //    //MaxSdk.SetDoNotSell(false);

        //    //MaxSdk.SetBannerPlacement(BANNER_AD_UNIT_ID, "MY_BANNER_PLACEMENT");
        //    //MaxSdk.ShowInterstitial(INTER_AD_UNIT_ID, "MY_INTER_PLACEMENT");
        //    //MaxSdk.ShowRewardedAd(REWARDED_AD_UNIT_ID, "MY_REWARDED_PLACEMENT");

        //};

        MaxSdk.SetSdkKey("5NQJ-Go6iJhpS_yCFNB-aRIG14EBroCx9cE7u0EbKwOnvL-8i97IbPTcUooKZ-FkbTsUU44q_TaupT1Wi9pIZM");
        MaxSdk.InitializeSdk();
        
        Debug.LogError("Initialize Aplovin SDK");
    }

	public string bannerAdUnitIdAndroid = "YOUR_BANNER_AD_UNIT_ID"; // Retrieve the id from your account
    public string bannerAdUnitIdIOS = "YOUR_BANNER_AD_UNIT_ID";

    public void InitializeBannerAds()
    {
        MaxSdkCallbacks.OnBannerAdLoadedEvent += OnBannerLoadedEvent;

        // Banners are automatically sized to 320x50 on phones and 728x90 on tablets
        // You may use the utility method `MaxSdkUtils.isTablet()` to help with view sizing adjustments
        MaxSdk.CreateBanner(bannerAdUnitId, MaxSdkBase.BannerPosition.BottomCenter);

        // Set background or background color for banners to be fully functional
        MaxSdk.SetBannerBackgroundColor(bannerAdUnitId, Color.green);
    }

    private void OnBannerLoadedEvent(string adUnitId)
    {
        bannerLoaded = true;
    }


	public string interstitialAdUnitIdAndroid = "YOUR_INTERSTITIAL_AD_UNIT_ID";
    public string interstitialAdUnitIdIOS = "YOUR_INTERSTITIAL_AD_UNIT_ID";


    private int interstitialRetryAttempt;

    public void InitializeInterstitialAds()
    {
        // Attach callback
        MaxSdkCallbacks.OnInterstitialLoadedEvent += OnInterstitialLoadedEvent;
        MaxSdkCallbacks.OnInterstitialLoadFailedEvent += OnInterstitialFailedEvent;
        MaxSdkCallbacks.OnInterstitialAdFailedToDisplayEvent += InterstitialFailedToDisplayEvent;
        MaxSdkCallbacks.OnInterstitialHiddenEvent += OnInterstitialDismissedEvent;

        // Load the first interstitial
        LoadInterstitial();
    }

    private void LoadInterstitial()
    {
        MaxSdk.LoadInterstitial(interstitialAdUnitId);
    }

    private void OnInterstitialLoadedEvent(string adUnitId)
    {
        Debug.LogError("OnInterstitialLoadedEvent");
        // Interstitial ad is ready to be shown. MaxSdk.IsInterstitialReady(interstitialAdUnitId) will now return 'true'

        // Reset retry attempt
        interstitialRetryAttempt = 0;
    }

    private void OnInterstitialFailedEvent(string adUnitId, int errorCode)
    {
        Debug.LogError("OnInterstitialFailedEvent " + errorCode);
        // Interstitial ad failed to load. We recommend retrying with exponentially higher delays.

        interstitialRetryAttempt++;
        double retryDelay = Mathf.Pow(2, interstitialRetryAttempt);

        Invoke("LoadInterstitial", (float)retryDelay);
    }

    private void InterstitialFailedToDisplayEvent(string adUnitId, int errorCode)
    {
        showing = false;

        if (onFinish != null) onFinish(true);

        // Interstitial ad failed to display. We recommend loading the next ad
        LoadInterstitial();
    }

    private void OnInterstitialDismissedEvent(string adUnitId)
    {
        showing = false;

        if (onFinish != null) onFinish(true);

        // Interstitial ad is hidden. Pre-load the next ad
        LoadInterstitial();
    }

	public string rewardedAdUnitIdAndroid = "YOUR_REWARDED_AD_UNIT_ID";
    public string rewardedAdUnitIdIOS = "YOUR_REWARDED_AD_UNIT_ID";
    private int rewardedRetryAttempt;

    public void InitializeRewardedAds()
    {
        // Attach callback
        MaxSdkCallbacks.OnRewardedAdLoadedEvent += OnRewardedAdLoadedEvent;
        MaxSdkCallbacks.OnRewardedAdLoadFailedEvent += OnRewardedAdFailedEvent;
        MaxSdkCallbacks.OnRewardedAdFailedToDisplayEvent += OnRewardedAdFailedToDisplayEvent;
        MaxSdkCallbacks.OnRewardedAdDisplayedEvent += OnRewardedAdDisplayedEvent;
        MaxSdkCallbacks.OnRewardedAdClickedEvent += OnRewardedAdClickedEvent;
        MaxSdkCallbacks.OnRewardedAdHiddenEvent += OnRewardedAdDismissedEvent;
        MaxSdkCallbacks.OnRewardedAdReceivedRewardEvent += OnRewardedAdReceivedRewardEvent;

        // Load the first RewardedAd
        LoadRewardedAd();
    }

    private void LoadRewardedAd()
    {
        MaxSdk.LoadRewardedAd(rewardedAdUnitId);
    }

    private void OnRewardedAdLoadedEvent(string adUnitId)
    {
        Debug.LogError("OnRewardedAdLoadedEvent");
        // Rewarded ad is ready to be shown. MaxSdk.IsRewardedAdReady(rewardedAdUnitId) will now return 'true'

        // Reset retry attempt
        rewardedRetryAttempt = 0;
    }

    private void OnRewardedAdFailedEvent(string adUnitId, int errorCode)
    {
        Debug.LogError("OnRewardedAdFailedEvent " + errorCode);
        // Rewarded ad failed to load. We recommend retrying with exponentially higher delays.

        rewardedRetryAttempt++;
        double retryDelay = Mathf.Pow(2, rewardedRetryAttempt);

        Invoke("LoadRewardedAd", (float)retryDelay);
    }

    private void OnRewardedAdFailedToDisplayEvent(string adUnitId, int errorCode)
    {
        showing = false;

        if (onFinish != null) onFinish(false);

        // Rewarded ad failed to display. We recommend loading the next ad
        LoadRewardedAd();
    }

    private void OnRewardedAdDisplayedEvent(string adUnitId) { }

    private void OnRewardedAdClickedEvent(string adUnitId) { }

    private void OnRewardedAdDismissedEvent(string adUnitId)
    {
        showing = false;

        if (onFinish != null) onFinish(false);

        // Rewarded ad is hidden. Pre-load the next ad
        LoadRewardedAd();
    }

    private void OnRewardedAdReceivedRewardEvent(string adUnitId, MaxSdk.Reward reward)
    {
        // Rewarded ad was displayed and user should receive the reward

        showing = false;

        if (onFinish != null) onFinish(true);

        //?? LoadRewardedAd();
    }

    public override void HideAd(AdType type)
    {
        if (type == AdType.banner)
        {
            MaxSdk.HideBanner(bannerAdUnitId);
        }
    }

    public override bool IsAdReady(AdType type)
    {
        if (showing) return false;

		if (type == AdType.banner) return bannerLoaded;
		if (type == AdType.interstitial || type == AdType.skipablevideo) return MaxSdk.IsInterstitialReady(interstitialAdUnitId);
        if (type == AdType.unskipablevideo) return MaxSdk.IsRewardedAdReady(rewardedAdUnitId);

        return false;
    }

    public override void ShowAd(AdType type, Action<bool> onFinish = null)
    {
        if (!inited)
        {
            Init();
            if (!inited) return;
        }

        if (showing) return;

        waitingForAdEnd = true;

        this.onFinish = onFinish;

        if (type == AdType.banner)
        {
            MaxSdk.ShowBanner(bannerAdUnitId);
        }

        else if (type == AdType.interstitial || type == AdType.skipablevideo)
        {
            if (MaxSdk.IsInterstitialReady(interstitialAdUnitId))
            {
                MaxSdk.ShowInterstitial(interstitialAdUnitId);
            }
        }

        else if (type == AdType.unskipablevideo)
        {
            if (MaxSdk.IsRewardedAdReady(rewardedAdUnitId))
            {
                MaxSdk.ShowRewardedAd(rewardedAdUnitId);
            }
        }
    }
}
