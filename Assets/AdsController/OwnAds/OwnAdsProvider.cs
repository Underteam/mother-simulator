﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class OwnAdsProvider : AdsProvider {

	public Canvas adCanvas;

	public Image imgVertical;
	public Image imgHorizontal;
	private Image img;

	private enum Orientation {
		vertical,
		horizontal,
	}

	private Orientation orientation;

	public string verticalImageURL = "";

	public string horizontalImageURL = "";

	private string imageURL;

	private bool updateImage = true;

	private Action<bool> onFinish;

	public override void Init() {

		if (adsSettings != null) {
			gameUrl = adsSettings.GetValue<string> ("GAMEURL", gameUrl);
			updateImage = adsSettings.GetValue<bool> ("UPDATEIMAGE", updateImage);
			chance = adsSettings.GetValue<float> (name + "CHANCE", chance);
		}

		adCanvas.gameObject.SetActive (false);
		imgVertical.gameObject.SetActive (false);
		imgHorizontal.gameObject.SetActive (false);

		float a = (float)Screen.width / Screen.height;
		if (a > 1) {
			orientation = Orientation.horizontal;
			img = imgHorizontal;
		} else {
			orientation = Orientation.vertical;
			img = imgVertical;
		}

		Initialize (updateImage);

		inited = true;
	}

	private void Initialize (bool updateImage) {

        if (AdsController.debugLogs) Debug.LogError ("!!!!!!!!!!!![OwnAdsProvider.Initialize] update image " + updateImage);

		imageURL = verticalImageURL;
		if ((float)Screen.width / Screen.height > 1)
			imageURL = horizontalImageURL;

		string fname = "horizontalImage.png";
		if (orientation == Orientation.vertical)
			fname = "verticalImage.png";

		if (!updateImage && System.IO.File.Exists (Application.persistentDataPath + "/Data/" + fname)) {
            if (AdsController.debugLogs) Debug.LogError ("!!!!!!!!!!!![OwnAdsProvider.Initialize] load local image");
			Texture2D tex = LoadTexture (Application.persistentDataPath + "/Data/" + fname);
			if (tex != null) {
				img.sprite = Sprite.Create (tex, Rect.MinMaxRect (0, 0, tex.width, tex.height), Vector2.zero);
				imageReady = true;
				return;
			}
		}

#if UNITY_2017_3_OR_NEWER

#if UNITY_ANDROID
        imageURL +=  Application.identifier + "&market=android";
#elif UNITY_IOS
		imageURL += Application.identifier + "&market=ios";
#endif

#else

#if UNITY_ANDROID
        imageURL +=  Application.identifier + "&market=android";
#elif UNITY_IOS
		imageURL += Application.bundleIdentifier + "&market=ios";
#endif

#endif


        GMADownloader.Instance().Download(imageURL, ImageDownloaded, DownloadError);
	}

	private void DownloadError(object result) {

		Logger.Instance ().Log ("[OwnAdsProvider.DownloadError]");

		if(!(result is WWW)) return;

		WWW www = (WWW)result;

		Logger.Instance().Log ("[DownloadError] " + www.url + " failed:" + www.error);
	}

	private bool imageReady;
	private void ImageDownloaded(object result) {

		Logger.Instance ().Log ("Image downloaded!!!!!!!!!!!");

		WWW www = result as WWW;
		if(www == null) return;

		string fname = "horizontalImage.png";
		if (orientation == Orientation.vertical)
			fname = "verticalImage.png";

		string wp;
		StoreFile (www, fname, out wp);

		imageReady = true;
		img.sprite = Sprite.Create (www.texture, Rect.MinMaxRect (0, 0, www.texture.width , www.texture.height), Vector2.zero);

		www.Dispose ();
	}

	private Texture2D LoadTexture(string path) {
	
		if (!System.IO.File.Exists (path))
			return null;

		byte[] data = System.IO.File.ReadAllBytes (path);

		Texture2D tex = new Texture2D (2, 2, TextureFormat.RGB24, false);

		tex.LoadImage (data);

		return tex;
	}

	private bool StoreFile(WWW www, string fname, out string write_path) {

		try {
			if (System.IO.Directory.Exists(Application.persistentDataPath + "/Data") == false)
				System.IO.Directory.CreateDirectory(Application.persistentDataPath + "/Data");

			write_path = Application.persistentDataPath + "/Data/" + fname;

			Logger.Instance ().Log ("Store to " + write_path);

			if (System.IO.File.Exists (write_path)) {
				Logger.Instance ().Log ("Delete file");
				System.IO.File.Delete (write_path);
			}

			Logger.Instance ().Log ("Write file");
			System.IO.File.WriteAllBytes(write_path, www.bytes);

			return true;

		} catch (System.Exception e) {

			Logger.Instance ().Log ("Exception: " + e.ToString());
			write_path = "";

			return false;
		}

		return false;
	}

	public override bool IsAdReady(AdType type) {

		if (chance <= 0) return false;
		
		if (type == AdType.interstitial)
			return imageReady;

		return false;
	}

	private bool shouldShowBanner;

	public override void ShowAd (AdType type, Action<bool> onFinish = null)
	{
		if (!inited)
		{
			Init();
			if (!inited) return;
		}

		if (chance <= 0) return;

		if (!imageReady) {
			return;
		}

		if (type == AdType.interstitial) {
		
			this.onFinish = onFinish;

			try {
				StartShow ();
			} catch (System.Exception e) {
				Logger.Instance ().Log ("Exception " + e);
				return;
			}

			shouldShowBanner = AdsController.Instance ().IsBannerShowing ();
			AdsController.Instance ().HideBanner ();

			adCanvas.gameObject.SetActive (true);
			ShowImage ();
		}
	}

	private void ShowImage() {

		img.gameObject.SetActive (true);
	}

	public void Close() {

		img.gameObject.SetActive (false);

		adCanvas.gameObject.SetActive (false);

		StopShow ();
	}

	[SerializeField]private string gameUrl;
	public void GoToUrl() {

		Invoke("OpenURL", 0.1f);

		Close ();
	}

	private void OpenURL() {

		Application.OpenURL (gameUrl);
	}

	//public GameObject toShow;
	private GameObject[] objects;
	private List<bool> states = new List<bool>();
	private bool audoListenerPause;
	private float timeScale;
	private float volume;
	private void StartShow() {

		volume = AudioListener.volume;
		audoListenerPause = AudioListener.pause;
		timeScale = Time.timeScale;

		AudioListener.volume = 1;
		AudioListener.pause = false;
		Time.timeScale = 1;

		objects = RootObjects ();

		states.Clear ();

		for (int i = 0; i < objects.Length; i++) {

			if (objects [i] == null) {
				states.Add (false);
				continue;
			}

			states.Add (objects [i].activeSelf);

			if(objects[i] == gameObject) continue;
			if(objects[i] == GMADownloader.Instance().gameObject) continue;

			objects [i].SetActive (false);
		}
	}

	private void StopShow() {

		AudioListener.volume = volume;
		AudioListener.pause = audoListenerPause;
		Time.timeScale = timeScale;

		if(objects == null) return;

		for (int i = 0; i < objects.Length; i++) {

			if(objects[i] == null) continue;

			if(objects[i] == gameObject) continue;
			if(objects[i] == GMADownloader.Instance().gameObject) continue;

			objects [i].SetActive (states[i]);
		}

		if (shouldShowBanner)
			AdsController.Instance ().ShowTopBanner ();
		
		if (onFinish != null) onFinish (true);
	}

	private GameObject[] RootObjects() {

		//#if UNITY_5_3_OR_NEWER
		//return UnityEngine.SceneManagement.SceneManager.GetActiveScene ().GetRootGameObjects ();
		//#else

		List<GameObject> list = new List<GameObject> ();
		Transform[] trs = FindObjectsOfType<Transform> ();

		for(int i = 0; i < trs.Length; i++) {
		GameObject go = trs [i].root.gameObject;
		if (!list.Contains (go))
		list.Add (go);
		}

		return list.ToArray();
		//#endif
	}
}
