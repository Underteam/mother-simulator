﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlScript : MonoBehaviour {

	public Dropdown adsProvider;

	public Dropdown adType;

	private List<AdsProvider> providers;

	private AdsController adsController;

	public Text lblReady;

	// Use this for initialization
	void Start () {
	
		adsController = AdsController.Instance ();

		Debug.Log ("Time scale " + Time.timeScale);

		Time.timeScale = 1;

		List<string> adsTypes =  new List<string>(System.Enum.GetNames (typeof(AdsProvider.AdType)));

		adType.AddOptions (adsTypes);
	
		providers = adsController.providers;
		List<string> adsProviders = new List<string> ();
		for (int i = 0; i < providers.Count; i++)
			adsProviders.Add (providers [i].name);
		adsProviders.Add ("All");
		
		adsProvider.AddOptions (adsProviders);

		adsProvider.value = adsProviders.Count - 1;

		Check ();
	}
	
	// Update is called once per frame
	void Update () {

		Check ();
	}

	public void AdsProviderSelected () {
	
		Debug.Log ("AdsProvider selected " + adsProvider.itemText + " " + adsProvider.value);

		Check ();
	}

	public void AdTypeSelected () {

		Debug.Log ("AdType selected " + adType.itemText + " " + adType.value);

		Check ();
	}

	private void Check () {

		if (adsProvider.value >= adsController.providers.Count) {
		
			for (int i = 0; i < adsController.providers.Count; i++)
				adsController.providers [i].enabled = true;
			
		} else {
		
			for (int i = 0; i < adsController.providers.Count; i++)
				adsController.providers [i].enabled = false;

			adsController.providers [adsProvider.value].enabled = true;
		}

		if (adsController.IsAdReady ((AdsProvider.AdType)adType.value))
			lblReady.text = "Ready";
		else
			lblReady.text = "Not ready";
	}

	public void ShowAd () {
	
		Check ();

		adsController.ShowAd ((AdsProvider.AdType)adType.value);
	}

	public void LoadScene(int s) {
	
		Application.LoadLevel (s);
	}
}
