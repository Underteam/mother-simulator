﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class AdsController : MonoBehaviour {

    public static bool debugLogs = false;

    public List<AdsProvider> providers;

	private static AdsController instance;

	private delegate void OnUpdate();

	public AdsSettings adsSettings;

	private bool ready;

	public Action onVideoFinished;

    public bool debugMode { get; set; }

	public static AdsController Instance()
    {
        if (instance == null)
        {
            Debug.LogError("Access ads controller");
            instance = FindObjectOfType<AdsController>();
            if (instance == null)
            {
                GameObject go = new GameObject("AdsController");
                instance = go.AddComponent<AdsController>();
            }
        }

        return instance;
    }

    public bool noAds;
    public void NoAds ()
    {
        noAds = true;
    }

    void Awake()
    {
		
        if (instance != null && instance != this) {
			DestroyImmediate(this.gameObject);
			return;
		}
        else instance = this;

		DontDestroyOnLoad (instance.gameObject);

		//showing = new Dictionary<AdsProvider.AdType, bool> ();
    }

	// Use this for initialization
	void Start () {
	    
		Screen.sleepTimeout = SleepTimeout.NeverSleep;

		if(!ready) StartCoroutine (InitializeCoroutine ());
	}
		
	private IEnumerator InitializeCoroutine ()
    {
        Debug.LogError("Initialize ads controller");

        float timer = 30;
        while (adsSettings != null && !adsSettings.ready && timer > 0)
        {
            timer -= Time.unscaledDeltaTime;
            yield return null;
        }

        try
        {
            if (providers != null)
            {
                for (int i = 0; i < providers.Count; i++)
                {
                    if (providers[i] == null)
                    {
                        Debug.LogError("1 Null provider " + i);
                        continue;
                    }
                    providers[i].SetSettings(adsSettings);
                }

                for (int i = 0; i < providers.Count; i++)
                {
                    if (providers[i] == null)
                    {
                        Debug.LogError("2 Null provider " + i);
                        continue;
                    }
                    providers[i].Init();
                }
            }
            else
            {
                Debug.LogError("Providers is null");
            }
        }
        catch (System.Exception e)
        {
            Debug.LogError("Error: " + e.ToString());
            yield break;
        }

		ready = true;
	}

    public void RemoveProvider(string provider)
    {
        for (int i = 0; i < providers.Count; i++)
        {
            if (providers[i].provider == provider)
            {
                providers.RemoveAt(i);
                i--;
            }
        }
    }

	// Update is called once per frame
	void Update () {

		if (!ready) return;

		if (shouldShowBanner && IsAdReady (AdsProvider.AdType.banner)) {
			shouldShowBanner = false;
			ShowAd (AdsProvider.AdType.banner);
		}
		
		for (int i = 0; i < providers.Count; i++)
			if (!providers [i].enabled)
				continue;
			else if (providers [i].update != null)
				providers [i].update ();
	}

	//private Dictionary<AdsProvider.AdType, bool> showing = new Dictionary<AdsProvider.AdType, bool> ();

	private int count = 0;

	public void ShowAd(AdsProvider.AdType type, System.Action<bool> onFinish = null) {

        if (debugMode)
        {
            if (onFinish != null) onFinish(true);
            return;
        }

        if (!ready)
        {
            if (onFinish != null) onFinish(false);
            return;
        }

		//if (!showing.ContainsKey (type)) showing.Add (type, false);
			
		Logger.Instance().Log ("ShowAd " + type + " " + " " + (onFinish == null));

		if (count % 2 == 0) {
			for (int i = 0; i < providers.Count; i++) {
				if (!providers [i].enabled) continue;
				if (!providers [i].onEven) continue;
				if (UnityEngine.Random.Range(0f, 100f) < providers[i].chance && providers [i].IsAdReady (type)) {
					Logger.Instance().Log("Show " + providers [i].provider);
					providers [i].ShowAd (type, onFinish);
					count++;
					return;
				}
			}
		} else {
			for (int i = 0; i < providers.Count; i++) {
				if (!providers [i].enabled) continue;
				if (!providers [i].onOdd) continue;
				if (UnityEngine.Random.Range(0f, 100f) < providers[i].chance && providers [i].IsAdReady (type)) {
					Logger.Instance().Log("Show " + providers [i].provider);
					providers [i].ShowAd (type, onFinish);
					count++;
					return;
				}
			}
		}

		/*for (int i = 0; i < providers.Count; i++) {
			if (!providers [i].enabled) continue;
			if (UnityEngine.Random.Range(0f, 100f) < providers[i].chance && providers [i].IsAdReady (type)) {
				Logger.Instance().Log("Show " + providers [i].provider);
				//showing [type] = true;
				//providers [i].ShowAd (type, (b) => {showing [type] = false; if(onFinish != null) onFinish(b);});
				providers [i].ShowAd (type, onFinish);
				return;
			}
		}//*/

		for (int i = 0; i < providers.Count; i++) {
			if (!providers [i].enabled) continue;
			if (providers [i].IsAdReady (type)) {
				Logger.Instance().Log("Show " + providers [i].provider);
				providers [i].ShowAd (type, onFinish);
				count++;
				return;
			}
		}

		Logger.Instance ().Log ("Failed " + IsAdReady (type));

        if (onFinish != null) onFinish(false);

    }

	public bool IsAdReady(AdsProvider.AdType type) {

		if (!ready) return false;

		for (int i = 0; i < providers.Count; i++) {
			if (!providers [i].enabled) continue;
			if (providers [i].IsAdReady (type))
				return true;
		}

		return false;
	}

	public bool IsBannerShowing() {
	
		for (int i = 0; i < providers.Count; i++) {
			if (providers [i].IsBannerShowing ())
				return true;
		}

		return false;
	}

	public void HideBanner () {
	
		for (int i = 0; i < providers.Count; i++)
			providers [i].HideBanner ();

		shouldShowBanner = false;
	}

	//void OnLevelWasLoaded(int level) {

	//	Logger.Instance ().Log ("Level was loaded " + adsSettings.ShouldShowTopBanner (level) + " " + adsSettings.ShouldShowInterstitial (level));

	//	if (adsSettings.ShouldShowTopBanner(level))
	//		ShowTopBanner ();
	//	else
	//		HideBanner ();

	//	if(adsSettings.ShouldShowInterstitial(level))
	//		TryToShowLargeBanner();
	//}

	public bool TryToShowLargeBanner(bool useOwnAds = true, Action<bool> onFinish = null) {

		float rnd = UnityEngine.Random.Range (0.0f, 1.0f);
        if (AdsController.debugLogs) Debug.Log ("Rnd " + rnd);
        if (rnd < 0.3f && IsAdReady(AdsProvider.AdType.interstitial))
        {
            ShowAd(AdsProvider.AdType.interstitial, onFinish);
            return true;
        }
        else if (IsAdReady(AdsProvider.AdType.skipablevideo))
        {
            ShowAd(AdsProvider.AdType.skipablevideo, onFinish);
            return true;
        }
        else if (IsAdReady(AdsProvider.AdType.interstitial))
        {
            ShowAd(AdsProvider.AdType.interstitial, onFinish);
            return true;
        }

        return false;
			
	}

	public void TryToShowVideoBanner() {

		if (IsAdReady (AdsProvider.AdType.unskipablevideo))
			ShowAd (AdsProvider.AdType.unskipablevideo, (b) => {
				if(b && onVideoFinished != null) onVideoFinished();
			});
	}

	public void TryToShowVideoBannerSkippable(bool useOwnAds = true) {

		if (IsAdReady (AdsProvider.AdType.skipablevideo))
			ShowAd (AdsProvider.AdType.skipablevideo);
	}

	private bool shouldShowBanner;

	public void ShowTopBanner() {

		shouldShowBanner = true;
	}
}
