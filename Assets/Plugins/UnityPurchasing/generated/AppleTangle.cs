#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("gp+Qn5WXgpPWlI/Wl5iP1oaXhILDxMfCxsXArOH7xcPGxMbPxMfCxsbn8PWj8vzl/LeGhpqT1r+YldjHdPf28P/ccL5wAZWS8/fGdwTG3PA2lcWBAczx2qAdLPnX+CxMhe+5Q9DG0vD1o/L95eu3hoaak9a1k4SCgp6ZhJ+Cj8fgxuLw9aPy9eX7t4aziOm6naZgt38ygpT95nW3ccV8d/P29XT3+fbGdPf89HT39/YSZ1//4Mbi8PWj8vXl+7eGhpqT1qSZmYIvwIk3caMvUW9PxLQNLiOHaIhXpPD1o+v48uDy4t0mn7FigP8IAp17n5CflZeCn5mY1reDgp6ZhJ+Cj8f78P/ccL5wAfv39/Pz9vV09/f2qtIUHSdBhin5sxfRPAebjhsRQ+Hh8vDl9KOlx+XG5/D1o/L85fy3hoZIAoVtGCSS+T2PucIuVMgPjgmdPtrWlZOEgp+Qn5WXgpPWhpman5WP1rW3xnT31Mb78P/ccL5wAfv39/dHxq4arPLEep5Feesok4UJkaiTSoaak9a1k4SCn5CflZeCn5mY1reDgYHYl4aGmpPYlZmb2ZeGhpqTlZfwxvnw9aPr5ff3CfLzxvX39wnG64zGdPeAxvjw9aPr+ff3CfLy9fT3Q8xbAvn49mT9R9fg2IIjyvstlODpZy3osaYd8xuoj3LbHcBUobqjGom3Xm4PJzyQatKd5yZVTRLt3DXp8RqLz3V9pdYlzjJHSWy5/J0J3QrccL5wAfv39/Pz9saUx/3G//D1o3mFd5Yw7a3/2WREDrK+BpbOaOMDr1Hz/4rhtqDn6IIlQX3VzbFVI5l9738oD72aA/Fd1Mb0Hu7IDqb/JZiS1pWZmJKfgp+ZmIXWmZDWg4WTY2iM+lKxfa0i4MHFPTL5uzjinyf+3fD38/Px9Pfg6J6CgoaFzNnZgZF5/kLWAT1a2taZhkDJ98Z6QbU5P++EA6v4I4mpbQTT9Uyjebur+wfWmZDWgp6T1oKek5jWl4aGmp+Vl0HtS2W00uTcMfnrQLtqqJU+vXbhhpqT1qSZmYLWtbfG6OH7xsDGwsSEl5WCn5WT1oWCl4KTm5OYgoXYxtnGdzXw/t3w9/Pz8fT0xndA7HdFXiqI1MM80yMv+SCdIlTS1ecBV1rL0JHWfMWcAft0OSgdVdkPpZytksBvutuOQRt6bSoFgW0EgCSBxrk31peYktaVk4SCn5CflZeCn5mY1obGdPJNxnT1VVb19Pf09Pf0xvvw/3bi3SafsWKA/wgCnXvYtlABsbuJXVWHZLGlozdZ2bdFDg0VhjsQVbrYtlABsbuJ/qjG6fD1o+vV8u7G4JSak9aFgpeYkpeEktaCk4SbhdaXxcCsxpTH/cb/8PWj8vDl9KOlx+X+qMZ09+fw9aPr1vJ09/7GdPfyxvlrywXdv97sPgg4Q0/4L6jqID3L6XN1c+1vy7HBBF9ttnjaIkdm5C6ak9a/mJXYx9DG0vD1o/L95eu3hqSTmp+XmJWT1pmY1oKen4XWlZOEvy6AacXik1eBYj/b9PX39vdVdPeP1peFhYObk4XWl5WVk4aCl5iVk5LD1eO946/rRWIBAGpoOaZMN66mplx8IywSCib/8cFGg4PX");
        private static int[] order = new int[] { 39,7,9,34,57,10,10,25,14,12,58,30,30,47,53,30,50,44,47,26,57,36,29,28,28,30,44,51,30,55,52,50,43,47,35,36,44,41,59,58,46,41,54,56,48,45,58,56,52,50,54,53,56,55,54,56,59,59,58,59,60 };
        private static int key = 246;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
