#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("DenGEZg85ehHBfasjEVefqu+lolEvRxkmviKaJjXa7h3rRBe0lVA1lVf6yM6SZGIAeNYIbzjOrCPD0NrZfQHE14SYcdvy+6CWb99JUcb6NasTVlr+w/dEZ6ZQStEGpk1HJtZb5/fGnDu+DbLKxnUVv4vuHPrgU4Akmfo5agZojjyml0MHgR/mvzQb3gNCF5MXTTShOC+5S2b1NnOtsYh+8GzkTLB+RQBC9ICAW5kj2OHKdf0tH3/lbq04hYMQ2DXepR82kEw4/JJ4Xc7/JKzrfOln7DWSN4cnnZF6onaoO4MWtJL6XqGrhrVAF58f55/iwgGCTmLCAMLiwgICZFYwhHDirM5iwgrOQQPACOPQY/+BAgICAwJCqhxIscfArdIagsKCAkI");
        private static int[] order = new int[] { 13,8,7,12,11,7,11,10,10,11,11,12,12,13,14 };
        private static int key = 9;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
