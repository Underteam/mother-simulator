﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class IAP : MonoBehaviour {

	public string iapName;

	[HideInInspector]public int iapid;

	public Text lblPrice;

    public List<Text> lblPrices;

    public UnityEvent onPurchased;

	[HideInInspector]public bool avaliable;

	public ProductType productType = ProductType.Consumable;

    public string price { get; private set; }

    public bool buyed { get { return PlayerPrefs.GetInt("IAP" + iapName + "Buyed", 0) == 1; } }

    public enum ProductType
    {
        Consumable,
        NonConsumable,
        Subscription,
    }

    private bool inited;
	public void Init () {

		if (productType == ProductType.NonConsumable)
        {
			if (PlayerPrefs.GetInt ("IAP" + iapName + "Buyed", 0) == 1) {
				if (onPurchased != null)
					onPurchased.Invoke ();
			}
		}

        IAPController.Instance().StartCoroutine(GetPrice());
        
        inited = true;
    }
	
	public virtual void Buyed () {

		if (productType == ProductType.NonConsumable) {
			PlayerPrefs.SetInt ("IAP" + iapName + "Buyed", 1);
		}

        Debug.Log("Buyed", this);

		if (onPurchased != null)
			onPurchased.Invoke ();
	}

	private IEnumerator GetPrice ()
    {
        for (int i = 0; i < lblPrices.Count; i++)
        {
            if (lblPrices[i] != null)
                lblPrices[i].text = "-";
        }

        while (!IAPController.Instance ().Ready ())
			yield return null;
	
        price = IAPController.Instance().GetPrice(iapid);

        if (lblPrice != null)
            lblPrice.text = price;

        for(int i = 0; i < lblPrices.Count; i++)
        {
            if (lblPrices[i] != null)
                lblPrices[i].text = price;
        }
	}

	void OnEnable ()
    {
        if (!inited) Init();
	}

	public void Buy () {
	
		IAPController.Instance ().InitiatePurchase (this);
	}
}
