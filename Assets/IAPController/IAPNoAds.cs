﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IAPNoAds : MonoBehaviour {
	
    public static bool noAds = false;
    public GameObject restoreButton;

    public List<GameObject> toHide;
    public List<GameObject> toShow;

	// Use this for initialization
	void Start () {
#if UNITY_ANDROID
        //restoreButton.gameObject.SetActive(false);
#endif
    }

    public void NoAds()
    {
        noAds = true;

        AdsController.Instance().NoAds();

        //restoreButton.gameObject.SetActive(false);

        for (int i = 0; i < toHide.Count; i++) toHide[i].SetActive(false);
        for (int i = 0; i < toShow.Count; i++) toShow[i].SetActive(true);

        Debug.LogError("NoAds");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
