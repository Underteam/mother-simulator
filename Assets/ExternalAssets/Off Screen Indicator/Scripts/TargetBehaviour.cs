﻿using System.Collections;
using System.Collections.Generic;
using PixelPlay.OffScreenIndicator;
using UnityEngine;

public class TargetBehaviour : MonoBehaviour {
    public Target target;
    public Indicator indicatorPrefab;

    private void OnEnable () {
        FindObjectOfType<OffScreenIndicator> ().AddTarget (target, indicatorPrefab, false, false);
    }

    private void OnDisable () {

    }

    private void OnDestroy () {

    }
}