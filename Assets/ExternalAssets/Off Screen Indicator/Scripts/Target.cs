﻿using UnityEngine;

namespace PixelPlay.OffScreenIndicator {

    [System.Serializable]
    public class Target {

        public Transform transform;

        public bool NeedBoxIndicator = true;

        public bool NeedArrowIndicator = true;

        public bool NeedDistanceText = true;

        public Indicator CurrentIndicator { set; get; } = null;

        public float GetDistanceFromCamera (Vector3 cameraPosition) {
            float distanceFromCamera = Vector3.Distance (cameraPosition, transform.position);
            return distanceFromCamera;
        }

        public void CreateIndicator (Indicator indicatorPrefab, Transform parent) {
            if (CurrentIndicator != null)
                return;

            CurrentIndicator = GameObject.Instantiate (indicatorPrefab, Vector3.zero, Quaternion.identity, parent);
        }

        public void Init (Transform targetObj, bool needBoxIndicator, bool needArrowIndicator, bool needDistanceText) {
            this.transform = targetObj;
            this.NeedBoxIndicator = needBoxIndicator;
            this.NeedArrowIndicator = needArrowIndicator;
            this.NeedDistanceText = needDistanceText;
        }

        public Target (Transform targetObj, bool needBoxIndicator, bool needArrowIndicator, bool needDistanceText) {
            Init (targetObj, needBoxIndicator, needArrowIndicator, needDistanceText);
        }
    }

    [System.Serializable]
    public class TimedTarget : Target {
        public float timer;

        public TimedTarget (Transform targetObj, bool needBoxIndicator, bool needArrowIndicator, bool needDistanceText, float time) : base (targetObj, needBoxIndicator, needArrowIndicator, needDistanceText) {
            this.timer = time;
        }

        public bool UpdateTimer () {
            timer -= Time.deltaTime;
            return timer < 0;
        }
    }

}