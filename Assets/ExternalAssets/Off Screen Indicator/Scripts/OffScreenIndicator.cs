﻿using System;
using System.Collections.Generic;
using System.Linq;
using PixelPlay.OffScreenIndicator;
using UnityEngine;

/// <summary>
/// Attach the script to the off screen indicator panel.
/// </summary>
public class OffScreenIndicator : MonoBehaviour {
    [Range (0.5f, 1f)]
    [Tooltip ("Distance offset of the indicators from the centre of the screen")]
    [SerializeField] private float screenBoundOffset = 1f;

    public bool updateScreenBounds = false;
    public bool useParentRectForPosition = true;

    private Camera mainCamera;
    private Canvas canvas;
    private Vector3 screenCentre;
    private Vector3 screenBounds;

    public static Action<Target, bool> TargetStateChanged;

    public List<Target> Targets { private set; get; } = new List<Target> ();

    public void AddTarget (Target target, Indicator indicatorPrefab, bool overrideSameTargetObj, bool useInstantiatedPrefab) {
        if (overrideSameTargetObj) {
            var sameObjIndex = Targets.FindIndex ((t) => t.transform == target.transform);
            if (sameObjIndex >= 0) {
                var oldIndicator = Targets[sameObjIndex].CurrentIndicator;
                Targets[sameObjIndex] = target;
                if (!useInstantiatedPrefab) {
                    Targets[sameObjIndex].CreateIndicator (indicatorPrefab, transform);
                    Destroy (oldIndicator.gameObject);
                } else
                    Targets[sameObjIndex].CurrentIndicator = oldIndicator;

                return;
            }
        }

        target.CreateIndicator (indicatorPrefab, transform);
        Targets.Add (target);
    }

    void Awake () {
        mainCamera = Camera.main;
        canvas = GetComponentInParent<Canvas> ();
        screenCentre = new Vector3 (Screen.width, Screen.height, 0) / 2;
        screenBounds = screenCentre * screenBoundOffset;
    }

    void LateUpdate () {
        DrawIndicators ();
    }

    /// <summary>
    /// Draw the indicators on the screen and set thier position and rotation and other properties.
    /// </summary>
    void DrawIndicators () {

        for (int i = Targets.Count - 1; i >= 0; i--) {
            var target = Targets[i];
            if (target.transform == null || (target is TimedTarget && (target as TimedTarget).UpdateTimer ())) {
                Destroy (target.CurrentIndicator.gameObject);
                Targets.Remove (Targets[i]);
            }

            Vector3 screenPosition = OffScreenIndicatorCore.GetScreenPosition (mainCamera, target.transform.position);
            bool isTargetVisible = OffScreenIndicatorCore.IsTargetVisible (screenPosition);
            float distanceFromCamera = target.NeedDistanceText ? target.GetDistanceFromCamera (mainCamera.transform.position) : float.MinValue; // Gets the target distance from the camera.
            Indicator indicator = target.CurrentIndicator;

            if (target.NeedBoxIndicator && isTargetVisible) {
                screenPosition.z = 0;

                indicator.IndicatorType = IndicatorType.BOX;
                indicator.transform.rotation = Quaternion.identity;

            } else if (target.NeedArrowIndicator && !isTargetVisible) {
                float angle = float.MinValue;

                if (updateScreenBounds)
                    screenBounds = screenCentre * screenBoundOffset;

                OffScreenIndicatorCore.GetArrowIndicatorPositionAndAngle (ref screenPosition, ref angle, screenCentre, screenBounds);
                indicator.transform.rotation = Quaternion.Euler (0, 0, angle * Mathf.Rad2Deg); // Sets the rotation for the arrow indicator.

                indicator.IndicatorType = IndicatorType.ARROW;
            }
            if (indicator) {
                // indicator.SetImageColor (target.TargetColor); // Sets the image color of the indicator.
                indicator.SetDistanceText (distanceFromCamera); //Set the distance text for the indicator.
                indicator.transform.position = screenPosition; //Sets the position of the indicator on the screen.

                if (useParentRectForPosition && !isTargetVisible) {

                    var indicatorParentRect = indicator.transform.parent as RectTransform;

                    Vector3[] corners = new Vector3[4];
                    indicatorParentRect.GetWorldCorners (corners);
                    Vector3 min = corners[0];
                    Vector3 max = corners[2];

                    Vector3 clampedPos = new Vector3 (Mathf.Clamp (screenPosition.x, min.x, max.x), Mathf.Clamp (screenPosition.y, min.y, max.y), screenPosition.z);
                    indicator.transform.position = clampedPos;
                }

                indicator.SetTextRotation (Quaternion.identity); // Sets the rotation of the distance text of the indicator.
            }
        }
    }
}