﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

/// <summary>
/// Assign this script to the indicator prefabs.
/// </summary>
public class Indicator : MonoBehaviour {

    public UnityEvent onBox;
    public UnityEvent onArrow;

    /// <summary>
    /// Gets if the game object is active in hierarchy.
    /// </summary>
    public bool Active {
        get {
            return transform.gameObject.activeInHierarchy;
        }
    }

    private IndicatorType ? indicatorType = null;
    public IndicatorType IndicatorType {
        set {
            if (indicatorType == null || indicatorType.Value != value) {
                switch (value) {
                    case IndicatorType.BOX:
                        onBox?.Invoke ();
                        // Debug.LogError ("Box");
                        break;

                    case IndicatorType.ARROW:
                        onArrow?.Invoke ();
                        // Debug.LogError ("Arrow");
                        break;
                }
            }

            indicatorType = value;
        }
        get => indicatorType.Value;
    }

    void Awake () {
        IndicatorType = IndicatorType.BOX;
    }

    public void SetDistanceText (float value) {

    }

    /// <summary>
    /// Sets the distance text rotation of the indicator.
    /// </summary>
    /// <param name="rotation"></param>
    public void SetTextRotation (Quaternion rotation) {
        // distanceText.rectTransform.rotation = rotation;
    }

    /// <summary>
    /// Sets the indicator as active or inactive.
    /// </summary>
    /// <param name="value"></param>
    public void Activate (bool value) {
        transform.gameObject.SetActive (value);
    }
}

public enum IndicatorType {
    BOX,
    ARROW
}