﻿using System.Collections;
using System.Collections.Generic;
using PixelPlay.OffScreenIndicator;
using Sirenix.OdinInspector;
using UnityEngine;

public class TimedTargetTest : MonoBehaviour {
    public Transform target;
    public Indicator indicatorPefab;

    [Button]
    public void Test () {
        GameObject.FindObjectOfType<OffScreenIndicator> ().AddTarget (new TimedTarget (target, true, true, false, 5), indicatorPefab, true, true);
    }
}